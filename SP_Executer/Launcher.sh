
Rand=$RANDOM
Host=`hostname`
OutFileName="/share/Exporters/SP_Executer/$Rand"

#Command="Select ID,ServiceName,SP_Name,Host_IP,Host_Port,DB_Name,User_Name,Password,Paramter_Types,ParameterValues FROM \`SP Executer Queue\` WHERE Status='Pending' INTO OUTFILE '$OutFileName' FIELDS TERMINATED BY '^'"

Command="Select ID,ServiceName,SP_Name,Host_IP,Host_Port,DB_Name,User_Name,Password,Paramter_Types,REPLACE(ParameterValues,'\\n','NEWLINE') FROM \`SP Executer Queue\` WHERE Status='Pending' INTO OUTFILE '$OutFileName' FIELDS TERMINATED BY '^'"

#Command="Select ID,ServiceName,SP_Name,Host_IP,Host_Port,DB_Name,User_Name,Password,Paramter_Types,ParameterValues FROM \`SP Executer Queue\` WHERE Status='Pending' INTO OUTFILE '$OutFileName' FIELDS ESCAPED BY '""' TERMINATED BY '^' LINES TERMINATED BY '\n'"


if [ "$Host" = "cnpvas04" ]
	then
		mysql --login-path=Reader -hCNPVAS03 -P3307 -e"$Command" Queues
	else
		mysql -uReader -pReader -hCNPVAS03 -P3307 -e"$Command" Queues
fi


while IFS= read -r Requests; do
		RequestID=`printf "%s\n" "$Requests" |cut -d"^" -f1`
		ServiceName=`printf "%s\n" "$Requests" | cut -d"^" -f2`
		SP_Name=`printf "%s\n" "$Requests" | cut -d"^" -f3`
		Host_IP=`printf "%s\n" "$Requests" | cut -d"^" -f4`
		Host_Port=`printf "%s\n" "$Requests" | cut -d"^" -f5`
		DB_Name=`printf "%s\n" "$Requests" | cut -d"^" -f6`
		User_Name=`printf "%s\n" "$Requests" | cut -d"^" -f7`
		Password=`printf "%s\n" "$Requests" | cut -d"^" -f8`
		Paramter_Types=`printf "%s\n" "$Requests" | cut -d"^" -f9`
		#ParameterValues=`printf "%s\n" "$Requests" | cut -d"^" -f10 | sed 's/|/,/g'`
		ParameterValues=`printf "%s\n" "$Requests" | cut -d"^" -f10`
		#ParameterValues=`printf "%s\n" "$Requests" | cut -d"^" -f10 |  sed 's/XXX/\\\n/g'`

		echo "Processing Request ID:$RequestID"
		Error=`java -jar SP_Executer.jar "$Host_IP" "$DB_Name" "$Host_Port" "$User_Name" "$Password" "$SP_Name" "$ParameterValues" "$Paramter_Types" |tr "\n" "|" | sed 's/'\''//g'`

		if [ "$Error" = "" ]
			then
				Command="UPDATE \`SP Queue\` SET Status='Sent' where ID=$RequestID"
			else
				Command="UPDATE \`SP Queue\` SET Status='Error',Error_Description='$Error' where ID=$RequestID"
		fi

		if [ "$Host" = "cnpvas04" ]
       			then
		                mysql --login-path=Writer -hCNPVAS03 -P3307 -e"$Command" Queues
		        else
               			mysql -uReader -pWriter -hCNPVAS03 -P3307 -e"$Command" Queues
		fi

		sleep 1
	done < $OutFileName

rm $OutFileName
