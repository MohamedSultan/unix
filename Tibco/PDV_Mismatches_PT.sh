Date=$1
Date_Formatted=`echo $Date | sed 's/-//g'`
Date_Before=`ksh /app_univ/Date_Scripts/XDayBefore.sh $Date -1`
Date_Before_Formatted=`echo $Date_Before | sed 's/-//g'`

echo "Getting Tibco MSISDNs"
cat /app_univ/StatsCollector/app.Collectors.Tibco.PDV_MSISDNs_$Date_Formatted|awk -F"," '{print $1+0}'| sort | uniq > /share/Tibco/Tibco_PDV_$Date

echo "Getting Billing Details"
cat /share/FTP/DWH_ODS/ODS/Processed/PDV_$Date_Formatted /share/FTP/DWH_ODS/ODS/Processed/PDV_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/Processed/PDV_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/ODS/Processed/PDV_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/PDV_$Date_Formatted /share/FTP/DWH_ODS/ODS/PDV_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/PDV_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/ODS/PDV_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/PDV_$Date_Formatted /share/FTP/DWH_ODS/PDV_$Date_Before_Formatted /share/FTP/DWH_ODS/PDV_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/PDV_DUMMY_$Date_Before_Formatted  | cut -d"," -f7,8 | awk 'BEGIN { FS = ",";OFS=","}{ SumArr[$2] += $1;} END {for (i in SumArr) { print i","SumArr[i]} }' | sort > /share/Tibco/Billing_PDV_Detailed_$Date

echo "Getting Success MSISDN Details"
join -j1 1 -j2 1 -t"," -o1.1 2.2  /share/Tibco/Tibco_PDV_$Date /share/Tibco/Billing_PDV_Detailed_$Date > /share/Importers/Tibco/Tibco_PDV_Success_$Date
Command="CREATE TABLE PDV_$Date_Formatted (MSISDN varchar(20) NOT NULL DEFAULT '',ProcessingTime Int,PRIMARY KEY (MSISDN)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="LOAD DATA INFILE '/share/Importers/Tibco/Tibco_PDV_Success_$Date' REPLACE INTO TABLE PDV_$Date_Formatted FIELDS TERMINATED BY ',' (MSISDN,ProcessingTime)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

#Total=`wc -l /share/Tibco/Tibco_PDV_$Date| sed 's/^ *//g;s/ /|/'| cut -d"|" -f1`
Command="REPLACE INTO \`PDV Requests Processing Times\`(Date,MaxProcessingTime,AverageProcessingTime,\`PT@95Percentile\`) SELECT '$Date',max(ProcessingTime),round(avg(ProcessingTime),3),95Percentile FROM PDV_"$Date_Formatted",(select ProcessingTime 95Percentile from (select @row := @row + 1 AS ID,Counts,PDV_"$Date_Formatted".* from (select * from PDV_"$Date_Formatted") PDV_"$Date_Formatted" , (SELECT @row := 0) r,(select count(*) Counts from PDV_"$Date_Formatted") MAX_) X where ID=round(Counts*.05,0)) Y"
#Command="REPLACE INTO \`PDV Requests Processing Times\`(Date,Success,Total,MaxProcessingTime,AverageProcessingTime,\`PT@95Percentile\`) SELECT '$Date',Count(*),'$Total',max(ProcessingTime),round(avg(ProcessingTime),3),95Percentile FROM PDV_"$Date_Formatted",(select ProcessingTime 95Percentile from (select @row := @row + 1 AS ID,Counts,PDV_"$Date_Formatted".* from (select * from PDV_"$Date_Formatted") PDV_"$Date_Formatted" , (SELECT @row := 0) r,(select count(*) Counts from PDV_"$Date_Formatted") MAX_) X where ID=round(Counts*.05,0)) Y"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="DROP TABLE PDV_$Date_Formatted"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

#Command="DROP VIEW PDV_"$Date_Formatted"_VW"
#mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco
