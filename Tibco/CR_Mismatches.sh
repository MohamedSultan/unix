Date=$1
DateFormatted=`echo $Date | sed 's/-//g'`
Date_Before=`ksh /app_univ/Date_Scripts/XDayBefore.sh $Date -1`
Date_Before_Formatted=`echo $Date_Before | sed 's/-//g'`

echo "Getting Tibco MSISDNs"
grep changeRatePlan /app_univ/StatsCollector/app.Collectors.Tibco.CR_TO_MSISDNs_$DateFormatted|sort | uniq > /share/Tibco/Tibco_CR_$Date
echo "Getting Tibco Errors"
cat /app_univ/StatsCollector/app.Collectors.Tibco.CR_Errors_$DateFormatted|sort -t"|" -k5 > /share/Importers/Tibco/Tibco_CR_Failed_$Date

Command="CREATE TABLE CR_Err_$DateFormatted (ErrDesc VARCHAR(200) NULL,Err_Count INT(11) NULL,Date VARCHAR(50)Not NULL,Service VARCHAR(50) NULL,MSISDN INT(11) NOT NULL, PRIMARY KEY (MSISDN, Date))COLLATE='latin1_swedish_ci' ENGINE=MyISAM"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco
Command="LOAD DATA INFILE '/share/Importers/Tibco/Tibco_CR_Failed_$Date' REPLACE INTO TABLE CR_Err_$DateFormatted FIELDS TERMINATED BY '|'(ErrDesc,Err_Count,Date,Service,MSISDN)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

echo "Getting Billing Details"
cat /share/FTP/DWH_ODS/ODS/Processed/CHANGE_RATEPLAN_$DateFormatted /share/FTP/DWH_ODS/ODS/Processed/CHANGE_RATEPLAN_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/Processed/CHANGE_RATEPLAN_DUMMY_$DateFormatted /share/FTP/DWH_ODS/ODS/Processed/CHANGE_RATEPLAN_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/CHANGE_RATEPLAN_$DateFormatted /share/FTP/DWH_ODS/ODS/CHANGE_RATEPLAN_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/CHANGE_RATEPLAN_DUMMY_$DateFormatted /share/FTP/DWH_ODS/ODS/CHANGE_RATEPLAN_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/CHANGE_RATEPLAN_$DateFormatted /share/FTP/DWH_ODS/CHANGE_RATEPLAN_$Date_Before_Formatted /share/FTP/DWH_ODS/CHANGE_RATEPLAN_DUMMY_$DateFormatted /share/FTP/DWH_ODS/CHANGE_RATEPLAN_DUMMY_$Date_Before_Formatted  | sort -t"," -k2 > /share/Tibco/Billing_CR_Detailed_$Date

echo "Getting Success MSISDN Details"
join -j1 1 -j2 2 -t"," -o1.1 2.1 2.3 2.4 2.5 2.6 2.7 /share/Tibco/Tibco_CR_$Date /share/Tibco/Billing_CR_Detailed_$Date > /share/Importers/Tibco/Tibco_Success_$Date
Command="CREATE TABLE CR_$DateFormatted (MSISDN varchar(20) NOT NULL DEFAULT '',Co_ID varchar(20) DEFAULT NULL,EndTime time DEFAULT NULL,StartTime time DEFAULT NULL,PRIMARY KEY (MSISDN)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="LOAD DATA INFILE '/share/Importers/Tibco/Tibco_Success_$Date' REPLACE INTO TABLE CR_$DateFormatted FIELDS TERMINATED BY ',' (MSISDN,Co_ID,EndTime,StartTime)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="create view CR_"$DateFormatted"_VW as select *,TimeInSecond(EndTime,StartTime)  TimeInSecond,if(TimeInSecond(EndTime,StartTime)>30*60,1,0) GreaterThan30 from CR_"$DateFormatted" order BY TimeInSecond(EndTime,StartTime) DESC"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Total=`wc -l /share/Tibco/Tibco_CR_$Date| sed 's/^ *//g;s/ /|/'| cut -d"|" -f1`
Command="CREATE VIEW CR_Err_"$DateFormatted"_Details as SELECT CR_Err_"$DateFormatted".ErrDesc,CR_Err_"$DateFormatted".Err_Count,CR_Err_"$DateFormatted".Date,CR_Err_"$DateFormatted".Service,ERROR_MAP.Type,CR_Err_"$DateFormatted".MSISDN FROM CR_Err_"$DateFormatted" LEFT OUTER JOIN CR_"$DateFormatted" ON CR_Err_"$DateFormatted".MSISDN = CR_"$DateFormatted".MSISDN INNER JOIN ERROR_MAP ON ERROR_MAP.Error = CR_Err_"$DateFormatted".ErrDesc WHERE CR_"$DateFormatted".MSISDN IS NULL"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="REPLACE INTO \`CR Requests\`(Date,Success,Total,MaxProcessingTime,AverageProcessingTime,\`PT@95Percentile\`,TechnicalErrors,NonTechErrors,NullErrors) SELECT '$Date',Count(*),'$Total',max(TimeInSecond),round(avg(TimeInSecond),3),95Percentile,Tech,NonTech,NullErr FROM CR_"$DateFormatted"_VW,(select TImeInSecond 95Percentile from (select @row := @row + 1 AS ID,Counts,CR_"$DateFormatted"_VW.* from (select * from CR_"$DateFormatted"_VW) CR_"$DateFormatted"_VW , (SELECT @row := 0) r,(select count(*) Counts from CR_"$DateFormatted"_VW) MAX_) X where ID=round(Counts*.05,0)) Y,(Select ifnull(COunt(*),0) Tech from CR_Err_"$DateFormatted"_Details where Type='Technical') Tech,(Select ifnull(COunt(*),0) NonTech from CR_Err_"$DateFormatted"_Details where Type='Non Technical') NonTech,(Select ifnull(COunt(*),0) NullErr from CR_Err_"$DateFormatted"_Details where Type is null) NullErr"
#echo $Command
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

echo "Dropping unneeded tables and views"

Command="DROP TABLE CR_$DateFormatted"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="DROP TABLE CR_Err_"$DateFormatted
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="DROP VIEW CR_"$DateFormatted"_VW"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="DROP VIEW CR_Err_"$DateFormatted"_Details"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco
