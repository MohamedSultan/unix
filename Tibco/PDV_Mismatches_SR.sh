Date=$1
Date_Formatted=`echo $Date | sed 's/-//g'`
Date_Before=`ksh /app_univ/Date_Scripts/XDayBefore.sh $Date -1`
Date_Before_Formatted=`echo $Date_Before | sed 's/-//g'`

echo "Getting Tibco MSISDNs"
cat /app_univ/StatsCollector/app.Collectors.Tibco.PDV_MSISDNs_$Date_Formatted|awk -F"," '{print $1+0}'| sort | uniq > /share/Tibco/Tibco_PDV__$Date

echo "Getting Failed Billing Customers"
cat /share/FTP/DWH_ODS/ODS/Processed/PDV_Hotline_customers_$Date_Formatted /share/FTP/DWH_ODS/ODS/PDV_Hotline_customers_$Date_Formatted /share/FTP/DWH_ODS/PDV_Hotline_customers_$Date_Formatted | cut -d"," -f1 | sort > /share/Tibco/Billing_PDV_Failed_Detailed_$Date

echo "Getting Failed MSISDN Details"
join -j1 1 -j2 1 -t"," -o1.1 2.1 -a1 /share/Tibco/Tibco_PDV__$Date /share/Tibco/Billing_PDV_Failed_Detailed_$Date > /share/Importers/Tibco/Tibco_PDV_All_$Date

Command="CREATE TABLE PDV_SR_$Date_Formatted (Tibco_MSISDN varchar(20) NOT NULL DEFAULT '',Failed_MSISDN  varchar(20) NOT NULL DEFAULT '',PRIMARY KEY (Tibco_MSISDN)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="LOAD DATA INFILE '/share/Importers/Tibco/Tibco_PDV_All_$Date' REPLACE INTO TABLE PDV_SR_$Date_Formatted FIELDS TERMINATED BY ',' (Tibco_MSISDN,Failed_MSISDN)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Total=`wc -l /share/Tibco/Tibco_PDV__$Date| sed 's/^ *//g;s/ /|/'| cut -d"|" -f1`
Command="REPLACE INTO \`PDV Requests Counts\`(Date,Success,Failed,Total) select '$Date',Success.Count Success,Failed.Count Failed,'$Total' from (select count(*) Count from PDV_SR_"$Date_Formatted" where Failed_MSISDN='') Success, (select count(*) Count from PDV_SR_"$Date_Formatted" where Failed_MSISDN<>'') Failed"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco

Command="DROP TABLE PDV_SR_$Date_Formatted"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Tibco
