Date=$1
Date_Formatted=`echo $Date | sed 's/-//g'`
Date_Before=`ksh /app_univ/Date_Scripts/XDayBefore.sh $Date -1`
Date_Before_Formatted=`echo $Date_Before | sed 's/-//g'`

echo "Getting Sieble MSISDNs"
grep Active /app_univ/StatsCollector/app.Collectors.Sieble.NewActivations_MSISDNs_$Date|sort -t"," -k5 | uniq > /share/Processing/NewActivations_$Date

echo "Getting Billing Details"
cat /share/FTP/DWH_ODS/ODS/Processed/NEW_ACTIVATION_$Date_Formatted /share/FTP/DWH_ODS/ODS/Processed/NEW_ACTIVATION_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/Processed/NEW_ACTIVATION_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/ODS/Processed/NEW_ACTIVATION_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/NEW_ACTIVATION_$Date_Formatted /share/FTP/DWH_ODS/ODS/NEW_ACTIVATION_$Date_Before_Formatted /share/FTP/DWH_ODS/ODS/NEW_ACTIVATION_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/ODS/NEW_ACTIVATION_DUMMY_$Date_Before_Formatted /share/FTP/DWH_ODS/NEW_ACTIVATION_$Date_Formatted /share/FTP/DWH_ODS/NEW_ACTIVATION_$Date_Before_Formatted /share/FTP/DWH_ODS/NEW_ACTIVATION_DUMMY_$Date_Formatted /share/FTP/DWH_ODS/NEW_ACTIVATION_DUMMY_$Date_Before_Formatted  | sort -t"," -k2 > /share/Processing/Billing_NewActivation_Detailed_$Date 

echo "Getting Success MSISDN Details"
join -j1 5 -j2 2 -t"," -o1.1 1.2 1.3 2.1 2.2 2.3 2.4 /share/Processing/NewActivations_$Date /share/Processing/Billing_NewActivation_Detailed_$Date > /share/Importers/Sieble/Billing_NewActivation_Success_$Date
Command="CREATE TABLE NewActivation_$Date_Formatted (Date VARCHAR(50) NOT NULL,number INT(11) NULL DEFAULT NULL,Status VARCHAR(50) NULL DEFAULT NULL,TransNumber INT(11) NULL DEFAULT NULL,CoID INT(11) NOT NULL,EndTime DATETIME NULL DEFAULT NULL,StartTime DATETIME NULL DEFAULT NULL,PRIMARY KEY (Date, CoID)) COLLATE='latin1_swedish_ci' ENGINE=MyISAM"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble

Command="LOAD DATA INFILE '/share/Importers/Sieble/Billing_NewActivation_Success_$Date' REPLACE INTO TABLE NewActivation_$Date_Formatted FIELDS TERMINATED BY ',' (number,Status,Date,TransNumber,CoID,EndTime,StartTime)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble

Command="create view NewActivation_"$Date_Formatted"_VW as select *,Tibco.TimeInSecond(EndTime,StartTime)  TimeInSecond,if(Tibco.TimeInSecond(EndTime,StartTime)>30*60,1,0) GreaterThan30 from NewActivation_$Date_Formatted order BY Tibco.TimeInSecond(EndTime,StartTime) DESC"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble

Total=`wc -l /share/Processing/NewActivations_$Date| sed 's/^ *//g;s/ /|/'| cut -d"|" -f1`
Command="REPLACE INTO \`NewActivations Requests\`(Date,Success,Total,MaxProcessingTime,AverageProcessingTime,\`PT@95Percentile\`) SELECT '$Date',Count(*),'$Total',max(TimeInSecond),round(avg(TimeInSecond),3),95Percentile FROM NewActivation_"$Date_Formatted"_VW,(select TImeInSecond 95Percentile from (select @row := @row + 1 AS ID,Counts,NewActivation_"$Date_Formatted"_VW.* from (select * from NewActivation_"$Date_Formatted"_VW) NewActivation_"$Date_Formatted"_VW , (SELECT @row := 0) r,(select count(*) Counts from NewActivation_"$Date_Formatted"_VW) MAX_) X where ID=round(Counts*.05,0)) Y"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble

Command="DROP TABLE NewActivation_$Date_Formatted"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble

Command="DROP VIEW NewActivation_"$Date_Formatted"_VW"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Sieble
