DateHour=$1
Date=`echo $DateHour | cut -c1-8`
Hour=`echo $DateHour | cut -c9-11`
echo $Date
echo $Hour
Date_Formated=`echo $Date | cut -c1-4`"-"`echo $Date | cut -c5-6`"-"`echo $Date | cut -c7-8`
echo $Date_Formated
OutputFileName="Result_$Date$Hour"
echo "$OutputFileName"
resultVar=''
Nodes="CallCollect1 CallCollect2 CallCollect3"
set -A Node_Name $Nodes
Node_ID=0
File_ID=1
while [ $Node_ID -lt 3 ]
	do
		echo "Processing Files on ${Node_Name[$Node_ID]}"
		FileName=$Date"-"$Hour"_MCP_SCP"$File_ID"_FSM.cdr.details"
		echo "$FileName"

	#	Command="grep -e 'Total Calls' -e 'Total Successful Calls' -e 'Total Errors' /app/prod/mcp/FSM_MCP/CDRs/stat/details/$FileName | sed 's/Total Successful Calls//;s/Total Calls//;s/Total Errors//' | sed 's/ //g' | tr '\n' '|' | sed 's/,//g'| awk 'BEGIN{FS=\"|\";OFS=\"|\"}  {print Date,Hour,Node,\$1,\$2,\$3}' Date=$Date_Formated Hour=$Hour Node=${Node_Name[$Node_ID]}"
	#	Command="grep -e 'Total Calls' -e 'Total Successful Calls' -e 'Total Errors' -e 'ODS errors' -e 'Charging errors' -e 'HLR errors' -e 'CF errors' /app/prod/mcp/FSM_MCP/CDRs/stat/details/$FileName |sed 's/Total Successful Calls//;s/Total Calls//;s/Total Errors//;s/ODS errors//;s/Charging errors//;s/HLR errors//;s/CF errors//'|sed 's/ //g' |  sed 's/\t//g' |  tr '\n' '|' | sed 's/,//g'| awk 'BEGIN{FS=\"|\";OFS=\"|\"}  {print Date,Hour,Node,\$1,\$2,\$3,\$4,\$5,\$6,\$7}' Date=$Date_Formated Hour=$Hour Node=${Node_Name[$Node_ID]}"
	#	CommandMSC="grep -m 1 'MSC errors' /app/prod/mcp/FSM_MCP/CDRs/stat/details/$FileName | sed 's/MSC errors//;' | sed 's/ //g' | sed 's/\t//g'"

		Command="grep -e 'Total Calls' -e 'Total Successful Calls' -e 'Total Errors' -e 'InitAPartyToIP state errors' -e 'ODS state errors' -e 'CCDB state errors' -e 'Charging state errors' -e 'NC check state errors' -e 'Collecting A-Choice state errors' -e 'Increment NC errors' -e 'Roaming state errors' -e 'Blacklist state errors' -e 'InitBCharging state errors' -e 'ConnectingBParty state errors' -e 'CollectingBChoice state errors' -e 'TerminateAUI state errors' -e 'ReconnectingB state errors' -e 'StartBCharging state errors' -e 'A-B-Connected state errors' -e 'Terminate charging state errors' -e 'Play Final Announcement state errors' /app/prod/mcp/FSM_MCP/CDRs/stat/details/$FileName  | sed 's/Total Calls//;s/Total Successful Calls//;s/Total Errors//;s/InitAPartyToIP state errors//;s/ODS state errors//;s/CCDB state errors//;s/InitBCharging state errors//;s/StartBCharging state errors//;s/Charging state errors//;s/NC check state errors//;s/Collecting A-Choice state errors//;s/Increment NC errors//;s/Roaming state errors//;s/Blacklist state errors//;s/ConnectingBParty state errors//;s/CollectingBChoice state errors//;s/TerminateAUI state errors//;s/ReconnectingB state errors//;s/A-B-Connected state errors//;s/Terminate charging state errors//;s/Play Final Announcement state errors//;' | sed 's/ //g' |  tr '\n' '|' | sed 's/,//g' | awk 'BEGIN{FS=\"|\";OFS=\"|\"}  {print Date,Hour,Node,\$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8,\$9,\$10,\$11,\$12,\$13,\$14,\$15,\$16,\$17,\$18,\$19,\$20,\$21}' Date=$Date_Formated Hour=$Hour Node=${Node_Name[$Node_ID]}" 

		ssh quality@${Node_Name[$Node_ID]} "$Command" >> /share/Importers/CallCollect/$OutputFileName
	#	resultVar=`ssh quality@${Node_Name[$Node_ID]} "$Command"`
	#	resultVar=$resultVar"|"`ssh quality@${Node_Name[$Node_ID]} "$CommandMSC"`
	#	echo $resultVar >> /share/Importers/CallCollect/$OutputFileName
		Node_ID=$(($Node_ID+1))
		File_ID=$(($File_ID+1))
	#	resultVar=''
done
#exit
ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/CallCollect/$OutputFileName" CNPVAS03 New_Transactions "Date,Hour,Node,Total,Success,Failed,InitAPartyToIP_Errors,ODS_Errors,CCDB_Errors,Charging_Errors,NC_check_Errors,Collecting_AChoice_Errors,Increment_NC_Errors,Roaming_Errors,Blacklist_Errors,InitBCharging_Errors,ConnectingBParty_Errors,CollectingBChoice_Errors,TerminateAUI_Errors,ReconnectingB_Errors,StartBCharging_Errors,ABConnected_Errors,Terminate_charging_Errors,Play_Final_Announcement_Errors" "|" CallCollect CallCollect_Transactions
