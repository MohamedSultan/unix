#!/usr/bin/ksh

Rand=$RANDOM
#mv "/share/CCN_Data/STATISTICS/"$1"/PMF_Files/PlatformMeasures/"* /share/CCN_X_Data/$1/PlatformMeasures/
while [ `ls -rlt  /share/CCN_Data/STATISTICS/$1/PMF_Files/PlatformMeasures/*_jambala_PlatformMeasures | wc -l | sed 's/^ *//g'` -ge 1 ]
	do
	echo "Starting Parsing of "$1
	ls /share/CCN_Data/STATISTICS/$1/PMF_Files/PlatformMeasures/*_jambala_PlatformMeasures > files_$Rand 
	while read FN
		do
		FileName="$FN" 
		FN=`echo $FN | cut -d"/" -f8`
		echo "Parsing File:"$FileName
		From=`echo $FN | cut -d"." -f2 | cut -d"-" -f1`
		To=`echo $FN | cut -d"." -f2 | cut -d"-" -f2 | cut -d"_" -f1`
		Date=`echo $FN | cut -d"." -f1 | sed 's/A//g'`

		/usr/xpg4/bin/grep -e moid -e \<r\> $FileName | tr '\n' '%' > $Rand
		echo "" >> $Rand
		OutputFile="/log/Processings/CCN/PlatformMeasures/"$Date
		echo $OutputFile
		cat $Rand | sed 's/<moid>/^/g;s/<\/moid>//g;s/<\/r>//g;s/%<r>/,/g;s/PlatformMeasures=//g;s/ Source = //g' | tr '^' '\n' | sed 's/,%//g;s/,$//g' | tail +2 | awk '{OFS=","} { print substr(Date_,7,2),CCNID,substr(Date_,1,4)"-"substr(Date_,5,2)"-"substr(Date_,7,2),From_,To_,$0}' Date_=$Date From_=$From To_=$To CCNID=$1>> $OutputFile
		rm $Rand
		
		if [ ! -d "/share/CCN_X_Data/$1/PlatformMeasures" ]; then
			echo "PlatformMeasures Dir does not exists, Dir Created"
			rm  -f /share/CCN_X_Data/$1/PlatformMeasures 2>/dev/null
  			mkdir /share/CCN_X_Data/$1/PlatformMeasures
			chmod 777 /share/CCN_X_Data/$1/PlatformMeasures
		fi
		
		mkdir -p /share/CCN_X_Data/$1/PlatformMeasures/ 
		mv -f $FileName /share/CCN_X_Data/$1/PlatformMeasures/
		echo "file is parsed"
	done < files_$Rand
done
rm files_$Rand
