Date=`echo $1 | sed 's/-//g'`
cd /share/CCN_X_Data
find . -name "A"$Date"*_jambala*" | sed 's/\//|/g;s/\./|/g;s/_/|/g' | awk '{FS="|";OFS="|"}{print $3,substr($5,2,4)"-"substr($5,6,2)"-"substr($5,8,2),substr($6,1,2),$8}' | sort | uniq -c | sed 's/^ *//g;s/ /|/'  > /share/Importers/CCN/CCN_FileCounts_$Date
Command="LOAD DATA INFILE '/share/Importers/CCN/CCN_FileCounts_$Date' REPLACE INTO TABLE CDR_Counts FIELDS TERMINATED BY '|' (Count,CCN,Date,Hour,Type)"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" CCN

