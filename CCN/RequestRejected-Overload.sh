#!/usr/bin/ksh

Rand=$RANDOM

Date="A20141203"
DCCN="DataCCN4"

while [ `ls -rlt /share/CCN_X_Data/$DCCN/CcnCounters/*jambala_CcnMeasureJob | grep $Date | wc -l | sed 's/^ *//g'` -ge 1 ]
	do
	echo "Starting Parsing of "$DCCN
	ls /share/CCN_X_Data/$DCCN/CcnCounters/*jambala_CcnMeasureJob | grep $Date > files_$Rand 
	while read FN
		do
		FileName="$FN" 
		FN=`echo $FN | cut -d"/" -f8`
		echo "Parsing File:"$FileName
		From=`echo $FN | cut -d"." -f2 | cut -d"-" -f1`
		To=`echo $FN | cut -d"." -f2 | cut -d"-" -f2 | cut -d"_" -f1`
		Date=`echo $FN | cut -d"." -f1 | sed 's/A//g'`
		

echo "" >> $Rand
		
		cat $FileName | tr -d "\t" |sed 's/<\/mv>/^/g' | sed 's/<mv>/#/g' | tr '\n' '!' | tr '^' '\n' | tr '#' '\n' | sed 's/!//g' | sed 's/<moid>CcnCounters=//g;s/ Source = //g;s/<\/moid><r>/,/g;s/<\/r>//g' | grep "RequestRejected-Overload" | grep -v xml | grep -v mi | cut -d, -f2,3 >> RequestRejected-Overload_"$DCCN".csv 

	mv $FileName $FileName.Processed

 rm $Rand
		echo "file is parsed"
	done < files_$Rand
done
rm files_$Rand
