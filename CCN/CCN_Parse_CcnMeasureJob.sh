#!/usr/bin/ksh

Rand=$RANDOM
#mv "/share/CCN_Data/STATISTICS/"$1"/PMF_Files/CcnCounters/"*CcnMeasureJob /share/CCN_X_Data/$1/CcnCounters/
while [ `ls -rlt  /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/*jambala_CcnMeasureJob | wc -l | sed 's/^ *//g'` -ge 1 ]
	do
	echo "Starting Parsing of "$1
	ls /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/*jambala_CcnMeasureJob > files_$Rand 
	while read FN
		do
		FileName="$FN" 
		FN=`echo $FN | cut -d"/" -f8`
		echo "Parsing File:"$FileName
		From=`echo $FN | cut -d"." -f2 | cut -d"-" -f1`
		To=`echo $FN | cut -d"." -f2 | cut -d"-" -f2 | cut -d"_" -f1`
		Date=`echo $FN | cut -d"." -f1 | sed 's/A//g'`

#		/usr/xpg4/bin/grep -e moid -e \<r\> $FileName | tr '\n' '%' > $Rand
		echo "" >> $Rand
		OutputFile="/log/Processings/CCN/CcnMeasureJob/Measure_"$Date
		echo $OutputFile
#		cat $Rand | tr -d "\t" |sed 's/<\/moid>%<r>/,/g;s/CcnCounters=//g;s/<\/r>%//g;s/ Source = //g'  | sed 's/<moid>/^/g' | tr "^" "\n" | tail +2 | awk '{OFS=","} { print substr(Date_,7,2),CCNID,substr(Date_,1,4)"-"substr(Date_,5,2)"-"substr(Date_,7,2),From_,To_,$0}' Date_=$Date From_=$From To_=$To CCNID=$1>> $OutputFile
		cat $FileName | tr -d "\t" |sed 's/<\/mv>/^/g' | sed 's/<mv>/#/g' | tr '\n' '!' | tr '^' '\n' | tr '#' '\n' |  sed 's/!//g' | grep _SYSTEM | sed 's/<moid>CcnCounters=//g;s/ Source = //g;s/<\/moid><r>/,/g;s/<\/r>//g' | awk '{OFS=","} { print substr(Date_,7,2),CCNID,substr(Date_,1,4)"-"substr(Date_,5,2)"-"substr(Date_,7,2),From_,To_,$0}' Date_=$Date From_=$From To_=$To CCNID=$1>> $OutputFile
		rm $Rand
		mkdir -p /share/CCN_X_Data/$1/CcnCounters/
		mv -f $FileName /share/CCN_X_Data/$1/CcnCounters/
		echo "file is parsed"
	done < files_$Rand
done
rm files_$Rand
