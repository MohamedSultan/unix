
Date=`echo $1 | sed 's/-//g'`
Rand=$RANDOM
CCN_ID=$2
FormattedHour=$3
Processing_Dir=/log/Processings/CCN/
Rand=$RANDOM

echo "Uniqing CCN_Counters Processing File"
cd $Processing_Dir/CCN_Counters
mv $Date $Date"_X_X"
sort $Date"_X_X" | uniq >> $Date

echo "Splitting File"
cd $Processing_Dir/CCN_Counters
/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)}' File_Name=$Date $Date

echo "Splitting Files level 2"
for Files_CCN in `ls $Date"_"*`
	do
		/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)"_"$2}' File_Name=$Date $Files_CCN
		rm $Files_CCN
done

for Files in `ls $Date"_"*`
	do
		ExportedFile="/share/Importers/CCN/Counters_Agg_"$Files"_"$Rand
		echo "Formatting File:$Files"
		awk '{FS=",";OFS=";"}{SumArr[substr($4,1,2)";"$2";"$3";"$6";"$7";"substr($3,9,2)] += $8;} END {for (i in SumArr) { print i";"SumArr[i] } }' $Files > $ExportedFile
		
		echo "Loading Data from: $ExportedFile"
		echo "Creating Temp Table"
		Command="CREATE TABLE CCN_Counters_$Rand (CCN_ID varchar(20) NOT NULL,Date varchar(11) NOT NULL,Hour varchar(2),Proccessor varchar(15) NOT NULL,CcnPerfCounter varchar(70) NOT NULL,Value varchar(10) NOT NULL,Day int(11) NOT NULL DEFAULT '0',PRIMARY KEY (Date,Hour,Proccessor,CCN_ID,CcnPerfCounter,Day)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN
		
		Command="LOAD DATA INFILE '$ExportedFile' REPLACE INTO TABLE CCN_Counters_$Rand FIELDS TERMINATED BY ';' (Hour,CCN_ID,Date,CcnPerfCounter,Proccessor,Day,Value)"
#echo $Command
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN 

		Command="REPLACE INTO CCN_Counters(Day,CCN_ID,Date,\`Actual Date\`,Hour,\`Actual Hour\`,Proccessor,CcnPerfCounter,Value) SELECT day(if(((Hour+ (SELECT Hours FROM TimeAdjustment)) >= 24),(Date + INTERVAL 1 DAY),Date)),CCN_ID,Date,if(((Hour+ (SELECT Hours FROM TimeAdjustment)) >= 24),(Date + INTERVAL 1 DAY),Date) AS \`Actual Date\`,Hour AS Hour,if(((Hour+ (SELECT Hours FROM TimeAdjustment)) >= 24),lpad(((Hour+ (SELECT Hours FROM TimeAdjustment))- 24),2,'0'),lpad((Hour + (SELECT Hours FROM TimeAdjustment)),2,'0'))AS \`Actual Hour\`,Proccessor AS Proccessor,CcnPerfCounter AS CcnPerfCounter,Value AS Value FROM CCN_Counters_$Rand"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN

		Command="DROP TABLE CCN_Counters_$Rand"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN
		
		rm $ExportedFile
		rm $Files
done

cd $Processing_Dir/CcnMeasureJob
echo "Uniqing CcnMeasureJob Processing File"
mv $Date $Date"_X_X"
sort $Date"_X_X" | uniq >> $Date

echo "Splitting File"
/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)}' File_Name=$Date "Measure_"$Date

echo "Splitting Files level 2"
for Files_CCN in `ls $Date"_"*`
        do
                /usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)"_"$2}' File_Name=$Date $Files_CCN
                rm $Files_CCN
done

for Files in `ls $Date"_"*`
        do
		ExportedFile="/share/Importers/CCN/MeasureJob_Agg_"$Files"_"$Rand
                echo "Formatting File:$Files"
		awk '{FS=",";OFS=";"}{SumArr[substr($4,1,2)";"$2";"$3";"$6";"$7";"substr($3,9,2)] += $8;} END {for (i in SumArr) { print i";"SumArr[i] } }' $Files > $ExportedFile

		echo "Loading Data from: $ExportedFile"
		Command="CREATE TABLE CCN_MeasureJob_$Rand (CCN_ID varchar(20) NOT NULL,Date varchar(11) NOT NULL,Hour varchar(2) NOT NULL COMMENT 'PLZ Add 3 Hours on its value to get the actual value',Proccessor varchar(15) NOT NULL,CcnPerfCounter varchar(70) NOT NULL,Value varchar(10) NOT NULL,Day int(11) NOT NULL DEFAULT '0',PRIMARY KEY (Date,Hour,Proccessor,CCN_ID,CcnPerfCounter,Day)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN	

		Command="LOAD DATA INFILE '$ExportedFile' REPLACE INTO TABLE CCN_MeasureJob_$Rand FIELDS TERMINATED BY ';' (Hour,CCN_ID,Date,CcnPerfCounter,Proccessor,Day,Value)"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN

		
		Command="REPLACE INTO CCN_MeasureJob(CCN_ID,Date,\`Actual Date\`,Hour,\`Actual Hour\`,Proccessor,CcnPerfCounter,Value) SELECT CCN_ID,Date,if(((Hour+ (SELECT Hours FROM TimeAdjustment)) >= 24),(Date + INTERVAL 1 DAY),Date) AS \`Actual Date\`,Hour AS Hour,if(((Hour+ (SELECT Hours FROM TimeAdjustment)) >= 24),lpad(((Hour+ (SELECT Hours FROM TimeAdjustment))- 24),2,'0'),lpad((Hour + (SELECT Hours FROM TimeAdjustment)),2,'0'))AS \`Actual Hour\`,Proccessor AS Proccessor,CcnPerfCounter AS CcnPerfCounter,Value AS Value FROM CCN_MeasureJob_$Rand"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN

		Command="DROP TABLE CCN_MeasureJob_$Rand"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN


		rm $ExportedFile
		rm $Files
done
