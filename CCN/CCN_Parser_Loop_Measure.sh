#Inorder to catch the pid and /app/Services/CCN/Watcher.sh will examine this file every min. to check that the process is running
echo $$ > /app/Services/CCN/pid

DataDir="/share/CCN_Data/STATISTICS"

while true
do
	for CCNs in `ls /share/CCN_Data/STATISTICS | grep CCN`
		do
		ksh /app/Services/CCN/CCN_Parse_CcnMeasureJob.sh $CCNs
	done
done
