
Date=`echo $1 | sed 's/-//g'`
Rand=$RANDOM

cp /log/Processings/CCN/PlatformMeasures/$Date /share/Importers/CCN/PlatformMeasures
Command="LOAD DATA INFILE '/share/Importers/CCN/PlatformMeasures/$Date' REPLACE INTO TABLE CCN_Platform_Measures FIELDS TERMINATED BY ',' (Day,CCN_ID,Date,FromTime,ToTime,Value,Proccessor,CpuLoad,MaxCpuLoad,MemUsed,MemFree,MemUsage,MaxMemUsage,ProcessorUptime,ClusterUptime,DiskUsage)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Processings

Command="Call Platform_Aggregator('$1')"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN
