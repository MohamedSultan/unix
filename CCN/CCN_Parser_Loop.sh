#Inorder to catch the pid and /app/Services/CCN/Watcher.sh will examine this file every min. to check that the process is running
echo $$ > /app/Services/CCN/pid

DataDir="/share/CCN_Data/STATISTICS"

while true
do
	for CCNs in `ls /share/CCN_Data/STATISTICS | /usr/xpg4/bin/grep -e CCN -e VPN`
		do
		echo "Stating Parsing of :$CCNs"
		ksh /app/Services/CCN/CCN_Parser.sh $CCNs
		echo "importing CCN:$CCNs"
	
		head /share/CCN_Data/Final/CcnCounters/CCN_Counters 		
		ksh Importer.sh
		echo "Parsing Of $CCNs is finished"
	done
done
