#!/usr/bin/ksh

cd /log/CCN_Data/Final/SignallingMeasures

ls /log/CCN_Data/$1/PMF_Files/SignallingMeasures >files 
while read FN
do
 
FileName="/log/CCN_Data/$1/PMF_Files/SignallingMeasures/$FN" 
temp4date=${FN##*A}       # To remove A output will be 20100415.0710-0715_jambala_CcnCounters.20100415072614
date=${temp4date%%.*}     #20100415
temp4time=${temp4date%%_*}  #20100415.0710-0715
temp4time2=${temp4time##*.}
time1=${temp4time2%%-*}
time2=${temp4time2##*-}


#**********Using grep to Extract them from file************************************************
	grep moid  $FileName> TempFile1 
	grep \<r\>  $FileName  > TempFile2
	Size=`wc -l TempFile2`
	i=1
        >TempFile22	
	while [ $i -le $Size ]
			do
			 sed -n ""$i","$(($i+44))" p" TempFile2 > TempFile21
			 i=$(($i+45))
                         awk 'BEGIN{ORS=":"}{if(NR==45){print $0"\n"}else{print $0}}' TempFile21 >> TempFile22

			done 	

			grep -v ^$ TempFile22 | sed 's/^://' >TempFile222
	
        	
	awk '{print "'$1'"":""'$date'"":""'$time1'"":""'$time2'"}' TempFile1 > TempFile3
#<moid>SS7Statistics=SAID=239, Source = SS7</moid>

        paste -d ':'  TempFile3 TempFile1 TempFile222 | sed -e 's/<moid>SS7Statistics=//' -e 's/, Source = /:/' -e 's/<\/moid>//' -e 's/<r>//g' -e 's/<\/r>//g'    >> signalling_measures 





if [ ! -d "/log/CCN_X_Data/$1/PMF_Files/SignallingMeasure/Received" ]
then                                                           
mkdir -p /log/CCN_X_Data/$1/PMF_Files/SignallingMeasure/Received     
fi                                                             
mv $FileName /log/CCN_X_Data/$1/PMF_Files/SignallingMeasure/Received
done <files
rm TempFile* 
