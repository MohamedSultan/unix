
Date=`echo $1 | sed 's/-//g'`
Rand=$RANDOM
CCN_ID=$2
FormattedHour=$3
Processing_Dir=/log/Processings/CCN/
Rand=$RANDOM

echo "Splitting File"
cd $Processing_Dir/CCN_Counters
/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)}' File_Name=$Date $Date

echo "Splitting Files level 2"
for Files_CCN in `ls $Date"_"*`
	do
		/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)"_"$2}' File_Name=$Date $Files_CCN
		rm $Files_CCN
done

for Files in `ls $Date"_"*`
	do
		ExportedFile="/share/CCN/Counters_Agg_"$Files"_"$Rand
		echo "Formatting File:$Files"
		awk '{FS=",";OFS=";"}{SumArr[substr($4,1,2)";"$2";"$3";"$6";"$7";"substr($3,9,2)] += $8;} END {for (i in SumArr) { print i";"SumArr[i] } }' $Files > $ExportedFile
		
		echo "Loading Data from: $ExportedFile"
		Command="LOAD DATA INFILE '$ExportedFile' REPLACE INTO TABLE CCN_Counters FIELDS TERMINATED BY ';' (Hour,CCN_ID,Date,CcnPerfCounter,Proccessor,Day,Value)"
#echo $Command
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN 
		
		rm $ExportedFile
		rm $Files
done

echo "Splitting File"
cd $Processing_Dir/CcnMeasureJob
/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)}' File_Name=$Date "Measure_"$Date

echo "Splitting Files level 2"
for Files_CCN in `ls $Date"_"*`
        do
                /usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > File_Name"_"substr($4,1,2)"_"$2}' File_Name=$Date $Files_CCN
                rm $Files_CCN
done

for Files in `ls $Date"_"*`
        do
		ExportedFile="/share/CCN/MeasureJob_Agg_"$Files"_"$Rand
                echo "Formatting File:$Files"
		awk '{FS=",";OFS=";"}{SumArr[substr($4,1,2)";"$2";"$3";"$6";"$7";"substr($3,9,2)] += $8;} END {for (i in SumArr) { print i";"SumArr[i] } }' $Files > $ExportedFile

		echo "Loading Data from: $ExportedFile"
		Command="LOAD DATA INFILE '$ExportedFile' REPLACE INTO TABLE CCN_MeasureJob FIELDS TERMINATED BY ';' (Hour,CCN_ID,Date,CcnPerfCounter,Proccessor,Day,Value)"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" CCN
		rm $ExportedFile
		rm $Files
done
