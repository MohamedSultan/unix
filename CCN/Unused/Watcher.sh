touch /app/Services/CCN/pid
Nofproc=`wc  -l /app/Services/CCN/pid |  tr -d /app/Services/CCN/pid`

if [ $Nofproc -eq 0 ]
then
		ksh /app/Services/CCN/CCN_Parser_Loop.sh 
	        echo "CCN application restarted @ `date` " >> /app/Services/CCN/Watcher.log	
fi


Pid=`head /app/Services/CCN/pid`

if [ $Nofproc -eq 1 ]
then
	          Result=`pwdx $Pid | cut  -d: -f2` 
	         
                  if [ $Result != "/app/Services/CCN" ]
				  then
				        ksh /app/Services/CCN/CCN_Parser_Loop.sh >/app/Services/CCN/log 2>log.CCN/err &
			                echo "CCN application rtarted @ `date` " >> /app/Services/CCN/Watcher.log
	                	  fi
fi

		            
if [ $Nofproc -gt 1 ]
then 
		while kpid read
			do
					kill -9 $kpid
	  	done </app/Services/CCN/pid

              		ksh /app/Services/CCN/CCN_Parser_Loop.sh
       	        	        echo "CCN application restarted @ `date` " >> /app/Services/CCN/Watcher.log
fi
