# CcnCounter
#-----------
App=$0
if [ -s /log/CCN_Data/Final/CcnCounters/CCN_Counters ] 
	then

cp /log/CCN_Data/Final/CcnCounters/CCN_Counters  /log/Shared/CCN/  >> /log/ImporterLog.txt

echo Date: `date` Service: CCN     >> /log/All_App_mysqlImport.Log

echo "Splitting CcnCounter File" >> /log/All_App_mysqlImport.Log

split -l 300 /log/Shared/CCN/CCN_Counters /log/Shared/CCN/Segment

for i in /log/Shared/CCN/Segment*
        do
                echo "moving $i to CCN_Counters" >> /log/All_App_mysqlImport.Log
                mv $i /log/Shared/CCN/CCN_Counters

                echo "Importing $i" >> /log/All_App_mysqlImport.Log

		mysqlimport --host=ActiveDB --user=Writer --password=Writer --ignore --force --columns=CCN_ID,Date_,FromTime,ToTime,CcnPerfCounter,Proccessor,Value --fields-terminated-by=,  CCN_Temp  /log/Shared/CCN/CCN_Counters >> /log/ImporterLog.txt

        done

>/log/CCN_Data/Final/CcnCounters/CCN_Counters

	fi

#platform_measures
#-----------------

if [ -s /log/CCN_Data/Final/PlatformMeasures/platform_measures ]
	then

cp /log/CCN_Data/Final/PlatformMeasures/platform_measures /log/Shared/CCN/

echo Date: `date` Service: `pwd`     >> /log/All_App_mysqlImport.Log

echo "Splitting platform_measures File" >> /log/All_App_mysqlImport.Log

split -l 300 /log/Shared/CCN/platform_measures /log/Shared/CCN/Segment

for i in /log/Shared/CCN/Segment*
	do
		echo "moving $i to platform_measures" >> /log/All_App_mysqlImport.Log
		mv $i /log/Shared/CCN/platform_measures
		
		echo "Importing $i" >> /log/All_App_mysqlImport.Log
		
		mysqlimport --host=ActiveDB --user=Writer --password=Writer --force --ignore --columns=CCN_ID,Date_,FromTime,ToTime,Value,Proccessor,CpuLoad,MaxCpuLoad,MemUsed,MemFree,MemUsage,MaxMemUsage,ProcessorUptime,ClusterUptime,DiskUsage --fields-terminated-by=, CCN_Temp /log/Shared/CCN/platform_measures  >> /log/ImporterLog.txt

	done


cp /log/CCN_Data/Final/PlatformMeasures/platform_measures /log/CCN_Data/Final/PlatformMeasures/platform_measures_temp

>/log/CCN_Data/Final/PlatformMeasures/platform_measures
	fi

#signalling_measures
#-------------------
if [ -s  /log/CCN_Data/Final/SignallingMeasures/signalling_measures ]
	then

cp /log/CCN_Data/Final/SignallingMeasures/signalling_measures /log/Shared/CCN/

echo Date: `date` Service: CCN     >> /log/All_App_mysqlImport.Log

echo "Splitting SignallingMeasures File" >> /log/All_App_mysqlImport.Log

split -l 300 /log/Shared/CCN/signalling_measures /log/Shared/CCN/Segment

for i in /log/Shared/CCN/Segment*
        do
                echo "moving $i to signalling_measures" >> /log/All_App_mysqlImport.Log
                mv $i /log/Shared/CCN/signalling_measures

		echo "Importing $i" >> /log/All_App_mysqlImport.Log
                

		mysqlimport --host=ActiveDB --user=Writer --password=Writer --force --ignore --columns=CCN_ID,Date_,FromTime,ToTime,Value,Proccessor,ICET_TCAP_TPS,ICET_TCAP_COMPONENT_SENT,ICET_TCAP_COMPONENT_RECEIVED,ICET_TCAP_ABORT_LOCAL,ICET_TCAP_ABORT_REMOTE,ANSI_TCAP_TPS,ANSI_TCAP_COMPONENT_SENT,ANSI_TCAP_COMPONENT_RECEIVED,ANSI_TCAP_ABORT_LOCAL,ANSI_TCAP_ABORT_REMOTE,M3_DATA_RECEIVED,M3_DATA_SENT,M3_SIO_RECEIVED,M3_SIO_SENT,M3_ASPAC_SENT,M3_ASPAC_ACK_SENT,M3_LINK_UNAVAILABLE,M3_CONGESTION_LEVEL_1,M3_CONGESTION_LEVEL_2,M3_CONGESTION_LEVEL_3,M3_ADJACENT_SP_INACCESSIBLE,M3_MSU_LOST,M3_DATA_LOST,M3_ASSOCIATION_UNAVAILABLE,M3_CONGESTION_TIME,MTPL3_LINK_UNAVAILABLE,FEIFHD_TCAP_BEGIN_REJECTED,MTPL2_MSU_SENT,MTPL2_MSU_RECEIVED,MTPL2_LINK_LOAD_SENT,MTPL2_LINK_LOAD_RECEIVED,SCTP_PACKAGE_SENT,SCTP_PACKAGE_RECEIVED,SCCP_FAILED_TC_USER_BIND,SAAL_SDU_SENT,SAAL_OCTET_SENT,SAAL_OCTET_RECEIVED,SAAL_LINK_LOAD_SENT,SAAL_LINK_LOAD_RECEIVED,SAAL_MSU_DISCARDED,SAAL_ALIGNMENT_FAILURES,Q703_MSU_SENT,Q703_MSU_RECEIVED,Q703_LINK_LOAD_SENT,Q703_LINK_LOAD_RECEIVED --fields-terminated-by=: CCN_Temp /log/Shared/CCN/signalling_measures    >> /log/ImporterLog.txt

	done
>/log/CCN_Data/Final/SignallingMeasures/signalling_measures
	fi
#oam_provisioning_counter
#------------------------
if [ -s  /log/CCN_Data/Final/oamProvisioningCounter/oam_provisioning_counter ]
	then
cp /log/CCN_Data/Final/oamProvisioningCounter/oam_provisioning_counter /log/Shared/CCN/
echo Date: `date` Service: CCN     >> /log/All_App_mysqlImport.Log

echo "Splitting oam_provisioning_counter File" >> /log/All_App_mysqlImport.Log

split -l 300 /log/Shared/CCN/oam_provisioning_counter /log/Shared/CCN/Segment

for i in /log/Shared/CCN/Segment*
        do
                echo "moving $i to oam_provisioning_counter" >> /log/All_App_mysqlImport.Log
                mv $i /log/Shared/CCN/oam_provisioning_counter

		echo "Importing $i" >> /log/All_App_mysqlImport.Log
                

		mysqlimport --host=ActiveDB --user=Writer --password=Writer --force --ignore --columns=CCN_ID,Date_,FromTime,ToTime,Value,Proccessor,AtomicTransaction,MOsTransaction,AuthenticateCounter,CreateCounter,RetrieveCounter,UpdateCounter,DeleteCounter,CompareCounter,AbandonCounter,ActionCounter,StartCounter,CommitCounter,RollbackCounter --fields-terminated-by=, CCN_Temp /log/Shared/CCN/oam_provisioning_counter >> /log/ImporterLog.txt
	
	done
>/log/CCN_Data/Final/oamProvisioningCounter/oam_provisioning_counter 
	fi
