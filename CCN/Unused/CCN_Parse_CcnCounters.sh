#!/usr/bin/ksh

Rand=$RANDOM
while [ `ls -rlt  /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters | wc -l | sed 's/^ *//g'` -ge 2 ]
	do
	echo "Starting Parsing of "$1
	ls /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters > files_$Rand 
	while read FN
		do
		FileName="/share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/$FN" 
		echo "Parsing File:"$FileName
		From=`echo $FN | cut -d"." -f2 | cut -d"-" -f1`
		To=`echo $FN | cut -d"." -f2 | cut -d"-" -f2 | cut -d"_" -f1`
		Date=`echo $FN | cut -d"." -f1 | sed 's/A//g'`

		/usr/xpg4/bin/grep -e moid -e \<r\> $FileName | tr '\n' '%' > $Rand
		echo "" >> $Rand
		sed 's/<moid>/^/g' $Rand | tr '^' '\n' | sed 's/CcnCounters=//g' | sed 's/<\/moid>%<r>/,/g' | sed 's/<\/r>%//g' | sed 's/ Source = //g' | tail +2 | awk '{OFS=","} { print substr(Date_,7,2),CCNID,substr(Date_,1,4)"-"substr(Date_,5,2)"-"substr(Date_,7,2),From_,To_,$0}' Date_=$Date From_=$From To_=$To CCNID=$1 > /share/CCN/$Rand
		rm $Rand

		echo "importing File"
		Command="LOAD DATA INFILE '/share/CCN/$Rand' REPLACE INTO TABLE CCN_Counters FIELDS TERMINATED BY ',' (Day,CCN_ID,Date_,FromTime,ToTime,CcnPerfCounter,Proccessor,Value)"
		mysql -hCNPVAS01 -uWriter -pWriter -e"$Command" Processings

		mv $FileName /share/CCN_X_Data/$1/PMF_Files/CcnCounters/
		echo "file Imported"
	done < files_$Rand
done

