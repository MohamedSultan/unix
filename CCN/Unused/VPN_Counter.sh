#!/usr/bin/ksh

cd /log/CCN_Data/$1/ 

#for VPN_File in `ls *` 
for VPN_File in `ls *VPN*` 
do

Node_Name=`echo $1`
Hour=`echo $VPN_File  | cut -f2 -d"." | cut -c 1,2 `
Min=`echo $VPN_File   | cut -f2 -d"." | cut -f1 -d"-" | cut -c3,4`
Year=`echo $VPN_File  | cut -c2-5` 
Month=`echo $VPN_File  | cut -c6,7`
Day=`echo $VPN_File    | cut -c8-9`
Date=`echo $Year`-`echo $Month`-`echo $Day`

Variable=`echo "Date,Hour,Min,Node_Name,"`

x=`grep '<mt>' $VPN_File | head -1`

if [ $x ]
then

Columns=`/usr/xpg4/bin/grep -e '<mt>' $VPN_File  | awk '{if ($0==X) { printf ";Stable_Key" ; printf $0} else { printf $0}} END { print "\n"} ' X=$x | grep -v ^$ | sed -e 's/<mt>/,/g' -e 's/<\/mt>//g' | tr ';' '\n' | grep -v ^$ | head -1 ` 


Column_names=$Variable$Columns

/usr/xpg4/bin/grep -e '<r>' -e '<moid>'  $VPN_File  | awk 'BEGIN{X="0"}{ if(substr($0,1,6)=="<moid>"){if( $0 != X ) {printf $0 ; X=$0}} else { printf $0}  } END{print "\n"}' |  sed 's/<moid>/;/g'  | tr ";" "\n" | grep -v ^$ | tr -d ',' | sed -e 's/>//g' -e 's/<//g' -e 's/\//,/g' -e 's/=//g' | tr -d [:alpha:] | awk '{print "'$Date','$Hour','$Min','$Node_Name',",$0} '| sed 's/,$//g' | sed 's/ //g' > /log/Shared/CCN/VPN_Counters/VPN_Counters

mysqlimport --host=ActiveDB --user=Writer --password=Writer --force  --columns=$Column_names --fields-terminated-by=,  CCN /log/Shared/CCN/VPN_Counters/VPN_Counters >> /log/ImporterLog.txt 
App=$0
cat error.log | awk '{ print App,",",$0} '  App=$App  >> /log/All_App_mysqlImport.Log
fi
#mv $VPN_File /log/CCN_X_Data/$1/PMF_Files/VPN_Counters/

done 

