#!/usr/bin/ksh

cd /log/CCN_Data/Final/PlatformMeasures
Check=`ls -A /log/CCN_Data/$1/PMF_Files/PlatformMeasures/`
while [ "$Check" ]
do
ls /log/CCN_Data/$1/PMF_Files/PlatformMeasures >files 
while read FN
do
FileName="/log/CCN_Data/$1/PMF_Files/PlatformMeasures/$FN" 
temp4date=${FN##*A}       # To remove A output will be 20100415.0710-0715_jambala_CcnCounters.20100415072614
date=${temp4date%%.*}     #20100415
temp4time=${temp4date%%_*}  #20100415.0710-0715
temp4time2=${temp4time##*.}
time1=${temp4time2%%-*}
time2=${temp4time2##*-}


#**********Using grep to Extract them from file************************************************
#<moid>PlatformMeasures=DEFAULT, Source = Proc_m1_s21</moid
#<r>5.63</r>                                               
#<r>5.74</r>                                               
#<r>803661.0</r>                                           
#<r>1293103.0</r>                                          
#<r>38.00</r>                                              
#<r>38.00</r>                                              
#<r>1071587.0</r>                                          
#<r></r>                                                   
#<r></r>                                                   
	grep moid  $FileName> TempFile1 
	grep \<r\>  $FileName  > TempFile2
	Size=`wc -l TempFile2`
	i=1
        >TempFile22	
	while [ $i -le $Size ]
			do
			 sed -n ""$i","$(($i+8))" p" TempFile2 > TempFile21
			 i=$(($i+9))
                         awk 'BEGIN{ORS=","}{if(NR==9){print $0"\n"}else{print $0}}' TempFile21 >> TempFile22

			done 	
	
        grep -v ^$ TempFile22 | sed 's/^,//' >TempFile222
	
	awk '{print "'$1'"",""'$date'"",""'$time1'"",""'$time2'"}' TempFile1 > TempFile3


	paste -d ','  TempFile3 TempFile1 TempFile222 | sed -e 's/<moid>PlatformMeasures=//' -e 's/ Source = //' -e 's/<\/moid>//' -e 's/<r>//g' -e 's/<\/r>//g'    >> platform_measures 




if [ ! -d "/log/CCN_X_Data/$1/PMF_Files/PlatformMeasures/Received" ]  
then                                                             
mkdir -p /log/CCN_X_Data/$1/PMF_Files/PlatformMeasures/Received          
fi                                                               
mv $FileName /log/CCN_X_Data/$1/PMF_Files/PlatformMeasures/Received
done <files
Check=`ls -A /log/CCN_Data/$1/PMF_Files/PlatformMeasures`
done
rm TempFile* files


