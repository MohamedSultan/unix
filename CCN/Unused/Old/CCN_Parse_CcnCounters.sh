#!/usr/bin/ksh

cd /log/CCN_Data/Final/CcnCounters
Check=`ls -A /log/CCN_Data/$1/PMF_Files/CcnCounters/`
while [ "$Check" ]
do
ls /log/CCN_Data/$1/PMF_Files/CcnCounters >files 
while read FN
do
FileName="/log/CCN_Data/$1/PMF_Files/CcnCounters/$FN" 
temp4date=${FN##*A}       # To remove A output will be 20100415.0710-0715_jambala_CcnCounters.20100415072614
date=${temp4date%%.*}     #20100415
temp4time=${temp4date%%_*}  #20100415.0710-0715
temp4time2=${temp4time##*.}
time1=${temp4time2%%-*}
time2=${temp4time2##*-}


#**********Using grep to Extract them from file************************************************

	grep moid  $FileName> TempFile1 
	grep \<r\>  $FileName> TempFile2 

	awk '{print "'$1'"",""'$date'"",""'$time1'"",""'$time2'"}' TempFile1 > TempFile3


	paste -d ','  TempFile3 TempFile1 TempFile2 | sed -e 's/<moid>CcnCounters=//' -e 's/ Source = //' -e 's/<\/moid>//' -e 's/<r>//' -e 's/<\/r>//'    >>CCN_Counters


if [ ! -d "/log/CCN_X_Data/$1/PMF_Files/CcnCounters/Received" ] 

then
mkdir -p /log/CCN_X_Data/$1/PMF_Files/CcnCounters/Received
fi
mv $FileName /log/CCN_X_Data/$1/PMF_Files/CcnCounters/Received
done <files
Check=`ls -A /log/CCN_Data/$1/PMF_Files/CcnCounters`
done
rm TempFile* 

