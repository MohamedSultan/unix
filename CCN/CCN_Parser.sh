#!/usr/bin/ksh
Nofgz=`find /share/CCN_Data/STATISTICS/$1/PMF_Files/ | wc -l`

cd /share/CCN_Data/STATISTICS/$1
echo "Number of files ="$Nofgz
if [ $Nofgz -gt 0 ]
	then
			mkdir -p /share/CCN_X_Data/$1/CcnCounters 2>/dev/null
			mkdir -p /share/CCN_X_Data/$1/PlatformMeasures 2>/dev/null
  			echo "ksh /app/Services/CCN/CCN_Parse_MeasureJob.sh $1 will strat" 
  			ksh /app/Services/CCN/CCN_Parse_CcnMeasureJob.sh $1 
  			echo "ksh /app/Services/CCN/CCN_Parse_MeasureJob.sh $1 ends"

  			echo "ksh /app/Services/CCN/CCN_Parse_CcnCounters.sh $1 will strat" 
  			ksh /app/Services/CCN/CCN_Parse_CcnCounters.sh $1
  			echo "ksh /app/Services/CCN/CCN_Parse_CcnCounters.sh $1 ends"

  			echo " ksh /app/Services/CCN/CCN_Parse_PlatformMeasures.sh $1 will start" 
  			ksh /app/Services/CCN/CCN_Parse_PlatformMeasures.sh $1
  			echo " ksh /app/Services/CCN/CCN_Parse_PlatformMeasures.sh $1 end" 

  			echo " ksh /app/Services/CCN/CCN_Parse_SS7Statistics.sh $1 will start" 
  			ksh /app/Services/CCN/CCN_Parse_SS7Statistics.sh $1
  			echo " ksh /app/Services/CCN/CCN_Parse_SS7Statistics.sh $1 end" 

#                	echo " ksh /app/Services/CCN/CCN_Parse_SignallingMeasures.sh $1 will start" 
#  			ksh /app/Services/CCN/CCN_Parse_SignallingMeasures.sh $1
# 		 	echo " ksh /app/Services/CCN/CCN_Parse_SignallingMeasures.sh $1 end" 

#  			echo "ksh /app/Services/CCN/CCN_Parse_oamProvisioningCounter.sh $1 will start" 
#  			ksh /app/Services/CCN/CCN_Parse_oamProvisioningCounter.sh $1
#  			echo "ksh /app/Services/CCN/CCN_Parse_oamProvisioningCounter.sh $1 ends" 
	else 
		echo "No Files are Found for "$1
fi
