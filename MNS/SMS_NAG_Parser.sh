DateHour=$1

DateHour_Formatted=`echo $DateHour | awk '{print substr($0,1,8)"."substr($0,9,2)}'`
FolderName=`echo $DateHour | awk '{print substr($0,1,8)}'`

#-- SMS Submit --#

ssh atarek@MNS-NAGServer "cat /home/nag/nagbill/bak/$FolderName/*$DateHour_Formatted* | sed 's/#/|/g' | cut -d\"|\" -f8,10,15,17,19,20 | grep 'SMSSubmit' | grep -v 'SP' | sed 's/\.//g;s/Reason: //g;s/Reason://g;s/<dataCoding = /|/g;s/>//g;s/ //g;s/,/|/g;s/status=//g;s///g'" > /app_univ/Services/MNS/Processings/NAGSubmit$DateHour

cat /app_univ/Services/MNS/Processings/NAGSubmit$DateHour | awk -F"|" '{if (NF==7) {print $1"|"$2"|"$3"|""|"$4"|"$5"|"$6"|"substr($7,1,4)"-"substr($7,5,2)"-"substr($7,7,2)"|"substr($7,9,2)} else {print $1"|"$2"|"$3"|"$4"|"$5"|"$6"|"$7"|"substr($8,1,4)"-"substr($8,5,2)"-"substr($8,7,2)"|"substr($8,9,2)}}' | sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/g' > /share/Importers/MNS/NAGSubmit$DateHour 

ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/MNS/NAGSubmit$DateHour" CNPVAS03 SMS_KPIs "Count,Sender_Name,API_Name,Reason,Status,DataCoding,Result,SMSC_Account,Date,Hour" "|" MNS MNS_SMS_Submit 


exit
#-- SMS Deliver --#
ssh atarek@MNS-NAGServer "cat /home/nag/nagbill/bak/$FolderName/*$DateHour_Formatted* | grep 'SMSDeli' | cut -d\"|\" -f9,10,15,17,19,21 | sed 's/<dataCoding = //g;s/>//g' | awk -F"|" '{print $1"|"$2"|""|""|"$3"|"$4"|"$6"|"substr($5,1,4)"-"substr($5,5,2)"-"substr($5,7,2)"|"substr($5,9,2)}'" > /app_univ/Services/MNS/Processings/NAGDeliver$DateHour

cat /app_univ/Services/MNS/Processings/NAGDeliver$DateHour | sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/g' > /share/Importers/MNS/NAGDeliver$DateHour

ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/MNS/NAGSubmit$DateHour" CNPVAS03 SMS_KPIs "Count,Sender_Name,API_Name,Reason,Status,DataCoding,Result,SMSC_Account,Date,Hour" "|" MNS MNS_SMS_Delivered
