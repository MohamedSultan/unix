Curdate=`date +%y%m%d`
cp CrashedTables.txt CrashedTables.old.txt
grep $Curdate $HOME/DataBase_Error.err | grep crashed | grep mysqld | sed 's/.*Table //g;s/ is marked as crashed.*//g' | sort | uniq -c | sed 's/^[ \t]*//' | sed 's/ /;/' > CrashedTables.txt

if [ "`diff CrashedTables.txt CrashedTables.old.txt`" = "" ]
	then
		echo "Data Unchanged"
	else
		echo "Found Crashed Tables"

		for Tables in `cat CrashedTables.txt`
			do
				if [ "$Tables" = `grep $Tables CrashedTables.old.txt` ]
					then
						count=`echo $Tables | cut -d";" -f1`
						TableName=`echo $Tables | cut -d";" -f2 | tr -d "'"`
						echo "Table:$TableName is Crashed"
	
						Query="call SEND_SMS('DB Alarm','Checkers','Table: "$TableName" is Crashed; Message is repeated "$count" times till now',(select group_stats.PSD()))"
			#echo $Query
						mysql -hCNPVAS01 -uWriter -pWriter -e"$Query" group_stats
					else
						echo "Table:$TableName is not crashed yet"
				fi
		done
		echo "Repairing Failed tables"
		cd /app_univ/DB_Scripts/Crashed_Repair;ksh Repairer.sh
fi
