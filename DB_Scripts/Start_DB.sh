


echo "Killing root mysqld Processes"
#-------------------------------------
RunningMysqldProcesses=`ps -ef | grep mysqld | grep root | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | wc -l | sed 's/^ *//g'`
while [ $RunningMysqldProcesses -gt 0 ]
	do
		RunningMysqldProcesses=`ps -ef | grep mysqld | grep root | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | wc -l | sed 's/^ *//g'`
		RunningMysqldProcess=`ps -ef | grep mysqld | grep root | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | head -1`
			echo "Killing Process ID:"$RunningMysqldProcess
			kill -9 "$RunningMysqldProcess"
done
#----------------------------------------------------------------------------
echo "Killing mysql mysqld Processes"
#-------------------------------------
RunningMysqldProcesses=`ps -ef | grep mysqld | grep mysql | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | wc -l | sed 's/^ *//g'`
while [ $RunningMysqldProcesses -gt 0 ]
	do
		RunningMysqldProcesses=`ps -ef | grep mysqld | grep mysql | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | wc -l | sed 's/^ *//g'`
		RunningMysqldProcess=`ps -ef | grep mysqld | grep mysql | grep -v 'grep' | sed 's/  */ /g;s/^ //g' | cut -d" " -f2 | head -1`
			echo "Killing Process ID:"$RunningMysqldProcess
			kill -9 "$RunningMysqldProcess"
done
#----------------------------------------------------------------------------
echo "Starting mysqld"
export PATH=${PATH}:/opt/mysql/mysql/bin
mysqld_multi --defaults-file=/etc/my.cnf.multi --log=/tmp/mysqld_multi.log start 
#----------------------------------------------------------------------------
echo "Please wait till the processes owned by mysql is started ...."
