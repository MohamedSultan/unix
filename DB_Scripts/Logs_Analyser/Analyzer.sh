
HostName=`hostname`

Date=`echo $1| sed 's/-//g' | cut -c3-10`
#HomeDir="/log_bkp/MySQL_Data"
HomeDir=$HOME
#echo $HostName
#CurrentDir=`pwd`


Hour=0
while [ Hour -le 23 ]
	do
	OutFile=/share/Importers/DB_Logger/"Analysis_"$Date"_"$Hour"_"$HostName

	grep -h "$Date  $Hour" $HomeDir/DataBase_Error.err $HomeDir/DataBase_Error.err.Yesterday $HomeDir/DataBase_Error.err.Yesterday.Yesterday|sed -e 's/:/|/' -e 's/:/|/'|tr -d "'" DataBase_Error |sed -e 's/ /|/' -e 's/ /|/' -e 's/ /|/' -e 's/ /|/' | grep -v 'table or database name .ssh' | grep -v 'table or database name .sunw' | cut -d"|" -f1,3,6,7 | sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/'|sed -e 's/\[//' -e 's/\]//' | awk 'BEGIN{FS="|" ; OFS="|"} {print $1,"20"substr($2,1,2)"-"substr($2,3,2)"-"substr($2,5,2),$3,$4,Host,$5}' Host=$HostName > $OutFile

	ksh /app_univ/StatsCollector/Importer.sh $OutFile CNPVAS01 DB_Logs "Count,Date,Hour,Type,HostName,Description" "|" Loggers DB_Logger_$HostName
	
	Hour=$((Hour + 1))
	
#	Command="LOAD DATA INFILE '$CurrentDir/Analysis_"$Date"_"$Hour"' REPLACE INTO TABLE DB_Logs FIELDS TERMINATED BY '|' (Count,Date,Hour,Type,Description)"
#	mysql -uWriter -pWriter -e"$Command" Loggers
	#rm /share/Importers/DB_Logger/"Analysis_"$Date"_"$Hour"_"$HostName
done
