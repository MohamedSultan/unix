HostName=`hostname`
CrashedTablesFile="/share/Importers/DB_Logger/CrashedTable.txt.$HostName"
if [ -e "$CrashedTablesFile" ]
	then
		rm $CrashedTablesFile
fi
Command="SELECT DISTINCT Crashed_Table_Details(Description,'Table Name') TableName,ifnull(Crashed_Table_Details(Description,'Partition'),'XXX') Partition FROM \`DB Transactions - Today\` where HostName='$HostName' and Description like '%Crashed%' and Description like '%mysqld%' and handled=0 into outfile '$CrashedTablesFile' FIELDS TERMINATED BY ';'" 
mysql -hCNPVAS01 -uWriter -pWriter -e"$Command" Loggers

while read CrashedTables
	do
	AffectedTable=`echo $CrashedTables| cut -d";" -f1`
	AffectedPartition=`echo $CrashedTables| cut -d";" -f2`

	echo $AffectedTable,$AffectedPartition
	
	if [ "$AffectedPartition" = "" ]
                then
			Query="call SEND_SMS('DB Alarm','Checkers','Fixing Table: "$AffectedTable"',(select group_stats.PSD()))"
                else
			Query="call SEND_SMS('DB Alarm','Checkers','Fixing Table: $AffectedTable, Partition $AffectedPartition',(select group_stats.PSD()))"
        fi

	mysql -hCNPVAS03 -uWriter -pWriter -e"$Query" group_stats

	echo "Updating Handled Loggers"
	Command="Update DB_Logs SET Handled=1 where Description like '%Crashed%' and Description like '%mysqld%' and Handled=0 and Description like '%"`echo $AffectedTable | cut -d" " -f1| sed 's/\./\//'`"%' and Date='"`date '+%Y-%m-%d'`"'"
	mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" Loggers

	
	if [ "$AffectedPartition" = "" ] || [ "$AffectedPartition" = "XXX" ]
		then	
			echo "Repairing Table $AffectedTable"
			Command="repair Table "$AffectedTable
			Query="call SEND_SMS('DB Alarm','Checkers','Fixing of Table: "$AffectedTable" is Finished',(select group_stats.PSD()))"
		else
			echo "Repairing Table $AffectedTable Partition $AffectedPartition"
			Command="ALTER TABLE $AffectedTable REPAIR PARTITION $AffectedPartition"
			Query="call SEND_SMS('DB Alarm','Checkers','Fixing of Table: $AffectedTable, Partition $AffectedPartition is Finished',(select group_stats.PSD()))"
	fi
	echo $Command
	mysql -hCNPVAS03 -uWriter -pWriter -e"$Command"
	mysql -hCNPVAS03 -uWriter -pWriter -e"$Query" group_stats
	echo "---------------------------------------------------"

done < $CrashedTablesFile
