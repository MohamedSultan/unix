VPN_ID=$1

Data_Dir=/share/CCN_Data/STATISTICS/$VPN_ID/PMF_Files/MOJO
CurrentDir=`pwd`
Processing_Dir=/app_univ/Services/VPN/Processings/$VPN_ID
Importing_Path=/share/Importers/VPN/$VPN_ID

cd $Data_Dir

No_Process=`ps -ef | grep $VPN_ID | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do
     echo `date`" Found another running Process, Waiting Process"
     sleep 10
     No_Process=`ps -ef | grep $VPN_ID | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
     No_Process=$(($No_Process+0))
done

#--Creating Importing Directory for each VPN--#
if [ -d "$Importing_Path" ]
then
     echo "Dir Exists"
else
     mkdir $Importing_Path
     chmod 777 $Importing_Path
fi
#----------------------------------------------#

#--Creating Processing Directory for each VPN--#
if [ -d "$Processing_Dir" ]
then
     echo "Dir Exists"
else
     mkdir $Processing_Dir
     chmod 777 $Processing_Dir
fi
#----------------------------------------------#

for FileName in `ls *MOJO.xml`
do

		#--Processing each MOJO file--#		
		
		echo "Processing File "$FileName
		
		Date_Time=`cat $FileName | grep '<mts>' | head -1 | sed 's/<mts>//g' | sed 's/<\/mts>//g'`
                Date=`echo $Date_Time | awk '{print substr($0,1,4)"-"substr($0,5,2)"-"substr($0,7,2)}'`
                Hour=`echo $Date_Time | awk '{print substr($0,9,2)}'`
		Day=`echo $Date_Time  | awk '{print substr($0,7,2)}'` 
		
		MI_Count=`cat $FileName | grep '<mi>' | wc -l | sed 's/^ *//g'`

		Count=1

	 	#--Processing each MI in this file--#	
		
		while [[ $Count -le $MI_Count ]]
		do
			#--Parsing the MI file--#

                        #--Generate MI file--#
                        cat $FileName | sed -n "/<mi>/,/<\/mi>/p" | sed 's/<mi>/Y/g' | tr '\n' X | tr Y '\n' | grep -v '^$' | head -n $Count | tail -1 > $Processing_Dir/MI_"$Count"
                        #---------End-------#

                        #--Generate Counters list file--#
                        cat $Processing_Dir/MI_"$Count" | tr X '\n' | grep '<mt>' | sed 's/<mt>//g' | sed 's/<\/mt>//g' > $Processing_Dir/Counter_"$Count"
                        #-----------End---------------#

                        #--Generate StableKeys file--#
                        cat $Processing_Dir/MI_"$Count" | tr X '\n' | tr X '\n' | grep -v '<mt>' | grep -v '<mts>' | grep -v '<gp>' | sed 's/<mv>/Y/g' |tr '\n' X  | tr Y '\n'| cut -d, -f2","5 | sed 's/stableKey=//g' | cut -d, -f1 | grep -v X > $Processing_Dir/StableKeys_"$Count"	
			#-----------End---------------#

			#--Generate Values file--#
			cat $Processing_Dir/MI_"$Count" | tr X '\n' | grep -v '<mt>' | grep -v '<mts>' | grep -v '<gp>' | sed 's/<mv>/Y/g' |tr '\n' X  | tr Y '\n'| cut -d, -f5 | sed 's/nodeName=jambala<\/moid>X//g' | sed 's/X<\/mv>X//g' | tr X '\n' | grep -v '^$' | sed 's/<r>//g' | sed 's/<\/r>//g' > $Processing_Dir/R_"$Count"
			#-----------End---------------#

			NumberOfCounters=`cat $Processing_Dir/Counter_"$Count" | wc -l | sed 's/^ *//g'`
                        NumberOfStableKeys=`cat $Processing_Dir/StableKeys_"$Count" | wc -l | sed 's/^ *//g'`			

			#--Updating Counter list file--#	
		        Counter=1
                        while [[ $Counter -le $NumberOfStableKeys ]]
                        do
                                cat $Processing_Dir/Counter_"$Count"  >> $Processing_Dir/CounterList_"$Count"
                                Counter=$(( $Counter + 1 ))
                        done	
			
			rm -f $Processing_Dir/Counter_"$Count"
			#-----------End---------------#

			#--Updating StableKeys file--#
			touch $Processing_Dir/SK_Temp1_"$Count"
			Counter=1
			while [[ $Counter -le $NumberOfCounters ]]
			do 
				paste -d"," $Processing_Dir/StableKeys_"$Count" $Processing_Dir/SK_Temp1_"$Count" > $Processing_Dir/SK_Temp2_"$Count"
				mv $Processing_Dir/SK_Temp2_"$Count" $Processing_Dir/SK_Temp1_"$Count" 
				Counter=$(( $Counter + 1 ))
			done 
	
			cat $Processing_Dir/SK_Temp1_"$Count" | tr ',' '\n' | grep -v '^$' > $Processing_Dir/StableKeys_List_"$Count" 			
			rm -f $Processing_Dir/StableKeys_"$Count"
			rm -f $Processing_Dir/SK_Temp2_"$Count"
			rm -f $Processing_Dir/SK_Temp1_"$Count"	
			#-----------End---------------#

			#--Generate the final file--#
			paste -d"," $Processing_Dir/StableKeys_List_"$Count" $Processing_Dir/R_"$Count" | sed 's/<\/mi>//g' > $Processing_Dir/StableKeys_R_"$Count"			
			rm -f $Processing_Dir/StableKeys_List_"$Count"
			rm -f $Processing_Dir/R_"$Count"	

			paste -d"," $Processing_Dir/StableKeys_R_"$Count" $Processing_Dir/CounterList_"$Count" | awk -F"," '{print Date_","Hour_","$1","$3","$2","VPN_","Day_}'  Date_=$Date Hour_=$Hour Day_=$Day VPN_=$VPN_ID | grep -v ',0,' >> $Importing_Path/$FileName 
			
			rm -f $Processing_Dir/CounterList_"$Count"
			rm -f $Processing_Dir/MI_"$Count"
			rm -f $Processing_Dir/StableKeys_R_"$Count"

			#--importing data to DB--#

			Count=$(( $Count + 1 ))
		done
		#------------------------------------------------------------------------------------------------------------------#
		
		#--importing data to DB--#
                ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "$Importing_Path/$FileName" CNPVAS03 VPN_Counters_MOJO "Date,Hour,StableKey,Counter,Value,VPN_ID,Day" "," VPN VPN_MOJO

		#--Moving FileName--#
                if [ -d "/share/CCN_X_Data/$VPN_ID/Mojo" ]
                then
                        echo "Dir Exists"
                else
                        mkdir /share/CCN_X_Data/$VPN_ID/Mojo
                fi

		mv -f $Data_Dir/$FileName /share/CCN_X_Data/$VPN_ID/Mojo/
done
