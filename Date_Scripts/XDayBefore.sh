
 #Usage:   XBeforeDays 'yyyy-mm-dd' Xdaybefore      --->   Get X day before the mentioned date
        #  XBeforeDays Xdaybefore      		    ------> Date by default yesterday date 

if [ $# == 1 ]
	then 
	Date_Date_In=`ksh /app/YesterdaysDate`
	Date=`echo $Date_Date_In`
	XDays=$1
else
	Date=$1
	XDays=$2
fi


Year=`echo $Date | cut -c3-4`
Month=`echo $Date | cut -c6-7`
Day=`echo $Date | cut -c9-10`

BeforeXDays=`/app_univ/Date_Scripts/Shift_Date 20$Year$Month$Day $XDays | sed 's/\(..\)\(..\)\(..\)\(..\)/\1\2-\3-\4/'`

echo $BeforeXDays
