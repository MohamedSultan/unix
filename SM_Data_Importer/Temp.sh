FileName=$1
Date=$2
tail +2 $FileName > /share/Importers/$FileName
mysql -uWriter -pWriter -hCNPVAS04 -e"TRUNCATE TABLE Traffic_copy" SOC
Command="LOAD DATA INFILE '/share/Importers/$FileName' REPLACE INTO TABLE Traffic_copy FIELDS TERMINATED BY ',' (SITE_LOGICAL,Network,HOUR_ID,SPEECH_TRAFFIC,DATA_MB)" 
mysql -uWriter -pWriter -hCNPVAS04 -e"$Command" SOC
echo "Updating Date"
Command="UPDATE Traffic_copy SET Date='$Date' WHERE Date='' or Date is null"
mysql -uWriter -pWriter -hCNPVAS04 -e"$Command" SOC
Command="replace into \`Activity Log\`(\`Activity Date\`, SiteID, \`Hub Name\`, Sub_Region, NE, Total_SPEECH_TRAFFIC, DATA_MB, \`Insertion Date\`, \`Activity Owner\`, CRQ, ID, SiteStatusBeforeActivity, SiteStatusAfterActivity, \`Data Inserted By\`, \`Activity Implemented By\`, Hour_From, Hour_To, Postponed) select \`Activity Date\`, SiteID, \`Hub Name\`, Sub_Region, NE,round(sum(SPEECH_TRAFFIC),2) Total_SPEECH_TRAFFIC, round(sum(T.DATA_MB),2), \`Insertion Date\`, \`Activity Owner\`, CRQ, ID, SiteStatusBeforeActivity, SiteStatusAfterActivity, \`Data Inserted By\`, \`Activity Implemented By\`, Hour_From, Hour_To, Postponed  from \`Activity Log\` AL,Traffic_copy T where left(AL.\`Activity Date\`,10)=T.Date and AL.SiteID=T.SITE_LOGICAL and T.HOUR_ID between Hour_From and Hour_To and \`Activity Date\` like '$Date%' and Total_SPEECH_TRAFFIC is null group by \`Activity Date\`, SiteID, \`Hub Name\`, Sub_Region, NE, \`Insertion Date\`, \`Activity Owner\`, CRQ, ID, SiteStatusBeforeActivity, SiteStatusAfterActivity, \`Data Inserted By\`, \`Activity Implemented By\`, Hour_From, Hour_To, Postponed "
echo "Updating Activity log"
mysql -uWriter -pWriter -hCNPVAS04 -e"$Command" SOC
