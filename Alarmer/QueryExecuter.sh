Query=$1
Host=$2
FileName=$3

#echo "Start Query Executer inside it " 
#echo $Query

#if [ "$Host" = "" ]
#	then
#		Host="CNPVAS01"
#fi
#echo $Host
if [ `echo $Query | grep '\`' | wc -l | sed 's/^ *//g'` != 0 ]
	then
	X=1
fi

#echo "Getting Host Definition"

Rand=$RANDOM
Command="select Connection_Name,lcase(Type),IP,Port,UserName,Password,DBName from DB_Connections Where Connection_Name='$Host' into outfile '/share/Exporters/Alarmer/$Rand' FIELDS TERMINATED BY ';'"
mysql -uReader -pReader -P3307 -hCNPVAS03 -e"$Command" Alarms
Type=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f2`
Host=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f3`
Port=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f4`
UserName=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f5`
Password=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f6`
DBName=`head -1 /share/Exporters/Alarmer/$Rand | cut -d";" -f7`
rm /share/Exporters/Alarmer/$Rand

if [ "$FileName" = "" ] 
	then
		if [ "$Host" = "" ]
		        then
				mysql -N -hCNPVAS03 -uAlarmer -pAlarmer -P3307 -e"$Query" 
			else
				mysql -N -h"$Host" -uAlarmer -pAlarmer -e"$1" 
		fi
	else
		if [ "$Type" = "mysql" ]
			then
				if [ "$Port" = "" ]
                                        then
                                                mysql -uAlarmer -pAlarmer -h"$Host" -e"$1" group_stats  | tr '\t' '|' > $FileName
                                        else
                                                mysql -uAlarmer -pAlarmer -P"$Port" -h"$Host" -e"$1" | tr '\t' '|' > $FileName
                                        fi
			else
				cd /app_univ/StatsCollector; java -jar Stats_Collector.jar Alarm_$Rand "" True "$Host" "$Port" "$DBName" "$UserName" "$Password" "$Type" "$Query" "|"
				cd -
				mv /app_univ/StatsCollector/Alarm_$Rand_* .
		fi
fi
#echo "end Query Executer"
