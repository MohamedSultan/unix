Current=$1
KPI=$2
Action=$3
Service=`echo $4 | tr -d ' '`
Receipients=`echo $5 | tr -d ' '`
Criticality=$6
Node_Name=$7
Template=$8	#Meassage
NextUpdate=$9
DB_Host=${10}
CurrentKPI_ColumnName=${11}
MailMessage=${12}
Comp_Current_grt_KPI1=${13}
KPI2=${14}
Current_Value2=${15}
Comp_Current_grt_KPI2=${16}
KPI3=${17} 
Current_Value3=${18}
Comp_Current_grt_KPI3=${19}
Current_2=${20}
Current_3=${21}
MailReceipients=${22}

echo "Start of Compairer"
echo "------------------"
echo "Current1="$Current
echo "Current2="$Current_2
echo "Current3="$Current_3
echo "KPI1=$KPI"
echo "KPI2=$KPI2"
echo "KPI3=$KPI3"

echo "Node Name=$Node_Name"

#Converting text to decimal numbers
#------------------------------------
Current=`echo $Current | tr -d " "`
Current=`echo "$Current"+0.0 | bc -l`
Current_Test=`echo 1000*"$Current"+0.0 | bc -l`

KPI=`echo $KPI | tr -d " "`
KPI=`echo "$KPI"+0.0 | bc -l`
KPI_Test=`echo 1000*"$KPI"+0.0 | bc -l`

if [ "$KPI2" = "" ] || [ "$KPI2" = "\N" ]
	then
		echo "No KPI2"
		Current_Greater_KPI_2="True"
		Current_Less_KPI_2="True"
	else
		echo "KPI2 Exists"
		KPI2=`echo $KPI2 | tr -d " "`
		KPI2=`echo "$KPI2"+0.0 | bc -l`
		KPI_Test2=`echo 1000*"$KPI2"+0.0 | bc -l`
		
		Current_Value2=`echo $Current_Value2 | tr -d " "`
		Current_Value2=`echo "$Current_Value2"+0.0 | bc -l`
		Current_Value2_Test=`echo 1000*"$Current_Value2"+0.0 | bc -l`
fi

if [ "$KPI3" = "" ] || [ "$KPI3" = "\N" ]
	then
		echo "No KPI3"
                Current_Greater_KPI_3="True"
                Current_Less_KPI_3="True"
	else
		echo "KPI3 Exists"
		KPI3=`echo $KPI3 | tr -d " "`
		KPI3=`echo "$KPI3"+0.0 | bc -l`
		KPI_Test3=`echo 1000*"$KPI3"+0.0 | bc -l`

		Current_Value3=`echo $Current_Value3 | tr -d " "`
                Current_Value3=`echo "$Current_Value3"+0.0 | bc -l`
                Current_Value3_Test=`echo 1000*"$Current_Value3"+0.0 | bc -l`
fi
#-------------------------------------------
echo "Currents" $Current_Test $Current_Value2_Test $Current_Value3_Test

Message=`echo $Template | sed "s/XKPI/$KPI/g;s/XCurrent/$Current/g;s/XNodeName/$Node_Name/g"`
echo "message= "$Message

AppDir=`pwd`
Random=`date '+%Y%m%d%H%M%S'`
#echo "Current_Test="$Current_Test
#----------------------------------------
#defining which is greater KPI or Current
#----------------------------------------
if [ $Current_Test -ge $KPI_Test ] && [ "$Comp_Current_grt_KPI1" = "1" ] 
	then
		Current_Greater_KPI_1="True"
		Current_Less_KPI_1="False"
		echo "Current1 greater than KPI"
fi
if [ $Current_Test -lt $KPI_Test ] && [ "$Comp_Current_grt_KPI1" = "0" ] 
	then
		Current_Less_KPI_1="True"
		Current_Greater_KPI_1="False"
		echo "Current1 less than KPI"
fi
#-----------------
if [ $Current_Value2_Test -ge $KPI_Test2 ] && [ "$Comp_Current_grt_KPI2" = "1" ] 
	then
		Current_Greater_KPI_2="True"
		Current_Less_KPI_2="False"
		echo "Current2greater than KPI"
fi
if [ $Current_Value2_Test -lt $KPI_Test2 ] && [ "$Comp_Current_grt_KPI2" = "0" ] 
	then
		Current_Less_KPI_2="True"
		Current_Greater_KPI_2="False"
		echo "Current2 less than KPI"
fi
#--------------------------
if [ $Current_Value3_Test -ge $KPI_Test3 ] && [ "$Comp_Current_grt_KPI3" = "1" ]
        then
                Current_Greater_KPI_3="True"
		Current_Less_KPI_3="False"
		echo "Current3 greater than KPI"
fi
if [ $Current_Value3_Test -lt $KPI_Test3 ] && [ "$Comp_Current_grt_KPI3" = "0" ]
        then
                Current_Less_KPI_3="True"
		Current_Greater_KPI_3="False"
		echo "Current3 less than KPI"
fi
#----------------------------------------------------------------------------
#Deciding Raising alarm or not
#----------------------------
Current_Greater_KPI="False"
Current_Less_KPI="False"
if [ "$Current_Greater_KPI_1" = "True" ] && [ "$Current_Greater_KPI_2" = "True" ] && [ "$Current_Greater_KPI_3" = "True" ] 
	then
		Current_Greater_KPI="True" 	
fi
if [ "$Current_Less_KPI_1" = "True" ] && [ "$Current_Less_KPI_2" = "True" ] && [ "$Current_Less_KPI_3" = "True" ]
	then
		Current_Less_KPI="True"
fi
#---------------------------------------------------------------------------------------
if [ "$Current_Greater_KPI" = "True" ] || [ "$Current_Less_KPI" = "True" ]
	then
		#Raising Alarm
		#---------------
		echo "Current:$Current, KPI="$KPI
		#Current=`echo "$Current"/1000 | bc -l`
		#KPI=`echo "$KPI"/1000 | bc -l`
		#-------------SMS-------------------------------------------------------------------------------------
		if [ ! `echo $Action | grep -i "SMS"` = "" ]
			then
				echo "Sending SMS"
				#Message="Service $Service is Having a Problem: $Current is Greater than or Equal to $KPI"
				ksh QueryExecuter.sh "Call Queues.Send_SMS('Alarmer','$Service','$Message','$Receipients')" "" "$Rand" &
		fi
		#-------------MOM-------------------------------------------------------------------------------------
		if [ ! `echo $Action | grep -i "MOM"` = "" ]
                        then
                		echo "Interfacing with MOM"
				#Summary="Current KPI="$Current", Threshold is:"$KPI
				#Summary="Current Failure Rate="$Current"%, KPI="$KPI"%"
				Notes="Notes"
				#ksh MOM.sh "$Node_Name" "$Criticality" "$Updated_Message" "$Service" "$Notes" "$KPI" "$NextUpdate" "$Current" "$Template"
				ksh MOM.sh "$Node_Name" "$Criticality" "$Message" "$Service" "$Notes" "$KPI" "$NextUpdate" "$Current" "$Template"
                fi
		#-------------Log-------------------------------------------------------------------------------------
		if [ ! `echo $Action | grep -i "Log"` = "" ]
                        then
                		echo "Problem:$Current is Greater than or Equal to $KPI"
                fi
		#-------------Mail-------------------------------------------------------------------------------------
		if [ ! `echo $Action | grep -i "Mail"` = "" ]
                        then
                		echo "Mail:$Current is Greater than or Equal to $KPI"
				echo "Sending Mail"
				echo "$MailReceipients Alarm $Template"
				ksh QueryExecuter.sh "Call Queues.Send_Mail('$Service','$MailReceipients','$Service Alarm',replace('$MailMessage','A5A5','\r\n'),'CNP.VAS@Vodafone.com')" &
                fi
		#-------------NEMO-------------------------------------------------------------------------------------
		if [ ! `echo $Action | grep -i "NEMO"` = "" ]
                        then
				Alarm_Category=$Node_Name
				Node_Name=$Service
				Description="$Template KPI: Current:$Current, KPI:$KPI"
				Site="1"
				Alarm_type="XXX"	

				#mapped to Service @ MOM				
                		echo $Node_Name > $AppDir/$Random
				#3: major, 4: critical
                                echo $Criticality >> $AppDir/$Random
				#put it empty as it concatinate it with description	
                                echo $Alarm_Category  >> $AppDir/$Random
                                echo $Description >> $AppDir/$Random
				#put it empyt
                                echo $Site >> $AppDir/$Random
                                echo $Alarm_type >> $AppDir/$Random
				
				scp $AppDir/$Random VASQ@NEMO:/home/nemo/$Random.txt
				mv $AppDir/$Random $AppDir/SentReports
                fi
		#---------------------------------------------------------------------------------------------------------------
	else
		echo "No Raise Alarm"
fi
