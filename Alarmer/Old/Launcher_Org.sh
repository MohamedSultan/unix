
#Extracts the now alarm event Only and passes it to alarm thread
#---------------------------------------------------------------


Count=`QueryExecuter "SELECT COUNT(*) from Alarms.Alarmer WHERE Active=1" `
Count=`expr "$Count" + 0`
Rand=$RANDOM
Command="SELECT @row := @row + 1 AS ID,Current,KPI,Action,ServiceName,Receipients,\`NEMO Criticality\`,\`At Hour\`,\`At Minute\`,\`Node Name\`,\`Message\`,\`Next Update(Min)\`,DB_Host FROM Alarms.Alarmer, (SELECT @row := 0) r WHERE Active=1 INTO OUTFILE '/share/Alarmer/$Rand' FIELDS TERMINATED BY '|'"
mysql -hActiveDB -uReader -pReader -e"$Command"
cp /share/Alarmer/$Rand  KPIs.txt
rm -f /share/Alarmer/$Rand

Counter=0
RunHour="False"
RunMinute="False"
while [ $Counter -lt $Count ]
	do
	Line=$(($Counter+1))
	Current=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f2`
	KPI=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f3`
	Action=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f4`
	Service=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f5|sed 's/^ //g'`
	Receipients=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f6`
	Criticality=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f7`
	At_Hours=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f8|tr -d " "|sed 's/,/ /g' | sed 's/\*/X/g'`
	At_Minutes=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f9| tr -d " " | sed 's/,/ /g' | sed 's/\*/X/g'`
	NodeName=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f10`
	Template=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f11`
	NextUpdate=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f12`
	DB_Host=`head -$Line KPIs.txt | tail -1 | cut -d '|' -f13`

	echo "Line#:$Line, DB_Host:$DB_Host"
	
	CurrentHour=`date '+%H'`
        CurrentMin=`date '+%M'`

	set -A At_Hour $At_Hours
	Step=0
	#echo $At_Hours| awk -F' ' '{ print NF}'
	NumberOfRecords=`echo $At_Hours| awk -F' ' '{ print NF}'`
	RunHour="False"
	RunMinute="False"
	while [ $Step -lt $NumberOfRecords ]
		do
        		if [ "${At_Hour[$Step]}" = "$CurrentHour" ] || [ "${At_Hour[$Step]}" = "0$CurrentHour" ] || [ "0${At_Hour[$Step]}" = "$CurrentHour" ] || [ "${At_Hour[$Step]}" = "X" ]
                		then
					HOUR=$CurrentHour
                        		RunHour="True"
        		fi
			Step=$(($Step+1))
	done

        set -A At_Minute $At_Minutes
        Step=0
        #echo $At_Minutes| awk -F' ' '{ print NF}'
        NumberOfRecords=`echo $At_Minutes| awk -F' ' '{ print NF}'`
	#Checking available Threads
        while [ $Step -lt $NumberOfRecords ]
                do
			#echo "${At_Minute[$Step]}"
                        if [ "${At_Minute[$Step]}" = "$CurrentMin" ] || [ "${At_Minute[$Step]}" = "0$CurrentMin" ] || [ "0${At_Minute[$Step]}" = "$CurrentMin" ] || [ "${At_Minute[$Step]}" = "X" ]
                                then
					MINUTE=$CurrentMin
                                        RunMinute="True"
                        fi
                        Step=$(($Step+1))
        done
	
	#echo "$RunHour $RunMinute"
	if [ "$RunHour" = "True" ] && [ "$RunMinute" = "True" ]
		then
			echo `date '+%Y-%m-%d %H:%M`" Running "`echo $Service | tr -s [:space:]`"Service @$HOUR:$MINUTE"";Check Status:$RunHour,$RunMinute"
			echo "Host:$DB_Host"
			ksh AlarmThread "$Current" "$KPI" "$Action" "$Service" "$Receipients" "$Criticality" "$NodeName" "$Template" "$NextUpdate" "$DB_Host" &
	else
		echo `date '+%Y-%m-%d %H:%M`" Service "`echo $Service | tr -s [:space:]`" Will Not Run Now"";Check Status:$RunHour,$RunMinute"
	fi
	
	Counter=$(($Counter+1))
done


