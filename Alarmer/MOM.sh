Node_Name=$1
Criticality=$2
Desc=$3
Service=$4
Notes=$5
KPI=$6
NextUpdate=$7
Current=$8

Rand=$RANDOM
Message="Current KPI="$Current", Threshold is:"$KPI
Command="REPLACE INTO \`MOM Alarm\` (\`Node Name\`,Severity,Summary,Service,Notes,\`Next Update\`,KPI,Transaction_ID,Time) VALUES('$Node_Name','$Criticality','$Desc','$Service','$Notes','$NextUpdate','$KPI','$Rand',UNIX_TIMESTAMP(current_timestamp()))"
echo $Command
mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Alarms

cd /app_univ/StatsCollector

Output=`java -jar Stats_Collector.jar app.Collectors.Alarms.MOM `/app_univ/Date_Scripts/CurDate``
echo $Output | grep Error: | wc -l | sed 's/^ *//g'
if [ `echo $Output | grep Error: | wc -l | sed 's/^ *//g'` -eq 0 ]
	then
		Command="Update \`MOM Alarm\` Set Success=1 WHERE Transaction_ID=$Rand"
		mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Alarms
	else
		echo "Error in inserting data to MOM"
		Command="Update \`MOM Alarm\` Set Failed=1 WHERE Transaction_ID=$Rand"
		mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Alarms
fi
echo "Done"
