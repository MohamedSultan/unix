Current=$1
KPI=$2
Action=$3
Service=`echo $4 | tr -d ' '`
Receipients=`echo $5 | tr -d ' '`
Criticality=$6
Node_Name=$7
Template=$8	#Meassage
NextUpdate=$9
DB_Host=${10}
CurrentKPI_ColumnName=${11}
MailMessage=${12}
Current_gt_KPI1=${13}
KPI2=${14}
CurrentKPI_ColumnName2=${15}
Comp_Current_grt_KPI2=${16}
KPI3=${17} 
CurrentKPI_ColumnName3=${18}
Comp_Current_grt_KPI3=${19}
Current_2=${20}
Current_3=${21}
MailReceipients=${22}

echo "Start of Alarm Thread for Service:$Service"
echo "------------------------------------------------"

#echo "Current:"$Current
echo "KPI1:"$KPI
echo "KPI2:"$KPI2
echo "KPI3:"$KPI3
#echo "Host:"$DB_Host
#echo "NextUpdate:$NextUpdate"
#echo $Current"?"$KPI
Rand=$RANDOM

#echo $Select
#Checking KPI Query or absolute value
KPI_Select=`echo $KPI | grep -i "Select"`
KPI_Call=`echo $KPI | grep -i "Call"`
if [ ! "$KPI_Select" = "" ] || [ ! "$KPI_Call" = "" ]
        then
		echo "Running Select Query or Call"
                ksh QueryExecuter.sh "$KPI" "$DB_Host" "$Rand"
		KPI=`cat $Rand | tail -1`
		echo $KPI	
		rm $Rand	
        else
                KPI=$KPI
fi
echo "Filnally KPI is "$KPI
#-------------------------------------------------------------------------------
Current_Select=`echo $Current | grep -i "select"`
Current_Call=`echo $Current | grep -i "call"`
if [ ! "$Current_Select" = "" ] || [ ! "$Current_Call" = "" ]
	then
		echo "Running Query Executer"
	#	Current=`ksh QueryExecuter.sh "$Current" "$DB_Host" "$Rand"`
		echo $Current
		echo $DB_Host
		echo $Rand
		ksh QueryExecuter.sh "$Current" "$DB_Host" "$Rand"

		if [ `wc -l $Rand | sed 's/^ *//g;s/ /|/' | cut -d"|" -f1` -eq 0 ]
			then
				echo "Query Didn't return any values, exiting thread"
				rm $Rand
				exit
		fi
	echo "Statrting gettin KPI col pos"
		#Getting KPI column pos
		#----------------------
		KPI_Field_Counter=1
		NumberOfFields=`head -1 $Rand | awk -F"|" '{print NF}'`
		echo $NumberOfFields
		KPI_Field=0
		KPI_Field2=0
		KPI_Field3=0
		while [ $KPI_Field_Counter -le $NumberOfFields ]
			do
				TestField=`head -1 $Rand | cut -d"|" -f$KPI_Field_Counter`
				if [ "$TestField" = "$CurrentKPI_ColumnName" ]
					then	
						KPI_Field=$KPI_Field_Counter
				fi
				if [ "$TestField" = "$CurrentKPI_ColumnName2" ]
					then
						KPI_Field2=$KPI_Field_Counter
				fi	
				if [ "$TestField" = "$CurrentKPI_ColumnName3" ]
					then
						KPI_Field3=$KPI_Field_Counter
				fi
				((KPI_Field_Counter+=1))
		done
		echo "Field Pos="$KPI_Field
		#----------------------------------------------------------------------------
		RecordID=1
		while read RecordLine
			do
				#Updating Message Template
				#--------------------------
				
				UpdatedTemplate=$Template
				Updated_Mail_Template=$MailMessage
				if [ $RecordID -gt 1 ]
					then
						Current=`echo $RecordLine | cut -d"|" -f$KPI_Field`
						if [ $KPI_Field2 -gt 0 ]
							then
								Current2=`echo $RecordLine | cut -d"|" -f$KPI_Field2`
						fi
						if [ $KPI_Field3 -gt 0 ]
							then
								Current3=`echo $RecordLine | cut -d"|" -f$KPI_Field3`
						fi
						#echo $Current $Current2 $Current3
						FieldCounter=1
						NumberOfFileds=`echo $RecordLine | awk -F"|" '{print NF}'`
						while [ $FieldCounter -le $NumberOfFileds ]
							do
							FieldName=`head -1 $Rand | cut -d"|" -f$FieldCounter`
							FieldValue=`echo $RecordLine | cut -d"|" -f$FieldCounter`
							UpdatedTemplate=$(echo $UpdatedTemplate | sed "s/\$$FieldName/$FieldValue/g")
							Updated_Mail_Template=$(echo $Updated_Mail_Template | sed "s/\$$FieldName/$FieldValue/g")
							#echo $Template 
							((FieldCounter+=1))
						done

						ksh Compairer.sh "$Current" "$KPI" "$Action" "$Service" "$Receipients" "$Criticality" "$Node_Name" "$UpdatedTemplate" "$NextUpdate" "$DB_Host" "$CurrentKPI_ColumnName" "$Updated_Mail_Template" "$Current_gt_KPI1" "$KPI2" "$Current2" "$Comp_Current_grt_KPI2" "$KPI3" "$Current3" "$Comp_Current_grt_KPI3" "$Current_2" "$Current_3" "$MailReceipients"
				fi
				((RecordID+=1))
		done < "$Rand"
else
		Current=$Current
		echo "Absolute Value"
		ksh Compairer.sh "$Current" "$KPI" "$Action" "$Service" "$Receipients" "$Criticality" "$Node_Name" "$Template" "$NextUpdate" "$DB_Host" "$CurrentKPI_ColumnName" "$MailMessage" "$Current_gt_KPI1" "$KPI2" "$Current2" "$Comp_Current_grt_KPI2" "$KPI3" "$Current3" "$Comp_Current_grt_KPI3" "$Current_2" "$Current_3" "$MailReceipients"
fi
rm $Rand
