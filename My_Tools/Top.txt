load averages:  2.07,  1.69,  1.43    11:15:07
171 processes: 160 sleeping, 3 running, 1 zombie, 6 stopped, 1 on cpu

Memory: 1024M real, 566M free, 302M swap in use, 2468M swap free


   PID USERNAME THR PRI NICE  SIZE   RES STATE    TIME    CPU COMMAND
  8221 peri      11  10    0 4304K 3656K run     17.9H 76.73% vsh
 28564 peri       5  58    0   13M 8344K sleep    0:02  0.31% peripro
  1729 peri       1  40    0 1648K 1184K sleep    0:00  0.25% csh
  2356 peri       1  49    0 2136K 1112K cpu      0:00  0.14% top
     1 root       1  58    0  848K  336K sleep    1:08  0.13% init
  2247 peri       1  38    2 2752K 2048K run      0:00  0.10% pr_sys_v5.plx
  2246 peri       1  42    2 1080K  776K sleep    0:00  0.10% sh
  1439 root       1  59  -20   12M 9776K sleep    2:16  0.07% mti
  1462 root       1  59  -20 8200K 6256K sleep    4:00  0.06% ccme_isup
 25269 peri       1  15    0 1080K  784K sleep    0:00  0.05% sh
  2354 peri       1  25    0  960K  616K sleep    0:00  0.05% sleep
  1499 root       1  59  -20 4296K 2920K sleep    4:05  0.04% mgmt
  2344 root       1  32    0  960K  616K sleep    0:00  0.04% sleep
   192 root      11  58    0 3560K 1944K sleep    0:56  0.04% syslogd
   199 root       1  60    0 1968K 1192K sleep    0:42  0.04% cron

