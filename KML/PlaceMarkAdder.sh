#Creates main parameters to create Site for KML folder

SiteName=$1
coordinates1=$2
coordinates2=$3

echo "<Placemark>"
echo "<name>$SiteName</name>"
echo "<styleUrl>#m_ylw-pushpin</styleUrl>"
echo "<Point>"
echo "<coordinates>$coordinates1,$coordinates2,0</coordinates>"
echo "</Point>"
echo "</Placemark>"
