
mysql -uReader -pReader -hCNPVAS03 -P3307 -e"SELECT CNPVAS_Password FROM Scheduler_Mail_Conf" Main_Conf | tail -1> Password.txt
Password=`cat Password.txt`
UserName='vf-eg\\CNPVAS'
#rm Password.txt

echo "Getting Current DownSites"
ftp -n EGOCT-PSD01 << !
        quote user $UserName
        quote pass $Password
        asc
        cd /KML
        prompt
        hash
        get SM_MOM_Downsites.csv
        quit
!
echo "Got DownSites"
echo "Processing DownSites"
cat SM_MOM_Downsites.csv | cut -d"," -f1 | awk 'BEGIN {FS = "|";OFS="|"}{if (length($1)==3) {print "0"$1} else {if (length($1)==2) {print "00"$1} else {if (length($1)==1) {print "0"$1} else {print $1}}}}' | sort > DownSites.txt

echo "Getting DownSite Coordinates"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 2.2 2.3 DownSites.txt Sites.txt > DownSites_Detailed.txt

echo "Building KML file"
#--------------------------------------------------------------------------------
cat Folder_Header.txt > DownSites.KML

for SitesRecord in `cat DownSites_Detailed.txt`
	do
		#echo $SitesRecord
		SiteName=`echo $SitesRecord | cut -d"," -f1`
		North=`echo $SitesRecord | cut -d"," -f2`
		East=`echo $SitesRecord | cut -d"," -f3`
		#echo $SiteName $North $East
		#echo "Processing down site:$SiteName"
		if [ "$North" = "" ]
			then
				echo "No Coordinates for $SiteName"
			else
				ksh PlaceMarkAdder.sh $SiteName $North $East >> DownSites.KML
		fi
done

cat Folder_Tail.txt >> DownSites.KML
#--------------------------------------------------------------------------------

ftp -n EGOCT-PSD01 << !
        quote user $UserName
        quote pass $Password
        asc
	cd /KML
        prompt
        hash
        mput DownSites.KML
        quit
!

