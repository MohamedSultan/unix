Path=$1
ServiceName=$Path
Service_Name=$2
Machine_Name=$3


cat $Path/properties.xml | grep -i 'entry' | grep -v 'SetMap' | grep -v 'WhereMap' | grep -v 'Quary_To' | grep -v 'WithDate_Where' | grep -v 'PrimaryKeys' | grep -v 'ServiceName' | sed 's/</ /' | sed 's/<\// /' | sed 's/entry>//' | sed 's/>/;/' | sed 's/"//' | sed 's/"//' | sed 's/entry//' | sed 's/key=//' | sed 's/^ //' | grep -v 'log.iDELIM' | grep -v 'log.iDELIM' | grep -v 'log.oDELIM' | grep -v 'Mapping From and To table Columns' > $Path/properties.xml.Processed 


#ToRemove=`awk -F";" '{if ($1==" ServiceName") print $2;}' $Path/properties.xml.Processed`

#cat $Path/properties.xml.Processed | sed 's/^ *//' | sed 's/ServiceName;//' | sed "s/$ToRemove//" > $Path/properties.xml.Temp.Processed

#mv $Path/properties.xml.Temp.Processed $Path/properties.xml.Processed

cat $Path/properties.xml.Processed | awk -F";" '{print $2}' | sed '/^$/d' | sed 's/ $//g' > $Path/Values
cat $Path/properties.xml.Processed | awk -F";" '{print $1}' | sed '/^$/d' > $Path/Columns_Name

tr '\n' ',' < $Path/Columns_Name > $Path/Temp 
mv $Path/Temp $Path/Columns_Name
tr '\n' '^' < $Path/Values > $Path/Temp 
mv $Path/Temp $Path/Values

Columns="ServiceName,ServicePath,HostName,"`cat $Path/Columns_Name`
Columns=`echo $Columns | awk '{print substr($0,1,length($0)-1)}'`

Values=$Service_Name"^"$ServiceName"^"$Machine_Name"^"`cat $Path/Values`
Values=`echo $Values | awk '{print substr($0,1,length($0)-1)}'`
echo $Values > $Path/Values_All

mv $Path/Values_All $Path/Values

mv $Path/Values /share/Values
#echo $Columns
#cat /log/Shared/Values
 
Command="LOAD DATA INFILE '/share/Values' REPLACE INTO TABLE COLLECTORS FIELDS TERMINATED BY '^' ($Columns)" 
mysql -uWriter -pWriter -hcnpvas01 -e"$Command" group_stats 


#rm /log/Shared/Values
rm $Path/properties.xml.Processed 
rm $Path/Columns_Name




