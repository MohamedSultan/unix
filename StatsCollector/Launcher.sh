
mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"UPDATE COLLECTORS SET Active=1 WHERE ServicePath='app.Collectors.Collection_App.Transactions'" Main_Conf
#mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"UPDATE COLLECTORS SET Active=1 WHERE ServicePath='app.Collectors.RamadanPromo2015.Gifts'" Main_Conf
#mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"UPDATE COLLECTORS SET Active=1 WHERE ServicePath='app.Collectors.RamadanPromo2015.Transactions'" Main_Conf

ServiceName=$1
Date=$2
CurrentDir=`pwd`

Rand=$RANDOM
FilePath="/share/StatsCollector/"$ServiceName"_"$Rand"_"$Date
if [ "$ServiceName" = "PromoCollector" ]
	then
		Command="Select ServicePath,Java,1,ServiceName from COLLECTORS where PromoCollector=1 and Active=1 and lcase(HostName)='"`hostname`"' order by Sequence INTO OUTFILE '$FilePath' FIELDS TERMINATED BY ';'"
	else
		Command="Select ServicePath,Java,Background,ServiceName from COLLECTORS where ServiceName='$ServiceName' and Active=1 and lcase(HostName)='"`hostname`"'  order by Sequence INTO OUTFILE '$FilePath' FIELDS TERMINATED BY ';'"
	fi

echo $Command

mysql -hCNPVAS03 -uReader -pReader -P3307 -e"$Command" Main_Conf
for ServiceDetails in `cat $FilePath`
	do
	Java=`echo $ServiceDetails| cut -d";" -f2`
	ServicePath=`echo $ServiceDetails| cut -d";" -f1`
	Background=`echo $ServiceDetails | cut -d";" -f3`
	ServiceName=`echo $ServiceDetails | cut -d";" -f4`
	if [ "$Java" = "1" ]
		then
			echo "Collecting Data for  $ServicePath on Date:$Date (java)"
		        if [ "$Background" = "0" ]
         		       then
                       			echo "Collecting java Data for  $ServicePath on Date:$Date"
                       			java -jar Stats_Collector.jar $ServicePath $Date
                		else
                       			echo "Collecting Data for  $ServicePath on Date:$Date (backgrounded)"
                       			java -jar Stats_Collector.jar $ServicePath $Date &
        		fi
		else
			echo "Collecting Data for  $ServicePath on Date:$Date (Script)"
			cd /app/Collectors/$ServiceName
			if [ "$Background" = "0" ]	
				then
					echo "Collecting java Data for  $ServicePath on Date:$Date"
					ksh $ServicePath $Date
				else
					echo "Collecting Data for  $ServicePath on Date:$Date (backgrounded)"
					ksh $ServicePath $Date &
			fi
			cd $CurrentDir
	fi
done

rm $FilePath
