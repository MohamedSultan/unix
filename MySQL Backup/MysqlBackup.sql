-- MySQL dump 10.13  Distrib 5.5.32, for solaris10 (sparc)
--
-- Host: 172.23.201.51    Database: References
-- ------------------------------------------------------
-- Server version	5.5.32-enterprise-commercial-advanced-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `References`
--

USE `References`;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `ServiceName Reformatter` BEFORE INSERT ON `JAVA_COLLECTORS` FOR EACH ROW BEGIN
    SET new.ServicePath=replace(new.ServicePath,'/','.');
    if (left(new.ServicePath,1)='.') then 
      SET new.ServicePath=substring(new.ServicePath,2,length(new.ServicePath)) ;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `UNIX_Scripts_before_insert` BEFORE INSERT ON `UNIX_Scripts` FOR EACH ROW BEGIN
	if right(new.`Script Path`,1)<>'/'
		then
			SET new.`Script Path`=concat(new.`Script Path`,'/');
	end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `UNIX_Scripts_before_update` BEFORE UPDATE ON `UNIX_Scripts` FOR EACH ROW BEGIN
	SET new.`Last Modification Date`=date_format(now(),'%Y-%m-%d %H-%i-%s');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'References'
--

--
-- Current Database: `Alarms`
--

USE `Alarms`;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Alarms`.`Receipients Checker (Insert)` BEFORE INSERT
    ON Alarms.Alarmer FOR EACH ROW
BEGIN
    if right(new.Receipients,1)<>';' and new.Receipients<>'' and new.Receipients is not null then
      SET new.Receipients=concat(new.Receipients,';');
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Alarms`.`Receipient Checker (Update)` BEFORE UPDATE
    ON Alarms.Alarmer FOR EACH ROW
BEGIN
    if right(new.Receipients,1)<>';' and new.Receipients<>'' and new.Receipients is not null then
      SET new.Receipients=concat(new.Receipients,';');
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `STORED_RECEPIENTS_before_insert` BEFORE INSERT ON `STORED_RECEPIENTS` FOR EACH ROW BEGIN
	set new.Email=concat(replace(trim(new.name),' ','.'),'@vodafone.com');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'Alarms'
--
/*!50003 DROP FUNCTION IF EXISTS `Receipient Converter Email` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Receipient Converter Email`(`Receipients` TEXT) RETURNS text CHARSET latin1
BEGIN
	DECLARE OccuranceCount int;
	DECLARE STEP int;
	DECLARE Receipients_All text;
	DECLARE Receipients_Ret text;
	declare Receipients_Name text;
	declare Receipients_EMail text;
	
	SET OccuranceCount=Main_Conf.Occurance_Count(Receipients,';')+1;
	SET Receipients_All='';
	
	SET STEP=0;
	WHILE STEP<OccuranceCount
		Do
		SET STEP=STEP+1;
		SET Receipients_Name=(select Main_Conf.SPLIT_STR(Receipients, ';', Step));
		if (Receipients_Name REGEXP '@') = 0 then
			set Receipients_EMail=(select GROUP_CONCAT(EMail SEPARATOR ';' ) FROM STORED_RECEPIENTS WHERE `Group`=Receipients_Name);
		else
			SET Receipients_EMail=Receipients_Name;
		end if;
		
		if Receipients_All='' then
			SET Receipients_All=Receipients_EMail;
		else
			SET Receipients_All=concat(Receipients_All,';',Receipients_EMail);
		end if;
#		select Step,Receipients_Name,Receipients_EMail,Receipients_All;
	end while;

#select Receipients_All;

# ----------Distincting MSISDNs---------------------
	SET OccuranceCount=Main_Conf.Occurance_Count(Receipients_All,';')+1;
	SET Receipients_Ret='';
	
	SET STEP=0;
	WHILE STEP<=OccuranceCount
		Do
		SET STEP=STEP+1;
		SET Receipients_EMail=(select Main_Conf.SPLIT_STR(Receipients_All, ';', Step));
		if Receipients_Ret='' then
			SET Receipients_Ret=Receipients_EMail;
		else
			if INSTR(Receipients_Ret,Receipients_EMail) = 0 and length(Receipients_EMail)>0 then
					SET Receipients_Ret=concat(Receipients_Ret,';',Receipients_EMail);
			end if;
		end if;
#		select Receipients_Name,Receipients_EMail,Receipients_All;
	end while;
		
	return Receipients_Ret;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Receipient Converter SMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Receipient Converter SMS`(`Receipients` teXT) RETURNS text CHARSET latin1
BEGIN
	DECLARE OccuranceCount int;
	DECLARE STEP int;
	DECLARE Receipients_All text;
	DECLARE Receipients_Ret text;
	declare Receipients_Name text;
	declare Receipients_Mobile text;
	
	SET OccuranceCount=Main_Conf.Occurance_Count(Receipients,';')+1;
	SET Receipients_All='';
	
	SET STEP=0;
	WHILE STEP<=OccuranceCount
		Do
		SET STEP=STEP+1;
		SET Receipients_Name=(select Main_Conf.SPLIT_STR(Receipients, ';', Step));
		if (Receipients_Name REGEXP '^[a-z]') > 0 then
			set Receipients_Mobile=(select GROUP_CONCAT(MobileNumber SEPARATOR ';' ) FROM STORED_RECEPIENTS WHERE `Group`=Receipients_Name);
		else
			SET Receipients_Mobile=Receipients_Name;
		end if;
		if Receipients_All='' then
			SET Receipients_All=Receipients_Mobile;
		else
			SET Receipients_All=concat(Receipients_All,';',Receipients_Mobile);
		end if;
#		select Receipients_Name,Receipients_Mobile,Receipients_All;
	end while;

# ----------Distincting MSISDNs---------------------
	SET OccuranceCount=Main_Conf.Occurance_Count(Receipients_All,';')+1;
	SET Receipients_Ret='';
	
	SET STEP=0;
	WHILE STEP<=OccuranceCount
		Do
		SET STEP=STEP+1;
		SET Receipients_Mobile=(select Main_Conf.SPLIT_STR(Receipients_All, ';', Step));
		if Receipients_Ret='' then
			SET Receipients_Ret=Receipients_Mobile;
		else
			if INSTR(Receipients_Ret,Receipients_Mobile) = 0 and length(Receipients_Mobile)>0 then
					SET Receipients_Ret=concat(Receipients_Ret,';',Receipients_Mobile);
			end if;
		end if;
#		select Receipients_Name,Receipients_Mobile,Receipients_All;
	end while;
		
	return Receipients_Ret;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `MailHandler`
--

USE `MailHandler`;

--
-- Dumping routines for database 'MailHandler'
--
/*!50003 DROP PROCEDURE IF EXISTS `Daily_Me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Daily_Me`(IN `iName` vaRCHAR(50), IN `ServiceName` vaRCHAR(70))
BEGIN
      DECLARE DataBaseName VARCHAR(70);
			DECLARE Query,Statement_TO VARCHAR(1000);
      
      Declare FirstName varchar(20);
			DECLARE PhoneNumber VARCHAR(20);
      DECLARE Email varchar(100);
			
		SET DataBaseName  = (SELECT DB_Name FROM Main_Conf.Services WHERE Report_Activated=1 and `Service Name` =ServiceName);
      
      SET QUERY=CONCAT('update Main_Conf.Service_Receipients set Daily_To=1,Daily_CC=0,Normal_To=0,Normal_CC=0,Exception=0 WHERE Name=\'',iName,'\' AND Service_DB=\'',DataBaseName,'\' AND Exception=0');
      SELECT QUERY;
      set @Query=QUERY;
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
      SET FirstName=SUBSTRING(iName, 1, LOCATE(' ', iName, 1)-1);
      select FirstName;
			SET PhoneNumber=(SELECT `Phone Number` from Main_Conf.`Receipients_MSISDN` where lcase(`Name`)=lcase(iName));
      
      select FirstName,PhoneNumber;
      
			CALL Queues.SEND_SMS('CNP VAS', ServiceName, concat('Dear ',FirstName,', Your Mail Has Been Migrated To Be Daily For ',ServiceName,' Service'), PhoneNumber);
			CALL Queues.SEND_SMS('CNP VAS', ServiceName, concat(iName,' Requested and Has Been Migrated To ',ServiceName,' Service Daily'), (select group_stats.PSD()));
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SEND_SMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SEND_SMS`(IN `Description` vaRCHAR(2000), IN `ServiceName` vaRCHAR(200))
BEGIN	
        CALL Queues.`Send_SMS`('VAS Quality', 'Mail2SMS',Description,(SELECT concat((SELECT group_concat(MSISDN SEPARATOR ';') x FROM Receipients WHERE `Service Name`=ServiceName ),';')));

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SEND_SMS_NEW` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SEND_SMS_NEW`(IN `Description` varCHAR(2000), IN `Service` VARCHAR(200), IN `MailSubject` VARCHAR(2000))
BEGIN	
        CALL Queues.`Send_SMS`('VAS Quality', 'Mail2SMS',Description,(SELECT concat((SELECT group_concat(MSISDN SEPARATOR ';') x FROM Receipients WHERE `Subject`=MailSubject ),';')));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Start_Me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Start_Me`(IN `Name` VARCHAR(50), IN `ServiceName` VARCHAR(70), IN `TO_CC` vaRCHAR(2))
BEGIN
	    DECLARE DataBaseName,Email   VARCHAR(70);
			DECLARE FirstName,LastName   VARCHAR(30);
			DECLARE Query,Statement_TO,Statement_CC,Statement   VARCHAR(100);
			DECLARE PhoneNumber VARCHAR(20);
			
			SET DataBaseName   = (SELECT DB_Name FROM Services WHERE `Service Name` =ServiceName);
			
			SET @Query=concat('DELETE FROM ',DataBaseName,'.Exception_Receipients_To',' WHERE Name=\'',Name,'\'');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			SET @Query=concat('DELETE FROM ',DataBaseName,'.Exception_Receipients_CC',' WHERE Name=\'',Name,'\'');
			PREPARE Statement_CC FROM @Query;
      EXECUTE  Statement_CC;
			
			set FirstName=Split(Name,1,' ');
			set LastName =Split(Name,2,' ');
			set Email    =concat(FirstName,'.',LastName,'@vodafone.com');

			if lcase(TO_CC) = 'to' then
				SET @Query=concat('INSERT INTO ',DataBaseName,'.Receipients_To(NAME,Email) VALUES(\'',Name,'\',\'',Email,'\')');
				PREPARE Statement FROM @Query;
				EXECUTE  Statement;
			else
				if lcase(To_CC) = 'cc' then
					SET @Query=concat('INSERT INTO ',DataBaseName,'.Receipients_CC(NAME,Email) VALUES(\'',Name,'\',\'',Email,'\')');
					PREPARE Statement FROM @Query;
					EXECUTE  Statement;
				end if;
			end if;
			
			SET PhoneNumber=(SELECT `Phone Number` from `All Reports Receipients` where To_Name=Name);
			CALL SEND_SMS('CNP VAS', ServiceName, concat('Dear, You Have Been Added To ',ServiceName,' Service'), PhoneNumber);
			CALL SEND_SMS('CNP VAS', ServiceName, concat(Name,' Requested and Has Been Added To ',ServiceName,' Service'), '20101843434;20195995054');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Stop_Me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Stop_Me`(IN `Receipient_Name` VARCHAR(50), IN `ServiceName` vaRCHAR(70))
BEGIN
      DECLARE DataBaseName_TO,DataBaseName_CC,DataBaseName   VARCHAR(70);
			DECLARE Query,Statement_TO,Statement_CC,Statement   VARCHAR(100);
			DECLARE PhoneNumber VARCHAR(20);
      DECLARE ReceipientName Varchar(20);
      
      if (Receipient_Name<>'CNP VAS') then
        SET ReceipientName= (select concat(substring(substring(Main_Conf.SPLIT_STR(Receipient_Name,' ',1),1,1),1,1),lcase(substring(Main_Conf.SPLIT_STR(Receipient_Name,' ',1),2,100))));
        
        SET DataBaseName   = concat('',(SELECT DB_Name FROM Main_Conf.Services WHERE Report_Activated=1 and `Service Name` =ServiceName),'');
        
        
        
        
        select DataBaseName,Receipient_Name,ServiceName;
        
         
        
        
        SET @Query=concat('replace into Main_Conf.Service_Receipients(Exception,Daily_To,Daily_CC,Normal_To,Normal_CC,Service_DB,Name) values (1,0,0,0,0,\'',DataBaseName,'\',\'',Receipient_Name,'\')');
        SELECT @Query;
        PREPARE Statement FROM @Query;
        EXECUTE  Statement;
        
        
        SET PhoneNumber=(SELECT `Phone Number` from Main_Conf.`Receipients_MSISDN` where `Name`=Receipient_Name);
        
        
        CALL Queues.SEND_SMS('CNP VAS', ServiceName, concat('Dear ',ReceipientName,', You Have Been Removed From ',ServiceName,' Service'), PhoneNumber);
        
        CALL Queues.SEND_SMS('CNP VAS', ServiceName, concat(Receipient_Name,' Requested and Has Been Removed From ',ServiceName,' Service'), (select group_stats.PSD()));
        
      end if;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `Queues`
--

USE `Queues`;

--
-- Dumping routines for database 'Queues'
--
/*!50003 DROP FUNCTION IF EXISTS `ServiceValidNow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `ServiceValidNow`(`StartHour` INT, `Every` INT, `Minute` INT) RETURNS int(11)
BEGIN
	DECLARE Valid int;
	SET Valid=0;
	
	while StartHour>=0
		do
			SET StartHour=(StartHour-Every);
	end while;
	SET StartHour=(StartHour+Every);
	
	while StartHour<=23
		do
			if StartHour=hour(now()) && Minute=minute(now()) then
				SET Valid=1;
			end if;
			Set StartHour=(StartHour+Every);
	end while;
	
	return Valid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular_Logger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular_Logger`()
BEGIN

      DECLARE Query_temp              VARCHAR (90000);

      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
      DECLARE ServiceName             VARCHAR(50);
      DECLARE Min2Add                 Integer;
   

      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Main_Conf.Services WHERE Report_Activated='1');
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',1) DB_Name FROM Main_Conf.Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
    	SET Frequency= ( SELECT  every   FROM Main_Conf.Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Start_H= ( SELECT Start_Hour   FROM Main_Conf.Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Minutes= ( SELECT  Mins    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
      SET ServiceName=( SELECT  `Service Name`    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
      SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
			
      SET Query_temp   = concat ('SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
			
      WHILE Counter <= NoOfRecords 
      DO		
				
				
				if Frequency != 1 then 
				
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				
				end if ;
				
				SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Main_Conf.Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Main_Conf.Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET H=( SELECT Start_Hour  FROM Main_Conf.Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET Minutes= ( SELECT  Mins    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
				Set Hour_arr = (SELECT Main_Conf.Hour_Frequency(H,Frequency));
				SET ServiceName=( SELECT  `Service Name`    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
        SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Main_Conf.Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
				Set Repeate = (24 / Frequency) ;
			
				IF Frequency != 24 then
						
						
						
						
						
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
									Set Repeate = Repeate - 1;
								End While;
						
					else
							Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				End if;
				Set Counter =Counter + 1;
	
      END WHILE;
			
      SELECT Query_temp;
      SET Query_temp=(SELECT REPLACE(Query_temp,'SELECT ','SELECT date_format(curdate(),\'%Y-%m-%d\') DATE, '));
      SELECT Query_temp;
      SET Query_temp=(CONCAT('SELECT @row := @row + 1 AS ID, Schedular.* FROM (',Query_temp,') Schedular, (SELECT @row := 0) r'));
      SELECT Query_temp;
      SET Query_temp=(CONCAT('INSERT IGNORE Schedular_Log(ID,Date,ServiceName,Min,Hours,Minutes_To_Add) select ID,DATE,ServiceName,Min,Hours,Minutes_To_Add from (',Query_temp,')X'));
      
      
      
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
   		EXECUTE  Statement_TO;
      
      

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Scheduler_Checker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Scheduler_Checker`()
BEGIN
  DECLARE No_Of_Records  integer;
  DECLARE Counter  integer;
  DECLARE Record_ID  integer;
  DECLARE Message  varchar(160);
  
  SET No_Of_Records=(select Count(*) from Schedular_Log_Extended
                          where date=(select date from Main_Conf.Today) and `Guard TIME`<=curtime() and `Mail Status` is null  and `Alert SMS Sent` is null);
                          
  
	SET Counter   = 1;
  
  WHILE Counter <= No_Of_Records 
      DO	
      SET Record_ID=(SELECT ID
                          from Schedular_Log_Extended
                          where date=(select date from Main_Conf.Today) and `Guard TIME`<=curtime() and `Mail Status` is null  and `Alert SMS Sent` is null
                          LIMIT 1);
      
      SET Message=(SELECT CONCAT(ServiceName,' Mail is not sent on ',Date,' @ ',Time) FROM Schedular_Log_Extended WHERE  ID=Record_ID and date=(select date from Main_Conf.Today) LIMIT 1);
      call Queues.Send_SMS('Schedular','Schedular Checker',Message,(select Main_Conf.PSD()));
      UPDATE Schedular_Log
        SET `Alert SMS Sent`=1 
        WHERE ID=Record_ID
        AND Date=(select date from Main_Conf.Today);
        
      SET Counter=Counter+1;
  END WHILE;      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Send_Mail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Send_Mail`(IN `ServiceIN` vaRCHAR(50), IN `Receipients` teXT, IN `SubjectIN` VARCHAR(100), IN `BodyIN` TEXT, IN `FromIN` VARCHAR(100))
BEGIN
	DECLARE NumberOfReceipients int;
	DECLARE STEP int;
	DECLARE SubReceipient text;
	
	#removing empty receipients
	if right(Receipients,1)=';' then
		SET Receipients=left(Receipients,length(Receipients)-1);
	end if;
	
	SET STEP=1;
	SET NumberOfReceipients=Main_Conf.Occurance_Count(Receipients,';')+1;
	if NumberOfReceipients>1 then
		WHILE STEP<=NumberOfReceipients
			do
				SET SubReceipient=Main_Conf.SPLIT_STR(Receipients, ';', STEP);
				insert into Queues.Send_Mail(Service,Receipients,Subject,Body,`From`)
					values(ServiceIN,SubReceipient,SubjectIN,BodyIN,FromIN);
				SET STEP=STEP+1;
		end WHILE;
	else
				insert into Queues.Send_Mail(Service,Receipients,Subject,Body,`From`)
					values(ServiceIN,Receipients,SubjectIN,BodyIN,FromIN);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Send_SMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Send_SMS`(IN `From_` vaRCHAR(25), IN `ServiceName` vaRCHAR(50), IN `Description` teXT, IN `Receipients` teXT)
BEGIN
	DECLARE NumberOfReceipients int;
	DECLARE STEP int;
	DECLARE SubReceipient VARCHAR(25);
	
	#removing empty receipients
	if right(Receipients,1)=';' then
		SET Receipients=left(Receipients,length(Receipients)-1);
	end if;
	
	SET STEP=1;
	SET NumberOfReceipients=Main_Conf.Occurance_Count(Receipients,';')+1;
	if NumberOfReceipients>1 then
		WHILE STEP<=NumberOfReceipients
			do
				SET SubReceipient=Main_Conf.SPLIT_STR(Receipients, ';', STEP);
				insert into Queues.`SP Queue`(ServiceName,Service,Param1,Param2,Param3,Param4,Param5,Param6,Param7)
					VALUES('Send_SMS',substring(ServiceName,1,50),'OTHER',From_,SubReceipient,Description,'0','2','0');
				SET STEP=STEP+1;
		end WHILE;
	else
		insert into Queues.`SP Queue`(ServiceName,Service,Param1,Param2,Param3,Param4,Param5,Param6,Param7)
			VALUES('Send_SMS',substring(ServiceName,1,50),'OTHER',From_,Receipients,Description,'0','2','0');
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Unused Schedular` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Unused Schedular`()
BEGIN
      DECLARE Query_temp              text;
      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
   
      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1' );
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':'),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d') );
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
			
      SET Query_temp   = concat ('SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');
      WHILE Counter <= NoOfRecords 
     DO		
		
				
				if Frequency != 1 then 
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm', '\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');		
			end if ;
				
			SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1'  and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				
			
		Set Repeate = (24 / Frequency) ;
			
			IF Frequency != 24 then
      
						IF Frequency = 1 then
								Set Repeate = 1;
								Set Start_H2 = '*';
                
								SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H2, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
                
						else
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
								Set Repeate = Repeate - 1;
								End While;
					end if;
				else
						Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
			End if;
									
			Set Counter =Counter + 1;
	
     END WHILE;
			
   
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
  		EXECUTE  Statement_TO;

  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `ServiceMeter`
--

USE `ServiceMeter`;

--
-- Dumping routines for database 'ServiceMeter'
--
/*!50003 DROP FUNCTION IF EXISTS `Combined_Status` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Combined_Status`(Status1 varchar(30),Status2 varchar(30),Status3 varchar(30)) RETURNS varchar(30) CHARSET latin1
BEGIN
	RETURN if
            (
            ((Status1 = 'Blue') OR (Status2 = 'Blue') or (Status3 = 'Blue')),
                'Blue',
                  if
                    (
                    ((Status1 = 'Red') OR (Status2 = 'Red') or (Status3 = 'Red')),
                      'Red',
                        if
                          (
                           ((Status1 = 'Yellow') OR (Status2 = 'Yellow') or (Status3 = 'Yellow')),
                              'Yellow',
                                'Green')));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Image` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Image`(ImageName varchar(50)) RETURNS blob
BEGIN
  DECLARE Image_DESC varchar(50);
	SET Image_DESC=(select Image_Map FROM IMAGES where Image_Name=ImageName);
	RETURN (select Image from PSD_Portal.ICONS where `Desc`=Image_DESC);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Status_Color` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Status_Color`(Niddle Double(10,3),Range1 Double(10,3),Range2 Double(10,3),Range3 Double(10,3),GYR int) RETURNS varchar(30) CHARSET latin1
BEGIN
	
RETURN 
(
if (Niddle is null,
  'Green',
    IF((`GYR` = 1),
      IF((`Niddle` >`Range1`),
        'Blue',
          IF((`Niddle` >=`Range2`),
            'Green',
              IF(((`Niddle` <`Range2`)AND (`Niddle` >=`Range3`)),
                'Yellow',
                  'Red'
              )
          )
      ),
        IF((`Niddle` >=`Range1`),
        'Red',
          IF(((`Niddle` <`Range1`)AND (`Niddle` >=`Range2`)),
            'Yellow',
              IF((`Niddle` <`Range3`),
                'Blue',
                  'Green'
              )
          )
        )
      )
    )
   );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Change Rate Plan KPI Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `Change Rate Plan KPI Updater`(DateIn varchar(25))
BEGIN
update CurrentKPIs
SET VALUE1=(select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='CR'),
    VALUE2=((select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='CR')-(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing CR')),
    VALUE3=(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing CR'),
    VALUE4=(SELECT Elapsed_Time FROM Tibco.`CRTO Requests Daily` WHERE Date=DateIn ),
    NiddleKPI=(((select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='CR')-(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing CR'))*100/(select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='CR')),
    NiddleKPI2=(SELECT Elapsed_Time FROM Tibco.`CRTO Requests Daily` WHERE Date=DateIn )
where ServiceName='Change Rate Plan';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ETopUp Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ETopUp Updater`(date VarChar(15),Hour_ Varchar(2))
BEGIN
  
	replace into CurrentKPIs(ServiceName,Value1,Value2,Value3,NiddleKPI,ThermoKPI)
  select 'EtopUP',Hour,sum(Success),sum(Failed),sum(Success)*100/(sum(Success)+sum(Failed)),sum(Failed)*100/(sum(Success)+sum(Failed)) from (
  SELECT Hour,sum(number_of_recharges) Success,0 Failed FROM `ETOPUP`.`Recharges_V` where date_=date and hour=Hour_
and Status='Success'
union all
SELECT Hour,0,sum(number_of_recharges) FROM `ETOPUP`.`Recharges_V` where date_=date and hour=Hour_
and Status<>'Success') X;

  select 'EtopUP',Hour,sum(Success),sum(Failed),sum(Success)*100/(sum(Success)+sum(Failed)),sum(Failed)*100/(sum(Success)+sum(Failed)) from (
  SELECT Hour,sum(number_of_recharges) Success,0 Failed FROM `ETOPUP`.`Recharges_V` where date_=date and hour=Hour_
and Status='Success'
union all
SELECT Hour,0,sum(number_of_recharges) FROM `ETOPUP`.`Recharges_V` where date_=date and hour=Hour_
and Status<>'Success') X;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Executive Summary` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `Executive Summary`()
BEGIN

SELECT Image(`Change Rate Plan`.Status) 'Change Rate Plan',
       Image(`Take Over`.Status) 'Take Over',
       Image(`PDV`.Status) 'PDV',
       Image(`New Activations`.Status) 'New Activations'
  FROM (SELECT Status
          FROM ServiceMeter
         WHERE ServiceName = 'Change Rate Plan') `Change Rate Plan`,
       (SELECT Status
          FROM ServiceMeter
         WHERE ServiceName = 'Take Over') `Take Over`,
       (SELECT Status
          FROM ServiceMeter
         WHERE ServiceName = 'PDV') `PDV`,
       (SELECT Status
          FROM ServiceMeter
         WHERE ServiceName = 'New Activations') `New Activations`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IT_KPI_Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `IT_KPI_Updater`(DateIN varchar(25))
BEGIN
	call `Change Rate Plan KPI Updater`(DateIN);
  call  NewActivation_KPI(DateIN);
  call PDV_Updater(DateIN);
  call `Take Over KPI Updater`(DateIN);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `NewActivation_KPI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `NewActivation_KPI`(DateIn varchar(25))
BEGIN
	UPDATE CurrentKPIs
   SET NiddleKPI =
          100*((SELECT Siebel.`Active New Activations`.Count
             FROM Siebel.`Active New Activations`
            WHERE Date = DateIn) - 
            (SELECT ifnull(count(*), 0)
                FROM Processings.`CoIDs of Sieble not in Billing - Yesterday`))/(SELECT Siebel.`Active New Activations`.Count
              FROM Siebel.`Active New Activations`
             WHERE Date = DateIn),
       Value1 =
          (SELECT Siebel.`Active New Activations`.Count
             FROM Siebel.`Active New Activations`
            WHERE Date = DateIn),
       Value2 =
          (SELECT Siebel.`Active New Activations`.Count
             FROM Siebel.`Active New Activations`
            WHERE Date = DateIn)
          - ((SELECT ifnull(count(*), 0)
                FROM Processings.`CoIDs of Sieble not in Billing - Yesterday`)),
       Value3 =
          ((SELECT ifnull(count(*), 0)
              FROM Processings.`CoIDs of Sieble not in Billing - Yesterday`))

















 WHERE ServiceName = 'New Activations';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `PDV_Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `PDV_Updater`(DateIN VARCHAR(20))
BEGIN
	UPDATE CurrentKPIs
   SET NiddleKPI =
          (100
           * (((SELECT ifnull(sum(Count), 0)
                  FROM Tibco.Header
                 WHERE ServiceName = 'SubmitPPValidationOffline'
                       AND Date = DateIN)
               + (SELECT ifnull(sum(Count), 0)
                    FROM Tibco.Header
                   WHERE ServiceName = 'PrepaidDataValidationUpdate'
                         AND Date = DateIN))
              - (SELECT count(*)
                   FROM Processings.Mismatches
                  WHERE Insert_Date = DateIN
                        AND Type = 'In Tibco Not In Billing'))
           / ((SELECT ifnull(sum(Count), 0)
                 FROM Tibco.Header
                WHERE ServiceName = 'SubmitPPValidationOffline'
                      AND Date = DateIN)
              + (SELECT ifnull(sum(Count), 0)
                   FROM Tibco.Header
                  WHERE ServiceName = 'PrepaidDataValidationUpdate'
                        AND Date = DateIN))),
       VALUE1 =
          ((SELECT ifnull(sum(Count), 0)
              FROM Tibco.Header
             WHERE ServiceName = 'SubmitPPValidationOffline'
                   AND Date = DateIN)
           + (SELECT ifnull(sum(Count), 0)
                FROM Tibco.Header
               WHERE ServiceName = 'PrepaidDataValidationUpdate'
                     AND Date = DateIN)),
       VALUE2 =
          (((SELECT ifnull(sum(Count), 0)
               FROM Tibco.Header
              WHERE ServiceName = 'SubmitPPValidationOffline'
                    AND Date = DateIN)
            + (SELECT ifnull(sum(Count), 0)
                 FROM Tibco.Header
                WHERE ServiceName = 'PrepaidDataValidationUpdate'
                      AND Date = DateIN))
           - (SELECT count(*)
                FROM Processings.Mismatches
               WHERE Insert_Date = DateIN
                     AND Type = 'In Tibco Not In Billing')),
       VALUE3 =
          (SELECT count(*)
             FROM Processings.Mismatches
            WHERE Insert_Date = DateIN AND Type = 'In Tibco Not In Billing'),
       NiddleKPI2 =
          (SELECT round(
                     ifnull(
                        `PDV Sum Of Average Elapsed Time`.`Tibco Sum Of Elapsed Time`,
                        0)
                     + ifnull(
                          `PDV Daily Average Elapsed Time`.`Billing Average Elapsed Time`,
                          0),
                     3)
                     AS `Billing & Tibco Total ET`
             FROM (   `Billing`.`PDV Daily Average Elapsed Time`
                   LEFT JOIN
                      `Tibco`.`PDV Sum Of Average Elapsed Time`
                   ON ((`PDV Sum Of Average Elapsed Time`.`date` =
                           `PDV Daily Average Elapsed Time`.`Date`)))
            WHERE `Billing`.`PDV Daily Average Elapsed Time`.date =
                     (SELECT date FROM group_stats.Yesterday))
 WHERE ServiceName = 'PDV';

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Salefny Borrow Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Salefny Borrow Updater`(date_IN VarChar(15),Hour_In Varchar(2))
BEGIN
    replace into CurrentKPIs(ServiceName,Value1,Value2,Value3,NiddleKPI,ThermoKPI)
    select 'SalefnyBorrow',
            Hour,
            sum(Sucess)+sum(Failed)+sum(Rejected),
            sum(Rejected),
            sum(Sucess)*100/(sum(Sucess)+sum(Failed)+sum(Rejected)),
            sum(Failed)*100/(sum(Sucess)+sum(Failed)+sum(Rejected)) from (
    SELECT Hour,date,sum(count) Sucess,0 Failed,0 Rejected
    from Salefny.Sub_Activation_Process
    where date=date_IN and status_code=5  and Hour=Hour_In
    group by date,hour

    union all 
    select Hour,date,0,sum(Elig_Failed)+sum(Activation_Failed)+sum(Queing),0 from (
    SELECT Hour,date,sum(count) Elig_Failed,0 Activation_Failed,0 Queing
    from Salefny.Sub_Eligability_Process
    where date=date_IN and status_code=4  and Hour=Hour_In
    group by date,Hour
    UNION ALL
    SELECT Hour,date,0,sum(count),0
    from Salefny.Sub_Activation_Process
    where date=date_IN and status_code=4  and Hour=Hour_In
    group by date,hour
    UNION ALL
    SELECT Hour,Date,0,0,sum(Count)
      FROM    Salefny.Sub_Eligability_Process
    WHERE date=date_IN AND Status_Code_Desc_ID = 4 and Hour=Hour_In
    group by Date,Hour) X
    group by date,hour

    union all
    SELECT Hour,date,0,0,sum(count) Elig_Rejection
    from Salefny.Sub_Eligability_Process
    where date=date_IN and status_code=3  and Hour=Hour_In
    group by date,hour) X
    group by Hour;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Salefny Payback Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Salefny Payback Updater`(date_IN VarChar(15),Hour_In Varchar(2))
BEGIN
    replace into CurrentKPIs(ServiceName,Value1,Value2,Value3,NiddleKPI,ThermoKPI)
    select 'SalefnyPayBack',
            Hour,
            sum(Elig_Rejected)+sum(Elig_Failed)+sum(Elig_Success)+sum(Deactivation_Rejected)+sum(Deactivation_Failed)+sum(Deactivation_Success) Total,
            sum(Elig_Rejected),
            sum(Deactivation_Success)*100/(sum(Elig_Rejected)+sum(Elig_Failed)+sum(Deactivation_Rejected)+sum(Deactivation_Failed)+sum(Deactivation_Success)),
            (sum(Elig_Failed)+sum(Deactivation_Failed))*100/(sum(Elig_Rejected)+sum(Elig_Failed)+sum(Deactivation_Rejected)+sum(Deactivation_Failed)+sum(Deactivation_Success)) 
    from (
    SELECT Hour_In `Hour`,
       ifnull(SUM(Count),0) AS Elig_Rejected,
       0 Elig_Failed,
       0 Elig_Success,
       0 Deactivation_Rejected,
       0 Deactivation_Failed,
       0 Deactivation_Success
from Salefny.Unsub_Eligability_Process where date=date_IN and Status_Code=3 and Hour=Hour_In 
union all
SELECT Hour_In,0,ifnull(SUM(Count),0),0,0,0,0 from Salefny.Unsub_Eligability_Process where date=date_IN and Status_Code=4 and Hour=Hour_In 
union all
SELECT Hour_In,0,0,ifnull(SUM(Count),0),0,0,0 from Salefny.Unsub_Eligability_Process where date=date_IN and Status_Code=5 and Hour=Hour_In
union all
SELECT Hour_In,0,0,0,ifnull(sum(Count),0),0,0 from Salefny.Unsub_Deactivation_Process where  date=date_IN and Status_Code=3 and Hour=Hour_In  
union all
SELECT Hour_In,0,0,0,0,ifnull(sum(Count),0),0 from Salefny.Unsub_Deactivation_Process where date=date_IN and Status_Code=4 and Hour=Hour_In   
union all
SELECT Hour_In,0,0,0,0,0,ifnull(sum(Count),0) from Salefny.Unsub_Deactivation_Process where date=date_IN and Status_Code=5 and Hour=Hour_In
) X;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SMS Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SMS Updater`(date VarChar(15),Hour_In Varchar(2))
BEGIN
	replace into CurrentKPIs(ServiceName,Value1,Value2,Value3,NiddleKPI,ThermoKPI)
  select 'SMS',Hour_In,sum(mo_success_SMS),sum(mo_timeout_SMS),sum(mo_success_SMS)*100/(sum(mo_success_SMS)+sum(mo_timeout_SMS)),sum(mo_timeout_SMS)*100/(sum(mo_success_SMS)+sum(mo_timeout_SMS)) 
  from SMS.Devices_Detailed
where Date_=date and Hour=Hour_In;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Take Over KPI Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `Take Over KPI Updater`(DateIn varchar(25))
BEGIN
	update CurrentKPIs
SET VALUE1=(select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='TO'),
    VALUE2=((select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='TO')-(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing TO')),
    VALUE3=(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing TO'),
    VALUE4=(SELECT Elapsed_Time FROM Tibco.`CRTO Requests Daily` WHERE Date=DateIn ),
    NiddleKPI=(((select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='TO')-(select count(*) FROM Processings.Mismatches where Insert_Date=DateIn and Type='In Tibco Not In Billing TO'))*100/(select sum(count) FROM Tibco.Header where Date=DateIn and ServiceName='TO')),
    NiddleKPI2=(SELECT Elapsed_Time FROM Tibco.`CRTO Requests Daily` WHERE Date=DateIn )
where ServiceName='Take Over';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `Main_Conf`
--

USE `Main_Conf`;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Mail` BEFORE INSERT ON `Receipients_Always_To` FOR EACH ROW BEGIN
    SET new.`To`=concat(replace(new.To_name,' ','.'),'@vodafone.com');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `group_stats`.`Recp Other Phone Number Checker Insert` BEFORE INSERT
    ON group_stats.Receipients_MSISDN FOR EACH ROW
BEGIN
    if substring(new.`Phone Number`,1,1)='1' then
			set new.`Phone Number`=concat('20',new.`Phone Number`);
		end if;
			
		if substring(new.`Phone Number`,1,1)='0' then
			set new.`Phone Number`=concat('2',new.`Phone Number`);
		end if;
    
    if new.`mail`='' or  new.`mail` is null then
      SET new.`mail`=(concat(REPLACE(new.Name,' ','.'),'@vodafone.com'));
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `group_stats`.`Recp Other Phone Number Checker Update` BEFORE UPDATE
    ON `Receipients_MSISDN` FOR EACH ROW
BEGIN
    if substring(new.`Phone Number`,1,1)='1' then
			set new.`Phone Number`=concat('20',new.`Phone Number`);
		end if;
			
		if substring(new.`Phone Number`,1,1)='0' then
			set new.`Phone Number`=concat('2',new.`Phone Number`);
		end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Service_Receipients_before_insert` BEFORE INSERT ON `Service_Receipients` FOR EACH ROW BEGIN
    if new.email='' then 
      SET new.`EMail`=concat(replace(new.Name,' ','.'),'@vodafone.com');
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `SingleQuote Removal Insert` BEFORE INSERT ON `Services` FOR EACH ROW BEGIN
    set new.`Desc` = REPLACE(new.`Desc`, ''', '');
    set new.`More Details` = REPLACE(new.`More Details`, ''', '');
    set new.DB_Name=trim(new.DB_Name);
END */;;
DELIMITER ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `SingleQuote Removal Update` BEFORE UPDATE ON `Services` FOR EACH ROW BEGIN
    set new.`Desc` = REPLACE(new.`Desc`, ''', '');
    set new.`More Details` = REPLACE(new.`More Details`, ''', '');
    set new.DB_Name=trim(new.DB_Name);
END */;;
DELIMITER ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `JavaCollector_Updater` AFTER UPDATE ON `Services` FOR EACH ROW BEGIN
    Update COLLECTORS
    Set Active=new.CollectData 
    Where ServiceName=new.DB_Name;
END */;;
DELIMITER ;

--
-- Dumping routines for database 'Main_Conf'
--
/*!50003 DROP FUNCTION IF EXISTS `Add_Receipients_F` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Add_Receipients_F`(Group_IN integer,New_DB_Name varchar(100)) RETURNS int(11)
BEGIN
	call Add_Receipients_P(Group_IN,New_DB_Name);
	RETURN '1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Char_Dublication_Remover` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Char_Dublication_Remover`(String varchar(100),To_Remove Varchar(1)) RETURNS varchar(100) CHARSET latin1
BEGIN
  DECLARE New_String  varchar(100);
  SET New_String=String;
	WHILE INSTR(New_String, concat(To_Remove,To_Remove))<>0
  DO
    SET New_String=replace(New_String,concat(To_Remove,To_Remove),To_Remove);
  END WHILE;
	RETURN New_String;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CronJob_ValidNow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `CronJob_ValidNow`(`Minute` vaRCHAR(50), `Hour` vaRCHAR(50), `DayOfWeek` vaRCHAR(50), `DayOfMonth` vaRCHAR(50), `MonthOfYear` VARCHAR(50)) RETURNS tinyint(4)
BEGIN
	DECLARE VALID INT;
	DECLARE ValidMinute Int;
	DECLARE ValidHour int;
	DECLARE ValidDayOfWeek int;
	DECLARE ValidDayOfMonth int;
	DECLARE ValidMonthOfYear Int;
	
	DECLARE CurrentMinute varchar(50);
	DECLARE CurrentHour varchar(50);
	DECLARE CurrentDayOfWeek varchar(50);
	DECLARE CurrentDayOfMonth varchar(50);
	DECLARE CurrentMonthOfYear varchar(50);
		

	SET CurrentMinute=date_format(now(),'%i')+0;
	SET ValidMinute=CronValidCheck(Minute,CurrentMinute);

	SET CurrentHour=date_format(now(),'%H')+0;
	SET ValidHour=CronValidCheck(Hour,CurrentHour);
	
	SET CurrentDayOfWeek=dayofweek(now())+0;
	SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);
	if ValidDayOfWeek != 1 then
		SET CurrentDayOfWeek=DAYNAME(now());
		SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);
	end if;
	if ValidDayOfWeek != 1 then
		SET CurrentDayOfWeek=left(DAYNAME(now()),3);
		SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);	
	end if;
	
	SET CurrentDayOfMonth=date_format(now(),'%d')+0;
	SET ValidDayOfMonth=CronValidCheck(DayOfMonth,CurrentDayOfMonth);
	
	SET CurrentMonthOfYear=date_format(now(),'%m')+0;
	SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);
	if ValidMonthOfYear != 1 then
		SET CurrentMonthOfYear=MonthNAME(now());
		SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);
	end if;
	if ValidMonthOfYear != 1 then
		SET CurrentMonthOfYear=left(MonthNAME(now()),3);
		SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);	
	end if;
	
	if ValidMinute=1 and ValidHour=1 and ValidDayOfWeek=1 and ValidDayOfMonth=1 and ValidMonthOfYear=1 then
		SET VALID=1;
	else
		SET VALID=0;
	end if;
	
	return VALID;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CronValidCheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `CronValidCheck`(`ParameterCheck` vaRCHAR(50), `ParameterCheckOn` VARCHAR(50)) RETURNS tinyint(4)
BEGIN
	DECLARE OccuranceCount int;
	DECLARE STEP int;
	DECLARE VALID int;
	SET VALID=0;
	
	SET OccuranceCount=Occurance_Count(ParameterCheck,',')+1;
	SET STEP=0;
	WHILE STEP<=OccuranceCount
		Do
		SET STEP=STEP+1;
		if ParameterCheck like '%*%' or SPLIT_STR(ParameterCheck,',',STEP)=ParameterCheckOn then
			SET VALID=1;
		end if;			
	end while;
	
	Return VALID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CurHour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `CurHour`() RETURNS varchar(2) CHARSET latin1
BEGIN
	RETURN date_format(now(),'%H');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `DataBaseName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `DataBaseName`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT DB_Name FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `EndDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `EndDate`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT `End Date` FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getnearestdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getnearestdate`(date1 VARCHAR (11)) RETURNS varchar(11) CHARSET latin1
BEGIN 
		 return (select Date
		 					from group_stats.temp_dates 
							where Date<date1 
							order by Date desc 
							limit 1);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Hour_Frequency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Hour_Frequency`(Start_Hour Int,Frequency Int) RETURNS varchar(100) CHARSET latin1
BEGIN
  Declare Loops integer;
	Declare Hours integer;
	Declare Hour_arr varchar(100);
  set Hour_arr = Start_Hour;
	set Hours = Start_Hour;
	Set Loops = (24/Frequency);
	if (Start_Hour < 24) and (Frequency < 24) and ( (24%Frequency) = 0 ) and (Frequency != 0)then
		set Hours=	Start_Hour + Frequency;
		while Loops > 1 do
		if  (Hours >= 24)   then
			  Set Hours =abs( 24 - Hours);
			  Set Hour_arr = concat ( Hour_arr ,':',Hours);
				Set Hours = Hours +Frequency;
		else
		   
		   Set Hour_arr = concat ( Hour_arr ,':',Hours); 
			 Set Hours = Hours +Frequency;
		end if;
		
			Set Loops = Loops -1;
		end while;
		return Hour_arr;
	    elseif (Start_Hour < 24) and (Frequency = 24) and ( (24%Frequency) = 0 ) and (Frequency != 0) then
	    Set Hour_arr = concat (Start_Hour);
		 	return Hour_arr;
		 else
	   
	  return 'Error';
	
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Last2Weeks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Last2Weeks`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 2 Week,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastHour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastHour`() RETURNS varchar(2) CHARSET latin1
BEGIN
	RETURN date_format(now()-interval 1 hour,'%H');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastMonth` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastMonth`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 Month,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastWeek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastWeek`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 Week,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LaunchDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LaunchDate`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT `Launch Date` FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `make_cursor_concat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `make_cursor_concat`(
    p_db_name       varchar(50),
    p_function_name varchar(50),
    p_table_name    varchar(1000),
    p_concat_column varchar(50)
) RETURNS int(11)
    MODIFIES SQL DATA
BEGIN
    DECLARE template text;
    SET template ='
    begin
        declare done boolean default false;
        declare first_loop boolean default true;
        declare myresult text;
        declare mycol text;
        declare get_col cursor for
            select distinct CONCAT_COLUMN from TABLE_NAME;
        declare continue handler for not found
            set done = true;

        set myresult = "";
        open get_col;
        myloop:
        loop
            fetch get_col into mycol;
            if (done) then
                leave myloop;
            end if;
            if (first_loop) then
                set first_loop = false;
            else
                set myresult = concat(myresult, "\n,");
            end if;
            set myresult = CONCAT(myresult, " ", myop,
            "( CASE WHEN CONCAT_COLUMN = ", 
                quote(mycol), 
                " THEN ", opcol, " ELSE NULL END) AS `",
                mycol, "`" );
        end loop;
        return myresult;
    end ';
    SET template = REPLACE(template, 'TABLE_NAME', p_table_name);
    SET template = REPLACE(template, 'CONCAT_COLUMN', p_concat_column);

    RETURN make_routine( 
        p_db_name, 
        p_function_name,
        'function',
        'myop varchar(10), opcol varchar(50)',
        'TEXT', 
        template);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `make_routine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `make_routine`(
    p_db        varchar(64),
    p_name      varchar(64),
    p_type      enum('procedure', 'function'),
    p_params    text,
    p_returns   varchar(30),
    p_body      text
) RETURNS tinyint(1)
    MODIFIES SQL DATA
BEGIN
    DECLARE routine_exists int;
    SET routine_exists = 
        (
         SELECT count(*) 
         FROM mysql.proc 
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type
         );
    IF (routine_exists > 0 ) THEN
        DELETE FROM mysql.proc
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type; 
    END IF;
    INSERT INTO mysql.proc 
        (db, name, specific_name, `type`, param_list, `returns`,  body, `definer`)
        VALUES (p_db, p_name, p_name, p_type, p_params, p_returns, p_body, user());
    RETURN 
        (
         SELECT COUNT(*) 
         FROM mysql.proc 
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type
         );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `NDC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `NDC`(MSISDN varchar(20)) RETURNS varchar(20) CHARSET latin1
BEGIN
  if left(MSISDN,3)='201' then
	  if length(MSISDN)<=11 then    
      if substring(MSISDN,1,4)='2010' then RETURN concat('20100',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2016' then RETURN concat('20106',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2019' then RETURN concat('20109',substring(MSISDN,5,11)); end if;
      
      if substring(MSISDN,1,4)='2011' then RETURN concat('20111',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2014' then RETURN concat('20114',substring(MSISDN,5,11)); end if;
      
      if substring(MSISDN,1,4)='2012' then RETURN concat('20122',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2017' then RETURN concat('20127',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2018' then RETURN concat('20128',substring(MSISDN,5,11)); end if;
    else                          
      if substring(MSISDN,1,5)='20151' then RETURN concat('20101',substring(MSISDN,6,12)); end if;
      if substring(MSISDN,1,5)='20152' then RETURN concat('20112',substring(MSISDN,6,12)); end if;
      if substring(MSISDN,1,5)='20150' then RETURN concat('20120',substring(MSISDN,6,12)); end if;
    end if;
  else
    if left(MSISDN,2)='01' then
      if length(MSISDN)<=10 then    
        if substring(MSISDN,1,3)='010' then RETURN concat('20100',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='016' then RETURN concat('20106',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='019' then RETURN concat('20109',substring(MSISDN,4,11)); end if;
       
        if substring(MSISDN,1,3)='011' then RETURN concat('20111',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='014' then RETURN concat('20114',substring(MSISDN,4,11)); end if;
        
        if substring(MSISDN,1,3)='012' then RETURN concat('20122',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='017' then RETURN concat('20127',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='018' then RETURN concat('20128',substring(MSISDN,4,11)); end if;
      else                          
        if substring(MSISDN,1,4)='0151' then RETURN concat('20101',substring(MSISDN,5,12)); end if;
        if substring(MSISDN,1,4)='0152' then RETURN concat('20112',substring(MSISDN,5,12)); end if;
        if substring(MSISDN,1,4)='0150' then RETURN concat('20120',substring(MSISDN,5,12)); end if;
      end if;
    end if;
  end if;
  return MSISDN;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `NewService_Rec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `NewService_Rec`() RETURNS varchar(200) CHARSET latin1
BEGIN
	RETURN (SELECT concat(GROUP_CONCAT(MSISDN SEPARATOR ';' ),';') from NewService_Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Occurance_Count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `Occurance_Count`(str varchar(200),Substr varchar(50) ) RETURNS int(11)
BEGIN	
	RETURN (SELECT (LENGTH(str) - LENGTH(REPLACE(str, substr, ''))) / LENGTH(substr));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Promo_Optin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Promo_Optin`(Service_Name VARCHAR (70), Date VARCHAR (20)
) RETURNS varchar(20) CHARSET latin1
begin
			declare MyResult varchar(20);
	  	 call group_stats.Service_New_Activation(Service_Name,Date,MyResult);
			 return MyResult;
			 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `PSD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `PSD`() RETURNS varchar(200) CHARSET latin1
BEGIN
	RETURN (SELECT concat(GROUP_CONCAT(MSISDN SEPARATOR ';' ),';') from PSD_Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `remove_non_alphanum_char_f` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `remove_non_alphanum_char_f`(prm_strInput varchar(255)) RETURNS varchar(255) CHARSET latin1
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE v_char VARCHAR(1);
  DECLARE v_parseStr VARCHAR(255) DEFAULT ' ';
 
WHILE (i <= LENGTH(prm_strInput) )  DO
 
  SET v_char = SUBSTR(prm_strInput,i,1); 
  
  IF v_char REGEXP  '^[A-Za-z0-9]+$' THEN  
    
        SET v_parseStr = CONCAT(v_parseStr,v_char);  

  END IF;
  SET i = i + 1;
END WHILE;
RETURN trim(v_parseStr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SDP_Name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SDP_Name`(MSISDN DECIMAL (20, 0), Data_Date VARCHAR (11)) RETURNS varchar(20) CHARSET latin1
BEGIN
      RETURN (SELECT SDP_Name
        FROM group_stats.msisdn_ranges Ranges
       WHERE (MSISDN >= Ranges.`Start`) AND (MSISDN <= Ranges.`End`) and Ranges.Data_Date=Data_Date);
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ServiceValidNow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `ServiceValidNow`(`StartHour` INT, `Every` INT, `Minute` INT) RETURNS int(11)
BEGIN
	DECLARE Valid int;
	SET Valid=0;
	
	while StartHour>=0
		do
			SET StartHour=(StartHour-Every);
	end while;
	SET StartHour=(StartHour+Every);
	
	while StartHour<=23
		do
			if StartHour=hour(now()) && Minute=minute(now()) then
				SET Valid=1;
			end if;
			Set StartHour=(StartHour+Every);
	end while;
	
	return Valid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Service_Description` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Service_Description`(ServiceName varchar(50)) RETURNS varchar(100) CHARSET latin1
BEGIN
	
	RETURN (SELECT concat(DB_Name,':',ServiceClass,':',`Launch Date`,':',ifnull(`End Date`,0)) Service_Description  FROM Services WHERE `Service Name` ='Hakawy Bokra' Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Service_Name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Service_Name`(Serviceid integer,Date_ varchar(15),Nodeid integer) RETURNS varchar(30) CHARSET latin1
BEGIN
      RETURN (SELECT Service_Name
        FROM Temp.Service_Mappings
       WHERE (Service_ID =Serviceid) AND (Retrieval_Date = Date_) and (Node_ID=Nodeid)) ;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SMS_Receipients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SMS_Receipients`(`Services` VARCHAR (30)) RETURNS varchar(500) CHARSET latin1
BEGIN
declare Service_Rcpt_Table varchar(30);
SET Service_Rcpt_Table = concat(Services ,Main_Conf.Send_SMS_Rcpt);

      RETURN ( select group_concat(Main_Conf.Name_Mobile.Mobile_Number SEPARATOR ';') from 
	 SMS_Receipients.Salefny
	 Left outer join 
	 Main_Conf.`Name_Mobile` 
	 on( SMS_Receipients.Salefny.Names=Main_Conf.Name_Mobile.Name) ) ;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Split Bad` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Split Bad`(`LongString` varchar(100), `CharID` Int, `delimiter` varchar(12)) RETURNS varchar(100) CHARSET latin1
BEGIN
	Declare Part varchar(100);
	Declare Delim varchar(12);
	
	SET LongString = replace(LongString,delimiter,',');
	
	if SUBSTRING(LongString,LENGTH(LongString))<>',' then
		SET LongString=CONCAT(LongString,',');
	end if;
	
	if CharID = 1 then SET Part = SUBSTRING_INDEX( LongString , ',', 1 );
		elseif CharID = 3 then SET Part =   SUBSTRING_INDEX(SUBSTRING_INDEX( LongString , ',', 2 ),',',-1);
		elseif CharID = 2 then SET Part =   SUBSTRING_INDEX(SUBSTRING_INDEX( LongString , ',', -2 ),',',1);
		elseif CharID = 4 then SET Part =   SUBSTRING_INDEX( LongString , ',', -1 );
	end if;
	return Part;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SPLIT_STR`(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
) RETURNS varchar(255) CHARSET latin1
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Today` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Today`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate(),'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Tomorrow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Tomorrow`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()+interval 1 day,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Yesterday` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Yesterday`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 day,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Receipients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Add_Receipients`(Group_IN integer,New_DB_Name varchar(100))
BEGIN
  DECLARE Query1 VARCHAR(50000);
  SET Query1 = (SELECT substring(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_To UNION ') SEPARATOR ''),1,length(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_To UNION ') SEPARATOR ''))-length('UNION')-1)  FROM (
                SELECT *
                FROM information_schema.`TABLES`
                WHERE TABLE_NAME='Receipients_To' and TABLE_schema not like '%.ssh%') `TABLES`,
                (
                SELECT *
                FROM Services
                WHERE `Group`=Group_IN ) Services
                WHERE Services.DB_Name=`TABLES`.table_schema AND month(`Launch Date`)<>month(curdate()) 
                ORDER BY Services.`Launch Date` DESC
                LIMIT 1);
      SET  Query1=concat('REPLACE INTO `',New_DB_Name,'`.Receipients_To(Email,Name) ',@Query);
      Select Query1;
      set @Query=Query1;
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
      select 'Part 1 finished';
        SET @Query = (SELECT substring(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_CC UNION ') SEPARATOR ''),1,length(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_CC UNION ') SEPARATOR ''))-length('UNION')-1)  FROM (
                SELECT *
                FROM information_schema.`TABLES`
                WHERE TABLE_NAME='Receipients_CC' and TABLE_schema not like '%.ssh%' ) `TABLES` ,
                (
                SELECT *
                FROM Services
                WHERE `Group`=Group_IN ) Services
                WHERE Services.DB_Name=`TABLES`.table_schema AND month(`Launch Date`)<>month(curdate())
                ORDER BY Services.`Launch Date` DESC
                LIMIT 1);
      SET  @Query=concat('REPLACE INTO `',New_DB_Name,'`.Receipients_CC(Email,Name) ',@Query);
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConvertReceipients_Tables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ConvertReceipients_Tables`(DB_ServiceName Varchar(50))
BEGIN
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_To','Normal_To');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_CC','Normal_CC');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_Daily_To','Daily_To');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_Daily_Cc','Daily_CC');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Exception_Receipients_To','Exception');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Exception_Receipients_CC','Exception');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Crontab_Command_Distributer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Crontab_Command_Distributer`(IN `CommandIN` texT)
BEGIN
	select SPLIT_STR(CommandIN,';',1),
				SPLIT_STR(CommandIN,';',2),
				SPLIT_STR(CommandIN,';',3),
				SPLIT_STR(CommandIN,';',4),
				SPLIT_STR(CommandIN,';',5),
				SPLIT_STR(CommandIN,';',6),
				SPLIT_STR(CommandIN,';',7),
				SPLIT_STR(CommandIN,';',8),
				SPLIT_STR(CommandIN,';',9);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `execute_query` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `execute_query`(
    p_query     text,
    p_var_name  varchar(50),
    p_comment   text
)
    MODIFIES SQL DATA
BEGIN
    SET @EXEC_QUERY = p_query;
    IF ( p_var_name IS NOT NULL) AND ( p_var_name != '') THEN
        SET @EXEC_QUERY = CONCAT('set @', p_var_name, ' = (', p_query, ')' );
    END IF;    
    IF ( @SHOW_QUERIES IS NOT NULL) THEN
        SELECT p_comment;
        SELECT @EXEC_QUERY AS query;
    END IF;
    PREPARE q FROM @EXEC_QUERY;
    EXECUTE q;
    DEALLOCATE prepare q;
    IF ( @PRESERVE_QUERY IS NULL) THEN
        SET @EXEC_QUERY = NULL;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Failed Functions Cleaner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `Failed Functions Cleaner`()
BEGIN
	DECLARE Functions_Count INTEGER;
	DECLARE Counter INTEGER;
	DECLARE FunctionName VARCHAR(70);
	
	DECLARE Query_temp text;
	
	drop table if exists group_stats.Sultan;
	
	CREATE TABLE `Sultan` (
		`Function_Name` VARCHAR(64) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
		`ID` INT(11) NOT NULL AUTO_INCREMENT,
		PRIMARY KEY (`ID`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=MyISAM;
	
	
	insert into group_stats.Sultan(Function_Name)
	select SPECIFIC_NAME from information_schema.ROUTINES
	where SPECIFIC_NAME like 'fn_%' and ROUTINE_SCHEMA='group_stats' and date_format(created,'%Y-%m-%d')<(select date from group_stats.Today);
	
	SET Counter=1;
	SET Functions_Count=(select count(*) from Sultan);
	select Functions_Count;
	
	WHILE Counter <= Functions_Count 
		DO		
			
			SET FunctionName=(Select Function_Name from Sultan WHERE ID =Counter);
			Select concat('Dropping Function:',FunctionName);
			SET Query_temp=(CONCAT('DROP FUNCTION ',FunctionName));
      
			set @Query=(Query_temp);
			select @Query;
			PREPARE Statement_To FROM @Query;
			EXECUTE  Statement_To;
			
			SET Counter=Counter+1;
	END WHILE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `frequency_reducer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `frequency_reducer`()
BEGIN


      UPDATE Services
         SET every=every*2, Freq_Updated=Freq_Updated+1
       WHERE every<=2 AND `Launch Date` < group_stats.LastWeek() and Freq_Updated=0 and Report_Activated=1;

      UPDATE Services
         SET every=4,Freq_Updated=Freq_Updated+1
      WHERE  every=2 AND `Launch Date` < group_stats.Last2Weeks() and Freq_Updated=1 and Report_Activated=1;
   
   

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_crosstab`(
        row_name        varchar(50),    
        col_name        varchar(50),    
        id_name         varchar(50),    
        col_from        varchar(300),   
        op              varchar(10),    
        op_col          varchar(50),    
        from_clause     varchar(1000),  
        where_clause    varchar(1000),  
        wanted_result   varchar(50)     
        )
    READS SQL DATA
BEGIN
    
    DECLARE crosstab_col_query varchar(5000);
    DECLARE crosstab_columns   varchar(5000);
    DECLARE crosstab_query     varchar(10000);
    DECLARE complete_op        varchar(50);
    DECLARE result_table       varchar(50);
    DECLARE engine_type        varchar(20);
    DECLARE table_type         varchar(20);
    DECLARE with_rollup        varchar(20) default '';
    SET @XTAB_FORMAT = 'show';
    SET @XTAB_ENGINE = 'MyISAM';
    SET @XTAB_QUERY = NULL;
    SET @XTAB_TABLE = NULL;

    IF (@WITH_ROLLUP IS NOT NULL) THEN
        SET with_rollup = ' WITH ROLLUP';
    END IF;

    IF (( wanted_result IS NULL ) OR ( wanted_result REGEXP '^\s*$' ))
       AND (@XTAB_FORMAT IS NOT NULL ) THEN
        SET wanted_result = LOWER(@XTAB_FORMAT);
    END IF;

    IF ( id_name IS NULL ) OR ( id_name REGEXP '^\s*$' ) THEN
        SET id_name = col_name;
    END IF;
    IF ( from_clause IS NULL ) THEN
            set from_clause = '';
    END IF;
    
    IF (col_from IS NULL) OR ( col_from REGEXP '^\s*$' ) THEN
        SET col_from = from_clause;
    END IF;

    IF (from_clause != '') AND ( from_clause NOT REGEXP "^\s*[Ff][Rr][Oo][Mm]" ) THEN
        SET from_clause = CONCAT("FROM ", from_clause);
    END IF;
    IF (col_from != '') AND ( col_from REGEXP "^\s*[Ff][Rr][Oo][Mm]" ) THEN
        SET col_from = TRIM( col_from );
        SET col_from = RIGHT( col_from , LENGTH(col_from) - 5  );
    END IF;
    IF ( where_clause IS NULL ) THEN
        SET where_clause = '';
    END IF;

    IF ( UPPER(op) = 'COUNT' ) THEN
        SET complete_op = "COUNT(*)";
    ELSE
        SET complete_op = CONCAT(op,"(", op_col, ")");
    END IF;

    CALL get_crosstab_columns(
            schema(), 
            col_from, 
            col_name, 
            op, 
            op_col, 
            crosstab_columns );

    SET crosstab_query = CONCAT(
            "SELECT ", 
            row_name, "\n,", 
            crosstab_columns, "\n, ",
            complete_op, " as total", " \n", 
            from_clause, " ", 
            where_clause,
            " \nGROUP BY ", 
            row_name);

    IF ( wanted_result LIKE 'table%' ) THEN
        SET result_table = SUBSTRING_INDEX(wanted_result, ':', -1);
        IF ( result_table IS NULL ) 
            OR ( result_table = 'table' )
            OR ( result_table = '' ) THEN
            SET result_table = CONCAT(SCHEMA(), ".tmp",FLOOR(RAND() * 100000 )); 
        END IF;
        SET wanted_result = 'table';
    END IF;
    
    IF ( @XTAB_TO_TABLE IS NOT NULL ) THEN
        SET result_table = @XTAB_TO_TABLE; 
    END IF;

    IF ( @XTAB_ENGINE IS NOT NULL ) THEN
        SET engine_type  = @XTAB_ENGINE;
        IF (engine_type NOT REGEXP '^\s*ENGINE\s*=') THEN
            SET engine_type= CONCAT('ENGINE=', engine_type);
        END IF;
    ELSE
        SET engine_type = '';
    END IF;

    IF ( @XTAB_TYPE IS NOT NULL ) THEN
        SET table_type  = @XTAB_TYPE;
    ELSE
        SET table_type = '';
    END IF;

    IF ( wanted_result IS NULL) OR ( wanted_result = '' ) THEN
        SET wanted_result = 'query';
    END IF;
    
    CASE
         WHEN wanted_result = 'table' THEN
            IF ( table_type = '') AND (LOWER(result_table) REGEXP 'tmp') THEN
                SET table_type = 'TEMPORARY';
            END IF;
            IF (engine_type = '') AND ( LOWER(result_table) REGEXP 'mem' ) THEN
                SET engine_type = 'ENGINE=HEAP';
            END IF;
            SET @XTAB_DROP = CONCAT("DROP TABLE IF EXISTS ", result_table);

            call execute_query(@XTAB_DROP, NULL, 'dropping result table'); 
            SET crosstab_query = CONCAT(
                    "CREATE ", 
                    table_type, 
                    " TABLE ", 
                    result_table, " ", 
                    engine_type, " ", 
                    crosstab_query
            );
            SET @XTAB_TABLE = result_table;
            SET @XTAB_QUERY = crosstab_query;
            call execute_query(
                CONCAT(@XTAB_QUERY, with_rollup), 
                NULL, 
                'crosstab to table' );
         WHEN wanted_result = 'query' THEN
            SET @XTAB_QUERY = crosstab_query;
            SET @XTAB_TABLE = NULL;
         WHEN wanted_result = 'show' THEN
            set @XTAB_TABLE = NULL;
            set @XTAB_QUERY = crosstab_query;
            call execute_query(CONCAT(
                @XTAB_QUERY, 
                with_rollup), 
                NULL, 
                'crosstab to screen');
        ELSE
            SET @XTAB_TABLE = NULL;
            SET @XTAB_QUERY = crosstab_query;
            SELECT "choices:" as 'syntax', '' as 'explanation'
            UNION 
            SELECT "table ", 'creates a temporary table and sets @XTAB_TABLE'
            UNION 
            SELECT "query",  'sets @XTAB_QUERY with the crosstab query'
            UNION
            SELECT "show", 'executes the query' ;
    END CASE;
    SET @XTAB_FORMAT = 'show';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab_columns` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `get_crosstab_columns`(
    p_db_name   varchar(50),
    p_from_clause varchar(1000),
    p_concat_column varchar(50),
    p_operation varchar(10),
    p_op_col    varchar(50),
    OUT column_list text
)
BEGIN
    DECLARE temp_function_name varchar(50);
    DECLARE mfresult int;
    SET temp_function_name = CONCAT(
            'fn_', 
            REPLACE(uuid(),'-',''));
    SET mfresult = make_cursor_concat( 
            p_db_name, 
            temp_function_name, 
            p_from_clause, 
            p_concat_column);
    SET @q = CONCAT( 
                'SELECT ', 
                temp_function_name, 
                '(', 
                quote(p_operation), 
                ',', 
                quote(p_op_col), 
                ')');
    call execute_query(
                @q, 
                'XCOLS', 
                'getting crosstab columns anonymously'); 
    DELETE 
    FROM mysql.proc 
    WHERE name = temp_function_name;
    SET column_list =  @XCOLS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab_simple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `get_crosstab_simple`(        
        row_name        varchar(50),    
        col_name        varchar(50),    
        op              varchar(10),    
        op_col          varchar(50),    
        from_clause     varchar(1000)   
)
    READS SQL DATA
BEGIN
    call get_crosstab(row_name, col_name, null, null, op, op_col, from_clause, null, null);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Mobile_Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Mobile_Updater`(Date_ varchar(20),Hour_ int)
BEGIN
  DECLARE Day_,Max_Time int;
  set Day_=day(Date_);
  
  truncate table SMS_To_Mobiles_Today;
	replace into SMS_To_Mobiles_Today(Count,A_MSISDN,B_MSISDN,Date,Hour)
            
            SELECT count(*) Counts,MO,MT,Date,hour
            FROM Processings.`MO SMS`
            WHERE     day = Day_ and Hour>=Hour_ AND Date = Date_ 
            GROUP BY MO,MT,Date,hour
            having count(*)>15; 
  SET Max_Time=(select max(Hour) from SMS_To_Mobiles_Today);
  
  delete from SMS_To_Mobiles_Today where Hour<>Max_Time;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Outlook_Checker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Outlook_Checker`()
BEGIN
  DECLARE No_Of_Records  integer;
  DECLARE Counter  integer;
  
  DECLARE Message  varchar(160);
  DECLARE Date_Time  varchar(30);
  
  SET No_Of_Records=(select Count(*) from OutlookChecker
                          WHERE `OutLook(Mail Received)`=0 and `Checker(Mail Sent)`=1 and `MessageIsSent`=0);
                          
  
  SELECT No_Of_Records;
	SET Counter   = 1;
  
  WHILE Counter <= No_Of_Records 
      DO		
      
       
        
      
      SET Date_Time=(SELECT Date_Hour_Min FROM OutlookChecker  where `OutLook(Mail Received)`=0 and `Checker(Mail Sent)`=1 and `MessageIsSent`=0 limit 1);
      SELECT Date_Time;
      SET Message=(SELECT CONCAT('On',replace(Date_Hour_Min,' ','@'),' outlook did not reply ') FROM OutlookChecker  where Date_Hour_Min=Date_Time);
      call Send_SMS('Checker','Outlook Checker',Message,'201001843434;201007021881;');
      UPDATE OutlookChecker
        SET MessageIsSent=1 
        WHERE Date_Hour_Min=Date_Time;
        
      SET Counter=Counter+1;
  END WHILE;      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Receipients_MSISDN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Receipients_MSISDN`()
BEGIN
	DECLARE Counter     INTEGER;
  DECLARE NoOfRecords INTEGER;
  DECLARE Statement   text;
  DECLARE DB_Name   varchar(200);
  DECLARE Statement_TO  text;

  select 'Truncating Receipients';
  TRUNCATE TABLE Processings.Receipients;

  select 'Processing To and CC Receipients';
  SET NoOfRecords   = (SELECT Count(distinct TABLE_SCHEMA) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC');
  SET Counter=1;
  
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
        SET DB_Name =(SELECT TABLE_SCHEMA FROM
        ( select TABLE_SCHEMA,@row := @row + 1 AS ID FROM (
              SELECT distinct TABLE_SCHEMA
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC'
              ) X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
        
            
        SET Statement=concat('REPLACE INTO Processings.Receipients(Name,Email,DB_Name) SELECT Name,Email,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_To union all SELECT Name,Email,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_CC');
          set @Query=Statement;
         
          PREPARE Statement_TO FROM @Query;
          EXECUTE  Statement_TO;
          
          SET Counter=Counter+1;
  END WHILE;
  
  
  select 'Processing Daily Receipients';
  SET NoOfRecords   = (SELECT Count(distinct TABLE_SCHEMA) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_Daily_Cc' or TABLE_NAME='Receipients_Daily_To');
  SET Counter=1;
  
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
        SET DB_Name =(SELECT TABLE_SCHEMA FROM
        ( select TABLE_SCHEMA,@row := @row + 1 AS ID FROM (
              SELECT distinct TABLE_SCHEMA
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_Daily_Cc' or TABLE_NAME='Receipients_Daily_To'
              ) X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
        
            
        SET Statement=concat('REPLACE INTO Processings.Receipients(Name,Email,DB_Name) SELECT CC_Name,CC,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_Daily_Cc union all SELECT To_Name,`To`,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_Daily_To');
          set @Query=Statement;
         
          PREPARE Statement_TO FROM @Query;
          EXECUTE  Statement_TO;
          
          SET Counter=Counter+1;
  END WHILE;
  
  select 'Updating Receipients';
  SET Statement='INSERT IGNORE INTO group_stats.Receipients_MSISDN(Mail,Name,`Phone Number`) select distinct Email, Name, `Phone Number` FROM Processings.`Receipients MSISDN`';
  set @Query=Statement;
  
  
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Receipients_MSISDN_old` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Receipients_MSISDN_old`()
BEGIN
	DECLARE Counter     INTEGER;
  DECLARE NoOfRecords INTEGER;
  DECLARE Statement   varchar(300000);
  DECLARE Statement_Part   varchar(200);

  SET NoOfRecords   = (SELECT Count(*) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC');
  SET Counter=1;
  SELECT NoOfRecords;
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
          SET Statement_Part=(
            SELECT Query FROM(
            select Query,@row := @row + 1 AS ID FROM (
              SELECT concat('SELECT Email,Name FROM ',table_schema,'.Receipients_To UNION') Query
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_To'
              union all
              SELECT concat('SELECT Email,Name FROM ',table_schema,'.Receipients_CC UNION')
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_CC') X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
            SELECT Statement_Part;
          SET Statement=(CONCAT(Statement,' ',ifnull(Statement_Part,'')));
          
          SET Counter=Counter+1;
  END WHILE;

  SELECT Statement;
  SET Statement=(SELECT substring(Statement,1,length(Statement)-length('UNION')-1));
  SET Statement=(CONCAT('SELECT Email,Name,`Phone Number` FROM (',Statement,') `All` left outer join (select CC,CC_Name,`Phone Number` from Receipients_Always_Cc Union select `To`,To_Name,`Phone Number` from Receipients_Always_To Union select `To`,To_Name,`Phone Number` from Receipients_Others) Added on (`All`.Name=Added.CC_Name)'));
  set @Query=Statement;
  SELECT Statement;
  
  
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Recipient Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Recipient Updater`()
BEGIN
	DECLARE STATEMENT varchar(3000); 
	SET group_concat_max_len = 3000;
	SET STATEMENT=(SELECT concat('select * from ',table_schema,'.',table_Name,' UNION ')
									from information_schema.TABLES
									where TABLE_NAME like '%receip%' and TABLE_TYPE='BASE TABLE' 
										and TABLE_NAME<>'Alarm_Receipients' 
										and TABLE_SCHEMA<>'group_stats');
	
	select STATEMENT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular_Logger_New` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular_Logger_New`()
BEGIN

      DECLARE Query_temp              VARCHAR (50000);

      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
      DECLARE ServiceName             VARCHAR(50);
      DECLARE Min2Add                 Integer;
   

      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1');
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1');
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1');
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
      SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
      SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
			
			
      SET Query_temp   = concat ('SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
			
      WHILE Counter <= NoOfRecords 
      DO		
				
				
				if Frequency != 1 then 
				
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				
				end if ;
				
				SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1');
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1');
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
        SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
			
				Set Repeate = (24 / Frequency) ;
			
				IF Frequency != 24 then
						
						
						
						
						
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
									Set Repeate = Repeate - 1;
								End While;
						
					else
							Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				End if;
				Set Counter =Counter + 1;
	
      END WHILE;
			
      
      SET Query_temp=(SELECT REPLACE(Query_temp,'SELECT ','SELECT date_format(curdate(),\'%Y-%m-%d\') DATE, '));
      SET Query_temp=(CONCAT('SELECT @row := @row + 1 AS ID, Schedular.* FROM (',Query_temp,') Schedular, (SELECT @row := 0) r'));
      SET Query_temp=(CONCAT('INSERT IGNORE Schedular_Log(ID,Date,ServiceName,Min,Hours,Minutes_To_Add) select ID,DATE,ServiceName,Min,Hours,Minutes_To_Add from (',Query_temp,')X'));
      select 'LOOP finished';
      select Query_temp into outfile '/database/MySQL_Data/Query_temp';
      
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
   		EXECUTE  Statement_TO;
      
      

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SEND_SMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SEND_SMS`(IN `From_` varchar(11), IN `ServiceName` varchar(50), IN `Description` varchar(2000), IN `Receipients` teXT)
BEGIN
/*
	#DECLARE DATE_ varchar(20);
#	DECLARE HOUR_ VARCHAR(2);
  DECLARE Description_Limitted VARCHAR(480);
  
  SET Description_Limitted=substring(Description,1,480);
	
	#SET DATE_=(SELECT date_format(curdate(),'%Y-%m-%d'));
	#SET HOUR_=(SELECT date_format(now(),'%H'));
	#insert into Main_Conf.Send_SMS(Date,Hour,From_,`Service`,Receipients,Description)
#					values(DATE_,HOUR_,From_,substring(ServiceName,1,50),Receipients,Description_Limitted);				

	insert into Main_Conf.Send_SMS(From_,`Service`,Receipients,Description)
					values(From_,substring(ServiceName,1,50),Receipients,Description_Limitted);				
*/
	CALL Queues.`Send_SMS`(From_, ServiceName, Description, Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Configuration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Configuration`(Data_Base_Name VARCHAR(50), 
                                                     Service_Name VARCHAR(70),
                                                     Report_Frequency_Mapping int(11),
                                                     Start_Hour int(3),Mins int(3),ServiceClass Double,
                                                     StartDate VARCHAR(11),End_Date varchar(11),
                                                     MachineName varchar(20),ServiceDesc varchar(3000),
                                                     OptInChannels	varchar(50),OptInFees	varchar(150),
                                                     CommunicationChannel	varchar(50),Group_	int(11),
                                                     MoreDetails	varchar(3000),SystemServiceID1	int(11),SystemServiceID2	int(11),SystemServiceID3	int(11),SystemServiceID4	int(11),SystemServiceID5	int(11),SystemServiceID6	int(11),SystemServiceID7	int(11),SystemServiceID8	int(11),SystemServiceID9	int(11),SystemServiceID0	int(11))
BEGIN
			
      SET @Query=concat('create database IF NOT EXISTS ',Data_Base_Name);
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Exception_Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
					
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Exception_Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;

			
			SET @Query=concat('create view ',Data_Base_Name,'.Receipients_Always as SELECT group_stats.Receipients_Always.To  `To`,group_stats.Receipients_Always.To_Name AS To_Name,group_stats.Receipients_Always.CC AS CC,group_stats.Receipients_Always.CC_Name AS CC_Name FROM group_stats.Receipients_Always');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		
			if End_Date = '' then
				set End_Date=(select date_format(curdate()+interval 10 year, '%Y-%m-%d'));
			end if;
			
			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`,MachineName,`Opt In Channels`,`Opt In Fees`,`Communication Channel`,`Group`,`More Details`,SystemServiceID1,SystemServiceID2,SystemServiceID3,SystemServiceID4,SystemServiceID5,SystemServiceID6,SystemServiceID7,SystemServiceID8,SystemServiceID9,SystemServiceID0,`Desc`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',	End_Date ,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,',',MachineName,',',OptInChannels,',',OptInFees,',',CommunicationChannel,',',Group_,',',MoreDetails,',',SystemServiceID1,',',SystemServiceID2,',',SystemServiceID3,',',SystemServiceID4,',',SystemServiceID5,',',SystemServiceID6,',',SystemServiceID7,',',SystemServiceID8,',',SystemServiceID9,',',SystemServiceID0,',',ServiceDesc,')');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('Create view  ',Data_Base_Name,'.Service_Details As  select `Service Name`,every as \'Every(Hour)\' from group_stats.Services where DB_Name=\'',Data_Base_Name,'\'');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('create view ',Data_Base_Name,'.`Service Details`  AS select ',Data_Base_Name,'.`Service_Details`.`Service Name` AS `Service Name`,`group_stats`.`Report_Frequency_Mapping`.`Description` AS `Description`,',Data_Base_Name,'.`Service_Details`.`Every(Hour)` AS `Every(Hour)` from ','(',Data_Base_Name,'.`Service_Details` left join `group_stats`.`Report_Frequency_Mapping` on','((',Data_Base_Name,'.`Service_Details`.`Every(Hour)` = `group_stats`.`Report_Frequency_Mapping`.`Every(Hour)`)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			
			SET @Query=concat('CREATE TABLE  IF NOT EXISTS  ',Data_Base_Name,'.Transactions ( Date varchar(11) NOT NULL, Type_ID int(11) NOT NULL ,Error_Code int(11) NOT NULL ,Other_Error varchar(20) NOT NULL ,Count bigint(20) ,Client_ID int(11) NOT NULL , Service_Id int(11) NOT NULL , PRIMARY KEY (Date,Type_ID,Error_Code,Other_Error,Client_ID,Service_Id)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE TABLE  IF NOT EXISTS ',Data_Base_Name,'.Subscriber_Counts (Client_ID int(11) NOT NULL,Type_ID int(11) NOT NULL, Package_ID varchar(11) NOT NULL, Count decimal(10,0) NOT NULL , Status int(4) NOT NULL ,  PRIMARY KEY (Client_ID,Type_ID,Package_ID,Status)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
 			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE TABLE  IF NOT EXISTS  group_stats.client_',MachineName,' (CLIENT_ID int(11) DEFAULT NULL,CLIENT_NAME varchar(50) DEFAULT NULL,MachineName varchar(20) DEFAULT NULL,Temp int(11) DEFAULT NULL)');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
      if MachineName='FMLYPRD1' then
        
				SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.`Subscriber Count` AS select ifnull(`client`.`CLIENT_NAME`,\'Unknown\') AS `CLIENT_NAME`,`group_stats`.`Subscribers_Status_FMLYPRD1`.`STATUS_DESC` AS `STATUS_DESC`,`group_stats`.`packages`.`Package_NAME` AS `Package_NAME`,',Data_Base_Name,'.`Subscriber_Counts`.`Service_id` AS `Service_id`,',Data_Base_Name,'.`Subscriber_Counts`.`Count` AS `Count` from (((',Data_Base_Name,'.`Subscriber_Counts` left join `group_stats`.`Subscribers_Status_FMLYPRD1` on((`group_stats`.`Subscribers_Status_FMLYPRD1`.`SUBSCRIBER_STATUS_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Status`))) left join `group_stats`.`packages` on((`group_stats`.`packages`.`Package_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Package_ID`))) left join `group_stats`.`client_FMLYPRD1` `client` on((`client`.`CLIENT_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Client_ID`)))');
      end if;
      
      if MachineName='PROMODB' then
        
				SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.`Subscriber Count` AS select ifnull(`client`.`CLIENT_NAME`,\'Unknown\') AS `CLIENT_NAME`,`group_stats`.`packages`.`Package_NAME` AS `Package_NAME`,`group_stats`.`transaction_type`.`TYPE_NAME` AS `TYPE_NAME`,`Subscriber_Counts_1`.`Count` AS `Count`,`group_stats`.`error_codes`.`ERROR_DESC` AS `ERROR_DESC`,`group_stats`.`error_codes`.`Error_Type` AS `Error_Type` from ((((',Data_Base_Name,'.`Subscriber_Counts` `Subscriber_Counts_1` left join `group_stats`.`client_PROMODB` `client` on((`client`.`CLIENT_ID` = `Subscriber_Counts_1`.`Client_ID`))) left join `group_stats`.`transaction_type` on((`group_stats`.`transaction_type`.`Type_ID` = `Subscriber_Counts_1`.`Type_ID`))) left join `group_stats`.`packages` on((`group_stats`.`packages`.`Package_ID` = `Subscriber_Counts_1`.`Package_ID`))) left join `group_stats`.`error_codes` on((`group_stats`.`error_codes`.`ERROR_CODE` = `Subscriber_Counts_1`.`Status`)))');
      end if;
			                                                                          
		  
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Transaction_View AS select ',Data_Base_Name,'.Transactions.Date AS Date,',Data_Base_Name,'.Transactions.Other_Error AS Other_Error,',Data_Base_Name,'.Transactions.Count AS Count,group_stats.error_codes.ERROR_DESC AS ERROR_DESC,group_stats.error_codes.Error_Type AS Error_Type,group_stats.transaction_type.TYPE_NAME AS TYPE_NAME from ((group_stats.error_codes join ',Data_Base_Name,'.Transactions on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Transactions.Error_Code))) join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Transactions.Type_ID)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.Opt_out AS select Date,sum(Count) Opt_out,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 2 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.Opt_in AS select Date,sum(Count) Opt_in,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 1 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
			
			
	
			
			

		
			END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Configuration_Old` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Configuration_Old`(Data_Base_Name VARCHAR(50), Service_Name VARCHAR(70),Report_Frequency_Mapping int(11),Start_Hour int(3),Mins int(3),ServiceClass Double,StartDate VARCHAR(11),End_Date varchar(11))
BEGIN
			
      SET @Query=concat('create database ',Data_Base_Name);
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Exception_Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
					
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Exception_Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;

			
			SET @Query=concat('create view ',Data_Base_Name,'.Receipients_Always as SELECT group_stats.Receipients_Always.To  `To`,group_stats.Receipients_Always.To_Name AS To_Name,group_stats.Receipients_Always.CC AS CC,group_stats.Receipients_Always.CC_Name AS CC_Name FROM group_stats.Receipients_Always');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		
			if End_Date = '' then
			set End_Date=(select date_format(curdate()+interval 10 year, '%Y-%m-%d'));
			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',	End_Date ,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,')');
		
			else

			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',End_Date,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,')');
					
			
		
			end if;
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('Create view  ',Data_Base_Name,'.Service_Details As  select `Service Name`,every as \'Every(Hour)\' from group_stats.Services where DB_Name=\'',Data_Base_Name,'\'');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('create view ',Data_Base_Name,'.`Service Details`  AS select ',Data_Base_Name,'.`Service_Details`.`Service Name` AS `Service Name`,`group_stats`.`Report_Frequency_Mapping`.`Description` AS `Description`,',Data_Base_Name,'.`Service_Details`.`Every(Hour)` AS `Every(Hour)` from ','(',Data_Base_Name,'.`Service_Details` left join `group_stats`.`Report_Frequency_Mapping` on','((',Data_Base_Name,'.`Service_Details`.`Every(Hour)` = `group_stats`.`Report_Frequency_Mapping`.`Every(Hour)`)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			
			SET @Query=concat('CREATE TABLE  ',Data_Base_Name,'.Transactions ( Date varchar(11) NOT NULL, Type_ID int(11) NOT NULL ,Error_Code int(11) NOT NULL ,Other_Error varchar(20) NOT NULL ,Count bigint(20) ,Client_ID int(11) NOT NULL , Service_Id int(11) NOT NULL , PRIMARY KEY (Date,Type_ID,Error_Code,Other_Error,Client_ID,Service_Id)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE TABLE ',Data_Base_Name,'.Subscriber_Counts (Client_ID int(11) NOT NULL,Type_ID int(11) NOT NULL, Package_ID varchar(11) NOT NULL, Count decimal(10,0) NOT NULL , Status int(4) NOT NULL ,  PRIMARY KEY (Client_ID,Type_ID,Package_ID,Status)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
 			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.`Subscriber Count` AS select group_stats.error_codes.Error_Type AS Error_Type,ifnull(group_stats.client.CLIENT_NAME,\'Unknown\') AS CLIENT_NAME,ifnull(group_stats.packages.Package_NAME,\'Unknown\') AS Package_NAME,ifnull(group_stats.packages.Pkg_desc,\'Unknown\') AS Pkg_desc,ifnull(group_stats.transaction_type.TYPE_NAME,\'Unknown\') AS TYPE_NAME,',Data_Base_Name,'.Subscriber_Counts.Count AS Count from ((((',Data_Base_Name,'.Subscriber_Counts left join group_stats.packages on((group_stats.packages.Package_ID = ',Data_Base_Name,'.Subscriber_Counts.Package_ID))) left join group_stats.client on((group_stats.client.CLIENT_ID = ',Data_Base_Name,'.Subscriber_Counts.Client_ID))) left join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Subscriber_Counts.Type_ID))) join group_stats.error_codes on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Subscriber_Counts.Status)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Transaction_View AS select ',Data_Base_Name,'.Transactions.Date AS Date,',Data_Base_Name,'.Transactions.Other_Error AS Other_Error,',Data_Base_Name,'.Transactions.Count AS Count,group_stats.error_codes.ERROR_DESC AS ERROR_DESC,group_stats.error_codes.Error_Type AS Error_Type,group_stats.transaction_type.TYPE_NAME AS TYPE_NAME from ((group_stats.error_codes join ',Data_Base_Name,'.Transactions on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Transactions.Error_Code))) join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Transactions.Type_ID)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Opt_out AS select Date,sum(Count) Opt_out,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 2 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Opt_in AS select Date,sum(Count) Opt_in,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 1 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
			
			
	
			
			

		
			END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Details`(ServiceName VARCHAR(50))
BEGIN
	DECLARE Query,Statement VARCHAR(100);
	DECLARE DataBaseName   VARCHAR(50);
	SET DataBaseName   = (SELECT DB_Name FROM Services WHERE `Service Name` =ServiceName);
	
	SET @Query=concat('SELECT `Service Name`,`Every(Hour)` FROM group_stats,',DataBaseName,'.`Service_Details`');
	
	select @Query;
			
      
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Opt_in` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Opt_in`()
BEGIN
	 	 DECLARE DataBaseNames   VARCHAR(10000);
		 Declare Query_concatenated VARCHAR(10000);
		 DECLARE Query_temp varchar(5000);
		
		 Declare Launch_Date,End_Date varchar(20);
		 Declare NoOfRecords Integer;
		 Declare Counter Integer;
		 Declare DataBaseName VARCHAR(10000);
		 Declare TableName Varchar(5000);
		 Declare CheckFile double;
		 set CheckFile = 0;
		 set Counter = 1;
	   set DataBaseNames = (select GROUP_CONCAT(DB_Name separator ':') DB_Name from Services);
		 set NoOfRecords = (select Count(DB_Name) No_Of_DB from Services);
		 Set DataBaseName = ( select substring_index(GROUP_CONCAT(DB_Name separator ':' ),':',1) DB_Name from Services);
		 set CheckFile = (select count(*) Check_File From INFORMATION_SCHEMA.TABLES WHERE TABLE_Name='Opt_in' and TABLE_SCHEMA=DataBaseName) ;
		 IF CheckFile = 1 Then
		 Set Launch_Date = ( select `Launch Date` from group_stats.Services where DB_Name=DataBaseName);
		 Set End_Date = ( select ifnull(`End Date`,0) from group_stats.Services where DB_Name=DataBaseName);
		 if End_Date = 0 then 
		  Set  Query_Concatenated= concat('select * from ',DataBaseName,'.Opt_in where Date >=\'',Launch_Date,'\'');
		
		 else
		 Set  Query_Concatenated= concat('select * from ',DataBaseName,'.Opt_in where Date >=\'',Launch_Date,'\' and Date<=\'',End_Date,'\'');
		 end if;
		 End If;
		 While Counter < NoOfRecords Do
		  set Counter = Counter +1 ;
			
			Set DataBaseName = ( select substring_index(substring_index(GROUP_CONCAT(DB_Name separator ':' ),':',Counter),':',-1) DB_Name from group_stats.Services);
		  
			set CheckFile = (select count(*) Check_File From INFORMATION_SCHEMA.TABLES WHERE TABLE_Name='Opt_in' and TABLE_SCHEMA=DataBaseName) ;
			IF CheckFile = 1 Then
			Set Launch_Date = ( select `Launch Date` from group_stats.Services where DB_Name=DataBaseName);
			Set End_Date = ( select ifnull(`End Date`,0) from group_stats.Services where DB_Name=DataBaseName);
			if End_Date = 0 then 
			Set  Query_temp= concat('select * from ',DataBaseName,'.Opt_in where Date >=\'',Launch_Date,'\'');
			
			else
			Set Query_temp = concat('select * from ',DataBaseName,'.Opt_in where Date >=\'',Launch_Date,'\' and Date<=\'',End_Date,'\'');
			 end if;
      set Query_Concatenated = concat ( Query_Concatenated , ' Union ' ,Query_temp);
			
			END IF;
			set CheckFile =0 ;
			END WHILE;	
	
			SET @Query   = Query_Concatenated ; 
			
  	  PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Opt_in2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Opt_in2`(ServiceName  VARCHAR(50),Date_From  VARCHAR(15), Date_To  VARCHAR(15))
BEGIN
		DECLARE DataBaseName   VARCHAR(50);
		DECLARE Statement_TO VARCHAR(200);
		DECLARE Query_temp varchar(200);
		
		if Date_From = 'XXX' then 
		  Set  Date_From= '2000-01-01';
		end if;
		
		if Date_To = 'XXX' then 
		  Set  Date_To= '3000-01-01';
		end if;
		
		set DataBaseName = (select DB_Name from Services where `Service Name`=ServiceName);
		
		Set  Query_temp= concat('SELECT *,concat(unix_timestamp(Date),\'000\') Date_UNIX_Timestamp from `',DataBaseName,'`.Opt_in WHERE Date BETWEEN \'',Date_From,'\' and \'',Date_To,'\'');
		
		
		
		SET @Query   = Query_temp ; 
		
		PREPARE Statement_TO FROM @Query;
		EXECUTE  Statement_TO;
		
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Opt_in_Test` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Opt_in_Test`(ServiceName  VARCHAR(50),Date_From  VARCHAR(15), Date_To  VARCHAR(15))
BEGIN
		DECLARE DataBaseName   VARCHAR(50);
		DECLARE Statement_TO VARCHAR(200);
		DECLARE Query_temp varchar(200);
		
		if Date_From = 'XXX' then 
		  Set  Date_From= '2000-01-01';
		end if;
		
		if Date_To = 'XXX' then 
		  Set  Date_From= '3000-01-01';
		end if;
		
		set DataBaseName = (select DB_Name from Services2 where `Service Name`=ServiceName);
		
		Set  Query_temp= concat('SELECT * from ',DataBaseName,'.Opt_in WHERE Date BETWEEN \'',Date_From,'\' and \'',Date_To,'\'');
		
		SET @Query   = Query_temp ; 
		
		PREPARE Statement_TO FROM @Query;
		EXECUTE  Statement_TO;
		
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Opt_out` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Opt_out`()
BEGIN
	 	 DECLARE DataBaseNames   VARCHAR(1000);
		 Declare Query_concatenated VARCHAR(1000);
		 dECLARE Query_temp varchar(100);
		
		 Declare Launch_Date,End_Date varchar(20);
		 Declare NoOfRecords Integer;
		 Declare Counter Integer;
		 Declare DataBaseName VARCHAR(200);
		 Declare TableName Varchar(500);
		 Declare CheckFile double;
		 set CheckFile = 0;
		 set Counter = 1;
	   set DataBaseNames = (select GROUP_CONCAT(DB_Name separator ':') DB_Name from Services);
		 set NoOfRecords = (select Count(DB_Name) No_Of_DB from Services);
		 Set DataBaseName = ( select substring_index(GROUP_CONCAT(DB_Name separator ':' ),':',1) DB_Name from Services);
		 set CheckFile = (select count(*) Check_File From INFORMATION_SCHEMA.TABLES WHERE TABLE_Name='Opt_out' and TABLE_SCHEMA=DataBaseName) ;
		 IF CheckFile = 1 Then
		 Set Launch_Date = ( select `Launch Date` from group_stats.Services where DB_Name=DataBaseName);
		 Set End_Date = ( select ifnull(`End Date`,0) from group_stats.Services where DB_Name=DataBaseName);
		 if End_Date = 0 then 
		  Set  Query_Concatenated= concat('select * from ',DataBaseName,'.Opt_out where Date >=\'',Launch_Date,'\'');
		
		 else
		 Set  Query_Concatenated= concat('select * from ',DataBaseName,'.Opt_out where Date >=\'',Launch_Date,'\' and Date<=\'',End_Date,'\'');
		 end if;
		 End If;
		 While Counter < NoOfRecords Do
		  set Counter = Counter +1 ;
			
			Set DataBaseName = ( select substring_index(substring_index(GROUP_CONCAT(DB_Name separator ':' ),':',Counter),':',-1) DB_Name from group_stats.Services);
		  
			set CheckFile = (select count(*) Check_File From INFORMATION_SCHEMA.TABLES WHERE TABLE_Name='Opt_out' and TABLE_SCHEMA=DataBaseName) ;
			IF CheckFile = 1 Then
			Set Launch_Date = ( select `Launch Date` from group_stats.Services where DB_Name=DataBaseName);
			Set End_Date = ( select ifnull(`End Date`,0) from group_stats.Services where DB_Name=DataBaseName);
			if End_Date = 0 then 
			Set  Query_temp= concat('select * from ',DataBaseName,'.Opt_out where Date >=\'',Launch_Date,'\'');
			
			else
			Set Query_temp = concat('select * from ',DataBaseName,'.Opt_out where Date >=\'',Launch_Date,'\' and Date<=\'',End_Date,'\'');
			 end if;
      set Query_Concatenated = concat ( Query_Concatenated , ' Union ' ,Query_temp);
			
	
			END IF;
			set CheckFile =0 ;
			END WHILE;	
				
			SET @Query   = Query_Concatenated ;      
  	  PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Opt_Out2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Opt_Out2`(ServiceName  VARCHAR(50),Date_From  VARCHAR(15), Date_To  VARCHAR(15))
BEGIN
		DECLARE DataBaseName   VARCHAR(50);
		DECLARE Statement_TO VARCHAR(200);
		DECLARE Query_temp varchar(200);
				
		if Date_From = 'XXX' then 
		  Set  Date_From= '2000-01-01';
		end if;
		
		if Date_To = 'XXX' then 
		  Set  Date_To= '3000-01-01';
		end if;
		
		set DataBaseName = (select DB_Name from Services where `Service Name`=ServiceName);
		
		Set  Query_temp= concat('SELECT *,concat(unix_timestamp(Date),\'000\') Date_UNIX_Timestamp from `',DataBaseName,'`.Opt_out WHERE Date BETWEEN \'',Date_From,'\' and \'',Date_To,'\'');
		
		SET @Query   = Query_temp ; 
		
		PREPARE Statement_TO FROM @Query;
		EXECUTE  Statement_TO;
		
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Sub_ConvertReceipients_Tables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Sub_ConvertReceipients_Tables`(DB_ServiceName Varchar(50),DeletingTable varchar(50),MappingColumn varchar(50))
BEGIN
  DECLARE QUERY VARCHAR(50000);
	
  SET QUERY=CONCAT('replace into Service_Receipients (EMail,Name,Service_DB,',MappingColumn,') select *,\'',DB_ServiceName,'\',1 from `',DB_ServiceName,'`.',DeletingTable);
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat(DeletingTable,' Insertion Done');
  select concat('Dropping Table ',DeletingTable);
  SET QUERY=CONCAT('DROP TABLE IF EXISTS `',DB_ServiceName,'`.',DeletingTable);
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat('Dropping Table ',DeletingTable,' is Done');
  select concat('Creating View ',DeletingTable);
  SET QUERY=CONCAT('DROP VIEW IF EXISTS `',DB_ServiceName,'`.',DeletingTable);
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  
  SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail,Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  
  if DeletingTable='Exception' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail ,Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\'  AND Exception =1');
  end if;

  
  if DeletingTable='Receipients_Daily_To' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail `To`,Name `To_Name` FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  end if;
  if DeletingTable='Receipients_Daily_Cc' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail CC ,Name CC_Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  end if;
  
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat(DeletingTable,' View Creation is Done');
  select concat(DeletingTable,' Conversion Done');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Time_Interval` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Time_Interval`(Start_Date VARCHAR (10),Start_H varchar(2),End_Date VARCHAR (10),End_H Varchar(2))
BEGIN

      DECLARE    Query         VARCHAR (2000);
			DECLARE    Query_temp    VARCHAR (2000);
      DECLARE    Statement     VARCHAR (2000);
			Declare    Var1          Varchar (20);
			Declare    End_          Varchar (40);
			Declare    Start_        Varchar (40);
			Declare    i             integer;
			
			Set Start_=concat(Start_Date,' ',Start_H);
			Set End_=concat(End_Date,' ',End_H);
			Set i = 0;
			
      Set Var1 = Start_;
			SET @Query_temp = concat ('select date_format(\'',Var1 ,'\' + interval ',i,' hour , \'%Y-%m-%d %H\') Date_');
				
					if Start_ < End_ then
			
			while Var1 !=   End_  do
			
					Set i = i+1 ;
					Set Var1 = ( select date_format(Start_ + interval i hour ,'%Y-%m-%d %H'));
					SET @Query_temp = concat (@Query_temp,' union ','select date_format(\'',Start_ ,'\' + interval ',i,' hour , \'%Y-%m-%d %H\') Date_');				
					
			End While;
			
					Set @Query = concat ( 'select substring(Date_,1,10) Date, substring(Date_,12,2) Hour from ( ', @Query_temp ,') Q '); 
			    	else
					Set @Query = concat (' select ', '\'Error: Start Date should be greater than the End Date\'');
					End if;
			
   		PREPARE Statement FROM @Query;
   		EXECUTE  Statement;

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `writesumpivot2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `writesumpivot2`( db CHAR(64), tbl CHAR(64), pivotcol CHAR(64), sumcol CHAR(64) )
BEGIN 
  DECLARE datadelim CHAR(1) DEFAULT '"'; 
  DECLARE comma CHAR(1) DEFAULT ','; 
  DECLARE singlequote CHAR(1) DEFAULT CHAR(39); 
  SET @sqlmode = (SELECT @@sql_mode); 
  SET @@sql_mode=''; 
  SET @sql = CONCAT('select group_concat(sumpivotarg SEPARATOR \'\') from (SELECT DISTINCT CONCAT(', singlequote,  
                     ',SUM(IF(', pivotcol, ' = ', datadelim, singlequote, comma, 
                     pivotcol, comma, singlequote, datadelim, comma, sumcol, ',0)) AS `',  
                     singlequote, comma, pivotcol, comma, singlequote, '`', singlequote,   
                     ') AS sumpivotarg FROM ', db, '.', tbl,  
                     ' WHERE ', pivotcol, ' IS NOT NULL) X' ) ; 
   
   
   
   PREPARE stmt FROM @sql; 
   EXECUTE stmt; 
   DROP PREPARE stmt; 
   SET @@sql_mode=@sqlmode; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `group_stats`
--

USE `group_stats`;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Mail` BEFORE INSERT ON `Receipients_Always_To` FOR EACH ROW BEGIN
    SET new.`To`=concat(replace(new.To_name,' ','.'),'@vodafone.com');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `group_stats`.`Recp Other Phone Number Checker Insert` BEFORE INSERT
    ON group_stats.Receipients_MSISDN FOR EACH ROW
BEGIN
    if substring(new.`Phone Number`,1,1)='1' then
			set new.`Phone Number`=concat('20',new.`Phone Number`);
		end if;
			
		if substring(new.`Phone Number`,1,1)='0' then
			set new.`Phone Number`=concat('2',new.`Phone Number`);
		end if;
    
    if new.`mail`='' or  new.`mail` is null then
      SET new.`mail`=(concat(REPLACE(new.Name,' ','.'),'@vodafone.com'));
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `group_stats`.`Recp Other Phone Number Checker Update` BEFORE UPDATE
    ON `Receipients_MSISDN` FOR EACH ROW
BEGIN
    if substring(new.`Phone Number`,1,1)='1' then
			set new.`Phone Number`=concat('20',new.`Phone Number`);
		end if;
			
		if substring(new.`Phone Number`,1,1)='0' then
			set new.`Phone Number`=concat('2',new.`Phone Number`);
		end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `Service_Receipients_before_insert` BEFORE INSERT ON `Service_Receipients` FOR EACH ROW BEGIN
    if new.email='' then 
      SET new.`EMail`=concat(replace(new.Name,' ','.'),'@vodafone.com');
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `SingleQuote Removal Insert` BEFORE INSERT ON `Services` FOR EACH ROW BEGIN
    set new.`Desc` = REPLACE(new.`Desc`, ''', '');
    set new.`More Details` = REPLACE(new.`More Details`, ''', '');
    set new.DB_Name=trim(new.DB_Name);
END */;;
DELIMITER ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `SingleQuote Removal Update` BEFORE UPDATE ON `Services` FOR EACH ROW BEGIN
    set new.`Desc` = REPLACE(new.`Desc`, ''', '');
    set new.`More Details` = REPLACE(new.`More Details`, ''', '');
    set new.DB_Name=trim(new.DB_Name);
END */;;
DELIMITER ;
--
-- WARNING: old server version. The following dump may be incomplete.
--
DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`%` */ /*!50003 TRIGGER `JavaCollector_Updater` AFTER UPDATE ON `Services` FOR EACH ROW BEGIN
    Update COLLECTORS
    Set Active=new.CollectData 
    Where ServiceName=new.DB_Name;
END */;;
DELIMITER ;

--
-- Dumping routines for database 'group_stats'
--
/*!50003 DROP FUNCTION IF EXISTS `Add_Receipients_F` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Add_Receipients_F`(Group_IN integer,New_DB_Name varchar(100)) RETURNS int(11)
BEGIN
	call Add_Receipients_P(Group_IN,New_DB_Name);
	RETURN '1';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Char_Dublication_Remover` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Char_Dublication_Remover`(String varchar(100),To_Remove Varchar(1)) RETURNS varchar(100) CHARSET latin1
BEGIN
  DECLARE New_String  varchar(100);
  SET New_String=String;
	WHILE INSTR(New_String, concat(To_Remove,To_Remove))<>0
  DO
    SET New_String=replace(New_String,concat(To_Remove,To_Remove),To_Remove);
  END WHILE;
	RETURN New_String;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CronJob_ValidNow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `CronJob_ValidNow`(`Minute` vaRCHAR(50), `Hour` vaRCHAR(50), `DayOfWeek` vaRCHAR(50), `DayOfMonth` vaRCHAR(50), `MonthOfYear` VARCHAR(50)) RETURNS tinyint(4)
BEGIN
	DECLARE VALID INT;
	DECLARE ValidMinute Int;
	DECLARE ValidHour int;
	DECLARE ValidDayOfWeek int;
	DECLARE ValidDayOfMonth int;
	DECLARE ValidMonthOfYear Int;
	
	DECLARE CurrentMinute varchar(50);
	DECLARE CurrentHour varchar(50);
	DECLARE CurrentDayOfWeek varchar(50);
	DECLARE CurrentDayOfMonth varchar(50);
	DECLARE CurrentMonthOfYear varchar(50);
		

	SET CurrentMinute=date_format(now(),'%i')+0;
	SET ValidMinute=CronValidCheck(Minute,CurrentMinute);

	SET CurrentHour=date_format(now(),'%H')+0;
	SET ValidHour=CronValidCheck(Hour,CurrentHour);
	
	SET CurrentDayOfWeek=dayofweek(now())+0;
	SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);
	if ValidDayOfWeek != 1 then
		SET CurrentDayOfWeek=DAYNAME(now());
		SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);
	end if;
	if ValidDayOfWeek != 1 then
		SET CurrentDayOfWeek=left(DAYNAME(now()),3);
		SET ValidDayOfWeek=CronValidCheck(DayOfWeek,CurrentDayOfWeek);	
	end if;
	
	SET CurrentDayOfMonth=date_format(now(),'%d')+0;
	SET ValidDayOfMonth=CronValidCheck(DayOfMonth,CurrentDayOfMonth);
	
	SET CurrentMonthOfYear=date_format(now(),'%m')+0;
	SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);
	if ValidMonthOfYear != 1 then
		SET CurrentMonthOfYear=MonthNAME(now());
		SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);
	end if;
	if ValidMonthOfYear != 1 then
		SET CurrentMonthOfYear=left(MonthNAME(now()),3);
		SET ValidMonthOfYear=CronValidCheck(MonthOfYear,CurrentMonthOfYear);	
	end if;
	
	if ValidMinute=1 and ValidHour=1 and ValidDayOfWeek=1 and ValidDayOfMonth=1 and ValidMonthOfYear=1 then
		SET VALID=1;
	else
		SET VALID=0;
	end if;
	
	return VALID;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CronValidCheck` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `CronValidCheck`(`ParameterCheck` vaRCHAR(50), `ParameterCheckOn` VARCHAR(50)) RETURNS tinyint(4)
BEGIN
	DECLARE OccuranceCount int;
	DECLARE STEP int;
	DECLARE VALID int;
	SET VALID=0;
	
	SET OccuranceCount=Occurance_Count(ParameterCheck,',')+1;
	SET STEP=0;
	WHILE STEP<=OccuranceCount
		Do
		SET STEP=STEP+1;
		if ParameterCheck like '%*%' or SPLIT_STR(ParameterCheck,',',STEP)=ParameterCheckOn then
			SET VALID=1;
		end if;			
	end while;
	
	Return VALID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `CurHour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `CurHour`() RETURNS varchar(2) CHARSET latin1
BEGIN
	RETURN date_format(now(),'%H');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `DataBaseName` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `DataBaseName`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT DB_Name FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `EndDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `EndDate`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT `End Date` FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getnearestdate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `getnearestdate`(date1 VARCHAR (11)) RETURNS varchar(11) CHARSET latin1
BEGIN 
		 return (select Date
		 					from group_stats.temp_dates 
							where Date<date1 
							order by Date desc 
							limit 1);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Hour_Frequency` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Hour_Frequency`(Start_Hour Int,Frequency Int) RETURNS varchar(100) CHARSET latin1
BEGIN
  Declare Loops integer;
	Declare Hours integer;
	Declare Hour_arr varchar(100);
  set Hour_arr = Start_Hour;
	set Hours = Start_Hour;
	Set Loops = (24/Frequency);
	if (Start_Hour < 24) and (Frequency < 24) and ( (24%Frequency) = 0 ) and (Frequency != 0)then
		set Hours=	Start_Hour + Frequency;
		while Loops > 1 do
		if  (Hours >= 24)   then
			  Set Hours =abs( 24 - Hours);
			  Set Hour_arr = concat ( Hour_arr ,':',Hours);
				Set Hours = Hours +Frequency;
		else
		   
		   Set Hour_arr = concat ( Hour_arr ,':',Hours); 
			 Set Hours = Hours +Frequency;
		end if;
		
			Set Loops = Loops -1;
		end while;
		return Hour_arr;
	    elseif (Start_Hour < 24) and (Frequency = 24) and ( (24%Frequency) = 0 ) and (Frequency != 0) then
	    Set Hour_arr = concat (Start_Hour);
		 	return Hour_arr;
		 else
	   
	  return 'Error';
	
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Last2Weeks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Last2Weeks`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 2 Week,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastHour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastHour`() RETURNS varchar(2) CHARSET latin1
BEGIN
	RETURN date_format(now()-interval 1 hour,'%H');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastMonth` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastMonth`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 Month,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LastWeek` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LastWeek`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 Week,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LaunchDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `LaunchDate`(ServiceName varchar(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	
	RETURN (SELECT `Launch Date` FROM Services WHERE `Service Name` =ServiceName Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `make_cursor_concat` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `make_cursor_concat`(
    p_db_name       varchar(50),
    p_function_name varchar(50),
    p_table_name    varchar(1000),
    p_concat_column varchar(50)
) RETURNS int(11)
    MODIFIES SQL DATA
BEGIN
    DECLARE template text;
    SET template ='
    begin
        declare done boolean default false;
        declare first_loop boolean default true;
        declare myresult text;
        declare mycol text;
        declare get_col cursor for
            select distinct CONCAT_COLUMN from TABLE_NAME;
        declare continue handler for not found
            set done = true;

        set myresult = "";
        open get_col;
        myloop:
        loop
            fetch get_col into mycol;
            if (done) then
                leave myloop;
            end if;
            if (first_loop) then
                set first_loop = false;
            else
                set myresult = concat(myresult, "\n,");
            end if;
            set myresult = CONCAT(myresult, " ", myop,
            "( CASE WHEN CONCAT_COLUMN = ", 
                quote(mycol), 
                " THEN ", opcol, " ELSE NULL END) AS `",
                mycol, "`" );
        end loop;
        return myresult;
    end ';
    SET template = REPLACE(template, 'TABLE_NAME', p_table_name);
    SET template = REPLACE(template, 'CONCAT_COLUMN', p_concat_column);

    RETURN make_routine( 
        p_db_name, 
        p_function_name,
        'function',
        'myop varchar(10), opcol varchar(50)',
        'TEXT', 
        template);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `make_routine` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `make_routine`(
    p_db        varchar(64),
    p_name      varchar(64),
    p_type      enum('procedure', 'function'),
    p_params    text,
    p_returns   varchar(30),
    p_body      text
) RETURNS tinyint(1)
    MODIFIES SQL DATA
BEGIN
    DECLARE routine_exists int;
    SET routine_exists = 
        (
         SELECT count(*) 
         FROM mysql.proc 
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type
         );
    IF (routine_exists > 0 ) THEN
        DELETE FROM mysql.proc
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type; 
    END IF;
    INSERT INTO mysql.proc 
        (db, name, specific_name, `type`, param_list, `returns`,  body, `definer`)
        VALUES (p_db, p_name, p_name, p_type, p_params, p_returns, p_body, user());
    RETURN 
        (
         SELECT COUNT(*) 
         FROM mysql.proc 
         WHERE 
            name= p_name
            AND db = p_db
            AND `type` = p_type
         );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `NDC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `NDC`(MSISDN varchar(20)) RETURNS varchar(20) CHARSET latin1
BEGIN
  if left(MSISDN,3)='201' then
	  if length(MSISDN)<=11 then    
      if substring(MSISDN,1,4)='2010' then RETURN concat('20100',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2016' then RETURN concat('20106',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2019' then RETURN concat('20109',substring(MSISDN,5,11)); end if;
      
      if substring(MSISDN,1,4)='2011' then RETURN concat('20111',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2014' then RETURN concat('20114',substring(MSISDN,5,11)); end if;
      
      if substring(MSISDN,1,4)='2012' then RETURN concat('20122',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2017' then RETURN concat('20127',substring(MSISDN,5,11)); end if;
      if substring(MSISDN,1,4)='2018' then RETURN concat('20128',substring(MSISDN,5,11)); end if;
    else                          
      if substring(MSISDN,1,5)='20151' then RETURN concat('20101',substring(MSISDN,6,12)); end if;
      if substring(MSISDN,1,5)='20152' then RETURN concat('20112',substring(MSISDN,6,12)); end if;
      if substring(MSISDN,1,5)='20150' then RETURN concat('20120',substring(MSISDN,6,12)); end if;
    end if;
  else
    if left(MSISDN,2)='01' then
      if length(MSISDN)<=10 then    
        if substring(MSISDN,1,3)='010' then RETURN concat('20100',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='016' then RETURN concat('20106',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='019' then RETURN concat('20109',substring(MSISDN,4,11)); end if;
       
        if substring(MSISDN,1,3)='011' then RETURN concat('20111',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='014' then RETURN concat('20114',substring(MSISDN,4,11)); end if;
        
        if substring(MSISDN,1,3)='012' then RETURN concat('20122',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='017' then RETURN concat('20127',substring(MSISDN,4,11)); end if;
        if substring(MSISDN,1,3)='018' then RETURN concat('20128',substring(MSISDN,4,11)); end if;
      else                          
        if substring(MSISDN,1,4)='0151' then RETURN concat('20101',substring(MSISDN,5,12)); end if;
        if substring(MSISDN,1,4)='0152' then RETURN concat('20112',substring(MSISDN,5,12)); end if;
        if substring(MSISDN,1,4)='0150' then RETURN concat('20120',substring(MSISDN,5,12)); end if;
      end if;
    end if;
  end if;
  return MSISDN;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `NewService_Rec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `NewService_Rec`() RETURNS varchar(200) CHARSET latin1
BEGIN
	RETURN (SELECT concat(GROUP_CONCAT(MSISDN SEPARATOR ';' ),';') from NewService_Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `NextMonth` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `NextMonth`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()+interval 1 Month,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Occurance_Count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `Occurance_Count`(str varchar(200),Substr varchar(50) ) RETURNS int(11)
BEGIN	
	RETURN (SELECT (LENGTH(str) - LENGTH(REPLACE(str, substr, ''))) / LENGTH(substr));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Promo_Optin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Promo_Optin`(Service_Name VARCHAR (70), Date VARCHAR (20)
) RETURNS varchar(20) CHARSET latin1
begin
			declare MyResult varchar(20);
	  	 call group_stats.Service_New_Activation(Service_Name,Date,MyResult);
			 return MyResult;
			 end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `PSD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `PSD`() RETURNS varchar(200) CHARSET latin1
BEGIN
	RETURN (SELECT concat(GROUP_CONCAT(MSISDN SEPARATOR ';' ),';') from PSD_Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `remove_non_alphanum_char_f` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `remove_non_alphanum_char_f`(prm_strInput varchar(255)) RETURNS varchar(255) CHARSET latin1
BEGIN
  DECLARE i INT DEFAULT 1;
  DECLARE v_char VARCHAR(1);
  DECLARE v_parseStr VARCHAR(255) DEFAULT ' ';
 
WHILE (i <= LENGTH(prm_strInput) )  DO
 
  SET v_char = SUBSTR(prm_strInput,i,1); 
  
  IF v_char REGEXP  '^[A-Za-z0-9]+$' THEN  
    
        SET v_parseStr = CONCAT(v_parseStr,v_char);  

  END IF;
  SET i = i + 1;
END WHILE;
RETURN trim(v_parseStr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SDP_Name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SDP_Name`(MSISDN DECIMAL (20, 0), Data_Date VARCHAR (11)) RETURNS varchar(20) CHARSET latin1
BEGIN
      RETURN (SELECT SDP_Name
        FROM group_stats.msisdn_ranges Ranges
       WHERE (MSISDN >= Ranges.`Start`) AND (MSISDN <= Ranges.`End`) and Ranges.Data_Date=Data_Date);
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ServiceValidNow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` FUNCTION `ServiceValidNow`(`StartHour` INT, `Every` INT, `Minute` INT) RETURNS int(11)
BEGIN
	DECLARE Valid int;
	SET Valid=0;
	
	while StartHour>=0
		do
			SET StartHour=(StartHour-Every);
	end while;
	SET StartHour=(StartHour+Every);
	
	while StartHour<=23
		do
			if StartHour=hour(now()) && Minute=minute(now()) then
				SET Valid=1;
			end if;
			Set StartHour=(StartHour+Every);
	end while;
	
	return Valid;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Service_Description` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Service_Description`(ServiceName varchar(50)) RETURNS varchar(100) CHARSET latin1
BEGIN
	
	RETURN (SELECT concat(DB_Name,':',ServiceClass,':',`Launch Date`,':',ifnull(`End Date`,0)) Service_Description  FROM Services WHERE `Service Name` ='Hakawy Bokra' Limit 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Service_Name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Service_Name`(Serviceid integer,Date_ varchar(15),Nodeid integer) RETURNS varchar(30) CHARSET latin1
BEGIN
      RETURN (SELECT Service_Name
        FROM Temp.Service_Mappings
       WHERE (Service_ID =Serviceid) AND (Retrieval_Date = Date_) and (Node_ID=Nodeid)) ;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SMS_Receipients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SMS_Receipients`(Services VARCHAR (30)) RETURNS varchar(500) CHARSET latin1
BEGIN
declare Service_Rcpt_Table varchar(30);
SET Service_Rcpt_Table = concat(Services ,group_stats.Send_SMS_Rcpt);

      RETURN ( select group_concat(group_stats.Name_Mobile.Mobile_Number SEPARATOR ';') from 
	 SMS_Receipients.Salefny
	 Left outer join 
	 group_stats.`Name_Mobile` 
	 on( SMS_Receipients.Salefny.Names=group_stats.Name_Mobile.Name) ) ;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Split Bad` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Split Bad`(`LongString` varchar(100), `CharID` Int, `delimiter` varchar(12)) RETURNS varchar(100) CHARSET latin1
BEGIN
	Declare Part varchar(100);
	Declare Delim varchar(12);
	
	SET LongString = replace(LongString,delimiter,',');
	
	if SUBSTRING(LongString,LENGTH(LongString))<>',' then
		SET LongString=CONCAT(LongString,',');
	end if;
	
	if CharID = 1 then SET Part = SUBSTRING_INDEX( LongString , ',', 1 );
		elseif CharID = 3 then SET Part =   SUBSTRING_INDEX(SUBSTRING_INDEX( LongString , ',', 2 ),',',-1);
		elseif CharID = 2 then SET Part =   SUBSTRING_INDEX(SUBSTRING_INDEX( LongString , ',', -2 ),',',1);
		elseif CharID = 4 then SET Part =   SUBSTRING_INDEX( LongString , ',', -1 );
	end if;
	return Part;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `SPLIT_STR`(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
) RETURNS varchar(255) CHARSET latin1
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Today` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Today`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate(),'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Tomorrow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Tomorrow`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()+interval 1 day,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `Yesterday` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `Yesterday`() RETURNS varchar(11) CHARSET latin1
BEGIN
	RETURN date_format(curdate()-interval 1 day,'%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_Receipients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Add_Receipients`(Group_IN integer,New_DB_Name varchar(100))
BEGIN
  DECLARE Query1 VARCHAR(50000);
  SET Query1 = (SELECT substring(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_To UNION ') SEPARATOR ''),1,length(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_To UNION ') SEPARATOR ''))-length('UNION')-1)  FROM (
                SELECT *
                FROM information_schema.`TABLES`
                WHERE TABLE_NAME='Receipients_To' and TABLE_schema not like '%.ssh%') `TABLES`,
                (
                SELECT *
                FROM Services
                WHERE `Group`=Group_IN ) Services
                WHERE Services.DB_Name=`TABLES`.table_schema AND month(`Launch Date`)<>month(curdate()) 
                ORDER BY Services.`Launch Date` DESC
                LIMIT 1);
      SET  Query1=concat('REPLACE INTO `',New_DB_Name,'`.Receipients_To(Email,Name) ',@Query);
      Select Query1;
      set @Query=Query1;
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
      select 'Part 1 finished';
        SET @Query = (SELECT substring(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_CC UNION ') SEPARATOR ''),1,length(group_Concat(concat('SELECT Email,Name FROM `',table_schema,'`.Receipients_CC UNION ') SEPARATOR ''))-length('UNION')-1)  FROM (
                SELECT *
                FROM information_schema.`TABLES`
                WHERE TABLE_NAME='Receipients_CC' and TABLE_schema not like '%.ssh%' ) `TABLES` ,
                (
                SELECT *
                FROM Services
                WHERE `Group`=Group_IN ) Services
                WHERE Services.DB_Name=`TABLES`.table_schema AND month(`Launch Date`)<>month(curdate())
                ORDER BY Services.`Launch Date` DESC
                LIMIT 1);
      SET  @Query=concat('REPLACE INTO `',New_DB_Name,'`.Receipients_CC(Email,Name) ',@Query);
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ConvertReceipients_Tables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ConvertReceipients_Tables`(DB_ServiceName Varchar(50))
BEGIN
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_To','Normal_To');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_CC','Normal_CC');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_Daily_To','Daily_To');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Receipients_Daily_Cc','Daily_CC');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Exception_Receipients_To','Exception');
  call Sub_ConvertReceipients_Tables(DB_ServiceName,'Exception_Receipients_CC','Exception');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Daily_Me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Daily_Me`(iName VARCHAR(50), ServiceName VARCHAR(70))
BEGIN
      DECLARE DataBaseName VARCHAR(70);
			DECLARE Query,Statement_TO VARCHAR(1000);
      
      Declare FirstName varchar(20);
			DECLARE PhoneNumber VARCHAR(20);
      DECLARE Email varchar(100);
			
			SET DataBaseName  = (SELECT DB_Name FROM Services WHERE Report_Activated=1 and `Service Name` =ServiceName);
      
      
			
			
			
      SET QUERY=CONCAT('update Service_Receipients set Daily_To=1,Daily_CC=0,Normal_To=0,Normal_CC=0,Exception=0 WHERE Name=\'',iName,'\' AND Service_DB=\'',DataBaseName,'\' AND Exception=0');
      SELECT QUERY;
      set @Query=QUERY;
      PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
      
			
      
			
      
			
      
			
			
      
      
			
      
			
      
      
			
      
      
			
      
			
      SET FirstName=SUBSTRING(iName, 1, LOCATE(' ', iName, 1)-1);
      select FirstName;
			SET PhoneNumber=(SELECT `Phone Number` from `Receipients_MSISDN` where lcase(`Name`)=lcase(iName));
      
      select FirstName,PhoneNumber;
      
			CALL SEND_SMS('CNP VAS', ServiceName, concat('Dear ',FirstName,', Your Mail Has Been Migrated To Be Daily For ',ServiceName,' Service'), PhoneNumber);
			CALL SEND_SMS('CNP VAS', ServiceName, concat(iName,' Requested and Has Been Migrated To ',ServiceName,' Service Daily'), (select group_stats.PSD()));
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `execute_query` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `execute_query`(
    p_query     text,
    p_var_name  varchar(50),
    p_comment   text
)
    MODIFIES SQL DATA
BEGIN
    SET @EXEC_QUERY = p_query;
    IF ( p_var_name IS NOT NULL) AND ( p_var_name != '') THEN
        SET @EXEC_QUERY = CONCAT('set @', p_var_name, ' = (', p_query, ')' );
    END IF;    
    IF ( @SHOW_QUERIES IS NOT NULL) THEN
        SELECT p_comment;
        SELECT @EXEC_QUERY AS query;
    END IF;
    PREPARE q FROM @EXEC_QUERY;
    EXECUTE q;
    DEALLOCATE prepare q;
    IF ( @PRESERVE_QUERY IS NULL) THEN
        SET @EXEC_QUERY = NULL;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Failed*/ /*!50003 Functions Cleaner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `Failed*/ /*!50003 Functions Cleaner`()
BEGIN
	DECLARE Functions_Count INTEGER;
	DECLARE Counter INTEGER;
	DECLARE FunctionName VARCHAR(70);
	
	DECLARE Query_temp text;
	
	drop table if exists group_stats.Sultan;
	
	CREATE TABLE `Sultan` (
		`Function_Name` VARCHAR(64) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
		`ID` INT(11) NOT NULL AUTO_INCREMENT,
		PRIMARY KEY (`ID`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=MyISAM;
	
	
	insert into group_stats.Sultan(Function_Name)
	select SPECIFIC_NAME from information_schema.ROUTINES
	where SPECIFIC_NAME like 'fn_%' and ROUTINE_SCHEMA='group_stats' and date_format(created,'%Y-%m-%d')<(select date from group_stats.Today);
	
	SET Counter=1;
	SET Functions_Count=(select count(*) from Sultan);
	select Functions_Count;
	
	WHILE Counter <= Functions_Count 
		DO		
			
			SET FunctionName=(Select Function_Name from Sultan WHERE ID =Counter);
			Select concat('Dropping Function:',FunctionName);
			SET Query_temp=(CONCAT('DROP FUNCTION ',FunctionName));
      
			set @Query=(Query_temp);
			select @Query;
			PREPARE Statement_To FROM @Query;
			EXECUTE  Statement_To;
			
			SET Counter=Counter+1;
	END WHILE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `frequency_reducer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `frequency_reducer`()
BEGIN


      UPDATE Services
         SET every=every*2, Freq_Updated=Freq_Updated+1
       WHERE every<=2 AND `Launch Date` < group_stats.LastWeek() and Freq_Updated=0 and Report_Activated=1;

      UPDATE Services
         SET every=4,Freq_Updated=Freq_Updated+1
      WHERE  every=2 AND `Launch Date` < group_stats.Last2Weeks() and Freq_Updated=1 and Report_Activated=1;
   
   

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `get_crosstab`(
        row_name        varchar(50),    
        col_name        varchar(50),    
        id_name         varchar(50),    
        col_from        varchar(300),   
        op              varchar(10),    
        op_col          varchar(50),    
        from_clause     varchar(1000),  
        where_clause    varchar(1000),  
        wanted_result   varchar(50)     
        )
    READS SQL DATA
BEGIN
    
    DECLARE crosstab_col_query varchar(5000);
    DECLARE crosstab_columns   varchar(5000);
    DECLARE crosstab_query     varchar(10000);
    DECLARE complete_op        varchar(50);
    DECLARE result_table       varchar(50);
    DECLARE engine_type        varchar(20);
    DECLARE table_type         varchar(20);
    DECLARE with_rollup        varchar(20) default '';
    SET @XTAB_FORMAT = 'show';
    SET @XTAB_ENGINE = 'MyISAM';
    SET @XTAB_QUERY = NULL;
    SET @XTAB_TABLE = NULL;

    IF (@WITH_ROLLUP IS NOT NULL) THEN
        SET with_rollup = ' WITH ROLLUP';
    END IF;

    IF (( wanted_result IS NULL ) OR ( wanted_result REGEXP '^\s*$' ))
       AND (@XTAB_FORMAT IS NOT NULL ) THEN
        SET wanted_result = LOWER(@XTAB_FORMAT);
    END IF;

    IF ( id_name IS NULL ) OR ( id_name REGEXP '^\s*$' ) THEN
        SET id_name = col_name;
    END IF;
    IF ( from_clause IS NULL ) THEN
            set from_clause = '';
    END IF;
    
    IF (col_from IS NULL) OR ( col_from REGEXP '^\s*$' ) THEN
        SET col_from = from_clause;
    END IF;

    IF (from_clause != '') AND ( from_clause NOT REGEXP "^\s*[Ff][Rr][Oo][Mm]" ) THEN
        SET from_clause = CONCAT("FROM ", from_clause);
    END IF;
    IF (col_from != '') AND ( col_from REGEXP "^\s*[Ff][Rr][Oo][Mm]" ) THEN
        SET col_from = TRIM( col_from );
        SET col_from = RIGHT( col_from , LENGTH(col_from) - 5  );
    END IF;
    IF ( where_clause IS NULL ) THEN
        SET where_clause = '';
    END IF;

    IF ( UPPER(op) = 'COUNT' ) THEN
        SET complete_op = "COUNT(*)";
    ELSE
        SET complete_op = CONCAT(op,"(", op_col, ")");
    END IF;

    CALL get_crosstab_columns(
            schema(), 
            col_from, 
            col_name, 
            op, 
            op_col, 
            crosstab_columns );

    SET crosstab_query = CONCAT(
            "SELECT ", 
            row_name, "\n,", 
            crosstab_columns, "\n, ",
            complete_op, " as total", " \n", 
            from_clause, " ", 
            where_clause,
            " \nGROUP BY ", 
            row_name);

    IF ( wanted_result LIKE 'table%' ) THEN
        SET result_table = SUBSTRING_INDEX(wanted_result, ':', -1);
        IF ( result_table IS NULL ) 
            OR ( result_table = 'table' )
            OR ( result_table = '' ) THEN
            SET result_table = CONCAT(SCHEMA(), ".tmp",FLOOR(RAND() * 100000 )); 
        END IF;
        SET wanted_result = 'table';
    END IF;
    
    IF ( @XTAB_TO_TABLE IS NOT NULL ) THEN
        SET result_table = @XTAB_TO_TABLE; 
    END IF;

    IF ( @XTAB_ENGINE IS NOT NULL ) THEN
        SET engine_type  = @XTAB_ENGINE;
        IF (engine_type NOT REGEXP '^\s*ENGINE\s*=') THEN
            SET engine_type= CONCAT('ENGINE=', engine_type);
        END IF;
    ELSE
        SET engine_type = '';
    END IF;

    IF ( @XTAB_TYPE IS NOT NULL ) THEN
        SET table_type  = @XTAB_TYPE;
    ELSE
        SET table_type = '';
    END IF;

    IF ( wanted_result IS NULL) OR ( wanted_result = '' ) THEN
        SET wanted_result = 'query';
    END IF;
    
    CASE
         WHEN wanted_result = 'table' THEN
            IF ( table_type = '') AND (LOWER(result_table) REGEXP 'tmp') THEN
                SET table_type = 'TEMPORARY';
            END IF;
            IF (engine_type = '') AND ( LOWER(result_table) REGEXP 'mem' ) THEN
                SET engine_type = 'ENGINE=HEAP';
            END IF;
            SET @XTAB_DROP = CONCAT("DROP TABLE IF EXISTS ", result_table);

            call execute_query(@XTAB_DROP, NULL, 'dropping result table'); 
            SET crosstab_query = CONCAT(
                    "CREATE ", 
                    table_type, 
                    " TABLE ", 
                    result_table, " ", 
                    engine_type, " ", 
                    crosstab_query
            );
            SET @XTAB_TABLE = result_table;
            SET @XTAB_QUERY = crosstab_query;
            call execute_query(
                CONCAT(@XTAB_QUERY, with_rollup), 
                NULL, 
                'crosstab to table' );
         WHEN wanted_result = 'query' THEN
            SET @XTAB_QUERY = crosstab_query;
            SET @XTAB_TABLE = NULL;
         WHEN wanted_result = 'show' THEN
            set @XTAB_TABLE = NULL;
            set @XTAB_QUERY = crosstab_query;
            call execute_query(CONCAT(
                @XTAB_QUERY, 
                with_rollup), 
                NULL, 
                'crosstab to screen');
        ELSE
            SET @XTAB_TABLE = NULL;
            SET @XTAB_QUERY = crosstab_query;
            SELECT "choices:" as 'syntax', '' as 'explanation'
            UNION 
            SELECT "table ", 'creates a temporary table and sets @XTAB_TABLE'
            UNION 
            SELECT "query",  'sets @XTAB_QUERY with the crosstab query'
            UNION
            SELECT "show", 'executes the query' ;
    END CASE;
    SET @XTAB_FORMAT = 'show';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab_columns` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `get_crosstab_columns`(
    p_db_name   varchar(50),
    p_from_clause varchar(1000),
    p_concat_column varchar(50),
    p_operation varchar(10),
    p_op_col    varchar(50),
    OUT column_list text
)
BEGIN
    DECLARE temp_function_name varchar(50);
    DECLARE mfresult int;
    SET temp_function_name = CONCAT(
            'fn_', 
            REPLACE(uuid(),'-',''));
    SET mfresult = make_cursor_concat( 
            p_db_name, 
            temp_function_name, 
            p_from_clause, 
            p_concat_column);
    SET @q = CONCAT( 
                'SELECT ', 
                temp_function_name, 
                '(', 
                quote(p_operation), 
                ',', 
                quote(p_op_col), 
                ')');
    call execute_query(
                @q, 
                'XCOLS', 
                'getting crosstab columns anonymously'); 
    DELETE 
    FROM mysql.proc 
    WHERE name = temp_function_name;
    SET column_list =  @XCOLS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_crosstab_simple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`sultan` PROCEDURE `get_crosstab_simple`(IN `row_name` varchar(50), IN `col_name` varchar(50), IN `op` varchar(10), IN `op_col` varchar(50), IN `from_clause` varchar(1000)   
)
    READS SQL DATA
BEGIN
    call get_crosstab(row_name, col_name, null, null, op, op_col, from_clause, null, null);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Mobile_Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Mobile_Updater`(Date_ varchar(20),Hour_ int)
BEGIN
  DECLARE Day_,Max_Time int;
  set Day_=day(Date_);
  
  truncate table SMS_To_Mobiles_Today;
	replace into SMS_To_Mobiles_Today(Count,A_MSISDN,B_MSISDN,Date,Hour)
            
            SELECT count(*) Counts,MO,MT,Date,hour
            FROM Processings.`MO SMS`
            WHERE     day = Day_ and Hour>=Hour_ AND Date = Date_ 
            GROUP BY MO,MT,Date,hour
            having count(*)>15; 
  SET Max_Time=(select max(Hour) from SMS_To_Mobiles_Today);
  
  delete from SMS_To_Mobiles_Today where Hour<>Max_Time;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Outlook_Checker` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Outlook_Checker`()
BEGIN
  DECLARE No_Of_Records  integer;
  DECLARE Counter  integer;
  
  DECLARE Message  varchar(160);
  DECLARE Date_Time  varchar(30);
  
  SET No_Of_Records=(select Count(*) from OutlookChecker
                          WHERE `OutLook(Mail Received)`=0 and `Checker(Mail Sent)`=1 and `MessageIsSent`=0);
                          
  
  SELECT No_Of_Records;
	SET Counter   = 1;
  
  WHILE Counter <= No_Of_Records 
      DO		
      
       
        
      
      SET Date_Time=(SELECT Date_Hour_Min FROM OutlookChecker  where `OutLook(Mail Received)`=0 and `Checker(Mail Sent)`=1 and `MessageIsSent`=0 limit 1);
      SELECT Date_Time;
      SET Message=(SELECT CONCAT('On',replace(Date_Hour_Min,' ','@'),' outlook did not reply ') FROM OutlookChecker  where Date_Hour_Min=Date_Time);
      call Send_SMS('Checker','Outlook Checker',Message,'201001843434;201007021881;');
      UPDATE OutlookChecker
        SET MessageIsSent=1 
        WHERE Date_Hour_Min=Date_Time;
        
      SET Counter=Counter+1;
  END WHILE;      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Receipients_MSISDN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Receipients_MSISDN`()
BEGIN
	DECLARE Counter     INTEGER;
  DECLARE NoOfRecords INTEGER;
  DECLARE Statement   text;
  DECLARE DB_Name   varchar(200);
  DECLARE Statement_TO  text;

  select 'Truncating Receipients';
  TRUNCATE TABLE Processings.Receipients;

  select 'Processing To and CC Receipients';
  SET NoOfRecords   = (SELECT Count(distinct TABLE_SCHEMA) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC');
  SET Counter=1;
  
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
        SET DB_Name =(SELECT TABLE_SCHEMA FROM
        ( select TABLE_SCHEMA,@row := @row + 1 AS ID FROM (
              SELECT distinct TABLE_SCHEMA
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC'
              ) X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
        
            
        SET Statement=concat('REPLACE INTO Processings.Receipients(Name,Email,DB_Name) SELECT Name,Email,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_To union all SELECT Name,Email,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_CC');
          set @Query=Statement;
         
          PREPARE Statement_TO FROM @Query;
          EXECUTE  Statement_TO;
          
          SET Counter=Counter+1;
  END WHILE;
  
  
  select 'Processing Daily Receipients';
  SET NoOfRecords   = (SELECT Count(distinct TABLE_SCHEMA) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_Daily_Cc' or TABLE_NAME='Receipients_Daily_To');
  SET Counter=1;
  
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
        SET DB_Name =(SELECT TABLE_SCHEMA FROM
        ( select TABLE_SCHEMA,@row := @row + 1 AS ID FROM (
              SELECT distinct TABLE_SCHEMA
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_Daily_Cc' or TABLE_NAME='Receipients_Daily_To'
              ) X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
        
            
        SET Statement=concat('REPLACE INTO Processings.Receipients(Name,Email,DB_Name) SELECT CC_Name,CC,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_Daily_Cc union all SELECT To_Name,`To`,\'',DB_Name,'\' FROM `',DB_Name,'`.Receipients_Daily_To');
          set @Query=Statement;
         
          PREPARE Statement_TO FROM @Query;
          EXECUTE  Statement_TO;
          
          SET Counter=Counter+1;
  END WHILE;
  
  select 'Updating Receipients';
  SET Statement='INSERT IGNORE INTO group_stats.Receipients_MSISDN(Mail,Name,`Phone Number`) select distinct Email, Name, `Phone Number` FROM Processings.`Receipients MSISDN`';
  set @Query=Statement;
  
  
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Receipients_MSISDN_old` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Receipients_MSISDN_old`()
BEGIN
	DECLARE Counter     INTEGER;
  DECLARE NoOfRecords INTEGER;
  DECLARE Statement   varchar(300000);
  DECLARE Statement_Part   varchar(200);

  SET NoOfRecords   = (SELECT Count(*) No_Of_Tables FROM information_schema.`TABLES` WHERE TABLE_NAME='Receipients_To' or TABLE_NAME='Receipients_CC');
  SET Counter=1;
  SELECT NoOfRecords;
  SET Statement='';

  WHILE Counter <= NoOfRecords 
        DO
          SET Statement_Part=(
            SELECT Query FROM(
            select Query,@row := @row + 1 AS ID FROM (
              SELECT concat('SELECT Email,Name FROM ',table_schema,'.Receipients_To UNION') Query
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_To'
              union all
              SELECT concat('SELECT Email,Name FROM ',table_schema,'.Receipients_CC UNION')
              FROM information_schema.`TABLES`
              WHERE TABLE_NAME='Receipients_CC') X, (SELECT @row := 0) r) Y
            WHERE ID=Counter);
            SELECT Statement_Part;
          SET Statement=(CONCAT(Statement,' ',ifnull(Statement_Part,'')));
          
          SET Counter=Counter+1;
  END WHILE;

  SELECT Statement;
  SET Statement=(SELECT substring(Statement,1,length(Statement)-length('UNION')-1));
  SET Statement=(CONCAT('SELECT Email,Name,`Phone Number` FROM (',Statement,') `All` left outer join (select CC,CC_Name,`Phone Number` from Receipients_Always_Cc Union select `To`,To_Name,`Phone Number` from Receipients_Always_To Union select `To`,To_Name,`Phone Number` from Receipients_Others) Added on (`All`.Name=Added.CC_Name)'));
  set @Query=Statement;
  SELECT Statement;
  
  
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Recipient Updater` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Recipient Updater`()
BEGIN
	DECLARE STATEMENT varchar(3000); 
	SET group_concat_max_len = 3000;
	SET STATEMENT=(SELECT concat('select * from ',table_schema,'.',table_Name,' UNION ')
									from information_schema.TABLES
									where TABLE_NAME like '%receip%' and TABLE_TYPE='BASE TABLE' 
										and TABLE_NAME<>'Alarm_Receipients' 
										and TABLE_SCHEMA<>'group_stats');
	
	select STATEMENT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular`()
BEGIN
      DECLARE Query_temp              text;
      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
   
      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1' );
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':'),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d') );
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
			
      SET Query_temp   = concat ('SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');
      WHILE Counter <= NoOfRecords 
     DO		
		
				
				if Frequency != 1 then 
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm', '\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');		
			end if ;
				
			SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1'  and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				
			
		Set Repeate = (24 / Frequency) ;
			
			IF Frequency != 24 then
      
						IF Frequency = 1 then
								Set Repeate = 1;
								Set Start_H2 = '*';
                
								SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H2, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
                
						else
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
								Set Repeate = Repeate - 1;
								End While;
					end if;
				else
						Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
			End if;
									
			Set Counter =Counter + 1;
	
     END WHILE;
			
   
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
  		EXECUTE  Statement_TO;

  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular_Logger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular_Logger`()
BEGIN

      DECLARE Query_temp              VARCHAR (90000);

      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
      DECLARE ServiceName             VARCHAR(50);
      DECLARE Min2Add                 Integer;
   

      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1');
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
      SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
      SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
			
      SET Query_temp   = concat ('SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
			
      WHILE Counter <= NoOfRecords 
      DO		
				
				
				if Frequency != 1 then 
				
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				
				end if ;
				
				SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1' );
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
        SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' );
			
				Set Repeate = (24 / Frequency) ;
			
				IF Frequency != 24 then
						
						
						
						
						
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
									Set Repeate = Repeate - 1;
								End While;
						
					else
							Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				End if;
				Set Counter =Counter + 1;
	
      END WHILE;
			
      SELECT Query_temp;
      SET Query_temp=(SELECT REPLACE(Query_temp,'SELECT ','SELECT date_format(curdate(),\'%Y-%m-%d\') DATE, '));
      SELECT Query_temp;
      SET Query_temp=(CONCAT('SELECT @row := @row + 1 AS ID, Schedular.* FROM (',Query_temp,') Schedular, (SELECT @row := 0) r'));
      SELECT Query_temp;
      SET Query_temp=(CONCAT('INSERT IGNORE Schedular_Log(ID,Date,ServiceName,Min,Hours,Minutes_To_Add) select ID,DATE,ServiceName,Min,Hours,Minutes_To_Add from (',Query_temp,')X'));
      
      
      
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
   		EXECUTE  Statement_TO;
      
      

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular_Logger_New` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular_Logger_New`()
BEGIN

      DECLARE Query_temp              VARCHAR (50000);

      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
      DECLARE ServiceName             VARCHAR(50);
      DECLARE Min2Add                 Integer;
   

      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1');
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1');
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1');
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
      SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
      SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
			
			
      SET Query_temp   = concat ('SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
			
      WHILE Counter <= NoOfRecords 
      DO		
				
				
				if Frequency != 1 then 
				
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				
				end if ;
				
				SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1');
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1');
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				SET ServiceName=( SELECT  `Service Name`    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
        SET Min2Add=( SELECT  ifnull(`Minutes_To_Add_In_Check`,0)    FROM Services  where DB_Name=DataBaseName and Report_Activated='1');
			
				Set Repeate = (24 / Frequency) ;
			
				IF Frequency != 24 then
						
						
						
						
						
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
									Set Repeate = Repeate - 1;
								End While;
						
					else
							Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'', ServiceName,'\' ServiceName, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA,\'',Min2Add,'\' Minutes_To_Add');
				End if;
				Set Counter =Counter + 1;
	
      END WHILE;
			
      
      SET Query_temp=(SELECT REPLACE(Query_temp,'SELECT ','SELECT date_format(curdate(),\'%Y-%m-%d\') DATE, '));
      SET Query_temp=(CONCAT('SELECT @row := @row + 1 AS ID, Schedular.* FROM (',Query_temp,') Schedular, (SELECT @row := 0) r'));
      SET Query_temp=(CONCAT('INSERT IGNORE Schedular_Log(ID,Date,ServiceName,Min,Hours,Minutes_To_Add) select ID,DATE,ServiceName,Min,Hours,Minutes_To_Add from (',Query_temp,')X'));
      select 'LOOP finished';
      select Query_temp into outfile '/database/MySQL_Data/Query_temp';
      
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
   		EXECUTE  Statement_TO;
      
      

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Schedular_Old` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Schedular_Old`()
BEGIN
      DECLARE Query_temp              text;
      DECLARE Launch_Date, End_Date   VARCHAR (20);
      DECLARE NoOfRecords             INTEGER;
      DECLARE Counter                 INTEGER;
      DECLARE Loops                   INTEGER;
			DECLARE Minutes                 INTEGER;
			DECLARE Start_H                 INTEGER;
			DECLARE H                       INTEGER;
			DECLARE Hours                   VARCHAR (5000);
			DECLARE Start_H2                VARCHAR (1);
			DECLARE Hour_arr                VARCHAR (5000);
      DECLARE DataBaseName            VARCHAR (1000);
      DECLARE Frequencies             VARCHAR (1000);
      DECLARE Repeate                 INTEGER;
      DECLARE Frequency               INTEGER;
   
      SET Counter   = 1;
			
      SET NoOfRecords   = (SELECT Count(DB_Name) No_Of_DB FROM Services WHERE Report_Activated='1' and upgraded=0);
   		SET DataBaseName = ( SELECT substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':'),':',1) DB_Name FROM Services WHERE Report_Activated='1' and `End Date` >=  date_format(now() ,'%Y-%m-%d') and upgraded=0);
    	SET Frequency= ( SELECT  every   FROM Services where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
			SET Start_H= ( SELECT Start_Hour   FROM Services where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
			SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
			
			
      SET Query_temp   = concat ('SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');
      WHILE Counter <= NoOfRecords 
     DO		
		
				
				if Frequency != 1 then 
					SET Query_temp   = concat (Query_temp, ' union   SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm', '\' File, \'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA');		
			end if ;
				
			SET DataBaseName = ( SELECT substring_index(substring_index(GROUP_CONCAT(DB_Name SEPARATOR ':' ),':',Counter),':',-1) DB_Name FROM Services WHERE Report_Activated='1' and upgraded=0 and `End Date` >=  date_format(now() ,'%Y-%m-%d'));
				SET Frequency= ( SELECT every  FROM Services where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
				SET H=( SELECT Start_Hour  FROM Services where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
				SET Minutes= ( SELECT  Mins    FROM Services  where DB_Name=DataBaseName and Report_Activated='1' and upgraded=0);
				Set Hour_arr = (SELECT Hour_Frequency(H,Frequency));
				
			
		Set Repeate = (24 / Frequency) ;
			
			IF Frequency != 24 then
      
						IF Frequency = 1 then
								Set Repeate = 1;
								Set Start_H2 = '*';
                
								SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H2, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
                
						else
								while Repeate >= 1  Do
									Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
									SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
								Set Repeate = Repeate - 1;
								End While;
					end if;
				else
						Set Start_H = (SELECT substring_index(substring_index(Hour_arr,':',Repeate),':',-1)); 
							SET Query_temp   = concat (Query_temp, ' union  SELECT \'D:','\\\\','Tools','\\\\','Applications','\\\\', DataBaseName,'\\\\',DataBaseName ,'.xlsm','\' File,\'',Minutes,'\' Min,\'',Start_H, '\' Hours,\'*\' DOM,\'*\' DOY,\'*\' DOW,\'Excel\' TOA' );	
			End if;
									
			Set Counter =Counter + 1;
	
     END WHILE;
			
   
   		set @Query=(Query_temp);
   		PREPARE Statement_TO FROM @Query;
  		EXECUTE  Statement_TO;

  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SDP_Alarm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SDP_Alarm`()
BEGIN

 
			DECLARE Mobile                VARCHAR (20);
			DECLARE NoOfRecords 					integer;
			DECLARE Counter 							integer;
			DECLARE Send_SMS							integer;
			DECLARE Query  								VARCHAR (1000);
      SET Counter = 1  ;
			
      SET NoOfRecords   = (SELECT Count(Mobile_Number) Count FROM group_stats.Service_Mobile WHERE Service='SDP Alarm');
   		Set Mobile = ( SELECT substring_index(GROUP_CONCAT(Mobile_Number SEPARATOR ':' ),':',1) Mobile_Number FROM group_stats.Service_Mobile WHERE Service='SDP Alarm' );
			Set Send_SMS = ( select count(*) from (select SDP_Name,sum(Count) Count from  Salefny.Air_Transactions_100_SDP_Count  where date=date_format(now(),'%Y-%m-%d') and Time=date_format(now() - interval 1 hour,'%H')  group by SDP_Name having sum(count) >500) q);
			if Send_SMS != 0 THEN
			set @Query=concat('insert into group_stats.Send_SMS (Date,Hour,From_,Service,Description,Receipients) select Date,Time,\Error 100 \ From_,\SDP Alarm\ Service,group_concat(\SDP:\,SDP_Name,\ --> \,Count) Desc_ ,\'',Mobile,'\' recp from ( select Date,Time,SDP_Name,sum(Count) Count from  Salefny.Air_Transactions_100_SDP_Count where date=date_format(now(),\'%Y-%m-%d\') and Time=date_format(now() - interval 1 hour,\'%H\')  group by Date,Time,SDP_Name having sum(count) >500) q');
			PREPARE Statement_TO FROM @Query;
   		EXECUTE  Statement_TO;
			
      WHILE Counter <= NoOfRecords 
      DO			
					SET Counter = Counter + 1;
					SET Mobile = ( SELECT substring_index(substring_index(GROUP_CONCAT(Mobile_Number SEPARATOR ':' ),':',Counter),':',-1) Mobile_Number FROM group_stats.Service_Mobile WHERE Service='SDP Alarm');
					set @Query=concat('insert into group_stats.Send_SMS (Date,Hour,From_,Service,Description,Receipients) select Date,Time,\Error 100 \ From_,\SDP Alarm\ Service,group_concat(\SDP:\,SDP_Name,\ --> \,Count) Desc_ ,\'',Mobile,'\' recp from ( select Date,Time,SDP_Name,sum(Count) Count from  Salefny.Air_Transactions_100_SDP_Count where date=date_format(now(),\'%Y-%m-%d\') and Time=date_format(now() - interval 1 hour,\'%H\')  group by Date,Time,SDP_Name having sum(count) >500) q');
					PREPARE Statement_TO FROM @Query;
   				EXECUTE  Statement_TO;
			End While;
		END IF;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SEND_SMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `SEND_SMS`(IN `From_` varchar(11), IN `ServiceName` varchar(50), IN `Description` varchar(2000), IN `Receipients` varchar(4000))
BEGIN
/*
	#DECLARE DATE_ varchar(20);
#	DECLARE HOUR_ VARCHAR(2);
  DECLARE Description_Limitted VARCHAR(480);
  
  SET Description_Limitted=substring(Description,1,480);
	
	#SET DATE_=(SELECT date_format(curdate(),'%Y-%m-%d'));
	#SET HOUR_=(SELECT date_format(now(),'%H'));
	#insert into Main_Conf.Send_SMS(Date,Hour,From_,`Service`,Receipients,Description)
#					values(DATE_,HOUR_,From_,substring(ServiceName,1,50),Receipients,Description_Limitted);				

	insert into Main_Conf.Send_SMS(From_,`Service`,Receipients,Description)
					values(From_,substring(ServiceName,1,50),Receipients,Description_Limitted);				
*/
	CALL Queues.`Send_SMS`(From_, ServiceName, Description, Receipients);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Configuration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Configuration`(Data_Base_Name VARCHAR(50), 
                                                     Service_Name VARCHAR(70),
                                                     Report_Frequency_Mapping int(11),
                                                     Start_Hour int(3),Mins int(3),ServiceClass Double,
                                                     StartDate VARCHAR(11),End_Date varchar(11),
                                                     MachineName varchar(20),ServiceDesc varchar(3000),
                                                     OptInChannels	varchar(50),OptInFees	varchar(150),
                                                     CommunicationChannel	varchar(50),Group_	int(11),
                                                     MoreDetails	varchar(3000),SystemServiceID1	int(11),SystemServiceID2	int(11),SystemServiceID3	int(11),SystemServiceID4	int(11),SystemServiceID5	int(11),SystemServiceID6	int(11),SystemServiceID7	int(11),SystemServiceID8	int(11),SystemServiceID9	int(11),SystemServiceID0	int(11))
BEGIN
			
      SET @Query=concat('create database IF NOT EXISTS ',Data_Base_Name);
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Exception_Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
					
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Exception_Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  IF NOT EXISTS  ',Data_Base_Name,'.Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;

			
			SET @Query=concat('create view ',Data_Base_Name,'.Receipients_Always as SELECT group_stats.Receipients_Always.To  `To`,group_stats.Receipients_Always.To_Name AS To_Name,group_stats.Receipients_Always.CC AS CC,group_stats.Receipients_Always.CC_Name AS CC_Name FROM group_stats.Receipients_Always');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		
			if End_Date = '' then
				set End_Date=(select date_format(curdate()+interval 10 year, '%Y-%m-%d'));
			end if;
			
			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`,MachineName,`Opt In Channels`,`Opt In Fees`,`Communication Channel`,`Group`,`More Details`,SystemServiceID1,SystemServiceID2,SystemServiceID3,SystemServiceID4,SystemServiceID5,SystemServiceID6,SystemServiceID7,SystemServiceID8,SystemServiceID9,SystemServiceID0,`Desc`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',	End_Date ,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,',',MachineName,',',OptInChannels,',',OptInFees,',',CommunicationChannel,',',Group_,',',MoreDetails,',',SystemServiceID1,',',SystemServiceID2,',',SystemServiceID3,',',SystemServiceID4,',',SystemServiceID5,',',SystemServiceID6,',',SystemServiceID7,',',SystemServiceID8,',',SystemServiceID9,',',SystemServiceID0,',',ServiceDesc,')');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('Create view  ',Data_Base_Name,'.Service_Details As  select `Service Name`,every as \'Every(Hour)\' from group_stats.Services where DB_Name=\'',Data_Base_Name,'\'');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('create view ',Data_Base_Name,'.`Service Details`  AS select ',Data_Base_Name,'.`Service_Details`.`Service Name` AS `Service Name`,`group_stats`.`Report_Frequency_Mapping`.`Description` AS `Description`,',Data_Base_Name,'.`Service_Details`.`Every(Hour)` AS `Every(Hour)` from ','(',Data_Base_Name,'.`Service_Details` left join `group_stats`.`Report_Frequency_Mapping` on','((',Data_Base_Name,'.`Service_Details`.`Every(Hour)` = `group_stats`.`Report_Frequency_Mapping`.`Every(Hour)`)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			
			SET @Query=concat('CREATE TABLE  IF NOT EXISTS  ',Data_Base_Name,'.Transactions ( Date varchar(11) NOT NULL, Type_ID int(11) NOT NULL ,Error_Code int(11) NOT NULL ,Other_Error varchar(20) NOT NULL ,Count bigint(20) ,Client_ID int(11) NOT NULL , Service_Id int(11) NOT NULL , PRIMARY KEY (Date,Type_ID,Error_Code,Other_Error,Client_ID,Service_Id)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE TABLE  IF NOT EXISTS ',Data_Base_Name,'.Subscriber_Counts (Client_ID int(11) NOT NULL,Type_ID int(11) NOT NULL, Package_ID varchar(11) NOT NULL, Count decimal(10,0) NOT NULL , Status int(4) NOT NULL ,  PRIMARY KEY (Client_ID,Type_ID,Package_ID,Status)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
 			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE TABLE  IF NOT EXISTS  group_stats.client_',MachineName,' (CLIENT_ID int(11) DEFAULT NULL,CLIENT_NAME varchar(50) DEFAULT NULL,MachineName varchar(20) DEFAULT NULL,Temp int(11) DEFAULT NULL)');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
      if MachineName='FMLYPRD1' then
        
				SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.`Subscriber Count` AS select ifnull(`client`.`CLIENT_NAME`,\'Unknown\') AS `CLIENT_NAME`,`group_stats`.`Subscribers_Status_FMLYPRD1`.`STATUS_DESC` AS `STATUS_DESC`,`group_stats`.`packages`.`Package_NAME` AS `Package_NAME`,',Data_Base_Name,'.`Subscriber_Counts`.`Service_id` AS `Service_id`,',Data_Base_Name,'.`Subscriber_Counts`.`Count` AS `Count` from (((',Data_Base_Name,'.`Subscriber_Counts` left join `group_stats`.`Subscribers_Status_FMLYPRD1` on((`group_stats`.`Subscribers_Status_FMLYPRD1`.`SUBSCRIBER_STATUS_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Status`))) left join `group_stats`.`packages` on((`group_stats`.`packages`.`Package_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Package_ID`))) left join `group_stats`.`client_FMLYPRD1` `client` on((`client`.`CLIENT_ID` = ',Data_Base_Name,'.`Subscriber_Counts`.`Client_ID`)))');
      end if;
      
      if MachineName='PROMODB' then
        
				SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.`Subscriber Count` AS select ifnull(`client`.`CLIENT_NAME`,\'Unknown\') AS `CLIENT_NAME`,`group_stats`.`packages`.`Package_NAME` AS `Package_NAME`,`group_stats`.`transaction_type`.`TYPE_NAME` AS `TYPE_NAME`,`Subscriber_Counts_1`.`Count` AS `Count`,`group_stats`.`error_codes`.`ERROR_DESC` AS `ERROR_DESC`,`group_stats`.`error_codes`.`Error_Type` AS `Error_Type` from ((((',Data_Base_Name,'.`Subscriber_Counts` `Subscriber_Counts_1` left join `group_stats`.`client_PROMODB` `client` on((`client`.`CLIENT_ID` = `Subscriber_Counts_1`.`Client_ID`))) left join `group_stats`.`transaction_type` on((`group_stats`.`transaction_type`.`Type_ID` = `Subscriber_Counts_1`.`Type_ID`))) left join `group_stats`.`packages` on((`group_stats`.`packages`.`Package_ID` = `Subscriber_Counts_1`.`Package_ID`))) left join `group_stats`.`error_codes` on((`group_stats`.`error_codes`.`ERROR_CODE` = `Subscriber_Counts_1`.`Status`)))');
      end if;
			                                                                          
		  
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Transaction_View AS select ',Data_Base_Name,'.Transactions.Date AS Date,',Data_Base_Name,'.Transactions.Other_Error AS Other_Error,',Data_Base_Name,'.Transactions.Count AS Count,group_stats.error_codes.ERROR_DESC AS ERROR_DESC,group_stats.error_codes.Error_Type AS Error_Type,group_stats.transaction_type.TYPE_NAME AS TYPE_NAME from ((group_stats.error_codes join ',Data_Base_Name,'.Transactions on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Transactions.Error_Code))) join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Transactions.Type_ID)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.Opt_out AS select Date,sum(Count) Opt_out,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 2 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW  ',Data_Base_Name,'.Opt_in AS select Date,sum(Count) Opt_in,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 1 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
			
			
	
			
			

		
			END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Configuration_Old` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Configuration_Old`(Data_Base_Name VARCHAR(50), Service_Name VARCHAR(70),Report_Frequency_Mapping int(11),Start_Hour int(3),Mins int(3),ServiceClass Double,StartDate VARCHAR(11),End_Date varchar(11))
BEGIN
			
      SET @Query=concat('create database ',Data_Base_Name);
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Exception_Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
					
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Exception_Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Receipients_CC  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
						
 			SET @Query=concat('Create table  ',Data_Base_Name,'.Receipients_To  (Email varchar(50) DEFAULT NULL,Name varchar(50) DEFAULT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;

			
			SET @Query=concat('create view ',Data_Base_Name,'.Receipients_Always as SELECT group_stats.Receipients_Always.To  `To`,group_stats.Receipients_Always.To_Name AS To_Name,group_stats.Receipients_Always.CC AS CC,group_stats.Receipients_Always.CC_Name AS CC_Name FROM group_stats.Receipients_Always');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		
			if End_Date = '' then
			set End_Date=(select date_format(curdate()+interval 10 year, '%Y-%m-%d'));
			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',	End_Date ,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,')');
		
			else

			SET @Query=concat('Insert into  group_stats.`Services`(`Service Name`,`DB_Name`,`ServiceClass`,`Launch Date`,`End Date`,`every`,`Start_Hour`,`Mins`) Values(',Service_Name,',',Data_Base_Name,',',ServiceClass,',',StartDate,',',End_Date,',',Report_Frequency_Mapping ,',',Start_Hour,',',Mins,')');
					
			
		
			end if;
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('Create view  ',Data_Base_Name,'.Service_Details As  select `Service Name`,every as \'Every(Hour)\' from group_stats.Services where DB_Name=\'',Data_Base_Name,'\'');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('create view ',Data_Base_Name,'.`Service Details`  AS select ',Data_Base_Name,'.`Service_Details`.`Service Name` AS `Service Name`,`group_stats`.`Report_Frequency_Mapping`.`Description` AS `Description`,',Data_Base_Name,'.`Service_Details`.`Every(Hour)` AS `Every(Hour)` from ','(',Data_Base_Name,'.`Service_Details` left join `group_stats`.`Report_Frequency_Mapping` on','((',Data_Base_Name,'.`Service_Details`.`Every(Hour)` = `group_stats`.`Report_Frequency_Mapping`.`Every(Hour)`)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			
			SET @Query=concat('CREATE TABLE  ',Data_Base_Name,'.Transactions ( Date varchar(11) NOT NULL, Type_ID int(11) NOT NULL ,Error_Code int(11) NOT NULL ,Other_Error varchar(20) NOT NULL ,Count bigint(20) ,Client_ID int(11) NOT NULL , Service_Id int(11) NOT NULL , PRIMARY KEY (Date,Type_ID,Error_Code,Other_Error,Client_ID,Service_Id)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE TABLE ',Data_Base_Name,'.Subscriber_Counts (Client_ID int(11) NOT NULL,Type_ID int(11) NOT NULL, Package_ID varchar(11) NOT NULL, Count decimal(10,0) NOT NULL , Status int(4) NOT NULL ,  PRIMARY KEY (Client_ID,Type_ID,Package_ID,Status)) ENGINE=MyISAM DEFAULT CHARSET=latin1');
 			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.`Subscriber Count` AS select group_stats.error_codes.Error_Type AS Error_Type,ifnull(group_stats.client.CLIENT_NAME,\'Unknown\') AS CLIENT_NAME,ifnull(group_stats.packages.Package_NAME,\'Unknown\') AS Package_NAME,ifnull(group_stats.packages.Pkg_desc,\'Unknown\') AS Pkg_desc,ifnull(group_stats.transaction_type.TYPE_NAME,\'Unknown\') AS TYPE_NAME,',Data_Base_Name,'.Subscriber_Counts.Count AS Count from ((((',Data_Base_Name,'.Subscriber_Counts left join group_stats.packages on((group_stats.packages.Package_ID = ',Data_Base_Name,'.Subscriber_Counts.Package_ID))) left join group_stats.client on((group_stats.client.CLIENT_ID = ',Data_Base_Name,'.Subscriber_Counts.Client_ID))) left join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Subscriber_Counts.Type_ID))) join group_stats.error_codes on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Subscriber_Counts.Status)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
		  SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Transaction_View AS select ',Data_Base_Name,'.Transactions.Date AS Date,',Data_Base_Name,'.Transactions.Other_Error AS Other_Error,',Data_Base_Name,'.Transactions.Count AS Count,group_stats.error_codes.ERROR_DESC AS ERROR_DESC,group_stats.error_codes.Error_Type AS Error_Type,group_stats.transaction_type.TYPE_NAME AS TYPE_NAME from ((group_stats.error_codes join ',Data_Base_Name,'.Transactions on((group_stats.error_codes.ERROR_CODE = ',Data_Base_Name,'.Transactions.Error_Code))) join group_stats.transaction_type on((group_stats.transaction_type.Type_ID = ',Data_Base_Name,'.Transactions.Type_ID)))');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Opt_out AS select Date,sum(Count) Opt_out,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 2 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
			
			
			SET @Query=concat('CREATE  VIEW ',Data_Base_Name,'.Opt_in AS select Date,sum(Count) Opt_in,\'',Service_Name,'\' Service From ',Data_Base_Name,'.Transactions where Type_ID = 1 and Error_Code = 2 group by Date,Service');
			
			PREPARE Statement_TO FROM @Query;
      EXECUTE  Statement_TO;
		
			
			
	
			
			

		
			END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Service_Details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Service_Details`(ServiceName VARCHAR(50))
BEGIN
	DECLARE Query,Statement VARCHAR(100);
	DECLARE DataBaseName   VARCHAR(50);
	SET DataBaseName   = (SELECT DB_Name FROM Services WHERE `Service Name` =ServiceName);
	
	SET @Query=concat('SELECT `Service Name`,`Every(Hour)` FROM group_stats,',DataBaseName,'.`Service_Details`');
	
	select @Query;
			
      
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Stop_Me` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Stop_Me`(Receipient_Name VARCHAR(50), ServiceName VARCHAR(70))
BEGIN
      DECLARE DataBaseName_TO,DataBaseName_CC,DataBaseName   VARCHAR(70);
			DECLARE Query,Statement_TO,Statement_CC,Statement   VARCHAR(100);
			DECLARE PhoneNumber VARCHAR(20);
      DECLARE ReceipientName Varchar(20);
      
      if (Receipient_Name<>'CNP VAS') then
        SET ReceipientName= (select concat(substring(substring(`Split`(Receipient_Name,1,' '),1,1),1,1),lcase(substring(`Split`(Receipient_Name,1,' '),2,100))));
        
        SET DataBaseName   = concat('',(SELECT DB_Name FROM Services WHERE Report_Activated=1 and `Service Name` =ServiceName),'');
        
        
        
        
        select DataBaseName,Receipient_Name,ServiceName;
        
         
        
        
        SET @Query=concat('replace into group_stats.Service_Receipients(Exception,Daily_To,Daily_CC,Normal_To,Normal_CC,Service_DB,Name) values (1,0,0,0,0,\'',DataBaseName,'\',\'',Receipient_Name,'\')');
        SELECT @Query;
        PREPARE Statement FROM @Query;
        EXECUTE  Statement;
        
        
        SET PhoneNumber=(SELECT `Phone Number` from group_stats.`Receipients_MSISDN` where `Name`=Receipient_Name);
        
        
        CALL SEND_SMS('CNP VAS', ServiceName, concat('Dear ',ReceipientName,', You Have Been Removed From ',ServiceName,' Service'), PhoneNumber);
        
        CALL SEND_SMS('CNP VAS', ServiceName, concat(Receipient_Name,' Requested and Has Been Removed From ',ServiceName,' Service'), (select group_stats.PSD()));
        
      end if;
   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Sub_ConvertReceipients_Tables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Sub_ConvertReceipients_Tables`(DB_ServiceName Varchar(50),DeletingTable varchar(50),MappingColumn varchar(50))
BEGIN
  DECLARE QUERY VARCHAR(50000);
	
  SET QUERY=CONCAT('replace into Service_Receipients (EMail,Name,Service_DB,',MappingColumn,') select *,\'',DB_ServiceName,'\',1 from `',DB_ServiceName,'`.',DeletingTable);
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat(DeletingTable,' Insertion Done');
  select concat('Dropping Table ',DeletingTable);
  SET QUERY=CONCAT('DROP TABLE IF EXISTS `',DB_ServiceName,'`.',DeletingTable);
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat('Dropping Table ',DeletingTable,' is Done');
  select concat('Creating View ',DeletingTable);
  SET QUERY=CONCAT('DROP VIEW IF EXISTS `',DB_ServiceName,'`.',DeletingTable);
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  
  SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail,Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  
  if DeletingTable='Exception' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail ,Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\'  AND Exception =1');
  end if;

  
  if DeletingTable='Receipients_Daily_To' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail `To`,Name `To_Name` FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  end if;
  if DeletingTable='Receipients_Daily_Cc' then
    SET QUERY=CONCAT('CREATE VIEW `',DB_ServiceName,'`.',DeletingTable,' AS SELECT EMail CC ,Name CC_Name FROM group_stats.Service_Receipients WHERE Service_DB=\'',DB_ServiceName,'\' AND ',MappingColumn,'=1');
  end if;
  
  SELECT QUERY;
  set @Query=QUERY;
  PREPARE Statement_TO FROM @Query;
  EXECUTE  Statement_TO;
  select concat(DeletingTable,' View Creation is Done');
  select concat(DeletingTable,' Conversion Done');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Time_Interval` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `Time_Interval`(Start_Date VARCHAR (10),Start_H varchar(2),End_Date VARCHAR (10),End_H Varchar(2))
BEGIN

      DECLARE    Query         VARCHAR (2000);
			DECLARE    Query_temp    VARCHAR (2000);
      DECLARE    Statement     VARCHAR (2000);
			Declare    Var1          Varchar (20);
			Declare    End_          Varchar (40);
			Declare    Start_        Varchar (40);
			Declare    i             integer;
			
			Set Start_=concat(Start_Date,' ',Start_H);
			Set End_=concat(End_Date,' ',End_H);
			Set i = 0;
			
      Set Var1 = Start_;
			SET @Query_temp = concat ('select date_format(\'',Var1 ,'\' + interval ',i,' hour , \'%Y-%m-%d %H\') Date_');
				
					if Start_ < End_ then
			
			while Var1 !=   End_  do
			
					Set i = i+1 ;
					Set Var1 = ( select date_format(Start_ + interval i hour ,'%Y-%m-%d %H'));
					SET @Query_temp = concat (@Query_temp,' union ','select date_format(\'',Start_ ,'\' + interval ',i,' hour , \'%Y-%m-%d %H\') Date_');				
					
			End While;
			
					Set @Query = concat ( 'select substring(Date_,1,10) Date, substring(Date_,12,2) Hour from ( ', @Query_temp ,') Q '); 
			    	else
					Set @Query = concat (' select ', '\'Error: Start Date should be greater than the End Date\'');
					End if;
			
   		PREPARE Statement FROM @Query;
   		EXECUTE  Statement;

   END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `writesumpivot2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `writesumpivot2`( db CHAR(64), tbl CHAR(64), pivotcol CHAR(64), sumcol CHAR(64) )
BEGIN 
  DECLARE datadelim CHAR(1) DEFAULT '"'; 
  DECLARE comma CHAR(1) DEFAULT ','; 
  DECLARE singlequote CHAR(1) DEFAULT CHAR(39); 
  SET @sqlmode = (SELECT @@sql_mode); 
  SET @@sql_mode=''; 
  SET @sql = CONCAT('select group_concat(sumpivotarg SEPARATOR \'\') from (SELECT DISTINCT CONCAT(', singlequote,  
                     ',SUM(IF(', pivotcol, ' = ', datadelim, singlequote, comma, 
                     pivotcol, comma, singlequote, datadelim, comma, sumcol, ',0)) AS `',  
                     singlequote, comma, pivotcol, comma, singlequote, '`', singlequote,   
                     ') AS sumpivotarg FROM ', db, '.', tbl,  
                     ' WHERE ', pivotcol, ' IS NOT NULL) X' ) ; 
   
   
   
   PREPARE stmt FROM @sql; 
   EXECUTE stmt; 
   DROP PREPARE stmt; 
   SET @@sql_mode=@sqlmode; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-13 12:24:11
