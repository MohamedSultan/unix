######################################## Check that the CNPVAS01 DB is alive  #####################
#! /usr/bin/ksh
cd /app/Alarms


HBResultErr=`wc -l HBResult | tr -s [:space:] | sed 's/^ //' | cut -f1 -d" "`

App=$0

if [ $HBResultErr -ne 0 ] 
	then	
		echo  "DB Works fine" 
	else
		echo Dead 
		echo "DBAlive,20108880166,CNPVAS,DB @ CNPVAS01 NOT ALIVE" >/log/Shared/DB_Alive/Send_SMS 
		echo Date: `date` Service: `pwd`     >> /log/All_App_mysqlImport.Log
		mysqlimport --host=ActiveDB --user=Writer --password=Writer --force --columns=Service,Receipients,From_,Description --fields-terminated-by=, group_stats /log/Shared/DB_Alive/Send_SMS >> /log/ImporterLog.txt 
		cat error.log | awk '{ print App,",",$0} '  App=$App  >> /log/All_App_mysqlImport.Log
fi		


######################################## Check that the CNPVAS02 DB is alive  #####################
#! /usr/bin/ksh
cd /app/Alarms

mysql -h ActiveDB -uroot -p'Sultan(999'  -e "STATUS"   > HBResult  

HBResultErr=`wc -l HBResult | tr -s [:space:] | sed 's/^ //' | cut -f1 -d" "`

App=$0

if [ $HBResultErr -ne 0 ] 
	then	
		echo  "DB Works fine" 
	else
		echo Dead 
		echo "DBAlive,20108880166,CNPVAS,DB @ CNPVAS02 NOT ALIVE" >> /log/Shared/DB_Alive/Send_SMS 
		echo Date: `date` Service: `pwd`     >> /log/All_App_mysqlImport.Log
		mysqlimport --host=ActiveDB --user=Writer --password=Writer --force --columns=Service,Receipients,From_,Description --fields-terminated-by=, group_stats /log/Shared/DB_Alive/Send_SMS >> /log/ImporterLog.txt 
		cat error.log | awk '{ print App,",",$0} '  App=$App  >> /log/All_App_mysqlImport.Log
fi		

