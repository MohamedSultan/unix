######################################## Check that the CNPVAS01 DB is alive  #####################
#! /usr/bin/ksh

WithSMS=$1

SlaveStatus=`mysql -uReplicator -pReplicator -e"Show slave status" -hCNPVAS01 | tr "\t" "|" | cut -d"|" -f11,12,19,20,33 | tail -1`
Slave_IO_Running=`echo $SlaveStatus | cut -d"|" -f1`
Slave_SQL_Running=`echo $SlaveStatus | cut -d"|" -f2`
Last_Errno=`echo $SlaveStatus | cut -d"|" -f3`
Last_Error=`echo $SlaveStatus | cut -d"|" -f4`
Seconds_Behind_Master=`echo $SlaveStatus | cut -d"|" -f5`

if [ "$Seconds_Behind_Master" = "" ]
	then
		Seconds_Behind_Master=0
fi
if [ "$Seconds_Behind_Master" = "NULL" ]
        then
                Seconds_Behind_Master=-1
fi
#echo "Seconds_Behind_Master:"$Seconds_Behind_Master
Seconds_Behind_Master=$((Seconds_Behind_Master + 0))

if [ "$Slave_IO_Running" = "Yes" ] && [ "$Slave_SQL_Running" = "Yes" ] && [ $Seconds_Behind_Master -le 1000 ]
	then
		echo "Everything is ok"
		echo "-----------------"
		echo "Slave_IO_Running="$Slave_IO_Running
		echo "Slave_SQL_Running="$Slave_SQL_Running
		echo "Last_Errno="$Last_Errno
		echo "Last_Error="$Last_Error
		echo "Seconds_Behind_Master="$Seconds_Behind_Master
	else
		echo "Found DB Replica problem"
		echo "------------------------"
                echo "Slave_IO_Running="$Slave_IO_Running
                echo "Slave_SQL_Running="$Slave_SQL_Running
                echo "Last_Errno="$Last_Errno
                echo "Last_Error="$Last_Error
                echo "Seconds_Behind_Master="$Seconds_Behind_Master
		
		ErrorDesc=`echo $Last_Error | tr "'" "X" | sed 's/X//g'`

		if [ "$WithSMS" = "" ] || [ "$WithSMS" = "True" ]
			then
				Message="DB Replica problem: Seconds_Behind_Master:$Seconds_Behind_Master, Error:$ErrorDesc"
				mysql -N -hCNPVAS01 -uWriter -pWriter -e"call Send_SMS('Replication','Replication','$Message',(select group_stats.PSD()))" group_stats
		fi
		
fi
