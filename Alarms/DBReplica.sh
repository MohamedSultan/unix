######################################## Check that the CNPVAS01 DB is alive  #####################
#! /usr/bin/ksh
cd /app/Alarms

Error_Exists=`grep "slave SQL thread aborted" /database/MySQL_Data/DataBase_Error.err | wc -l | tr -d [:space:]`
Started=`grep "Slave SQL thread initialized," /database/MySQL_Data/DataBase_Error.err | wc -l | tr -d [:space:]`
Time=`/app/CurDateHour | tr -d [:space:] `

App=$0

if [ $Error_Exists -eq 0 ] 
	then	
		echo  "Replica Works fine" 
	else
		echo Slave Down 
		mysql -N -hActiveDB -uroot -p'Sultan(999' -e"call Send_SMS('Replication','Replication','Slave Is Down',(select group_stats.PSD()))" group_stats
		#cp  /database/MySQL_Data/DataBase_Error.err  /database/MySQL_Data/DataBase_Error.err_$Time
		>/database/MySQL_Data/DataBase_Error.err
fi		

if [ $Started -eq 0 ]
        then
                echo  "Kollo Tamam"
        else
                echo Slave Up
                mysql -N -hActiveDB -uroot -p'Sultan(999' -e"call Send_SMS('Replication','Replication','Slave Is Up',(select group_stats.PSD()))" group_stats
                #cp  /database/MySQL_Data/DataBase_Error.err  /database/MySQL_Data/DataBase_Error.err_$Time
                >/database/MySQL_Data/DataBase_Error.err
fi
