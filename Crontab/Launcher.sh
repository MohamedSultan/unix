
#Extracts the now Crontab events and run it
#------------------------------------------

HostName=`hostname`
Rand=$RANDOM
FileName=/share/Crontab/$Rand"_"$HostName
Count=`mysql -hCNPVAS03 -P3307 -uReader -pReader -e"SELECT ifnull(COUNT(*),0) Counts from Main_Conf.Crontab_With_Validity WHERE Active=1 and HostName='$HostName' and Validity=1" | tail -1`
Count=`expr "$Count" + 0`
Command="SELECT @row := @row + 1 AS ID,Command,\`Service Name\` FROM Main_Conf.Crontab_With_Validity, (SELECT @row := 0) r WHERE Active=1 and HostName='$HostName' and Validity=1 INTO OUTFILE '$FileName' FIELDS TERMINATED BY '!'"
mysql -hCNPVAS03 -P3307 -uReader -pReader -e"$Command"

cp $FileName Y
cat $FileName

Counter=0
echo "Count="$Count
while [ $Counter -lt $Count ]
	do
	Counter=$(($Counter+1))
	Line=$Counter
	ID=`head -$Line $FileName | tail -1 | cut -d '!' -f1`
	Command=`head -$Line $FileName | tail -1 | cut -d '!' -f2`
	ServiceName=`head -$Line $FileName | tail -1 | cut -d '!' -f3`


	echo "Running $Command"	
	ksh CommandExecuter.sh "$Command" "$ServiceName" &
	Script_Exporters=`ps -ef | grep Script_Exporter.sh | wc -l | sed 's/^ *//g'`
	Command1="REPLACE INTO Loggers.Crontab(ServiceName,Type,Description,HostName,Script_Exporter_Count) VALUES('$ServiceName','Info','$Command','$HostName','$Script_Exporters')"
	mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"$Command1" &
	sleep 1
done

rm $FileName
