#/app/LastHour 
DateHour=$1
#CurrentDir=`pwd`
echo $DateHour
Command="cat /share/Report_CDR_CF/Report_CDR_CF_ss7gw2_$DateHour.cdr | cut -d"," -f1,2,3,5,6,9,10,11,12,13,14,15,16,17,18,19 | tr ',' '|' | awk 'BEGIN {FS=\"|\";OFS=\"|\"} {print substr(\$1,1,4)\"-\"substr(\$1,5,2)\"-\"substr(\$1,7,2),substr(\$2,1,2),\$3,\$4,\$5,\$6,\$7,\$8,\$9,\$10,\$11,\$12,\$13,\$14,\$15,\$16}' | sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/'"

echo $Command
ssh cdr2ftp@172.21.10.12 "$Command" > /share/Importers/Call_Filteration/ss7_$DateHour
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/Call_Filteration/ss7_$DateHour" CNPVAS03 CDR_Cf_Calls "Count,Date,Hour,Gateway,Call_Type,Service_Code,MSC_gt,Action,Action1,Action2,Action3,Service_Type,Call_Status,SMS_Status,List_Type_ID,Package_ID" "|" CallFilteration CF_ss7 ""

Command="cat /share/Report_CDR_CF/Report_CDR_CF_cfstats1_$DateHour.cdr | cut -d "," -f1,2,3,5,6,11,15,19 | tr ',' '|' | awk 'BEGIN {FS=\"|\";OFS=\"|\"} {print substr(\$1,1,4)\"-\"substr(\$1,5,2)\"-\"substr(\$1,7,2),substr(\$2,1,2),\$3,\$4,\$5,\$6,\$7,\$8}'| sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/'"

echo $Command
ssh cdr2ftp@172.21.10.12 "$Command" > /share/Importers/Call_Filteration/DB_$DateHour
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/Call_Filteration/DB_$DateHour" CNPVAS03 CDR_Database_Trans "Count,Date,Hour,Node,Call_Type,Service_Type,Call_Desc,Channel,Status" "|" CallFilteration CF_Database "" 


