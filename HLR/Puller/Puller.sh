if [ $1 ]
        then
               date=`echo $1 | cut -c1-8`
               Hour=`echo $1 | cut -c9-10` 
        else
       		echo "Date is not passed, last hour is assumed" 
                date=`ksh /app_univ/Date_Scripts/LastHour | cut -c1-8`
                Hour=`ksh /app_univ/Date_Scripts/LastHour | cut -c9-10`
fi

scp msultan@HLR:/export/home/msultan/stats/PM_XML/C$date.$Hour*ndsrds* /share/CSDB/NDS_RDS
scp msultan@HLR:/export/home/msultan/stats/PM_XML/C$date.$Hour*hlrfe*  /share/CSDB/HLR_FE
scp msultan@HLR:/export/home/msultan/stats/PM_XML/C$date.$Hour*ndspgw* /share/CSDB/NDS_PGW
scp msultan@HLR:/export/home/msultan/stats/PM_XML/C$date.$Hour*ndsbe*  /share/CSDB/NDS_BE

find /share/CSDB/ -type f -mtime +4 -exec rm -f '{}' \;
