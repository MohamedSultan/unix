FilePath=$1
Rand=$RANDOM
ProcessingsDir=/app_univ/Services/HLR/Parser/Processings/

cd $ProcessingsDir

/usr/xpg4/bin/grep -e '<measTypes>' -e '<measResults>' -e 'ArrayIdWithIndex' $FilePath  | tr '\n' '|' | tr -d '\"' > $Rand
echo "" >> $Rand
mv $Rand Temp_$Rand
cat Temp_$Rand | sed 's/\&quot;/%/g'| perl -pe 's{(%[%\|]+%)}{($x=$1)=~s/ /_/g;$x}ge' | sed 's/<measTypes>/;/g;s/%//g'| tr ';' '\n' | sed 's/<\/measTypes>//g;s/<measResults>//g;s/<\/measResults>|//g'> $Rand
rm Temp_$Rand

>Import_$Rand
while read Measures
do
	if [ "$Measures" != "" ]
		then
			MeasureType=`echo $Measures | cut -d"|" -f1`
                        MeasureResult=`echo $Measures | cut -d"|" -f2`
			FieldCount=`echo $MeasureType| tr ' ' '\n' | wc -l | sed 's/^ *//g'`
			echo $FieldCount		
set -A FieldNames $MeasureType
set -A FieldValues $MeasureResult
			Fields=1
                        while [ Fields -le $FieldCount ]
				do
					Fields=`expr $Fields + 1`
					echo ${FieldNames[$Fields]}"|"${FieldValues[$Fields]} >> Import_$Rand
			done
	fi
done < $Rand

echo $Rand
