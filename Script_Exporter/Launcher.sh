
Rand=$RANDOM
ScriptDir=$1
ScriptName=$2
HostName=`hostname`

if [ "$ScriptName" = "ksh" ]
	then
		ScriptName=$3
fi

Parm1=$3
Parm2=$4
Parm3=$5
Parm4=$6
Parm5=$7
Parm6=$8
Parm7=$9
Parm8=${10}
Parm9=${11}
Parm10=${12}
Parm11=${13}
Parm12=${14}
Parm13=${15}
Parm14=${16}
Parm15=${17}
Parm16=${18}
Parm17=${19}
Parm18=${20}
Parm19=${21}
Parm20=${22}

DataDir=/share/Exporters/Script_Generator/
OutFile="$DataDir/$Rand"

if [ -e "$OutFile""_" ]
	then
		rm "$OutFile""_"
fi

Command="select \`Script Body\` from UNIX_Scripts WHERE \`Script Name\`='$ScriptName' and (\`Script Path\`='$ScriptDir' OR left(\`Script Path\`,length(\`Script Path\`)-1)='$ScriptDir') LIMIT 1 into outfile '$OutFile"_"' FIELDS ESCAPED BY '""' TERMINATED BY ',' LINES TERMINATED BY '\n' "
#echo $Command
if [ "$HostName" = "cnpvas04" ]
	then
		mysql --login-path=Reader -P3307 -hcnpvas03 -e"$Command" References
	else
		mysql -uReader -pReader -P3307 -hcnpvas03 -e"$Command" References
fi

if [ -s "$OutFile""_" ] 
	then
#		echo "find the script"
#		ls -lrt $OutFile
		dos2unix $OutFile"_" > $OutFile 2>/dev/null
		rm $OutFile"_" 
		#echo "executing: $OutFile"
		#cat $OutFile
		ksh $OutFile "$Parm1" "$Parm2" "$Parm3" "$Parm4" "$Parm5" "$Parm6" "$Parm7" "$Parm8" "$Parm9" "$Parm10" "$Parm11" "$Parm12" "$Parm13" "$Parm14" "$Parm15" "$Parm16" "$Parm17" "$Parm18" "$Parm19" "$Parm20"

		#echo $OutFile
	else
		echo "Didn't find script"
fi
rm $OutFile
