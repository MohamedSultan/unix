cd /app_univ/Services/SDP/SOLARIS/ 

ProcessingDir=/app_univ/Services/SDP/SOLARIS/Processing

Importer=/app_univ/StatsCollector/Importer.sh

Importing_Path=/share/Importers/SDP/SOLARIS/SC

#----Check if there are any processes----#

No_Process=`ps -ef | grep SDP | grep SDP_ServiceClass_SOLARIS | grep -v grep | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do
     echo `date`" Found another running Process, Exiting Process"
     exit
done

#-----Listing all files-----#
for FileName in `ls *.stat`
do

        echo "Removing Empty Files"
        find . -size 0 -exec rm -f {} \;

        echo "Processing File $FileName"
 	lines_count=`cat $FileName | grep -v 'Date' | wc -l`        
	Date=`cat $FileName | head -n 2 | tail -1 | cut -d"," -f1 | awk '{print substr($0,1,10)}'`
        Hour=`cat $FileName | head -n 2 | tail -1 | cut -d"," -f1 | awk '{print substr($0,12,2)}'`
        Day=`echo $Date | awk '{print substr($0,9,2)}'`
        Month=`echo $Date | awk '{print substr($0,6,2)}'`
        SDP=`echo $FileName | awk -F"_" '{print $1}'`		
	
	#-- Getting Header --#
        cat $FileName | head -1 | tr ',' '\n' | tail +3  > $ProcessingDir/$FileName.Header.Temp 
	Header_Count=`cat $ProcessingDir/$FileName.Header.Temp | wc -l`	
	
	#-- Getting Values --#
 	Values_Count=`cat $FileName | grep -v 'Date' | cut -d "," -f 3- | tr ',' '\n' | wc -l`
	cat $FileName | grep -v 'Date' | cut -d "," -f 3- | tr ',' '\n' > $ProcessingDir/$FileName.Values
	
	#-- Updating Header File --#
	Counter=1
	while [[ $Counter -le $lines_count ]]
        do
            cat $ProcessingDir/$FileName.Header.Temp >> $ProcessingDir/$FileName.Header
            Counter=$(( $Counter + 1 ))
        done     	
	
	#-- Getting ID --#	
	cat $FileName | grep -v 'Date' | cut -d"," -f2 > $ProcessingDir/$FileName.ID.Temp 
 	touch $ProcessingDir/$FileName.ID.Temp1
	Counter=1
	while [[ $Counter -le $Header_Count ]]
	do 
		paste -d"," $ProcessingDir/$FileName.ID.Temp $ProcessingDir/$FileName.ID.Temp1 > $ProcessingDir/$FileName.ID.Temp2
		mv $ProcessingDir/$FileName.ID.Temp2 $ProcessingDir/$FileName.ID.Temp1  
		Counter=$(( $Counter + 1 )) 
	done

 	cat $ProcessingDir/$FileName.ID.Temp1 | tr ',' '\n' | grep -v '^$' > $ProcessingDir/$FileName.ID

	paste -d"," $ProcessingDir/$FileName.ID $ProcessingDir/$FileName.Values > $ProcessingDir/$FileName.Values.ID
	paste -d"," $ProcessingDir/$FileName.Values.ID $ProcessingDir/$FileName.Header | grep -v ',0,' | awk -F"," '{print Date_","Hour_","$0","SDP_","Day_","Month_}' Date_=$Date Hour_=$Hour SDP_=$SDP Day_=$Day Month_=$Month > $Importing_Path/$FileName
        
	#-- Importing Data to DB --#
        echo "Importing to DB"  
        ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "$Importing_Path/$FileName" CNPVAS03 SDP_ServiceClass_Solaris "Date,Hour,ID,Value,Counter,SDP,Day,Month" "," SDP SDP_SC_Solaris
         
        #-- Removing the Processed files --#
        echo "Removing files"  
        rm -f $ProcessingDir/$FileName* 
        mv -f $FileName /share/FTP/SDP_Per/Parsed/SOLARIS/ServiceClass/

done

