cd /share/FTP/SDP_Per/SDP_statistics/SOLARIS/System

Importer=/app_univ/StatsCollector/Importer.sh

Importing_Path=/share/Importers/SDP/SOLARIS/System

#----Check if there are any processes----#

No_Process=`ps -ef | grep SDP_System_SOLARIS.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do

    echo `date`" Found another running Process, Waiting Process"
    sleep 10
    No_Process=`ps -ef | grep SDP_System_SOLARIS.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
    No_Process=$(($No_Process+0))

done

#-----Listing all files-----#
for FileName in `ls *.stat`
do
 	echo "Removing Empty Files"
        find . -size 0 -exec rm -f {} \;	
	
	echo "Processing File $FileName"
	lines_count=`cat $FileName | wc -l`
	Counters=`cat $FileName | head -1`
	Counters_count=`echo $Counters | awk -F"," '{print NF}'`
	SDP=`echo $FileName | awk -F"_" '{print $1}'`
	echo $SDP

	Record_counter=2

#-----Loop for each record-----#
	while [[ $Record_counter -le $lines_count ]]
	do

		count=2	
		Record=`cat $FileName | head -n $Record_counter | tail -1`
		Date=`echo $Record | cut -d"," -f1 | awk '{print substr($0,1,10)}'` 
		Hour=`echo $Record | cut -d"," -f1 | awk '{print substr($0,12,2)}'` 

#-----Loop for each counter and building the new file-----#
		while [[ $count -le $Counters_count ]]
		do
		
			Value=`echo $Record | cut -d"," -f$count`
			Countername=`echo $Counters | cut -d"," -f$count`	
	        	FinalRecord=`echo $Date","$Hour","$Value","$Countername","$SDP`	
		
			echo $FinalRecord >> $Importing_Path/$FileName

			count=$(( $count + 1 ))
			
		done			

			Record_counter=$(( $Record_counter + 1 ))

	done
	
#-----Importing the parsed file to the DB-----#
	echo "Importing to DB"
        ksh $Importer "$Importing_Path/$FileName" CNPVAS03 SDP_System_Solaris "Date,Hour,Value,Counter,SDP" "," SDP SDP_STATS_System_Solaris
	
	mv -f $FileName /share/FTP/SDP_Per/Parsed/SOLARIS/System/$FileName  

done

