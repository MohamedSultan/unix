cd /log/FTP/SDP_Per/Linux_SDPs 
Importer=/app_univ/StatsCollector/Importer.sh

for FileName in `ls *.log`
do
        echo $FileName
#       	cp $FileName /log/FTP/SDP_Per/Pendings 
	mv  $FileName $FileName.Processings
        cp $FileName.Processings ../Processings/$FileName

	echo "Processing Physical Memory Top command for file: "$FileName

	sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Shwing|processes,load,Memory|status|/','/|Swap|/p' | grep -h '|Mem|'| sed 's/||/|/g' | cut -d"|" -f1,2,3,7,9,11,15 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7}' > /share/Importers/SDP/MemTop_Physical_$FileName

	echo "Importing Physical Memory Data"
        ksh $Importer "/share/Importers/SDP/MemTop_Physical_$FileName" CNPVAS03 MemoryTop_Physical_Linux "Date,SDP,Hour,PhysicalTotal,PhysicalInUse,PhysicalFree" "|" SDP SDP_Performance


	echo "Processing Swap Memory Top command for file: "$FileName

        sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Shwing|processes,load,Memory|status|/','/|Swap|/p' | grep -h '|Swap|'| sed 's/||/|/g' | cut -d"|" -f1,2,3,7,9,11,15 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7}' > /share/Importers/SDP/MemTop_Swap_$FileName

        echo "Importing Swap Memory Data"
        ksh $Importer "/share/Importers/SDP/MemTop_Swap_$FileName" CNPVAS03 MemoryTop_Swap_Linux "Date,SDP,Hour,SwapTotal,SwapInUse,SwapFree" "|" SDP SDP_Performance
	
	echo "Processing CPU for file: "$FileName
	# sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|CPU|usage|/,/Showing|memory|usage|/p'|grep -v 'Showing|CPU|usage|' | grep -v 'Showing|memory|usage' | grep -v '|Linux|'|grep -v '|%user|%nice|%system|%iowait|%steal|%idle'|cut -d"|" -f1,2,3,10,11,12,13,14,15,16|awk '{FS="|";OFS="|"}{if($4 != "") {print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9}}' > /share/Importers/SDP/CPULinux_$FileName

	sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|CPU|usage|/,/Showing|memory|usage|/p' | grep -v 'Showing|CPU|usage|' | grep -v 'Showing|memory|usage|' | head -4 | tail -1 | cut -d"|" -f1,2,3,10,11,12,13,14,15 | awk '{FS="|";OFS="|"}{if($4 != "") {print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9}}' > /share/Importers/SDP/CPULinux_$FileName


	echo "Importing CPU Data"
	ksh $Importer "/share/Importers/SDP/CPULinux_$FileName" CNPVAS03 CPU_Linux "Date,SDP,Hour,user,nice,system,iowait,steal,idle" "|" SDP SDP_Performance

	echo "Processing Disk Space for file: "$FileName
        sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|diskspace|utilization|/,/Showing|CPU|usage|/p' | grep -v 'Showing|diskspace|utilization|' | grep -v 'Showing|CPU|usage|' | cut -d"|" -f1,2,3,6,7,8,9,10,11 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9}' > /share/Importers/SDP/DiskSpaceLinux_$FileName

        echo "Importing DiskSpace Data" 
        ksh $Importer "/share/Importers/SDP/DiskSpaceLinux_$FileName" CNPVAS03 Disk_Space "Date,SDP,Hour,Filesystem,Size,Used,Available,Capacity,MountedOn" "|" SDP "SDP_Performance"

	mv $FileName.Processings ../Parsed/$FileName
        rm  ../Processings/$FileName

done
