cd /share/FTP/SDP_Per/SDP_statistics/REDHAT/ServiceClass

ProcessingDir=/app_univ/Services/SDP/REDHAT/Processing

Importer=/app_univ/StatsCollector/Importer.sh

Importing_Path=/share/Importers/SDP/REDHAT/SC

#----Check if there are any processes----#
No_Process=`ps -ef | grep SDP_ServiceClass_REDHAT.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do

    echo `date`" Found another running Process, Waiting Process"
    sleep 10
    No_Process=`ps -ef | grep SDP_ServiceClass_REDHAT.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
    No_Process=$(($No_Process+0))

done

#-----Listing all files-----#
for FileName in `ls *.stat`
do
	echo "Removing Empty Files"
        find . -size 0 -exec rm -f {} \;
        
	echo "Processing File $FileName"
	lines_count=`cat $FileName | wc -l`
	SDP=`echo $FileName | awk -F"_" '{print $1}'`

	cat $FileName | head -1 | tr ',' '\n' | tail +3  > $ProcessingDir/$FileName.Header 

 	Record_counter=2	

	while [[ $Record_counter -le $lines_count ]]
        do
		Date=`cat $FileName | head -n $Record_counter | tail -1 | cut -d"," -f1 | awk '{print substr($0,1,10)}'`
		Hour=`cat $FileName | head -n $Record_counter | tail -1 | cut -d"," -f1 | awk '{print substr($0,12,2)}'`
		Day=`echo $Date | awk '{print substr($0,9,2)}'`
		ID=`cat $FileName | head -n $Record_counter | tail -1 | cut -d"," -f2`		
		
		cat $FileName | head -n $Record_counter | tail -1 | tr ',' '\n' | tail +3 > $ProcessingDir/$FileName.$Record_counter 

		paste -d"," $ProcessingDir/$FileName.Header $ProcessingDir/$FileName.$Record_counter | awk -F"," '{print Date_","Hour_","ID_","$1","$2","Day_","SDP_}' Date_=$Date Hour_=$Hour ID_=$ID Day_=$Day SDP_=$SDP | grep -v ',0,' >> $Importing_Path/$FileName 

		rm -f $ProcessingDir/$FileName.$Record_counter	
		
		Record_counter=$(( $Record_counter + 1 ))	


	done	

 	#-- Importing Data to DB --#
        echo "Importing to DB"  
       	ksh $Importer "$Importing_Path/$FileName" CNPVAS03 SDP_ServiceClass_Redhat "Date,Hour,ID,Counter,Value,Day,SDP" "," SDP SDP_SC_Redhat
         
	#-- Removing the Processed files --#
        echo "Removing files"  
        rm -f $ProcessingDir/$FileName* 
	mv -f $FileName /share/FTP/SDP_Per/Parsed/REDHAT/ServiceClass/$FileName

done
