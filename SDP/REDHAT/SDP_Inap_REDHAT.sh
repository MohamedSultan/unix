cd /share/FTP/SDP_Per/SDP_statistics/REDHAT/Inap

Importer=/app_univ/StatsCollector/Importer.sh

Importing_Path=/share/Importers/SDP/REDHAT/Inap

#----Check if there are any processes----#

No_Process=`ps -ef | grep SDP_Inap_REDHAT.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do
     echo $No_Process
     echo `date`" Found another running Process, Waiting Process"
     sleep 10
     No_Process=`ps -ef | grep SDP_Inap_REDHAT.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
     No_Process=$(($No_Process+0))

done

#-----Listing all files-----#
for FileName in `ls *.log`
do
	echo "Removing Empty Files"
        find . -size 0 -exec rm -f {} \;

	echo "Processing File $FileName"

	Date=`echo $FileName | cut -d"_" -f3 | awk '{print substr($0,1,4)"-"substr($0,5,2)"-"substr($0,7,2)}'`
	Hour=`echo $FileName | cut -d"_" -f4 | awk '{print substr($0,1,2)}'`	
	SDP=`echo $FileName | cut -d"_" -f1`

	#-- Processing "First,Intermediate,Final" --#	
	cat $FileName | grep -v tmp | grep -v Name | grep -v Total | grep -v Max | grep -v 'Regulated First' | grep -v FI | grep -v FR | grep -v II | sed 's/  */ /g;s/^ //g' | sed 's/ /|/g' | sed 's/-/0/g' | grep -v '^$' | awk -F"|" '{print Date_","Hour_","$1","$2","$3","$4","$5","$6","$7","SDP_}' Date_=$Date Hour_=$Hour SDP_=$SDP > $Importing_Path/$FileName

 	#-- Processing "FI,FR,II" --#
        cat $FileName | /usr/xpg4/bin/grep -e 'Regulated First' -e 'FI' -e 'FR' -e 'II' | sed 's/  */ /g;s/^ //g' | sed 's/ /|/g' | sed 's/-/0/g' | awk -F"|" '{print Date_","Hour_","$1$2","$3","$4","$5","$6","$7","$8","SDP_}' Date_=$Date Hour_=$Hour SDP_=$SDP >> $Importing_Path/$FileName

	#-- Processing "Total" --#
        cat $FileName | /usr/xpg4/bin/grep -e 'Total' | sed 's/  */ /g;s/^ //g' | sed 's/ /|/g' | sed 's/-/0/g' | awk -F"|" '{print Date_","Hour_","$2","$3","$4","$5","$6","$7","$8","SDP_}' Date_=$Date Hour_=$Hour SDP_=$SDP >> $Importing_Path/$FileName 
	
	#-- Importing Data to temp table --#
	ksh $Importer "$Importing_Path/$FileName" CNPVAS03 SDP_Inap_Redhat_Temp "Date,Hour,Name,Successful,Failed,Throput_s,Avg_Response_Time,Min_Response_Time,Max_Response_Time,SDP" "," SDP SDP_Inap_REDHAT "" 1

	mv -f $FileName /share/FTP/SDP_Per/Parsed/REDHAT/Inap/$FileName
	
	Command="call SDP_Inap_REDHAT_Agg()"
	mysql -hCNPVAS03 -P3306 -uWriter -pWriter -e"$Command" SDP

done

