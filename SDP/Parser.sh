cd /log/FTP/SDP_Per/Pendings
Importer=/app_univ/StatsCollector/Importer.sh
echo "hello"
for FileName in `ls *.log`
do
        echo $FileName
        mv -f $FileName $FileName.Processings
        cp $FileName.Processings ../Processings/$FileName
	#rm $FileName
        #exit
        echo "Showing diskspace utilization" >> ../Processings/$FileName
        echo "Processing Disk Space for file: "$FileName
        sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|diskspace|utilization|/,/Showing|CPU|usage|/p' | grep -v 'Showing|diskspace|utilization|' | grep -v 'Showing|CPU|usage|' | cut -d"|" -f1,2,3,6,7,8,9,10,11 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9}' > /share/Importers/SDP/DiskSpace_$FileName

#        command="LOAD DATA INFILE '/share/Importers/SDP/DiskSpace_$FileName' REPLACE INTO TABLE Disk_Space FIELDS TERMINATED BY '|' (Date,SDP,Hour,Filesystem,Size,Used,Available,Capacity,MountedOn)"
	
        echo "Importing DiskSpace Data" 
	ksh $Importer "/share/Importers/SDP/DiskSpace_$FileName" CNPVAS03 Disk_Space "Date,SDP,Hour,Filesystem,Size,Used,Available,Capacity,MountedOn" "|" SDP "SDP_Performance"

	echo "Processing CPU Usage for file: "$FileName
	sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|CPU|usage|/,/Showing|memory|usage|/p' | grep -v 'Showing|CPU|usage|' | grep -v 'SunOS'|grep -v 'Showing|memory|usage|' | grep -v '%usr|%sys|%wio|%idle' | cut -d"|" -f1,2,3,9,10,11,12 | awk '{FS="|";OFS="|"}{if($4 != "") {print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9,$10}}' > /share/Importers/SDP/CPU_$FileName

#	command="LOAD DATA INFILE '/share/Importers/SDP/CPU_$FileName' REPLACE INTO TABLE CPU FIELDS TERMINATED BY '|' (Date,SDP,Hour,usr,sys,wio,idle)" 
	ksh $Importer "/share/Importers/SDP/CPU_$FileName" CNPVAS03 CPU "Date,SDP,Hour,usr,sys,wio,idle" "|" SDP SDP_Performance
	

	echo "Importing CPU Data"
#	mysql -uWriter -pWriter -hCNPVAS03 -e"$command" SDP

	echo "Processing Memory for file: "$FileName
	sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Showing|memory|usage|/','/Shwing|processes|info|/p' | grep -v 'Showing|memory|usage|' | grep -v 'Shwing|processes|info|' | grep -v 'kthr|memory|page|disk|faults|cpu' | grep -v 'swap|free|re|mf|pi|po|fr|de|sr|s1|s2|s3|s4|in|sy|cs|us|sy|id'| cut -d "|" -f1,2,3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25}' > /share/Importers/SDP/Mem_$FileName

#	command="LOAD DATA INFILE '/share/Importers/SDP/Mem_$FileName' REPLACE INTO TABLE Memory FIELDS TERMINATED BY '|' (Date,SDP,Hour,r,b,w,swap,free,re,mf,pi,po,fr,de,sr,s1,s2,s3,s4,in_,sy1,cs,us,sy2,id)"
	
	echo "Importing Memory Data"
	ksh $Importer "/share/Importers/SDP/Mem_$FileName" CNPVAS03 Memory "Date,SDP,Hour,r,b,w,swap,free,re,mf,pi,po,fr,de,sr,s1,s2,s3,s4,in_,sy1,cs,us,sy2,id" "|" SDP SDP_Performance 
#	mysql -uWriter -pWriter -hCNPVAS03 -e"$command" SDP

#	mv $FileName.Processings ../Parsed/$FileName
#	rm  ../Processings/$FileName

	echo "Processing Memory Top command for file: "$FileName 
	sed "s/  */|/g;s/:/|/g;s/\[//g;s/\]//g" ../Processings/$FileName | sed -n '/Shwing|processes,load,Memory|status|/','/|Memory|/p' | grep -v '|Shwing|processes,load,Memory|status|' | grep -v '|load|averages|' | grep -v '|processes|'| sed 's/||/|/g' | cut -d"|" -f1,2,3,7,9,11,15 | awk '{FS="|";OFS="|"}{print substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$1,$3,$4,$5,$6,$7}' > /share/Importers/SDP/MemTop_$FileName

#	command="LOAD DATA INFILE '/share/Importers/SDP/MemTop_$FileName' REPLACE INTO TABLE MemoryTop FIELDS TERMINATED BY '|' (Date,SDP,Hour,PhysicalReal,PhysicalFree,SwapInUse,SwapFree)" 
	
	echo "Importing Memory Data"
	ksh $Importer "/share/Importers/SDP/MemTop_$FileName" CNPVAS03 MemoryTop "Date,SDP,Hour,PhysicalReal,PhysicalFree,SwapInUse,SwapFree" "|" SDP SDP_Performance 
#	mysql -uWriter -pWriter -hCNPVAS03 -e"$command" SDP

	mv -f $FileName.Processings ../Parsed/$FileName
        rm  ../Processings/$FileName

done
