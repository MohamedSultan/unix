#!/usr/bin/ksh

Rand=$RANDOM
#mv "/share/CCN_Data/STATISTICS/"$1"/PMF_Files/CcnCounters/"*CcnMeasureJob /share/CCN_X_Data/$1/CcnCounters/
while [ `ls -rlt  /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/*jambala_CcnCounters | wc -l | sed 's/^ *//g'` -ge 2 ]
	do
	echo "Starting Parsing of "$1
	ls /share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/*jambala_CcnCounters > files_$Rand 
	while read FN
		do
		FileName="/share/CCN_Data/STATISTICS/$1/PMF_Files/CcnCounters/$FN" 
		FileName=$FN
		echo "Parsing File:"$FileName
		From=`echo $FN | cut -d"." -f2 | cut -d"-" -f1`
		To=`echo $FN | cut -d"." -f2 | cut -d"-" -f2 | cut -d"_" -f1`

		Length=`echo $FN | cut -d"." -f1 | wc -c | sed 's/^ *//g'`
		Start=`expr $Length - 8` 
		Date=`echo $FN | cut -d"." -f1 | cut -c$Start-$Length`
#		echo "Date:"$Date

		#/usr/xpg4/bin/grep -e moid -e \<r\> $FileName | tr '\n' '%' > $Rand
		awk '/_SYSTEM/{print;getline;print;}' $FileName | tr '\n' '%' > $Rand
		echo "" >> $Rand
		sed 's/<moid>/^/g' $Rand | tr '^' '\n' | sed 's/CcnCounters=//g' | sed 's/<\/moid>%<r>/,/g' | sed 's/<\/r>%//g' | sed 's/ Source = //g' | tail +2 | awk '{OFS=","} { print substr(Date_,7,2),CCNID,substr(Date_,1,4)"-"substr(Date_,5,2)"-"substr(Date_,7,2),From_,To_,$0}' Date_=$Date From_=$From To_=$To CCNID=$1 >> /log/Processings/CCN/CCN_Counters/$Date
		rm $Rand
		
		mkdir /share/CCN_X_Data/$1/CcnCounters/ 2>/dev/null
		mkdir -p /share/CCN_X_Data/$1/CcnCounters/
		mv -f $FileName /share/CCN_X_Data/$1/CcnCounters/
		echo "file is parsed"
	done < files_$Rand
done
rm  files_$Rand

