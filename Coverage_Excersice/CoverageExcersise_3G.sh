Date=$1

FilePath_3G=/reg_share/iFiles/TestFeeds/3G/
ParsedPath=/reg_share/iFiles/TestFeeds/Parsed/Location_Update

Date_Formatted=`echo $Date | awk '{print substr($0,6,2)""substr($0,9,2)""substr($0,1,4)}'`

#--Processing_3G--# 
cd $FilePath_3G

for FileName in `ls hdr.$Date_Formatted*.lba`
do
        echo "Processing file "$FileName
       
	FileName_Formatted=`echo $FileName | awk '{print substr($0,1,length($FileName) - 4)}'`
	
	bia_data_Reader $FileName | sed 's/ //g' | /usr/xpg4/bin/grep -e"StartTimeinstringformat:" -e"IMSI:" -e"MobilityInfoIuLac:" -e"MobilityInfoIuSac:" -e"IMEI:" -e"IT7_Variable::DataId:" -e"IT7_Variable::CallType:" -e"IT7_Variable::DataField:" | sed 's/StartTimeinstringformat:/Y/g' | tr '\n' '|' | tr 'Y' '\n' | /usr/xpg4/bin/grep -e "|IT7_Variable::DataId:313|" -e"|IT7_Variable::CallType:2093|" | sed 's/IMSI://g;s/IMEI://g;s/MobilityInfoIuLac://g;s/MobilityInfoIuSac://g;s/IT7_Variable::DataId://g;s/IT7_Variable::CallType://g;s/IT7_Variable::DataField://g;s/\[//g;s/\]//g' | grep -v "Notyetpopulated." > /reg_share/iFiles/TestFeeds/Parsed/Location_Update/$FileName_Formatted

	cat /reg_share/iFiles/TestFeeds/Parsed/Location_Update/$FileName_Formatted | grep "fffe" > /reg_share/iFiles/TestFeeds/Parsed/Bad_Coverage/$FileName_Formatted".BadCoverage"
	
	rm -f $FileName
done
