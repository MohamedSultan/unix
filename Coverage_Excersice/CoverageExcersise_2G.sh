Date=$1

FilePath_2G=/reg_share/iFiles/TestFeeds/2G

Date_Formatted=`echo $Date | awk '{print substr($0,6,2)""substr($0,9,2)""substr($0,1,4)}'`

#--Processing_2G--# 
cd $FilePath_2G

for FileName in `ls hdr.$Date_Formatted*`
do
        echo "Processing file "$FileName
       
	FileName_Formatted=`echo $FileName | awk '{print substr($0,1,17)".dat"}'`

	bia_data_Reader $FileName | sed 's/ //g;s/VariableField#/X/g' | /usr/xpg4/bin/grep -e"startTimeStampinstringformat:" -e"IMSIasastring:" -e"LastCellLac:" -e"CurrentCellId:" -e"IT7_Variable::DataId:" -e"IT7_Variable::CallType:" -e"IT7_Variable::DataField:" | sed 's/startTimeStampinstringformat:/Y/g;' | tr '\n' 'Z'| tr 'Y' '\n' | sed 's/Z//g' | sed 's/ADR::/|/g;s/IMSIasastring:/|/g;s/LastCellLac:/|/g;s/CurrentCellId:/|/g;s/IT7_Variable::DataId:/|/g;s/IT7_Variable::CallType://g;s/IT7_Variable::DataField://g;s/\[//g;s/\]//g' | grep "|1109772" | awk -F"|" '{print substr($1,7,4)"-"substr($1,1,2)"-"substr($1,4,2)"|"substr($1,11,2)"|"$2"|"$3"|"$4"|"$5"|"$6"|"$7"|"$8"|"$9"|""2G"}' | sort -t"|" -k3 > /app_univ/Services/Coverage_Excersice/2G/Parsed/$FileName_Formatted

	join -t"|" -j1 3 -j2 1 -a1 -o 1.1 1.2 1.3 2.2 2.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 /app_univ/Services/Coverage_Excersice/2G/Parsed/$FileName_Formatted /app_univ/Services/Coverage_Excersice/Customers_Data | awk -F"|" '{print $1"|"$2"|"$3"|"substr($4,1,length($4)-7)"|"$5"|"$6"|"$7"|"$8"|"$9"|"$10"|"$11"|"$12"|"$13}' > /share/Importers/Coverage_Excercise/2G/$FileName_Formatted 

	#ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/Coverage_Excercise/2G/$FileName_Formatted" CNPVAS03 Coverage_Excercise_RawData "Date,Hour,IMSI,TAC,Customer_Segment,LAC,Cell,Parameter1,Parameter2,Parameter3,Parameter4,Parameter5,Call_Type" "|" Coverage_Excercise Coverage_Excercise_2G

	mv $FileName $FileName".Parsed"
done
