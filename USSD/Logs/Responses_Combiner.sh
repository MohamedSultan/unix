
DateTime=$1
OldRestrival=$2

Rand_Import=$RANDOM

Date=`echo $DateTime | cut -c1-4`"-"`echo $DateTime | cut -c5-6`"-"`echo $DateTime | cut -c7-8`
Hour=`echo $DateTime | cut -c9-10`

echo `date`" Creating Table Of $DateTime"
Command="CREATE TABLE Responses_Pending_$DateTime (Date VARCHAR(11) NOT NULL,Hour VARCHAR(2) NOT NULL,Service VARCHAR(20) NOT NULL,Response VARCHAR(300) NOT NULL DEFAULT '0' COLLATE 'utf8_general_ci',Count DOUBLE NULL DEFAULT NULL,Neglect VARCHAR(11) NULL DEFAULT 'NEW',Day INT(11) NOT NULL DEFAULT '0',Month INT(11) NOT NULL DEFAULT '0',PRIMARY KEY (Date, Hour, Service, Response, Day, Month)) COLLATE='latin1_swedish_ci' ENGINE=MyISAM ROW_FORMAT=DYNAMIC"
mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD

echo `date`" Loading Aggregated Response data Of $DateTime"
if [ "$OldRestrival" = "" ]
	then
		echo "Importing New Data"
		ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/USSD/Responses_Agg_$DateTime" CNPVAS03 Responses_Pending_$DateTime "Count,Day,Month,Service,Date,Hour,Response" "|" USSD USSD_Responses "" "$Rand_Import"
	else
		echo "retriving data from Responses table"
		Command="REPLACE INTO Responses_Pending_$DateTime (Count,Day,Month,Service,Date,Hour,Response,Neglect) SELECT sum(count),Day,Month,Service,Date,Hour,Response,Neglect FROM Responses WHERE date='$Date' and month=Month('$Date') and day=Day('$Date') and hour='$Hour' GROUP BY Day,Month,Service,Date,Hour,Response,Neglect"
		mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD
fi

echo "Combining repeated responses Of $DateTime"
Command="replace into Responses_Pending_$DateTime(Date,Hour,Service,Response,Count,Neglect,Day,Month) SELECT Date, Hour, Responses_Pending_$DateTime.Service, AUTO_AGG_RESPONSE_MAP.Replacement, sum(Count), 'Agg', Day, Month from Responses_Pending_$DateTime,AUTO_AGG_RESPONSE_MAP where Date='$Date' and day=day('$Date') and Month=Month('$Date') and Hour='$Hour' and Neglect<>'Agg' and Responses_Pending_$DateTime.response like AUTO_AGG_RESPONSE_MAP.Response and Responses_Pending_$DateTime.Service = AUTO_AGG_RESPONSE_MAP.Service group by Date, Hour, Service, AUTO_AGG_RESPONSE_MAP.Response, Day, Month"
mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD

echo `date`" Deleting repeated responses Of $DateTime"
Command="DELETE FROM Responses_Pending_$DateTime using Responses_Pending_$DateTime inner join AUTO_AGG_RESPONSE_MAP on (Responses_Pending_$DateTime.Response like AUTO_AGG_RESPONSE_MAP.Response and Responses_Pending_$DateTime.Service = AUTO_AGG_RESPONSE_MAP.Service) WHERE (Date='$Date' and day=day('$Date') and Month=Month('$Date') and Hour='$Hour' and Neglect<>'Agg') or Date<=(select date from group_stats.Yesterday)"
mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD

echo `date`" Inserting shrinked data Of $DateTime"
if [ "$OldRestrival" = "" ]
        then
		echo "Old Data"
	else
		echo "Delete Responses all Data"
		Command="DELETE FROM Responses WHERE Date='$Date' and day=day('$Date') and Month=Month('$Date') and Hour='$Hour' and Neglect<>'Agg'"
		mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD
fi
		
Command="REPLACE INTO Responses(Date, Hour, Service, Response, Count, Neglect, Day, Month) SELECT Date, Hour, Service, Response, Count, Neglect, Day, Month from Responses_Pending_$DateTime where Date='$Date' and Hour='$Hour'"
mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD

echo `date`" Dropping Table Responses_Pending_$DateTime"
Command="DROP TABLE Responses_Pending_$DateTime"
mysql --login-path=Writer -hCNPVAS03 -e"$Command" USSD
