
Rand=$RANDOM
echo "Truncating DISTINCT_RESPONSES_Pending Table"
mysql -uWriter -pWriter -hCNPVAS03 -e"TRUNCATE TABLE DISTINCT_RESPONSES_Pending" USSD
echo "Filling DISTINCT_RESPONSES_Pending with null responses"
mysql -uWriter -pWriter -hCNPVAS03 -e"replace into DISTINCT_RESPONSES_Pending(Response, Category) select Response, Category from DISTINCT_RESPONSES where category is null" USSD
echo "Mapping null responses with Auto Response categories"
Command="SELECT DISTINCT_RESPONSES_Pending.Response,AUTO_RESPONSE_MAP.Category FROM USSD.DISTINCT_RESPONSES_Pending INNER JOIN USSD.AUTO_RESPONSE_MAP AUTO_RESPONSE_MAP ON (DISTINCT_RESPONSES_Pending.Response LIKE AUTO_RESPONSE_MAP.Response) WHERE DISTINCT_RESPONSES_Pending.Category IS NULL into outfile '/share/Importers/USSD/Category_Agg/Categorized_Response_$Rand' FIELDS TERMINATED BY '|'"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD
echo "Loading mapped responses"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/USSD/Category_Agg/Categorized_Response_$Rand" CNPVAS03 DISTINCT_RESPONSES "Response,Category" "|" USSD USSD_Categorizer
