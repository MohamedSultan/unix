CurrentDateTime=$1
echo $CurrentDateTime
LastDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime -1`
NextDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime 1`
RequestedDateTime=`echo $CurrentDateTime |cut -c1-4`"-"`echo $CurrentDateTime |cut -c5-6`"-"`echo $CurrentDateTime |cut -c7-8`"|"`echo $CurrentDateTime |cut -c9-10`"|"
RequestedDateTime_Req=`echo $CurrentDateTime |cut -c1-4`"-"`echo $CurrentDateTime |cut -c5-6`"-"`echo $CurrentDateTime |cut -c7-8`":"`echo $CurrentDateTime |cut -c9-10`":"

echo "Aggregating Responses"
#---------------------------
cat /share/Importers/USSD/Responses_$CurrentDateTime /share/Importers/USSD/Responses_$LastDateTime /share/Importers/USSD/Responses_$NextDateTime | grep $RequestedDateTime | awk 'BEGIN {FS = "|";OFS="|"}{Day=substr($5,9,2);Month=substr($5,6,2);SumArr[Day"|"Month"|"$2"|"$5"|"$6"|"$7] += $4;} END {for (i in SumArr) { print SumArr[i]"|"i} }'  > /share/Importers/USSD/Responses_Agg_$CurrentDateTime

ksh Responses_Combiner.sh $CurrentDateTime &

