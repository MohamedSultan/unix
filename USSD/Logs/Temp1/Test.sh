CurrentDateTime=$1
LastDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime -1`
NextDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime 1`
RequestedDateTime=`echo $CurrentDateTime |cut -c1-4`"-"`echo $CurrentDateTime |cut -c5-6`"-"`echo $CurrentDateTime |cut -c7-8`":"`echo $CurrentDateTime |cut -c9-10`":"

#-------------------------------------------------------------------------------------------------------------------
echo "Aggregating Requests"
#--------------------------
#grep -h $RequestedDateTime /share/Importers/USSD/Requests_$CurrentDateTime /share/Importers/USSD/Requests_$LastDateTime /share/Importers/USSD/Requests_$NextDateTime | cut -d":" -f3,5,6,9,11 | awk 'BEGIN {FS = ":";OFS=":"}{if (length($4)>=3) {print $1,$2,$3,"Others",$5} else {print $1,$2,$3,$4,$5}}' | awk 'BEGIN {FS = ":";OFS=":"}{if ($5=="MAP_HLR") {print $1,$2,$3,$4,"1",substr($2,9,2),substr($2,6,2)} else {print $1,$2,$3,$4,"0",substr($2,9,2),substr($2,6,2)}}' | sort | uniq -c | sed 's/^ *//g;s/ /:/' > /share/Importers/USSD/Requests_Agg_$CurrentDateTime

grep -h $RequestedDateTime /share/Importers/USSD/Requests_$CurrentDateTime /share/Importers/USSD/Requests_$LastDateTime /share/Importers/USSD/Requests_$NextDateTime | cut -d":" -f2,4,5,6,7 | awk 'BEGIN {FS = ":";OFS=":"}{if (length($4)>=5) {print $1,$2,$3,"Others",$5} else {print $1,$2,$3,$4,$5}}' | awk 'BEGIN {FS = ":";OFS=":"}{if (length($5)>0) {print $1,$2,$3,$4,"1",substr($2,9,2),substr($2,6,2)} else {print $1,$2,$3,$4,"0",substr($2,9,2),substr($2,6,2)}}' | sort | uniq -c | sed 's/^ *//g;s/ /:/' > /share/Importers/USSD/Requests_Agg_$CurrentDateTime

#Command="LOAD DATA INFILE '/share/Importers/USSD/Requests_Agg_$CurrentDateTime' REPLACE INTO TABLE Requests FIELDS TERMINATED BY ':' (Count,Service,Date,Hour,Request,First_Hit,Day,Month)"
#mysql --host=CNPVAS03 -uWriter -pWriter -e"$Command" USSD &
echo "Loading Aggregated Request data"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/USSD/Requests_Agg_$CurrentDateTime" CNPVAS03 Requests "Count,Service,Date,Hour,Request,First_Hit,Day,Month" ":" USSD USSD_Requests 


#rm /share/Importers/USSD/Requests_$CurrentDateTime /share/Importers/USSD/Requests_$LastDateTime /share/Importers/USSD/Requests_$NextDateTime 
#rm /share/Importers/USSD/Responses_$CurrentDateTime /share/Importers/USSD/Responses_$LastDateTime /share/Importers/USSD/Responses_$NextDateTime
#rm /share/Importers/USSD/Responses_Agg_$CurrentDateTime
#rm /share/Importers/USSD/Requests_Agg_$CurrentDateTime
#rm /share/Importers/USSD/Distinct_Responses_$CurrentDateTime
