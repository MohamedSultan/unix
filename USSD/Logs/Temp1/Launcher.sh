#!/bin/ksh
Date_Hour=$1     #Hour 2010110911

#echo "Getting Files Dated: "$Date_Hour

#br="ussdbr4 ussdbr2 ussdbr3 ussdc2br1 ussdc2br2 ussdc2br3 ussdbr1"
br="ussdc2br1 ussdc2br2 ussdc2br3 ussdbr4 bw1_ext bw2_ext"
set -A Browser_Name $br
USSD_BR_ID=0
if [ -e "/share/Importers/USSD/Responses_$Date_Hour" ]
	then
		echo "Removing /share/Importers/USSD/Responses_$Date_Hour"
		rm "/share/Importers/USSD/Responses_$Date_Hour"
fi
if [ -e "/share/Importers/USSD/Requests_$Date_Hour" ]
	then
		echo "Removing /share/Importers/USSD/Requests_$Date_Hour"
		rm "/share/Importers/USSD/Requests_$Date_Hour"
fi
while [ $USSD_BR_ID -lt 6 ]
        do
		echo "Processing Responses on ${Browser_Name[$USSD_BR_ID]}"
		ksh Responses.sh $Date_Hour ${Browser_Name[$USSD_BR_ID]} Responses_$Date_Hour &
#		echo "Processing Requests on ${Browser_Name[$USSD_BR_ID]}"
#		ksh Requests.sh $Date_Hour ${Browser_Name[$USSD_BR_ID]} Requests_$Date_Hour &
        	USSD_BR_ID=$(($USSD_BR_ID+1))
done

Responses_Process=`ps -ef | grep Responses.sh | grep $Date_Hour | grep -v grep | wc -l | sed 's/^ *//g'`
Responses_Process=$(($Responses_Process+0))
                while [ $Responses_Process -ge 1 ]
                        do
                                echo `date`" Found another running Process, Waiting Process"
                                sleep 10
                                Responses_Process=`ps -ef | grep Responses.sh | grep $Date_Hour | grep -v grep | wc -l | sed 's/^ *//g'`
                                Responses_Process=$(($Responses_Process+0))
                done


LastDateTime=`ksh /app_univ/Date_Scripts/NextHour $Date_Hour -1`
NextDateTime=`ksh /app_univ/Date_Scripts/NextHour $Date_Hour 1`
ksh Aggregator.sh $Date_Hour
ksh Aggregator.sh $LastDateTime
ksh Aggregator.sh $NextDateTime

Date=`echo $Date_Hour | cut -c1-4`"-"`echo $Date_Hour | cut -c5-6`"-"`echo $Date_Hour | cut -c7-8`
Command="CALL USSD.USSD_Category_Agg('$Date')"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD


ksh Response_Mapper.sh 

exit


Rand=$RANDOM
mysql -uWriter -pWriter -hCNPVAS03 -e"TRUNCATE TABLE DISTINCT_RESPONSES_Pending" USSD
mysql -uWriter -pWriter -hCNPVAS03 -e"replace into DISTINCT_RESPONSES_Pending(Response, Category) select Response, Category from DISTINCT_RESPONSES where category is null" USSD
Command="SELECT DISTINCT_RESPONSES_Pending.Response,AUTO_RESPONSE_MAP.Category FROM USSD.DISTINCT_RESPONSES_Pending INNER JOIN USSD.AUTO_RESPONSE_MAP AUTO_RESPONSE_MAP ON (DISTINCT_RESPONSES_Pending.Response LIKE AUTO_RESPONSE_MAP.Response) WHERE DISTINCT_RESPONSES_Pending.Category IS NULL into outfile '/share/Importers/USSD/Category_Agg/Categorized_Response_$Rand' FIELDS TERMINATED BY '|'"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/USSD/Category_Agg/Categorized_Response_$Rand" CNPVAS03 DISTINCT_RESPONSES "Response,Category" "|" USSD USSD_Categorizer

