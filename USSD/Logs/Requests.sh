#!/bin/ksh
Date_Hour=$1     #Hour 2010110911
USSD_BR_ID=$2
OutputFileName=$3

Command="ssh VASQ@$USSD_BR_ID find /home/VASQ/SCLogs/ -name *$Date_Hour.log*"
Needed_Files_Temp=`ssh CNPVAS02 "$Command"`
Command="ssh VASQ@$USSD_BR_ID find /home/VASQ/SCLogs/ -name *$Date_Hour.log* | wc -l "
Needed_Files_Count=`ssh CNPVAS02 "$Command"`

set -A Services $SCs
set -A Needed_Files $Needed_Files_Temp

i=0
while [ $i -lt $Needed_Files_Count ]
	do
        	FileName=`echo ${Needed_Files[$i]} | cut -d"/" -f6`
                ServiceName=`echo ${Needed_Files[$i]} | cut -d"/" -f5`
		echo "Processing Requests of $FileName on $USSD_BR_ID"
		if [ "$ServiceName" != "666" ] && [ "$ServiceName" != "858" ] && [ "$ServiceName" != "505" ] && [ "$ServiceName" != "506" ] && [ "$ServiceName" != "511" ] && [ "$ServiceName" != "1" ]
			then
				if [ `echo $FileName | awk '{print substr($1,length($1)-2,3)}'` = ".gz" ]
					then
						Command="ssh VASQ@$USSD_BR_ID zcat ${Needed_Files[$i]}|grep -i Request: | sed 's/ /:/' | sed 's/#/:/' | cut -d: -f1,2,5,8 | sed 's/\[op//;s/ //g;s/0,MAP_HLR/MAP_HLR/g' | awk 'BEGIN{FS=\":\";OFS=\":\"}  {print NodeName,ServiceName,FileName,\$1,\$2,\$3,\$4 }' FileName=$FileName ServiceName=$ServiceName NodeName=$USSD_BR_ID"
					else
						Command="ssh VASQ@$USSD_BR_ID grep -i Request: ${Needed_Files[$i]}  | sed 's/ /:/' | sed 's/#/:/' | cut -d: -f1,2,5,8 | sed 's/\[op//;s/ //g;s/0,MAP_HLR/MAP_HLR/g' | awk 'BEGIN{FS=\":\";OFS=\":\"}  {print NodeName,ServiceName,FileName,\$1,\$2,\$3,\$4}' FileName=$FileName ServiceName=$ServiceName NodeName=$USSD_BR_ID "
				fi
#				echo $Command
				ssh CNPVAS02 "$Command >> /share/Importers/USSD/$OutputFileName"
			fi
                i=$(($i+1))
done
