#!/bin/ksh
Date_Hour=$1     #Hour 2010110911
USSD_BR_ID=$2
OutputFileName=$3
Command="ssh VASQ@$USSD_BR_ID find /home/VASQ/SCLogs/ -name *$Date_Hour.log*"
Needed_Files_Temp=`ssh CNPVAS02 "$Command"`
Command="ssh VASQ@$USSD_BR_ID find /home/VASQ/SCLogs/ -name *$Date_Hour.log* | wc -l "
Needed_Files_Count=`ssh CNPVAS02 "$Command"`
set -A Services $SCs
set -A Needed_Files $Needed_Files_Temp
#echo $Needed_Files_Count
#echo $Needed_Files_Temp
i=0
while [ $i -lt $Needed_Files_Count ]
        do
                FileName=`echo ${Needed_Files[$i]} | cut -d"/" -f6`
                echo "Processing Responses of $FileName on $USSD_BR_ID"
                ServiceName=`echo ${Needed_Files[$i]} | cut -d"/" -f5`
                if [ "$ServiceName" != "XXX" ] && [ "$ServiceName" != "XXX" ]
                        then
                                echo "Running Data "$Date_Hour > XXX
                                if [ `echo $FileName | awk '{print substr($1,length($1)-2,3)}'` = ".gz" ]
                                        then
                                                Command="zcat ${Needed_Files[$i]} | grep -v 'Request:' ${Needed_Files[$i]}  | sed 's/:.*Response://g' |  sed ':begin;$"'!'"N;s/\n/  /;tbegin'| sed 's/\[op:/\n/g' | sed 's/.*\]  20/20/g' | sed '$ d'|  tr -d ''  |sort | uniq -c | sed 's/^[ \t]*//' | sed 's/ /|/;s/ /|/;s/ /|/' | awk 'BEGIN{FS=\"|\";OFS=\"|\"}  {print NodeName,ServiceName,FileName,\$1,\$2,\$3,\$4,substr(\$2,9,2)}' FileName=$FileName ServiceName=$ServiceName NodeName=$USSD_BR_ID"
                                        else
                                                Command="grep -v 'Request:' ${Needed_Files[$i]}  | sed 's/:.*Response://g' |  sed ':begin;$"'!'"N;s/\n/  /;tbegin'| sed 's/\[op:/\n/g' | sed 's/.*\]  20/20/g' | sed '$ d' |  tr -d ''  | sort | uniq -c | sed 's/^[ \t]*//' | sed 's/ /|/;s/ /|/;s/ /|/' | awk 'BEGIN{FS=\"|\";OFS=\"|\"}  {print NodeName,ServiceName,FileName,\$1,\$2,\$3,\$4,substr(\$2,9,2)}' FileName=$FileName ServiceName=$ServiceName NodeName=$USSD_BR_ID"
                                fi
                                ssh -o ServerAliveInterval=20 -o ServerAliveCountMax=3 VASQ@$USSD_BR_ID "$Command" >> /share/Importers/USSD/$OutputFileName
                fi
                i=$(($i+1))
done
