#!/bin/ksh
Date_Hour=$1     #Hour 2010110911

#echo "Getting Files Dated: "$Date_Hour

br="ipgw2ext ipgw1ext ipgw3ext"
set -A Browser_Name $br
USSD_BR_ID=0
> Connector_Response_"$Date_Hour".txt
while [ $USSD_BR_ID -lt 3 ]
        do
        Needed_Files_Temp=`ssh VASQ@${Browser_Name[$USSD_BR_ID]} find /home/VASQ/Connectors/MCKMINICALL/ -name *$Date_Hour.log`
        Needed_Files_Count=`ssh VASQ@${Browser_Name[$USSD_BR_ID]} find /home/VASQ/Connectors/MCKMINICALL/ -name *$Date_Hour.log | wc -l`
        set -A Services $SCs
        set -A Needed_Files $Needed_Files_Temp

	i=0
        while [ $i -lt $Needed_Files_Count ]
                do
			echo "Handling File:${Needed_Files[$i]} ,Node No $USSD_BR_ID of 7, Node Name:${Browser_Name[$USSD_BR_ID]}, File No:$i of $Needed_Files_Count"
                        FileName=`echo ${Needed_Files[$i]} | cut -d"/" -f6`
                        #ServiceName=`echo ${Needed_Files[$i]} | cut -d"/" -f5`
			if [ "$ServiceName" != "XXX" ]
			#if [ "$ServiceName" = "800" ]
				then
					echo "Running Data "$Date_Hour > XXX
					Command="cat ${Needed_Files[$i]} | grep 'HTTP response' | sed 's/ /:/' | cut -d":" -f1,2,7,8,9| sort | uniq -c | sed 's/^ *//g' | sed 's/ /:/' | sed 's/RESPONSE=//g'"
					ssh VASQ@${Browser_Name[$USSD_BR_ID]} "$Command" | awk 'BEGIN{FS=":";OFS="|"} { print NodeName,FileName,$1,$2,$3,$4,$5,$6,$7,$8,$9}' FileName=$FileName NodeName=${Browser_Name[$USSD_BR_ID]} >> Connector_Response_"$Date_Hour".txt
			fi
                        i=$(($i+1))
        done
        USSD_BR_ID=$(($USSD_BR_ID+1))
done

cp Connector_Response_"$Date_Hour".txt Connector_Response_Last

echo "Importing Responses Files"
mv Connector_Response_"$Date_Hour".txt /log/Shared/USSD/

Command="LOAD DATA INFILE '/log/Shared/USSD/Connector_Response_"$Date_Hour".txt' REPLACE INTO TABLE USSD_Connectors_Responses CHARACTER SET utf8 FIELDS TERMINATED BY '|' (Node_Name,FileName,Count,Date,Hour,Response)"
echo "Command:"$Command
mysql -hActiveDB -uWriter -pWriter -e"$Command" Processings
