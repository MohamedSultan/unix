Date_Hour=$1     #Hour 2010110911

#echo "Getting Files Dated: "$Date_Hour

br="ipgw2ext ipgw1ext ipgw3ext"
set -A Browser_Name $br
USSD_BR_ID=0
> Connectors_"$Date_Hour".txt
while [ $USSD_BR_ID -lt 3 ]
        do
        Needed_Files_Temp=`ssh VASQ@${Browser_Name[$USSD_BR_ID]} find /home/VASQ/Connectors/MCKMINICALL/ -name *$Date_Hour.log*`
        Needed_Files_Count=`ssh VASQ@${Browser_Name[$USSD_BR_ID]} find /home/VASQ/Connectors/MCKMINICALL/ -name *$Date_Hour.log* | wc -l `
        set -A Services $SCs
        set -A Needed_Files $Needed_Files_Temp

        i=0
        while [ $i -lt $Needed_Files_Count ]
                do
                        echo "Handling File:${Needed_Files[$i]} ,Node No $USSD_BR_ID of 3, Node Name:${Browser_Name[$USSD_BR_ID]}, File No:$i of $Needed_Files_Count"
                        FileName=`echo ${Needed_Files[$i]} | cut -d"/" -f6`
                        #ServiceName=`echo ${Needed_Files[$i]} | cut -d"/" -f5`
                        if [ "$ServiceName" != "XXX" ]
                        #if [ "$ServiceName" = "800" ]
                                then
                                        echo "Running Data "$Date_Hour > XXX
					Command="cat ${Needed_Files[$i]} | grep ConnRequest| cut -d\" \" -f1,2,7,8 |tr \"'\" \"X\"|sed 's/\[//g;s/\]//g;s/Originator://g;s/destination://g;s/,//g;s/X//g;s/ /;/g'"
	
					ssh VASQ@${Browser_Name[$USSD_BR_ID]} "$Command"  | awk 'BEGIN{FS=";";OFS="|"} { print NodeName,File_Name,$1,$2,$3,$4,$5,$6,$7}' File_Name=$FileName NodeName=${Browser_Name[$USSD_BR_ID]} >> Connectors_"$Date_Hour".txt
					Command="zcat ${Needed_Files[$i]} | grep ConnRequest| cut -d\" \" -f1,2,7,8 |tr \"'\" \"X\"|sed 's/\[//g;s/\]//g;s/Originator://g;s/destination://g;s/,//g;s/X//g;s/ /;/g'"
					ssh VASQ@${Browser_Name[$USSD_BR_ID]} "$Command"  | awk 'BEGIN{FS=";";OFS="|"} { print NodeName,File_Name,$1,$2,$3,$4,$5,$6,$7}' File_Name=$FileName NodeName=${Browser_Name[$USSD_BR_ID]} >> Connectors_"$Date_Hour".txt
			fi
                        i=$(($i+1))
        done
        USSD_BR_ID=$(($USSD_BR_ID+1))
done

cp Connectors_"$Date_Hour".txt Connectors_Last.txt

echo "Importing Requests Files"
cp Connectors_"$Date_Hour".txt /share/Importers/USSD/
rm Connectors_"$Date_Hour".txt
Rand=$RANDOM
echo "Loading File /share/Importers/USSD/Connectors_"$Date_Hour".txt"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/USSD/Connectors_$Date_Hour.txt" CNPVAS03 Connectors_Temp "NodeName,FileName,Date,Time,MSISDN,ShortCode" "|" USSD USSD_Connectors Replace "$Rand"
#ksh /app_univ/StatsCollector/Auto_Importer.sh "$Rand"

#Command="LOAD DATA INFILE '/share/Importers/USSD/Connectors_"$Date_Hour".txt' REPLACE INTO TABLE Connectors_Temp CHARACTER SET utf8 FIELDS TERMINATED BY '|' (NodeName,FileName,Date,Time,MSISDN,ShortCode)"
#echo "Command:"$Command
#mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" USSD

DatePart=`echo $Date_Hour | cut -c1-4`"-"`echo $Date_Hour | cut -c5-6`"-"`echo $Date_Hour | cut -c7-8`
Command="REPLACE INTO Connectors(Date,Hour,ShortCode,Count) SELECT Date,substring(Time,1,2),ShortCode,count(*) from Connectors_Temp where Date='$DatePart' group by Date,substring(Time,1,2),ShortCode"

mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" USSD

Command="DELETE FROM Connectors_Temp WHERE Date<='$DatePart' - interval 4 Day or ShortCode='505'"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" USSD



