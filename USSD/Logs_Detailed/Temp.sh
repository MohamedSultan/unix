Date_Hour=$1

Date="'"`echo $Date_Hour|cut -c1-4 `"-"`echo $Date_Hour|cut -c5-6`"-"`echo $Date_Hour|cut -c7-8`"'"
OutputDir=/share/USSD/

echo "Starting Of Getting Stats"

if [ -e "$OutputDir/Requests_$Date_Hour" ]
        then
                echo "Removing $OutputDir/Requests_$Date_Hour"
                rm "$OutputDir/Requests_$Date_Hour"
fi


for Browser_Name in ussdc2br1 ussdc2br2 ussdc2br3 ussdbr4 bw1_ext bw2_ext
        do
                echo "Processing Requests on $Browser_Name"
                ksh Requests.sh $Browser_Name $Date_Hour >> "$OutputDir/Requests_$Date_Hour"
done

