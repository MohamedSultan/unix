#!/bin/ksh
Browser_Name=$1
Date_Hour=$2     #Hour 2010110911
OutFilePath=$3

Needed_Files_Temp=`ssh VASQ@$Browser_Name find /home/VASQ/SCLogs/ -name *$Date_Hour.log*`
Needed_Files_Count=`ssh VASQ@$Browser_Name find /home/VASQ/SCLogs/ -name *$Date_Hour.log* | wc -l `
set -A Needed_Files $Needed_Files_Temp

File_ID=0
while [ $File_ID -lt $Needed_Files_Count ]
	do
        	FileName=`echo ${Needed_Files[$File_ID]} | cut -d"/" -f6`
                ServiceName=`echo ${Needed_Files[$File_ID]} | cut -d"/" -f5`
		if [ "$ServiceName" != "XXX" ]
		#if [ "$ServiceName" = "800" ]
			then
				if [ `echo $FileName | awk '{print substr($1,length($1)-2,3)}'` = ".gz" ]
					then
						echo "parsing gzip log file: ${Needed_Files[$File_ID]} on Machine: $Browser_Name"
						Command="grep -v 'Request:' ${Needed_Files[$File_ID]} | tr '\n' ' ' | sed 's/....-..-.. ..:..:....../^&/g'| tr '^' '\n' | grep -i Response:  | sed 's/ /:/' | sed 's/-/:/g'| sed 's/\[/:/' |sed 's/\]/:/'| cut -d[ -f1 |cut -d: -f1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23 | sed 's/Response//g' |sed 's/ //'|sed 's/ //'|sed 's/:/-/' | sed 's/:/-/'| sed 's/] : /:/'|sed 's/ :/:/' "
						ssh VASQ@$Browser_Name "$Command" | awk 'BEGIN{FS=":";OFS="|"}  {Response=$7$8$9$10$11$12$13; print NodeName,ServiceName,FileName,substr($1,9,2),$1,$2,$5,$6,Response,$2":"$3":"$4}' FileName=$FileName ServiceName=$ServiceName NodeName=$Browser_Name >> $OutFilePath
					else
						echo "parsing log file: ${Needed_Files[$File_ID]} on Machine: $Browser_Name"
						Command="cat ${Needed_Files[$File_ID]} | grep -v 'Request:' | tr '\n' ' ' | sed 's/....-..-.. ..:..:....../^&/g'| tr '^' '\n' | grep -i Response:  | sed 's/ /:/' | sed 's/-/:/g'| sed 's/\[/:/' |sed 's/\]/:/'| cut -d[ -f1 |cut -d: -f1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23 | sed 's/Response//g' |sed 's/ //'|sed 's/ //'|sed 's/:/-/' | sed 's/:/-/'| sed 's/] : /:/'|sed 's/ :/:/' "
						ssh VASQ@$Browser_Name "$Command" | awk 'BEGIN{FS=":";OFS="|"}  {Response=$7$8$9$10$11$12$13; print NodeName,ServiceName,FileName,substr($1,9,2),$1,$2,$5,$6,Response,$2":"$3":"$4}' FileName=$FileName ServiceName=$ServiceName NodeName=$Browser_Name >> $OutFilePath
				fi
			fi
                        File_ID=$(($File_ID+1))
        done
