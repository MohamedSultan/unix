DateTime=$1
cd /share/USSD
ProcessingDir=/share/USSD/Processing/
       
if [ "$DateTime" = "" ]
	then
		DateTime=`/app_univ/Date_Scripts/LastHour`
fi

DateTimeMinus=`ksh /app_univ/Date_Scripts/LastHour $DateTime`
DateTimePlus=`ksh /app_univ/Date_Scripts/NextHour $DateTime`

echo $DateTimeMinus $DateTime $DateTimePlus

#----------------------------------------------------------------------------------------------------------------------------

echo "Prepairning Requests to Requests_$DateTime at `date` "

echo "Exporting First Hits Requests"          
cat Requests_$DateTime | grep MAP_HLR | awk -F"|" '{ print $2"@"$3"@"$7"@"$8"|"$1"|"$4"|"$5"|"$6"|"$9 }' | sort > $ProcessingDir/Req_$DateTime

echo "Exporting Subsequent Requests"
cat Requests_$DateTime | grep -v MAP_HLR | awk -F"|" '{ print $2"@"$3"@"$7"@"$8"|"$1"|"$4"|"$5"|"$6"|"$9 }' | sort > $ProcessingDir/RemainingHits_$DateTime

Step=1
	while [ `wc -l $ProcessingDir/RemainingHits_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1` -gt 0 ]
		do
			Step=$(($Step+1))
			echo "Selecting Hit#"$Step
			sort $ProcessingDir/RemainingHits_$DateTime | /usr/xpg4/bin/awk -F"|" '!a[$1]++' | sort > $ProcessingDir/Part_$DateTime
			join -t"|" -j1 1 -j2 1 -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 2.6  $ProcessingDir/Req_$DateTime $ProcessingDir/Part_$DateTime | sort |  sed 's/\(.*\)|/\1\^/'> $ProcessingDir/Part2_Req_$DateTime
			mv $ProcessingDir/Part2_Req_$DateTime $ProcessingDir/Req_$DateTime
			comm -23 $ProcessingDir/RemainingHits_$DateTime $ProcessingDir/Part_$DateTime > $ProcessingDir/RemainingHits_Temp_$DateTime 
			mv $ProcessingDir/RemainingHits_Temp_$DateTime $ProcessingDir/RemainingHits_$DateTime
done
		
cat $ProcessingDir/Req_$DateTime | sed 's/\^*$//g' > /share/USSD/Combined_Requests_$DateTime

rm $ProcessingDir/Req_$DateTime $ProcessingDir/RemainingHits_$DateTime

cd /app_univ/Services/USSD/Req_Res_joiner; ksh Launcher.sh $DateTime

exit        
        
Command="LOAD DATA INFILE '/log/Shared/USSD/Req_Res_Aggregated_$Date' REPLACE INTO TABLE Requests_Responses_Detailed CHARACTER SET utf8 FIELDS TERMINATED BY '|' (month,Count,Request,Day,Service,File_Name,Date,Hour,Response)"
mysql -hActiveDB -uWriter -pWriter -e"$Command" USSD

Command="LOAD DATA INFILE '/log/Shared/USSD/"Req_Res_$Date.txt"' REPLACE INTO TABLE Requests_VS_Responses_MSISDNs CHARACTER SET utf8 FIELDS TERMINATED BY '|' (Node_Name, Request_ID,Request,Day,Service,FileName,Date,Hour,MSISDN,Response,Time)"
#echo "Command:"$Command
#mysql -hActiveDB -uWriter -pWriter -e"$Command" Processings 

echo `date`" ================================================= "
