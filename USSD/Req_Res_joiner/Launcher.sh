DateTime=$1
cd /share/USSD
DataDir=/share/Importers/USSD/Req_Res_Joiner/
       
if [ "$DateTime" = "" ]
	then
		DateTime=`/app_univ/Date_Scripts/LastHour`
fi

DateTimeMinus=`/app_univ/Date_Scripts/LastHour $DateTime`
DateTimePlus=`/app_univ/Date_Scripts/NextHour $DateTime`

echo $DateTimeMinus $DateTime $DateTimePlus

echo "Prepairning Responses Of Response_$DateTime at `date` "

cat Responses_$DateTime |cut -d"|" -f1,2,7,8,9,10 > $DataDir/Temp_Res_$DateTime
head -500 Responses_$DateTimePlus |cut -d"|" -f1,2,7,8,9,10  >> $DataDir/Temp_Res_$DateTime
tail -500 Responses_$DateTimeMinus |cut -d"|" -f1,2,7,8,9,10 >> $DataDir/Temp_Res_$DateTime

cat $DataDir/Temp_Res_$DateTime | sort -r -t"|" -k6 | /usr/xpg4/bin/awk -F"|" '!a[$1,$2,$3,$4]++' | sed 's/|/@/;s/|/@/;s/|/@/' | sed 's/|/^/g'  | sort  > $DataDir/Res_$DateTime
                
rm $DataDir/Temp_Res_$DateTime

#----------------------------------------------------------------------------------------------------------------------------

echo "Prepairning Requests to Requests_$DateTime at `date` "
          
sed 's/\^/(/g;s/|/\^/g;s/(/|/g' Combined_Requests_$DateTime > $DataDir/Req_$DateTime

cat $DataDir/Missing_Requests_$DateTimeMinus | awk -F"|" '{ print $1"@"$2"@"$3"@"$4"|"$5"|"$6"|"$7"|"$8"|"$9 }' | sed 's/|/^/g' >> $DataDir/Req_$DateTime

mv $DataDir/Req_$DateTime $DataDir/Temp_Req_$DateTime
sort $DataDir/Temp_Req_$DateTime > $DataDir/Req_$DateTime
rm $DataDir/Temp_Req_$DateTime

#----------------------------------------------------------------------------------------------------------------------------
        
echo "Joining the 2 files: Res_$DateTime & Req_$DateTime at "`date` 
        
join -t"^" -j1 1 -j2 1 -a2 -o 2.1 2.3 2.4 2.5 2.2 2.6 1.3 1.2 $DataDir/Res_$DateTime $DataDir/Req_$DateTime | sed 's/|/\!/g;s/@/^/g;s/\^/|/g' > $DataDir/"Req_Res_"$DateTime"_inclundingMissings"
        
echo "Joining Done for: Res_$DateTime & Req_$DateTime at "`date`" ,data exported to Req_Res_"$DateTime"_inclundingMissings"

grep '||$' $DataDir/"Req_Res_"$DateTime"_inclundingMissings" > $DataDir/Missing_Requests_$DateTime
grep -v '||$' $DataDir/"Req_Res_"$DateTime"_inclundingMissings" > $DataDir/Req_Res_$DateTime

echo "Join Analysis"
echo "-------------"

Res_In=`wc -l Responses_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
Res_Uniq=`wc -l $DataDir/Res_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
Req_Uniq=`wc -l $DataDir/Req_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
joined=`wc -l $DataDir/"Req_Res_"$DateTime"_inclundingMissings" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
MissingReq=`wc -l $DataDir/Missing_Requests_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
Found=`wc -l $DataDir/Req_Res_$DateTime | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`

echo "$Res_In Responses are uniqued to $Res_Uniq VS $Req_Uniq Requests"
#echo "$joined requests are joined"
echo "Found $Found requests out of $Req_Uniq; $MissingReq are missing"

#--------------------------------------------------------------------------------------------------------------------

echo "aggregating data"
Date=`echo $DateTime | cut -c1-4`"-"`echo $DateTime | cut -c5-6`"-"`echo $DateTime | cut -c7-8`
Hour=`echo $DateTime | cut -c9-10`

grep "|"$Date"|"$Hour":" $DataDir/Req_Res_$DateTime $DataDir/Req_Res_$DateTimeMinus $DataDir/Req_Res_$DateTimePlus | sed 's/:/|/;s/:/|/' | cut -d"|" -f3,7,8,10,11,13 | sort | uniq -c | sed 's/^ *//g;s/ /|/' | awk -F"|" '{print substr($3,6,2)"|"$0}'  > $DataDir/Req_Res_Agg_$DateTime

ksh /app_univ/StatsCollector/Importer.sh "$DataDir/Req_Res_Agg_$DateTime" CNPVAS03 Requests_Responses_Detailed "Month,Count,Service,Date,Hour,Day,Request,Response" "|" USSD USSD_Req_Response_Agg





