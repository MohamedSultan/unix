Rand=$RANDOM
echo "Creating DISTINCT_RESPONSES_Pending_$Rand Table"
Command="CREATE TABLE DISTINCT_RESPONSES_Pending_$Rand (Response VARCHAR(300) NOT NULL DEFAULT '0' COLLATE 'utf8_general_ci',Category VARCHAR(50) NULL DEFAULT NULL,InsertionDate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (Response)) COLLATE='latin1_swedish_ci' ENGINE=MyISAM ROW_FORMAT=DYNAMIC"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4

echo "Filling DISTINCT_RESPONSES_Pending with null responses"
Command="replace into DISTINCT_RESPONSES_Pending_$Rand(Response, Category) select Response, Category from DISTINCT_RESPONSES_NULL where category is null"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4

echo "Mapping null responses with Auto Response categories"
Command="SELECT DISTINCT_RESPONSES_Pending_$Rand.Response,AUTO_RESPONSE_MAP.Category FROM USSD_C4.DISTINCT_RESPONSES_Pending_$Rand INNER JOIN USSD_C4.AUTO_RESPONSE_MAP AUTO_RESPONSE_MAP ON (DISTINCT_RESPONSES_Pending_$Rand.Response LIKE AUTO_RESPONSE_MAP.Response) WHERE DISTINCT_RESPONSES_Pending_$Rand.Category IS NULL into outfile '/share/Importers/USSD_C4/Category_Agg/Categorized_Response_$Rand' FIELDS TERMINATED BY '|'"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4
###exit
echo "Loading mapped responses"
ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/USSD_C4/Category_Agg/Categorized_Response_$Rand" CNPVAS03 DISTINCT_RESPONSES "Response,Category" "|" USSD_C4 USSD_Categorizer

echo "Dropping DISTINCT_RESPONSES_Pending_$Rand Table"
Command="DROP TABLE DISTINCT_RESPONSES_Pending_$Rand"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4

