#!/bin/ksh
Date_Hour=$1     #Hour 2010110911

#echo "Getting Files Dated: "$Date_Hour

##if [ -e "/share/Importers/USSD/Responses_$Date_Hour" ]
 ##       then
  ##              echo "Removing /share/Importers/USSD/Responses_$Date_Hour"
  ##              rm "/share/Importers/USSD/Responses_$Date_Hour"
##fi
##if [ -e "/share/Importers/USSD/Requests_$Date_Hour" ]
##        then
##                echo "Removing /share/Importers/USSD/Requests_$Date_Hour"
##                rm "/share/Importers/USSD/Requests_$Date_Hour"
##fi

for Browser_Name in ussdC4br1 ussdC4br2 ussdC4br3
        do
                echo "Processing Responses on $Browser_Name"
	## we put the Responses.sh script @ the server, due to the '^M' problem
	ksh Responses_New.sh $Date_Hour $Browser_Name Responses_$Date_Hour &
##	echo "Processing Requests on $Browser_Name"
##        ksh Script_Exporter.sh /app_univ/Services/USSD/Logs/ Requests.sh $Date_Hour $Browser_Name Requests_$Date_Hour &
        USSD_BR_ID=$(($USSD_BR_ID+1))
done

Responses_Process=`ps -ef | grep Responses_New.sh | grep $Date_Hour | grep -v grep | wc -l | sed 's/^ *//g'`
Responses_Process=$(($Responses_Process+0))
                while [ $Responses_Process -ge 1 ]
                        do
                                echo `date`" Found another running Process, Waiting Process"
                                sleep 10
                                Responses_Process=`ps -ef | grep Responses_New.sh | grep $Date_Hour | grep -v grep | wc -l | sed 's/^ *//g'`
                                Responses_Process=$(($Responses_Process+0))
                done

LastDateTime=`ksh /app_univ/Date_Scripts/NextHour $Date_Hour -1`
NextDateTime=`ksh /app_univ/Date_Scripts/NextHour $Date_Hour 1`
ksh Aggregator.sh $Date_Hour
ksh Aggregator.sh $LastDateTime
ksh Aggregator.sh $NextDateTime

Date=`echo $Date_Hour | cut -c1-4`"-"`echo $Date_Hour | cut -c5-6`"-"`echo $Date_Hour | cut -c7-8`
Hour=`echo $Date_Hour | cut -c9-10`
Command="CALL USSD.USSD_Category_Agg('$Date')"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4 &

###exit

ksh Response_Mapper.sh  &
exit

Command="replace into Category_Analysis_Service (Date, Count,Service,Category) select date,sum(count) as Count_,service,category from Categorized_Responses where month=month('$Date') and day=day('$Date') and date='$Date' group by date,service,category"
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_C4 &

