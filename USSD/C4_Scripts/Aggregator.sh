CurrentDateTime=$1
echo $CurrentDateTime
LastDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime -1`
NextDateTime=`ksh /app_univ/Date_Scripts/NextHour $CurrentDateTime 1`
RequestedDateTime=`echo $CurrentDateTime |cut -c1-4`"-"`echo $CurrentDateTime |cut -c5-6`"-"`echo $CurrentDateTime |cut -c7-8`"|"`echo $CurrentDateTime |cut -c9-10`"|"
RequestedDateTime_Req=`echo $CurrentDateTime |cut -c1-4`"-"`echo $CurrentDateTime |cut -c5-6`"-"`echo $CurrentDateTime |cut -c7-8`":"`echo $CurrentDateTime |cut -c9-10`":"

echo "Aggregating Responses"
#---------------------------
cat /share/Importers/USSD_C4/Responses_$CurrentDateTime /share/Importers/USSD_C4/Responses_$LastDateTime /share/Importers/USSD_C4/Responses_$NextDateTime | grep $RequestedDateTime | awk 'BEGIN {FS = "|";OFS="|"}{Day=substr($5,9,2);Month=substr($5,6,2);SumArr[Day"|"Month"|"$2"|"$5"|"$6"|"$7] += $4;} END {for (i in SumArr) { print SumArr[i]"|"i} }'  > /share/Importers/USSD_C4/Responses_Agg_$CurrentDateTime

##ksh Script_Exporter.sh /app_univ/Services/USSD/Logs/ Responses_Combiner.sh $CurrentDateTime &
ksh Responses_Combiner.sh $CurrentDateTime &
exit
#-------------------------------------------------------------------------------------------------------------------
echo "Aggregating Requests"
#--------------------------

grep -h $RequestedDateTime_Req /share/Importers/USSD/Requests_$CurrentDateTime /share/Importers/USSD/Requests_$LastDateTime /share/Importers/USSD/Requests_$NextDateTime | cut -d":" -f2,4,5,6,7 | awk 'BEGIN {FS = ":";OFS=":"}{if (length($4)>=5) {print $1,$2,$3,"Others",$5} else {print $1,$2,$3,$4,$5}}' | awk 'BEGIN {FS = ":";OFS=":"}{if (length($5)>0) {print $1,$2,$3,$4,"1",substr($2,9,2),substr($2,6,2)} else {print $1,$2,$3,$4,"0",substr($2,9,2),substr($2,6,2)}}' | sort | uniq -c | sed 's/^ *//g;s/ /:/' > /share/Importers/USSD/Requests_Agg_$CurrentDateTime

echo "Loading Aggregated Request data"
ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/USSD/Requests_Agg_$CurrentDateTime" CNPVAS03 Requests "Count,Service,Date,Hour,Request,First_Hit,Day,Month" ":" USSD USSD_Requests 


