
ForceNowID=$1
QueryExecuter=/app_univ/AutoImporter/QueryExecuter.sh
if [ "$ForceNowID" = "" ]
	then
		ForceNowID=0
		Importer_Process=`ps -ef | grep Auto_Importer.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		Importer_Process=$(($Importer_Process+0))
		while [ $Importer_Process -gt 1 ]
		     do
		        echo `date`" Found another running Process, Exiting"
		        exit
		done
fi

Rand=$RANDOM
rm Error_$Rand 2>/dev/null
PendingImportsCount=/share/Importers/AutoImport/Pending_Count_$Rand
PendingImports=/share/Importers/AutoImport/Pending_$Rand
rm $PendingImports 2>/dev/null
		
Command="SELECT ifnull(COUNT(*),0) from group_stats.\`Pending Imports\` WHERE Status='Pending1' and ForcedID ='$ForceNowID' INTO OUTFILE '$PendingImportsCount'"
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command"
Count=`cat $PendingImportsCount`
Count=`expr "$Count" + 0`
rm $PendingImportsCount

Command="SELECT ID FROM group_stats.\`Pending Imports\` WHERE Status='Pending' and ForcedID ='$ForceNowID' ORDER BY ID DESC INTO OUTFILE '$PendingImports' FIELDS TERMINATED BY '!'"
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command"
	
Counter=0
while [ $Counter -lt $Count ]
	do
	Counter=$(($Counter+1))
        ID=`head -$Counter $PendingImports | tail -1 | cut -d '!' -f1`
	echo "Processing Import Request ID:"$ID
        ksh $QueryExecuter $ID $ForcedID &
	sleep 1
done
rm $PendingImports 2>/dev/null
rm $PendingImportsCount 2>/dev/null
rm Error_$Rand
echo "All Data Imported"
