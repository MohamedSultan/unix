
ID=$1
ForcedID=$2

Importer_Process=`ps -ef | grep QueryExecuter.sh | grep -v grep | wc -l | sed 's/^ *//g'`
Importer_Process=$(($Importer_Process+0))
while [ $Importer_Process -gt 400 ]
	do
		echo `date`" Found another running Process, Ending Process"
		exit
		sleep 10
		Importer_Process=`ps -ef | grep QueryExecuter.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		Importer_Process=$(($Importer_Process+0))
done

if [ "$ForcedID" = "" ]
	then
		ForcedID=0
fi

Rand=$RANDOM
PendingImports=/share/Importers/AutoImport/Pending_$Rand
rm Error_$Rand 2>/dev/null
rm $PendingImports 2>/dev/null
sleep 1
		
Command="SELECT FilePath,MachineIP,RemoveFile,WithCharacterSet,TableName,Columns,FieldsTerminatedBy,DatabaseName,ID,\`Replace\` FROM group_stats.\`Pending Imports\` WHERE ID='$ID' and Status='Pending1' and ForcedID ='$ForceNowID' INTO OUTFILE '$PendingImports' FIELDS TERMINATED BY '!'"
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command" group_stats

if [ -s "$PendingImports" ] 
	then
		X=1
	else
		echo "Didn't find the requested Pending ID"
		#ls -lrt $PendingImports
		rm $PendingImports
		exit
fi	

	
FilePath=`head -1 $PendingImports | tail -1 | cut -d '!' -f1`
MachineIP=`head -1 $PendingImports | tail -1 | cut -d '!' -f2`
RemoveFile=`head -1 $PendingImports | tail -1 | cut -d '!' -f3`
WithCharacterSet=`head -1 $PendingImports | tail -1 | cut -d '!' -f4`
TableName=`head -1 $PendingImports | tail -1 | cut -d '!' -f5`
Columns=`head -1 $PendingImports | tail -1 | cut -d '!' -f6`
FieldsTerminatedBy=`head -1 $PendingImports | tail -1 | cut -d '!' -f7`
DatabaseName=`head -1 $PendingImports | tail -1 | cut -d '!' -f8`
ID=`head -1 $PendingImports | tail -1 | cut -d '!' -f9`
Replace=`head -1 $PendingImports | tail -1 | cut -d '!' -f10`
	
echo "Processing Import Request ID:"$ID
	
Command="UPDATE group_stats.\`Pending Imports\` SET Status='Processing' WHERE ID='$ID'"	
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command" group_stats &

FileName=`echo $FilePath | sed 's/.*\///g'`
#NewFileName=$FileName"_"$ID"_"$ForcedID
NewFileName=$FileName
#echo "Moving File $FileName To  /share/Importers/StatsCollector/$NewFileName"
mv $FilePath /share/Importers/StatsCollector/$NewFileName 2>Error_$Rand
	
if [ -s Error_$Rand ]
	then
		echo "Found Error"
		Error=`cat Error_$Rand | sed "s/\'//g"`
		Command="UPDATE group_stats.\`Pending Imports\` SET Status='Error',Error_Desc='$Error' WHERE ID='$ID' and ForcedID ='$ForceNowID'"
       		mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command" group_stats &
		rm Error_$Rand 2>/dev/null
	else
		
		if [ "$Replace" = "Replace" ] || [ "$Replace" = "REPLACE" ] || [ "$Replace" = "replace" ] 
			then
				Command="LOAD DATA INFILE '/share/Importers/StatsCollector/$NewFileName' REPLACE INTO TABLE \`$DatabaseName\`.\`$TableName\` CHARACTER SET UTF8 FIELDS TERMINATED BY '$FieldsTerminatedBy' ($Columns)" 
			else
				Command="LOAD DATA INFILE '/share/Importers/StatsCollector/$NewFileName' IGNORE INTO TABLE \`$DatabaseName\`.\`$TableName\` CHARACTER SET UTF8 FIELDS TERMINATED BY '$FieldsTerminatedBy' ($Columns)"
		fi
		echo "Importing file:"$FilePath
		#echo $Command
		if [ "$ForceNowID" = "0" ]
			then
				mysql -h$MachineIP -uImporter -pImporter123 -e"$Command" group_stats 2>Error_$Rand
			else
				mysql -h$MachineIP -uImporter -pImporter123 -e"$Command" group_stats 2> Error_$Rand &
		fi
		Loading=`ps auxww | grep LOAD | grep $FileName_$ID_$ForcedID | grep -v grep`
		while [ "$Loading" != "" ]
			do	
				echo "Waiting for Load data finish"
				sleep 10
		 		Loading=`ps auxww | grep LOAD | grep $NewFileName | grep -v grep`
		done
			
		if [ -s Error_$Rand ]
			then
	                        echo "Found Error Load Data"
				Error=`cat Error_$Rand | sed "s/\'//g"`
       		                Command="UPDATE group_stats.\`Pending Imports\` SET Status='Error',Error_Desc='$Error' WHERE ID='$ID' and ForcedID ='$ForceNowID'"
                      		mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command" group_stats &
				mv /share/Importers/StatsCollector/$NewFileName $FilePath 
				rm Error_$Rand 2>/dev/null
			else
				Command="UPDATE group_stats.\`Pending Imports\` SET Status='Finished' WHERE ID='$ID' and ForcedID ='$ForceNowID'"
		        	mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command" group_stats
				
				if [ "$RemoveFile" = "TRUE" ] || [ "$RemoveFile" = "True" ] || [ "$RemoveFile" = "true" ]
					then
						echo "Removing file: /share/Importers/StatsCollector/$NewFileName"
						rm /share/Importers/StatsCollector/$NewFileName
				fi
				echo "Import Finished"
		fi
fi
rm $PendingImports 2>/dev/null
rm Error_$Rand 2>/dev/null
