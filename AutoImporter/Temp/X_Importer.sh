
ForceNowID=$1
QueryExecuter=/app_univ/AutoImporter/Temp/Y_Importer.sh

Rand=$RANDOM
rm Error_$Rand 2>/dev/null
PendingImportsCount=/share/Importers/AutoImport/Pending_Count_$Rand
PendingImports=/share/Importers/AutoImport/Pending_$Rand
rm $PendingImports 2>/dev/null
		
Command="SELECT ifnull(COUNT(*),0) from group_stats.\`Pending Imports\` WHERE Status='Pending1' and ForcedID ='$ForceNowID' INTO OUTFILE '$PendingImportsCount'"
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command"
Count=`cat $PendingImportsCount`
Count=`expr "$Count" + 0`
rm $PendingImportsCount

Command="SELECT ID FROM group_stats.\`Pending Imports\` WHERE Status='Pending1' and ForcedID ='$ForceNowID' ORDER BY ID ASC INTO OUTFILE '$PendingImports' FIELDS TERMINATED BY '!'"
mysql -hCNPVAS01 -uImporter -pImporter123 -e"$Command"
	
Counter=0
Counter2=0
while [ $Counter -lt $Count ]
	do
	Counter=$(($Counter+1))
        ID=`head -$Counter $PendingImports | tail -1 | cut -d '!' -f1`
	echo "Processing Import Request ID:"$ID
        ksh $QueryExecuter $ID $ForcedID &
	
	Counter2=$(($Counter2+1))
	if [ $Counter2 -ge 400 ] 
		then
			sleep 10
			Counter2=0
	fi	
done
rm $PendingImports 2>/dev/null
rm $PendingImportsCount 2>/dev/null
rm Error_$Rand
echo "All Data Imported"
