
QueryExecuter=/app_univ/AutoImporter/QueryExecuter.sh
Importer_Process=`ps -ef | grep Auto_Importer.sh | grep -v grep | wc -l | sed 's/^ *//g'`
Importer_Process=$(($Importer_Process+0))
while [ $Importer_Process -gt 1 ]
     do
        echo `date`" Found another running Process, Exiting"
        exit
done

Rand=$RANDOM
rm Error_$Rand 2>/dev/null
PendingImportsCount=/share/Importers/AutoImport/Pending_Count_$Rand
PendingImports=/share/Importers/AutoImport/Pending_$Rand
rm $PendingImports 2>/dev/null
		
Command="SELECT ifnull(COUNT(*),0) from Main_Conf.\`Pending Imports\` WHERE Status='Pending' and ForcedID ='0' INTO OUTFILE '$PendingImportsCount'"
mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command"
Count=`cat $PendingImportsCount`
Count=`expr "$Count" + 0`
rm $PendingImportsCount

Command="SELECT ID FROM Main_Conf.\`Pending Imports\` WHERE Status='Pending' and ForcedID ='0' ORDER BY ID ASC INTO OUTFILE '$PendingImports' FIELDS TERMINATED BY '!'"
mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command"
	
Counter=0
Counter2=0

while [ $Counter -lt $Count ]
	do
	Counter=$(($Counter+1))
        ID=`head -$Counter $PendingImports | tail -1 | cut -d '!' -f1`
	echo "Processing Import Request ID:"$ID
        ksh $QueryExecuter $ID &

	Process_Count=`ps -ef | grep AutoImporter | wc -l | sed 's/^ *//g'`	
	#Counter2=$(($Counter2+1))
	while [ $Process_Count -ge 300 ]
		do
			echo "Witing"
			sleep 10
			Process_Count=`ps -ef | grep AutoImporter | wc -l | sed 's/^ *//g'`
	done
	#if [ $Counter2 -ge 100 ] 
	#	then
	#		sleep 20
	#		Counter2=0
	#fi	
done
rm $PendingImports 2>/dev/null
rm $PendingImportsCount 2>/dev/null
rm Error_$Rand
echo "All Data Imported"
