
ID=$1
ForcedID=$2

if [ "`echo $ID | grep -i "@"`" = "" ]
	then
		echo "no need to replace @"
	else
		ID=`echo $1| cut -d"@" -f1`
		ForcedID=`echo $1| cut -d"@" -f2`
fi

if [ "$ForcedID" = "" ]
        then

		Importer_Process=`ps -ef | grep QueryExecuter.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		Importer_Process=$(($Importer_Process+0))
		while [ $Importer_Process -gt 400 ]
			do
				echo `date`" Found another running Process, Ending Process"
				exit
				sleep 10
				Importer_Process=`ps -ef | grep QueryExecuter.sh | grep -v grep | wc -l | sed 's/^ *//g'`
				Importer_Process=$(($Importer_Process+0))
		done
		ForcedID=0
fi

Rand=$RANDOM
PendingImports=/share/Importers/AutoImport/Pending_$Rand
rm -f Error_$Rand 2>/dev/null
rm -f $PendingImports 2>/dev/null
sleep 1
		
Command="SELECT FilePath,MachineIP,RemoveFile,WithCharacterSet,TableName,Columns,FieldsTerminatedBy,DatabaseName,ID,\`Replace\` FROM Main_Conf.\`Pending Imports\` WHERE ID='$ID' and Status='Pending' and ForcedID ='$ForcedID' INTO OUTFILE '$PendingImports' FIELDS TERMINATED BY '!'"
#mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command" Main_Conf
mysql --login-path=Importer -hCNPVAS03 -P3307 -e"$Command" Main_Conf

if [ -s "$PendingImports" ] 
	then
		X=1
	else
		echo "Didn't find the requested Pending ID"
		#ls -lrt $PendingImports
		rm -f $PendingImports
		exit
fi	

	
FilePath=`head -1 $PendingImports | tail -1 | cut -d '!' -f1`
MachineIP=`head -1 $PendingImports | tail -1 | cut -d '!' -f2`
RemoveFile=`head -1 $PendingImports | tail -1 | cut -d '!' -f3`
WithCharacterSet=`head -1 $PendingImports | tail -1 | cut -d '!' -f4`
TableName=`head -1 $PendingImports | tail -1 | cut -d '!' -f5`
Columns=`head -1 $PendingImports | tail -1 | cut -d '!' -f6`
FieldsTerminatedBy=`head -1 $PendingImports | tail -1 | cut -d '!' -f7`
DatabaseName=`head -1 $PendingImports | tail -1 | cut -d '!' -f8`
ID=`head -1 $PendingImports | tail -1 | cut -d '!' -f9`
Replace=`head -1 $PendingImports | tail -1 | cut -d '!' -f10`
	
echo "Processing Import Request ID:"$ID
	
Command="UPDATE Main_Conf.\`Pending Imports\` SET Status='Processing' WHERE ID='$ID'"	
#mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command" Main_Conf &
mysql --login-path=Importer -hCNPVAS03 -P3307 -e"$Command" Main_Conf &

FileName=`echo $FilePath | sed 's/.*\///g'`
NewFileName=$FileName"_"$ID"_"$ForcedID
#echo "Moving File $FileName To  /share/Importers/StatsCollector/$NewFileName"
mv $FilePath /share/Importers/AutoImport/$NewFileName 2>Error_$Rand
	
if [ -s Error_$Rand ]
	then
		echo "Found Error"
		Error=`cat Error_$Rand | sed "s/\'//g"`
		Command="UPDATE Main_Conf.\`Pending Imports\` SET Status='Error',Error_Desc='$Error' WHERE ID='$ID' and ForcedID ='$ForcedID'"
       		#mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command" Main_Conf &
       		mysql --login-path=Importer -hCNPVAS03 -P3307 -e"$Command" Main_Conf &
		rm -f Error_$Rand 2>/dev/null
	else
		
		if [ "$Replace" = "Replace" ] || [ "$Replace" = "REPLACE" ] || [ "$Replace" = "replace" ] || [ "$Replace" = "True" ] || [ "$Replace" = "true" ] || [ "$Replace" = "TRUE" ]
			then
				Command="LOAD DATA INFILE '/share/Importers/AutoImport/$NewFileName' REPLACE INTO TABLE \`$DatabaseName\`.\`$TableName\` CHARACTER SET UTF8 FIELDS TERMINATED BY '$FieldsTerminatedBy' ($Columns)" 
			else
				Command="LOAD DATA INFILE '/share/Importers/AutoImport/$NewFileName' IGNORE INTO TABLE \`$DatabaseName\`.\`$TableName\` CHARACTER SET UTF8 FIELDS TERMINATED BY '$FieldsTerminatedBy' ($Columns)"
		fi
		echo "Importing file:"$FilePath
		echo $Command
#		if [ "$ForcedID" = "0" ]
#			then
				#mysql -h$MachineIP -uImporter -pImporter123 -e"$Command" 2>Error_$Rand
				mysql --login-path=Importer -h$MachineIP -e"$Command" 2>Error_$Rand
#			else
#				#mysql -h$MachineIP -uImporter -pImporter123 -e"$Command" 2> Error_$Rand &
				mysql --login-path=Importer -h$MachineIP -e"$Command" 2> Error_$Rand &
#		fi
#		Loading=`ps auxww | grep LOAD | grep $FileName_$ID_$ForcedID | grep -v grep`
#		while [ "$Loading" != "" ]
#			do	
#				echo "Waiting for Load data finish"
#				sleep 10
#		 		Loading=`ps auxww | grep LOAD | grep $NewFileName | grep -v grep`
#		done
			
		if [ -s Error_$Rand ]
			then
	                        echo "Found Error Load Data"
				Error=`cat Error_$Rand | sed "s/\'//g"`
       		                Command="UPDATE Main_Conf.\`Pending Imports\` SET Status='Error',Error_Desc='$Error' WHERE ID='$ID' and ForcedID ='$ForcedID'"
                      		#mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command" Main_Conf &
                      		mysql --login-path=Importer -hCNPVAS03 -P3307 -e"$Command" Main_Conf &
				mv /share/Importers/AutoImport/$NewFileName $FilePath 
				#rm -f Error_$Rand 2>/dev/null
			else
				Command="UPDATE Main_Conf.\`Pending Imports\` SET Status='Finished' WHERE ID='$ID' and ForcedID ='$ForcedID'"
		        	mysql --login-path=Importer -hCNPVAS03 -P3307 -e"$Command" Main_Conf
		        	#mysql -hCNPVAS03 -P3307 -uImporter -pImporter123 -e"$Command" Main_Conf
				
				if [ "$RemoveFile" = "TRUE" ] || [ "$RemoveFile" = "True" ] || [ "$RemoveFile" = "true" ]
					then
						echo "Removing file: /share/Importers/AutoImport/$NewFileName"
						rm -f /share/Importers/AutoImport/$NewFileName
				fi
				echo "Import Finished"
		fi
fi
rm -f $PendingImports 2>/dev/null
rm -f Error_$Rand 2>/dev/null
