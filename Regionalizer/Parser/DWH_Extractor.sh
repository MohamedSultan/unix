
Pattern=$1
BIA_SERVER_HOME=/app/Services/Regionalizer/Server/; export BIA_SERVER_HOME
cd /SMS_fc/LU/$Pattern
OutputDir=/SMS_fc/LU/Parsed/$Pattern
for FileName in `ls *.dat`
	do
		mv $FileName $FileName".Processing"

		bia_data_Reader $FileName".Processing"| /usr/xpg4/bin/grep -e"startTimeStamp in string format" -e"Start Time in string format" -e"IMSI" -e"IuLac" -e"IuSac" -e"IMSI as a string:" -e"FirstCellLac" -e"FirstCellId" -e"Begin of HybridDataRecord section." | sed 's/\*//g' |sed 's/:IMEI as a string: /IMEI:/g;s/:FirstCellLac : /Lac:/g;s/:IMSI as a string: /IMSI:/g;s/FirstCellId: /CellID:/g;s/IMEISV as a string: /IMEISV:/g;s/startTimeStamp in string format: /Date:/g;s/IT7_Variable::DataId           /XI/g;s/IT7_Variable::DataField        /F/g;s/Begin of HybridDataRecord section. /XN/g;s/IMSI                           : /IMSI:/g;s/IMEI                           : /IMEI:/g;s/IMEISV                         : /IMEISV:/g;s/Start Time in string format    : /Date:/g' | tr '\n' ':' | tr 'X' '\n' | tr 'X' '\n' |  tr '\n' 'X' |  tr 'N' '\n' | awk -F"," '{print substr($1,13,4)"-"substr($1,7,2)"-"substr($1,10,2)"|"substr($1,18,2)"|"$0"|"FileFrom }' FileFrom=$FromDir | sed 's/:X//g;s/:Date://g;s/:IMSI as a string: /|/g;s/:IMSI:/|/g;s/:MobilityInfo IuLac: /|/g;s/:MobilityInfo IuSac: /|/g;s/:FirstCellLac : /|/g;s/:CellID:/|/g;s/:IMEI as a string: /|/g;s/:IMEISV:/|/g;s/:XI: /|/g;s/:ADR::IMEI is not yet populated. /|/g;s/:ADR::IMEISV is not yet populated. /|/g' | cut -d"|" -f1,2,3,4,5,6,7,8 > $OutputDir"/"$FileName
	mv $FileName".Processing" $FileName".Parsed"

done
