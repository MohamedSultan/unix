
Dir=$1
Force=$2
#CurrentDir=`pwd`
DirTest=`echo $Dir | sed 's/.*\/\//XXXXX/g'`

if [ ! -d "$Dir" ] || [ "$DirTest" = "XXXXX" ]
	then
		echo "Dir you are requesting to uniq :$Dir is not exists, Exiting Uniq.sh"
		exit
#	else
#		echo "Dir Exit"
fi
cd $Dir
InFiles=`ls | sed 's/ *//g'`
#echo "uniqing files in $Dir, current dir is "`pwd`
if [ "$InFiles" = "Uniq" ]
	then
#		echo "Only Uniq file exists, nothing to do"
		cd $CurrentDir
		exit
else
		echo "Found more files to process on dir:$Dir"
fi

NumberOfFiles=`ls -l | wc -l | sed 's/^ *//g'`
if [ $NumberOfFiles -ge 10 ] || [ "$Force" = "Force" ] || [ "$Force" = "force" ]
	then
		if [ -e Processing ]
			then
				rm Processing
		fi
		mkdir Processing
		mv *_IMSI* ./Processing
		mv *.dat* ./Processing
		if [ -e "Uniq" ]
			then
				mv Uniq ./Processing
		fi
		cd Processing
		cat * | sort -r -t"|" -k1| /usr/xpg4/bin/awk -F"|" '!a[$1]++' | sort  > ../Uniq
		chmod 777 ../Uniq
		cd ..
		rm -fr Processing
else
	echo "nothing to do, number of files less than 10"
	cd $CurrentDir
fi
cd $CurrentDir
#echo "Exit of Uniq"
