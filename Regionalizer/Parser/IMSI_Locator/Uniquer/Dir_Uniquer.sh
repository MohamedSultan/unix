
Dir=$1
cd $Dir
for Hours in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
	do
		cd $Dir/$Hours
		for Dirs in `ls`
			do
				if [ -d $Dirs ]
					then
						echo "Uniqing Dir: $Dir/$Hours/$Dirs"
						ksh /app/Services/Regionalizer/Uniquer/Uniquer.sh $Dir/$Hours/$Dirs Force
					else
						echo "$Dirs is a file"
				fi
		done
done
