DateDir=$1
oDir=$2

CurrentDir=`pwd`
Rand=$RANDOM
Counter=1
MaxConcurrentProcesses=1

OtherProcess=`ps -ef | grep Splitter.sh | grep "$DateDir" | grep -v grep | wc -l | sed 's/^ *//g'`

if [ $OtherProcess -gt 5 ] 
        then
                echo "Splitter:Found another Process or MaxConcurrentProcesses greater than $MaxConcurrentProcesses, exit process, Other Proc
esses:$OtherProcess"
                exit
        else
                echo "Didn't find any other running processes, continuing with the app"
fi

if [ ! -d $oDir ]
        then
                echo "Dir not found"
                exit
	else
		cd $oDir
fi

for iFile in `ls`
        do
                if [ ! "$iFile" = "Processing" ] && [ ! "$iFile" = "" ] && [ ! "$iFile" = "Splitted" ]
                        then
                                echo "Splitting File:$iFile into Hours and IMSIs"
                                if [ ! -d Processing ]
                                        then
                                                mkdir Processing
                                fi
                                mv $iFile Processing
                                cd Processing
                                /usr/xpg4/bin/awk  '{FS="|";OFS="|"}{print $3,$4,$5,$6 > opFileName"_Hour_"$2"_IMSI_"substr($3,14,2)}' opFileName=$iFile $iFile
                                rm $iFile
                                if [ ! -d ../Splitted ]
                                        then
                                                mkdir ../Splitted
                                fi
				echo "Moving Splitted Files"
                                mv $iFile"_"* ../Splitted &
				cd $CurrentDir/Distributers
				ksh Distributer.sh $DateDir &
                                cd $oDir
                        else
                                echo "not parsing $iFile"
                fi
done
