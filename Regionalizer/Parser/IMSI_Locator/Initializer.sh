Pattern=$1
FilePath=$2

FileName=`echo $FilePath | sed 's/.*\///g'`
Rand=$RANDOM
#OutputDir=/app/Services/Regionalizer/oFiles/
OutputDir=/app_univ/Regionalizer/oFiles/
FromDir=`echo $FilePath|sed 's/iFiles\///g;s/\/hdr.*//g'`
#ProcessingDir=$OutputDir"/Processings/"$Rand"/"
ProcessingDir=$OutputDir"/Processings/"$Pattern"/"
CurrentDir=`pwd`

Size=`du -sh $FilePath | sed 's/^ *//g' | tr '\t' ';' | cut -d";" -f1`
Command="INSERT INTO Extractor(FileName,FileSize,Path) VALUES ('$FileName','$Size','$FilePath')"
mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Loggers &

if [ -e "$FilePath" ]
	then
		if [ -d "$ProcessingDir" ]
			then
				echo "ProcessingDir:$ProcessingDir exists"
			else
				echo "Creating Dir:$ProcessingDir"
				mkdir -p $ProcessingDir
		fi

		cat $FilePath | sed 's/:/|/' | cut -d"|" -f1,3,7,10,11  > $ProcessingDir/$FileName  

		cd $ProcessingDir
		cat $FileName | grep -v '\--' | /usr/xpg4/bin/awk  -F"|" '{print > $1}' 
		rm $FileName

		for DateDirs in `ls 20*`
			do
				echo "checking $DateDirs"
				if [ -d "$OutputDir/$DateDirs/Pendings/Processing" ] && [ -d "$OutputDir/$DateDirs/Pendings/Splitted" ]
					then
						echo "Dir: $DateDirs exists"
					else
						echo "Creating Dir:$DateDirs"
						mkdir /db/oFiles/$DateDirs
						ln -fns /db/oFiles/$DateDirs $OutputDir/$DateDirs
						mkdir -p $OutputDir"/"$DateDirs"/Pendings/Processing"
						mkdir -p $OutputDir"/"$DateDirs"/Pendings/Splitted"
						chmod 777 $OutputDir/$DateDirs
				
						for Hours in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
							do
								if [ ! -d $OutputDir/$DateDirs/$Hours ]
									then
										mkdir $OutputDir/$DateDirs/$Hours
										chmod 777 $OutputDir/$DateDirs/$Hours
										for IMSIs in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99
											do  
												mkdir $OutputDir/$DateDirs/$Hours/$IMSIs
				       			                                         chmod 777 $OutputDir/$DateDirs/$Hours/$IMSIs
										done
								fi
						done
				fi
				echo "Moving corresponding File content"
				mv $DateDirs $OutputDir"/"$DateDirs"/Pendings/"$FileName$Rand
				cd $CurrentDir
				ksh Splitter.sh $DateDirs $OutputDir"/"$DateDirs"/Pendings/"
		done

		cd $CurrentDir
		#rm -r $ProcessingDir
		#mv $FilePath $FilePath.XXX
		#rm $FilePath
	else
		echo "File:$FilePath does not exist"
fi
