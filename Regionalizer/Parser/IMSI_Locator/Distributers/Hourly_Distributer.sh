
#Mark=$1
DateDir=$1
DataDir=/app/Services/Regionalizer/oFiles/$DateDir
Hours=$2
Rand=$RANDOM
CurrentDir=`pwd`
MaxConcurrentProcesses=1
Max_Uniq_Processes=15

#OtherProcess=`ps -ef | grep Hourly_Distributer.sh | grep "$DateDir" | grep " $Hours"| grep -v grep | wc -l | sed 's/^ *//g'`
OtherProcess=`ps -ef | grep "Hourly_Distributer.sh $DateDir $Hours"| grep -v grep | wc -l | sed 's/^ *//g'`
OtherProcess=$(($OtherProcess+0))

if [ $OtherProcess -gt $MaxConcurrentProcesses ]
        then
                echo "Hourly Distributer:$Hours: Found another Process or MaxConcurrentProcesses greater than $MaxConcurrentProcesses, exit process, Other Processes:$OtherProcess"
                exit
        else
                echo "Didn't find any other running processes, continuing with the app"
fi

cd $DataDir/Pendings/Splitted
rm *__* 
rm *_IMSI_ 

echo "mv *_Hour_"$Hours"_* $DataDir/$Hours/"
mv *"_Hour_"$Hours* $DataDir/$Hours/
for IMSIs in 0 1 2 3 4 5 6 7 8 9 
	do
          	echo "mv *_Hour_"$Hours"_IMSI_$IMSIs* $DataDir/$Hours/"
          	mv *"_Hour_"$Hours"_IMSI_"$IMSIs* $DataDir/$Hours/
done

echo "========================================================================================="
echo "========================================================================================="

for IMSIs in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99
	do 
		rm -f $DataDir/$Hours/*_IMSI_
		echo "mv $DataDir/$Hours/*_IMSI_"$IMSIs" $DataDir/$Hours/$IMSIs "
		mv $DataDir/$Hours/*"_IMSI_"$IMSIs $DataDir/$Hours/$IMSIs  
				
		Uniq_Process=`ps -ef | grep Uniquer.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		Uniq_Process=$(($Uniq_Process+0))
		while [ $Uniq_Process -gt $Max_Uniq_Processes ]
        		do
				echo "$DataDir/$Hours/$IMSIs: Waiting ...., Uniq_Process=$Uniq_Process"
               			sleep 2
				Uniq_Process=`ps -ef | grep Uniquer.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		done
		cd $CurrentDir/../Uniquer
		ksh Uniquer.sh $DataDir/$Hours/$IMSIs Force & 
done


