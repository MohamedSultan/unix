
DateDir=$1
CurrentDir=`pwd`

for Hours in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
        do
                echo "Distributing Data of Date:$DateDir, Hour:$Hours"
		cd $CurrentDir
		ksh Hourly_Distributer.sh $DateDir $Hours  &
		sleep 1
done
