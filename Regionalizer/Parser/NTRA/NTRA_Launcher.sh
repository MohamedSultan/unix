
Rand=$RANDOM

OtherProcess=`ps -ef | grep NTRA_Launcher.sh | grep -v grep | wc -l | sed 's/^ *//g'`
if [ $OtherProcess -gt 2 ] 
        then
                echo "Found another Process, exit process"
                exit
        else
                echo "Didn't find any other running processes, continuing with the app"
fi

Command="SELECT TransID,Pattern,oFilePath,iFilePath FROM Extractor WHERE State='Parsed' and NTRA is null INTO OUTFILE '/share/Exporters/NTRA/$Rand' FIELDS TERMINATED BY '|'"
mysql -uReader -pReader -hCNPVAS03 -P3307 -e"$Command" Loggers

for Files in `cat /share/Exporters/NTRA/$Rand`
	do
		TransID=`echo $Files | cut -d"|" -f1`
		Pattern=`echo $Files | cut -d"|" -f2`
		oFilePath=`echo $Files | cut -d"|" -f3`
		iFilePath=`echo $Files | cut -d"|" -f4`
		#FilePath=`echo $iFilePath | sed 's/iFiles/CDRs/'`

		FileName=`echo $oFilePath | sed 's/.*\///g'`

		NTRA_Process=`ps -ef | grep NTRA.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		NTRA_Process=$(($NTRA_Process+0))
		while [ $NTRA_Process -gt 7 ]
     			do
        			echo `date`"NTRA Process still running.." 
				echo ""
			        sleep 10 
			        NTRA_Process=`ps -ef | grep NTRA.sh | grep -v grep | wc -l | sed 's/^ *//g'`
		done
		
		echo "Starting processing of $iFilePath , $TransID"
		#-----------------------------------------------------
		Command="UPDATE Extractor SET NTRA='Processing' WHERE TransID='$TransID' AND iFilePath='$iFilePath'"
		mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"$Command" Loggers &

		ksh NTRA.sh "$oFilePath" "$Pattern" "$TransID" "$iFilePath" &
done
