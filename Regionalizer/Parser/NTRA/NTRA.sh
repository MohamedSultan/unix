cd /Flash/NTRA

FilePath=$1
Pattern=$2
TransID=$3
DB_FileName=$4

Rand=$RANDOM

FileName=`echo $FilePath | sed 's/.*\///g'`
#DB_FileName=`echo $FilePath | sed 's/CDRs/iFiles/'`

mysql -uReader -pReader -hCNPVAS03 -e"SELECT distinct IMSI FROM IMSIs" NTRA > IMSIs_$Rand
sort IMSIs_$Rand > IMSIs_Sorted_$Rand
cp $FilePath .
gzcat $FileName | sort -t"|" -k4 > $FileName".Sorted_2G"
gzcat $FileName | sort -t"|" -k7 > $FileName".Sorted_3G"

echo "Processing File:$FilePath"
join -j1 1 -j2 6 -t"|" -o 2.1 2.2 2.4 2.9 2.10 2.11 2.12 2.13 2.14 IMSIs_Sorted_$Rand $FileName".Sorted_2G" | awk -F"|" '{print Type"|"FilePath_"|"$1"|"substr($2,11,8)"|"$3"|"$4"|"$5"|"$6"|"$7"|"$8"|"$9}' Type=$Pattern FilePath_=$FilePath > /share/Importers/NTRA/$Rand
join -j1 1 -j2 7 -t"|" -o 2.1 2.3 2.7 2.9 2.10 IMSIs_Sorted_$Rand $FileName".Sorted_3G" | awk -F"|" '{print Type"|"FilePath_"|"$1"|"substr($2,11,8)"|"$3"|"$4"|"$5"|""|""|""|"}' Type=$Pattern FilePath_=$FilePath >> /share/Importers/NTRA/$Rand

rm $FileName".Sorted"
rm $FileName".Sorted_3G"
rm $FileName".Sorted_2G"
rm IMSIs_Sorted_$Rand
rm IMSIs_$Rand

command="LOAD DATA INFILE '/share/Importers/NTRA/$Rand' REPLACE INTO TABLE CurrentLoc FIELDS TERMINATED BY '|' (G,FilePath,Date,Time,IMSI,LAC,Cell,BssMapCause,\`Paging Cause\`,\`CC Cause\`,\`MM Cause\`)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$command" NTRA
command="LOAD DATA INFILE '/share/Importers/NTRA/$Rand' REPLACE INTO TABLE HistoryLoc FIELDS TERMINATED BY '|' (G,FilePath,Date,Time,IMSI,LAC,Cell,BssMapCause,\`Paging Cause\`,\`CC Cause\`,\`MM Cause\`)"
mysql -uWriter -pWriter -hCNPVAS03 -e"$command" NTRA

Command="UPDATE Extractor SET NTRA='Done' WHERE TransID='$TransID' AND iFilePath='$DB_FileName'"
mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"$Command" Loggers &

rm /share/Importers/NTRA/$Rand
rm $FileName
echo "$FilePath is Processed"

