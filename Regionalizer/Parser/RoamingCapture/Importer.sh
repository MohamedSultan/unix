DataType=$1
FilePath=$2

if [ "$DataType" = "A" ]
	then
		echo "Importing Air Interface"
		ksh /app_univ/StatsCollector/Importer.sh $FilePath CNPVAS03 "AIR Interface" "\`Start Time\`,\`End Time\`,IMSI,MSC,BSC,LastCellLac,CurrentCellId,BssMapCause,dtapRrCause,dtapCcCause,dtapMmCause,CallType,VarDataId1,CallType1,VarDataField1,VarDataId2,CallType2,VarDataField2,VarDataId3,CallType3,VarDataField3,VarDataId4,CallType4,VarDataField4,VarDataId5,CallType5,VarDataField5,VarDataId6,CallType6,VarDataField6,VarDataId7,CallType7,VarDataField7,VarDataId8,CallType8,VarDataField8,VarDataId9,CallType9,VarDataField9,VarDataId10,CallType10,VarDataField10,VarDataId11,CallType11,VarDataField11,VarDataId12,CallType12,VarDataField12,VarDataId13,CallType13,VarDataField13,VarDataId14,CallType14,VarDataField14,VarDataId15,CallType15,VarDataField15,VarDataId16,CallType16,VarDataField16,VarDataId17,CallType17,VarDataField17,VarDataId18,CallType18,VarDataField18,VarDataId19,CallType19,VarDataField19,VarDataId20,CallType20,VarDataField20" "|" RoamingCapture "Roaming Capture A"
fi
if [ "$DataType" = "IUCS" ]
	then
		echo "Importing IUCS"
		ksh /app_univ/StatsCollector/Importer.sh $FilePath CNPVAS03 "IUCS" "\`Start Time\`,\`End Time\`,\`Call Type\`,IMSI,\`MobilityInfo IuLac\`,\`MobilityInfo IuSac\`,\`MobilityInfo IuMCC\`,\`MobilityInfo IuMNC\`,VarDataId1,VarDataField1,VarDataId2,VarDataField2,VarDataId3,VarDataField3,VarDataId4,VarDataField4,VarDataId5,VarDataField5,VarDataId6,VarDataField6,VarDataId7,VarDataField7,VarDataId8,VarDataField8,VarDataId9,VarDataField9,VarDataId10,VarDataField10,VarDataId11,VarDataField11,VarDataId12,VarDataField12,VarDataId13,VarDataField13,VarDataId14,VarDataField14,VarDataId15,VarDataField15,VarDataId16,VarDataField16,VarDataId17,VarDataField17,VarDataId18,VarDataField18,VarDataId19,VarDataField19,VarDataId20,VarDataField20,VarDataId21,VarDataField21,VarDataId22,VarDataField22,VarDataId23,VarDataField23,VarDataId24,VarDataField24,VarDataId25,VarDataField25,VarDataId26,VarDataField26,VarDataId27,VarDataField27,VarDataId28,VarDataField28,VarDataId29,VarDataField29,VarDataId30,VarDataField30,VarDataId31,VarDataField31,VarDataId32,VarDataField32,VarDataId33,VarDataField33,VarDataId34,VarDataField34,VarDataId35,VarDataField35,VarDataId36,VarDataField36,VarDataId37,VarDataField37,VarDataId38,VarDataField38,VarDataId39,VarDataField39,VarDataId40,VarDataField40" "|" RoamingCapture "Roaming Capture IUCS"
fi
if [ "$DataType" = "MAP" ]
	then
		echo "Importing MAP"
		ksh /app_univ/StatsCollector/Importer.sh $FilePath CNPVAS03 "MAP" "\`Start Time\`,\`End Time\`,OPC,DPC,TcapAbortCause,\`Calling Digits\`,\`Called Digits\`,LastMsgOpErrorCode,callType,VarDataId1,VarDataField1,VarDataId2,VarDataField2,VarDataId3,VarDataField3,VarDataId4,VarDataField4,VarDataId5,VarDataField5,VarDataId6,VarDataField6,VarDataId7,VarDataField7,VarDataId8,VarDataField8,VarDataId9,VarDataField9,VarDataId10,VarDataField10,VarDataId11,VarDataField11,VarDataId12,VarDataField12,VarDataId13,VarDataField13,VarDataId14,VarDataField14,VarDataId15,VarDataField15,VarDataId16,VarDataField16,VarDataId17,VarDataField17,VarDataId18,VarDataField18,VarDataId19,VarDataField19,VarDataId20,VarDataField20" "|" RoamingCapture "Roaming Capture MAP"
fi

