Search_Pattern=$1
MaxConcurrentProcesses=$2
Random=$RANDOM
Files2B_Parsed_Dir=Files2B_Parsed/Files2B_Parsed_$Random

CurrentDir=`pwd`

find /reg_share/iFiles/ -name "*.Parsed" -exec rm {} \; 

find /reg_share/iFiles/$Search_Pattern -name "*.dat" | grep $Search_Pattern > $Files2B_Parsed_Dir
find /reg_share/iFiles/$Search_Pattern -name "*.lba" | grep $Search_Pattern >> $Files2B_Parsed_Dir
#MaxConcurrentProcesses=30
CurrentDir=`pwd`

#------Parser and Extractor Processes checker------------------------
OtherProcess=`ps -ef | grep Parser.sh | grep $Search_Pattern | grep -v grep | wc -l | sed 's/^ *//g'`
CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern| grep -v grep | wc -l | sed 's/^ *//g'`
if [ $OtherProcess -gt 1 ] && [ $CurrentConcurrentProcesses -gt $MaxConcurrentProcesses ]
	then
		echo "Found another Process or MaxConcurrentProcesses greater than $MaxConcurrentProcesses, exit process, Other Processes:$OtherProcess, Extractors=$CurrentConcurrentProcesses"
		exit
	else
		echo "Didn't find any other running processes, continuing with the app"
fi
#-------------------------------------------------------------------------------------------------------
#-------Lanching Extractors---------------------------------------
#-------------------------------------------------------------------------------------------------------
for Files in `cat $Files2B_Parsed_Dir`
	do
		FileName=`echo $Files | sed 's/.*\///g'`
		echo $Files","$FileName
		CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern| grep -v grep | wc -l | sed 's/^ *//g'`
		#CPU=`top | grep 'CPU states:'| cut -d"," -f2 | sed 's/user//g;s/\%//g;s/ *//g;s/\.//'`
		CPU=`prstat -t 1 1 | grep Region| tail -1 | sed 's/ /|/g;s/\%//g' | awk -F"|" '{print $NF}'`
		if [ $CurrentConcurrentProcesses -le $MaxConcurrentProcesses ] && [ $CPU -le 70 ]
			then
				echo "Processings $FileName"
				ksh $CurrentDir/Extractor.sh $Search_Pattern $Files & #$FileName &
				echo "Processing $FileName ..."
			else
				while [ $CurrentConcurrentProcesses -gt $MaxConcurrentProcesses ] && [ $CPU -le 70 ]
					do
						echo "Found more than $MaxConcurrentProcesses processes=$CurrentConcurrentProcesses, waiting 10 seconds; CPU=$CPU"
						sleep 20
						CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern | grep -v grep | wc -l | sed 's/^ *//g'`
						#CPU=`top | grep 'CPU states:'| cut -d"," -f2 | sed 's/user//g;s/\%//g;s/ *//g;s/\.//'`
						CPU=`prstat -t 1 1 | grep Region| tail -1 | sed 's/ /|/g;s/\%//g' | awk -F"|" '{print $NF}'`
				done
				ksh $CurrentDir/Extractor.sh $Search_Pattern $Files & #$FileName &
		fi
			
done
cd $CurrentDir
rm $Files2B_Parsed_Dir
echo "All Files are parsed"
