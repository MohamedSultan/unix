Pattern=$1
FilePath=$2
#FileName=$3
FileName=`echo $FilePath | sed 's/.*\///g'`
FileDate=`echo $FileName | cut -d"." -f2 | awk '{print substr($1,5,4)"-"substr($1,1,2)"-"substr($1,3,2)}'`
Rand=$RANDOM
OutputDir=/reg_share/CDRs/$FileDate/$Pattern
ProcessingDir=/Flash/CDR_Creator_Processing/$Pattern
CurrentDir=`pwd`
BIA_SERVER_HOME=/app_univ/Regionalizer/Server/; export BIA_SERVER_HOME
HostName=`hostname`

echo "$Pattern"


if [ -e "$FilePath" ]
	then
		mv $FilePath $FilePath".Processing."$HostName
		cp $FilePath".Processing."$HostName $ProcessingDir/$FileName

		Size=`du -sh $FilePath".Processing."$HostName | sed 's/^ *//g' | tr '\t' ';' | cut -d";" -f1`
		Command="INSERT INTO Extractor(TransID,iFilePath,FileSize,Hostname,Pattern) VALUES ('$Rand','$FilePath','$Size','$HostName','$Pattern')"
		mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"$Command" Loggers &

		mkdir -p $OutputDir
		mkdir -p $ProcessingDir
		#FilePath=$FilePath".Processing"


		bia_data_Reader $ProcessingDir/$FileName | sed 's/ //g' | /usr/xpg4/bin/grep -e"startTimeStampinstringformat:" -e"endTimeStampinstringformat:" -e"IMSIasastring:" -e"MSC:" -e"MSCasaninteger:" -e"BSC:" -e"BSCasaninteger:" -e"LastCellLac:" -e"CurrentCellId:" -e"BssMapCause:" -e"dtapRrCause:" -e"dtapCcCause:" -e"dtapMmCause:" -e"IMEIasastring:" -e"CalledNumasastring:" -e"CallType:" -e"IT7_Variable::DataId:" -e"IT7_Variable::CallType:" -e"IT7_Variable::ParmType:" -e"IT7_Variable::DataField:" -e"Timeinstringformat:" -e"StartTimeinstringformat:" -e"EndTimeinstringformat:" -e"CallType:" -e"SCCPCauseType:" -e"SCCPCauseValue:" -e"IMSI:" -e"IMEI:" -e"MobilityInfoIuLac:" -e"MobilityInfoIuSac:" -e"MobilityInfoIuMCC:" -e"MobilityInfoIuMNC:" -e"IT7_Variable::DataId:" -e"IT7_Variable::CallType:" -e"IT7_Variable::ParmType:" -e"IT7_Variable::DataField:" -e"OPC:" -e"DPC:" -e"RANAPCAUSE(inclextbit):" -e"PAGINGCAUSE:" -e"CCCAUSE:" -e"MMCAUSE:" -e"CalledPartyNumber:" -e"callnumber:" -e"Equipment:Processor:CallNumber:" > $ProcessingDir/$FileName".CDR"

		cat $ProcessingDir/$FileName".CDR" | sed 's/StartTimeinstringformat:/^/g;s/startTimeStampinstringformat:/^/g' | tr '\n' '|' | tr '^' '\n' | sed 's/EndTimeinstringformat://g;s/SCCPCauseType://g;s/SCCPCauseValue://g;s/IMSI://g;s/IMEI://g;s/MobilityInfoIuLac://g;s/MobilityInfoIuSac://g;s/MobilityInfoIuMCC://g;s/MobilityInfoIuMNC://g;s/OPC://g;s/DPC://g;s/RANAPCAUSE//g;s/PAGINGCAUSE://g;s/CCCAUSE://g;s/MMCAUSE://g;s/CalledPartyNumber://g;s/(//g;s/)//g;s/inclextbit://g;s/endTimeStampinstringformat://g;s/IMSIasastring://g;s/MSC://g;s/MSCasaninteger://g;s/BSC://g;s/BSCasaninteger://g;s/LastCellLac://g;s/CurrentCellId://g;s/BssMapCause://g;s/dtapRrCause://g;s/dtapCcCause://g;s/dtapMmCause://g;s/IMEIasastring://g;s/CalledNumasastring://g' | sed 's/:://g;s/IT7_VariableDataId://g;s/|IT7_VariableCallType:/./g;s/|IT7_VariableParmType:/./g;s/|IT7_VariableDataField:/./g;s/\[//g;s/\]//g;s/|Timeinstringformat:/./g;s/CallType://g;s/callnumber://g;s/Equipment:Processor:CallNumber://g' | grep -v '^$' | /usr/xpg4/bin/awk  '{FS="|";OFS="|"}{if ($1!="") {print substr($1,7,4)"-"substr($1,1,2)"-"substr($1,4,2),$0}}' > $OutputDir/$FileName 

		

		mv $FilePath".Processing."$HostName $FilePath".Parsed"
		rm $ProcessingDir/$FileName $ProcessingDir/$FileName".CDR"
		FinishedAt=`date '+%Y-%m-%d %H:%M:%S'`
		gzip $OutputDir/$FileName
		Command="UPDATE Extractor SET State='Parsed',FinishedAt='$FinishedAt',oFilePath='$OutputDir/$FileName.gz',ProcessingTime=TimeDiff('$FinishedAt',Insertion_Date_Time) WHERE iFilePath='$FilePath' AND TransID='$Rand'"
		mysql -uWriter -pWriter -hCNPVAS03 -P3307 -e"$Command" Loggers &
#		ksh /app_univ/Regionalizer/Parser/NTRA/NTRA.sh $OutputDir/$FileName $Pattern
	else
		echo "File Does not exist!!"
fi
