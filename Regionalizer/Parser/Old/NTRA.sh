
NTRA_Process=`ps -ef | grep NTRA.sh | grep -v grep | wc -l | sed 's/^ *//g'`
NTRA_Process=$(($NTRA_Process+0))
while [ $NTRA_Process -gt 7 ]
     do
        echo `date`"NTRA Process still running.." 
	exit
        sleep 2
        #NTRA_Process=`ps -ef | grep NTRA.sh | grep -v grep | wc -l | sed 's/^ *//g'`
done





cd /SMS_fc/NTRA
for Files in `ls *[D,G]`
	do
		mv $Files $Files.Processing
		echo "Processing File:$Files"
		grep -h 6020220246206[66-98] $Files.Processing | cut -d"|" -f1,3,4,5,6,7,8,9,10,11 > /share/Importers/NTRA/$Files
		command="LOAD DATA INFILE '/share/Importers/NTRA/$Files' REPLACE INTO TABLE CurrentLoc FIELDS TERMINATED BY '|' (Date,Time,IMSI,LAC,Cell,BssMapCause,\`Paging Cause\`,\`CC Cause\`,\`MM Cause\`,G)"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$command" NTRA
		command="LOAD DATA INFILE '/share/Importers/NTRA/$Files' REPLACE INTO TABLE HistoryLoc FIELDS TERMINATED BY '|' (Date,Time,IMSI,LAC,Cell,BssMapCause,\`Paging Cause\`,\`CC Cause\`,\`MM Cause\`,G)"
		mysql -uWriter -pWriter -hCNPVAS03 -e"$command" NTRA
		rm /share/Importers/NTRA/$Files
		echo "$Files is Processed"
		mv $Files.Processing ../DataCastCDRs/$Files
#		gzip ../DataCastCDRs/$Files
done
