FilePath=$2
#FileName=$3
FileName=`echo $FilePath | sed 's/.*\///g'`
Pattern=$1
Rand=$RANDOM
OutputDir=/app/Services/Regionalizer/oFiles/
FromDir=`echo $FilePath|sed 's/iFiles\///g;s/\/hdr.*//g'`
ProcessingDir=$OutputDir"/Processings/"$Rand"/"
CurrentDir=`pwd`
BIA_SERVER_HOME=/app/Services/Regionalizer/Server/; export BIA_SERVER_HOME

Size=`du -sh $FilePath | sed 's/^ *//g' | tr '\t' ';' | cut -d";" -f1`
Command="INSERT INTO Extractor(FileName,FileSize,Path) VALUES ('$FileName','$Size','$FilePath')"
mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Loggers &

if [ -e "$FilePath" ]
	then
		mkdir $ProcessingDir
		mv $FilePath $FilePath".Processing"
		FilePath=$FilePath".Processing"

		bia_data_Reader $FilePath| /usr/xpg4/bin/grep -e"startTimeStamp in string format" -e"Start Time in string format" -e"IMSI" -e"IuLac" -e"IuSac" -e"IMSI as a string:" -e"FirstCellLac" -e"FirstCellId" -e"Begin of HybridDataRecord section." -e"RANAP CAUSE (incl ext bit)" -e"PAGING CAUSE" -e"CC CAUSE" -e"MM CAUSE" -e"BssMapCause" -e"dtapCcCause" -e"dtapRrCause" -e"dtapRrCause" -e"dtapMmCause" | sed 's/\*//g' |sed 's/:IMEI as a string: /IMEI:/g;s/:FirstCellLac : /Lac:/g;s/:IMSI as a string: /IMSI:/g;s/FirstCellId: /CellID:/g;s/IMEISV as a string: /IMEISV:/g;s/startTimeStamp in string format: /Date:/g;s/IT7_Variable::DataId           /XI/g;s/IT7_Variable::DataField        /F/g;s/Begin of HybridDataRecord section. /XN/g;s/IMSI                           : /IMSI:/g;s/IMEI                           : /IMEI:/g;s/IMEISV                         : /IMEISV:/g;s/Start Time in string format    : /Date:/g;s/PAGING CAUSE  /pagingCause/g;s/RANAP CAUSE/ranapCause/g;s/:ranapCause (incl ext bit)     : /|/g' | tr '\n' ':' | tr 'X' '\n' | tr 'X' '\n' |  tr '\n' 'X' |  tr 'N' '\n' > $ProcessingDir"/"$FileName"_Temp_"$Rand

		cat $ProcessingDir"/"$FileName"_Temp_"$Rand | awk -F"," '{print substr($1,13,4)"-"substr($1,7,2)"-"substr($1,10,2)"|"substr($1,18,2)"|"$0"|"FileFrom }' FileFrom=$FromDir | sed 's/:X//g;s/:Date://g;s/:IMSI as a string: /|/g;s/:IMSI:/|/g;s/:MobilityInfo IuLac: /|/g;s/:MobilityInfo IuSac: /|/g;s/:FirstCellLac : /|/g;s/:CellID:/|/g;s/:IMEI as a string: /|/g;s/:IMEISV:/|/g;s/:XI: /|/g;s/:ADR::IMEI is not yet populated. /|/g;s/:ADR::IMEISV is not yet populated. /|/g;s/:BssMapCause: /|/g;s/:dtapRrCause: /|/g;s/:dtapCcCause: /|/g;s/:dtapMmCause: /|/g;s/:pagingCause                 : /|/g;s/:CC CAUSE                       : /|/g;s/:MM CAUSE                       : /|/g;s/:ranapCause (incl ext bit)     : /|/g' | cut -d"|" -f1,2,4,5,6,7,8,9,10,11,12,13 > $ProcessingDir$FileName"_"$Rand

		#Backup CDR Part
		#---------------
		cat $ProcessingDir"/"$FileName"_Temp_"$Rand | awk -F"," '{print substr($1,13,4)"-"substr($1,7,2)"-"substr($1,10,2)"|"substr($1,18,2)"|"$0"|"FileFrom }' FileFrom=$FromDir | sed 's/:X//g;s/:Date://g;s/:IMSI as a string: /|/g;s/:IMSI:/|/g;s/:MobilityInfo IuLac: /|/g;s/:MobilityInfo IuSac: /|/g;s/:FirstCellLac : /|/g;s/:CellID:/|/g;s/:IMEI as a string: /|/g;s/:IMEISV:/|/g;s/:XI: /|/g;s/:ADR::IMEI is not yet populated. /|/g;s/:ADR::IMEISV is not yet populated. /|/g;s/:BssMapCause: /|/g;s/:dtapRrCause: /|/g;s/:dtapMmCause: /|/g;s/:pagingCause                 : /|/g;s/:CC CAUSE                       : /|/g;s/:MM CAUSE                       : /|/g;s/:ranapCause (incl ext bit)     : /|/g;s/ /|/' | cut -d"|" -f1,2,4,5,6,7,8,9,10,11,12,13 > /app/Services/Regionalizer/NTRA/$FileName"_"$Pattern

		ksh /app/Services/Regionalizer/Parser/NTRA.sh &

		cd $ProcessingDir
		cat $ProcessingDir$FileName"_"$Rand | grep -v '\--' | /usr/xpg4/bin/awk  -F"|" '{print > $1}' 

		for DateDirs in `ls 20*`
			do
				echo "checking $DateDirs"
				if [ -d "$OutputDir/$DateDirs/Pendings/Processing" ] && [ -d "$OutputDir/$DateDirs/Pendings/Splitted" ]
					then
						echo "Dir: $DateDirs exists"
					else
						echo "Creating Dir:$DateDirs"
						mkdir /db/oFiles/$DateDirs
						ln -fns /db/oFiles/$DateDirs $OutputDir/$DateDirs
						mkdir -p $OutputDir"/"$DateDirs"/Pendings/Processing"
						mkdir -p $OutputDir"/"$DateDirs"/Pendings/Splitted"
						chmod 777 $OutputDir/$DateDirs
				
						for Hours in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
							do
								if [ ! -d $OutputDir/$DateDirs/$Hours ]
									then
										mkdir $OutputDir/$DateDirs/$Hours
										chmod 777 $OutputDir/$DateDirs/$Hours
										for IMSIs in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99
											do  
												mkdir $OutputDir/$DateDirs/$Hours/$IMSIs
				       			                                         chmod 777 $OutputDir/$DateDirs/$Hours/$IMSIs
										done
								fi
						done
				fi
				echo "Moving corresponding File content"
				mv $DateDirs $OutputDir"/"$DateDirs"/Pendings/"$FileName$Rand
				ksh $CurrentDir/Splitter.sh $DateDirs $OutputDir"/"$DateDirs"/Pendings/"
		#		ksh $CurrentDir/Distributer.sh $DateDirs $OutputDir"/"$DateDirs"/Pendings/"
		#		ksh $CurrentDir/Distributer.sh $OutputDir"/"$DateDirs"/"$FileName$Rand $OutputDir"/"$DateDirs $FileName$Rand
		done

	cd $CurrentDir
	rm -r $ProcessingDir
	#mv $FilePath $FilePath.XXX
	rm $FilePath
fi
