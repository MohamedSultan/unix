oFileDate=$1

echo "Removing Symbolic link $oFileDate"
cd /app/Services/Regionalizer/oFiles/
rm $oFileDate
echo "Moving data from /db/oFiles/$oFileDate to  /app/Services/Regionalizer/oFiles/$oFileDate"
mv /db/oFiles/$oFileDate /app/Services/Regionalizer/oFiles/
echo "Done"
