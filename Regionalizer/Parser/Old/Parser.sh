Search_Pattern=$1
MaxConcurrentProcesses=$2
Random=$RANDOM

CurrentDir=`pwd`

find /reg_share/iFiles -name "*.dat" | grep $Search_Pattern > Files2B_Parsed_$Random
#MaxConcurrentProcesses=30
CurrentDir=`pwd`

OtherProcess=`ps -ef | grep Parser.sh | grep $Search_Pattern | grep -v grep | wc -l | sed 's/^ *//g'`
CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern| grep -v grep | wc -l | sed 's/^ *//g'`
if [ $OtherProcess -gt 1 ] && [ $CurrentConcurrentProcesses -gt $MaxConcurrentProcesses ]
	then
		echo "Found another Process or MaxConcurrentProcesses greater than $MaxConcurrentProcesses, exit process, Other Processes:$OtherProcess, Extractors=$CurrentConcurrentProcesses"
		exit
	else
		echo "Didn't find any other running processes, continuing with the app"
fi
#-------------------------------------------------------------------------------------------------------

for Files in `cat Files2B_Parsed_$Random`
	do
		FileName=`echo $Files | sed 's/.*\///g'`
		#echo $Files","$FileName
		CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern| grep -v grep | wc -l | sed 's/^ *//g'`
		if [ $CurrentConcurrentProcesses -le $MaxConcurrentProcesses ]
			then
				echo "Processings $FileName"
	#			cp $Files $Files.X
				ksh $CurrentDir/Extractor.sh $Search_Pattern $Files & #$FileName &
				echo "Processing $FileName ..."
			else
				while [ $CurrentConcurrentProcesses -gt $MaxConcurrentProcesses ]
					do
						echo "Found more than $MaxConcurrentProcesses processes=$CurrentConcurrentProcesses, waiting 10 seconds"
						sleep 10
						CurrentConcurrentProcesses=`ps -ef | grep Extractor |grep $Search_Pattern | grep -v grep | wc -l | sed 's/^ *//g'`
					done
		fi
			

done
cd $CurrentDir
rm Files2B_Parsed_$Random
