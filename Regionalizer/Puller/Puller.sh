SearchingDir=$1
SaveinDir=$2
Machine=$3

Rand=$RANDOM

if [ "$3" = "" ]
	then
		Machine="Datacast"
fi

CurrentConcurrentProcesses=`ps -ef | grep $SearchingDir | grep $SaveinDir | grep -v grep | wc -l | sed 's/^ *//g'`
if [ $CurrentConcurrentProcesses -gt 1 ]
                        then
				echo "Found another process, exit script..."
				exit
			else
				echo "Didn't Find any other process, continuing with script"
fi

while [ 1 ]
	do
	Expected_FilePath=`ssh bia@$Machine "date +hdr.%m%d%Y.%H%M'*'.dat"`
	FilePath=`ssh bia@$Machine "ls ./Alkan/$SearchingDir/$Expected_FilePath"`
	FileName=`echo $FilePath | sed 's/.*\///g'`
	echo "Searching for $Expected_FilePath"
	if [ "$FilePath" = "" ]
		then
			echo "File not found, Waiting 5 Seconds"
			sleep 5 
		else
			echo "File is Found, FilePath is $FilePath"
			#scp bia@$Machine:$FilePath $SaveinDir
			scp bia@$Machine:$FilePath "/Flash/Puller/$FileName_"$Rand
			mv "/Flash/Puller/$FileName_"$Rand $SaveinDir/$FileName
			Command="REPLACE INTO Regionalizer(FilePath) VALUES('$FilePath')"
			mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Loggers
			echo "waiting 50 Seconds"
			sleep 50
	fi
done
