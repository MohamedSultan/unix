
#-----------------------Parameter Validation---------------------------------------------
#----------------------------------------------------------------------------------------
if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ] || [ "$4" = "" ] || [ "$5" = "" ] 
        then
                echo "You must enter"
                echo "1. FileName path"
                echo "2. MSISDN_Field_ID" 
		echo "3. FieldSeparator"
                echo "4. DateHour in the format YYYYMMDDHH"
		echo "5. MSISDN format in (1)201XXX (2)01XXX (3)10XXX "
		echo "6. Iterators, Default 2 Hours"
                exit
fi

Iterator=$6
Format=$5
if [ ! "$Format" = "1" ] && [ ! "$Format" = "2" ] && [ ! "$Format" = "3" ]
	then
		echo "5. MSISDN format in (1)201XXX (2)01XXX (3)10XXX "
                exit
fi

if [ "$Iterator" = "" ]
	then
		Iterator=2
fi

FilePath=$1 #Input file must be in the format of 2010XXX,
FileName=`echo $FilePath | sed 's/\(.*\)\//\1|/' | cut -d"|" -f2`
FileDir=`echo $FilePath | sed 's/\(.*\)\//\1\|/' | sed 's/\|.*//g'`"/"
MSISDN_Field_ID=$2
FieldSeparator=$3
MonitoringDateHour=$4 #Format YYYYMMDDHH Date and hour to serach in
ScriptDir=/app_univ/Regional/Regionalizer/
ProcessingsDir=$ScriptDir/Processings/
Rand=$RANDOM

Command="INSERT INTO Regionalizer_Parser(iFileName,Iterator,State,TransID) VALUES('$FilePath','$Iterator','Start','$Rand')"
mysql -uWriter -pWriter -e"$Command" -hCNPVAS01 Loggers &

#-----------------------Script Start-----------------------------------------------------
#----------------------------------------------------------------------------------------

Counter=0
FOUND=0
UNFOUND=0
if [ -e $FilePath".Regionalized" ]
	then
		rm -f $FilePath".Regionalized"
fi
if [ -e $FilePath".NotRegionalized" ]
	then
		rm -f $FilePath".NotRegionalized"
fi

echo "Iterator #$Counter"
Total=`wc -l $FilePath | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
echo "Regionalizing $Total MSISDNs"
#----------------------------------
ksh $ScriptDir/Regionalizer_Part.sh $FilePath $MSISDN_Field_ID $FieldSeparator $MonitoringDateHour $Format
#----------------------------------------------------------------------------------------------------------
Found=`wc -l $FilePath".Regionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
Unfound=`wc -l $FilePath".NotRegionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
FOUND=$(($Found+$FOUND))
UNFOUND=$(($Unfound+$UNFOUND))

echo "Found $Found Regionalized, $Unfound Unregionalized"
total=$(($Found+$Unfound))
if [ $total -gt 0 ]
	then
       	 SuccessRate=$(($Found*100/$total))
fi
SR=$(($Found*100/$Total))
echo "Success Rate for this part:"$SuccessRate"%,addition to overall SR:"$SR"%"

#mv $FilePath".Regionalized" $FilePath".Part1"
while [ $Counter -lt $Iterator ]
	do
		Counter=$(($Counter+1))
		echo "Iterator #$Counter"
		echo "Regionalizing $FileName on $NewMonitoringDateHour" 
		#------------------------------------------------------
		NewMonitoringDateHour=`ksh /app_univ/Date_Scripts/NextHour "$MonitoringDateHour" $Counter`
		#==================================================================================================================
		ksh $ScriptDir/Regionalizer_Part.sh $FilePath".NotRegionalized" $MSISDN_Field_ID $FieldSeparator $NewMonitoringDateHour $Format No
		#==================================================================================================================
		
		Found=`wc -l $FilePath".NotRegionalized.Regionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
		Unfound=`wc -l $FilePath".NotRegionalized.NotRegionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
		FOUND=$(($Found+$FOUND))
		UNFOUND=$(($Unfound+$UNFOUND))
		total=$(($Found+$Unfound))
		if [ $total -gt 0 ]	
			then
				SuccessRate=$(($Found*100/$total))
		fi
		SR=$(($Found*100/$Total))
		
		echo "Found $Found Regionalized, $Unfound Unregionalized"
		echo "Success Rate for this part:"$SuccessRate"%,addition to overall SR:"$SR"%"
		#mv $FilePath".NotRegionalized.Regionalized" $FilePath".Part2"
		cat $FilePath".NotRegionalized.Regionalized" >> $FilePath".Regionalized"
		rm -f $FilePath".NotRegionalized.Regionalized"
		mv -f $FilePath".NotRegionalized.NotRegionalized" $FilePath".NotRegionalized"

		echo "Regionalizing $FileName on $NewMonitoringDateHour"
		#------------------------------------------------------
		NewMonitoringDateHour=`ksh /app_univ/Date_Scripts/NextHour "$MonitoringDateHour" "-"$Counter`
		#==================================================================================================================
		ksh $ScriptDir/Regionalizer_Part.sh $FilePath".NotRegionalized" $MSISDN_Field_ID $FieldSeparator $NewMonitoringDateHour $Format No
		#==================================================================================================================
		Found=`wc -l $FilePath".NotRegionalized.Regionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
		Unfound=`wc -l $FilePath".NotRegionalized.NotRegionalized" | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
		FOUND=$(($Found+$FOUND))
		UNFOUND=$(($Unfound+$UNFOUND))
		total=$(($Found+$Unfound))
		if [ $total -gt 0 ]     
                        then
                                SuccessRate=$(($Found*100/$total))
                fi
		SR=$(($Found*100/$Total))

		echo "Found $Found Regionalized, $Unfound Unregionalized"
		echo "Success Rate for this part:"$SuccessRate"%,addition to overall SR:"$SR"%"
		mv -f $FilePath".NotRegionalized.NotRegionalized" $FilePath".NotRegionalized"
		#mv $FilePath".NotRegionalized.Regionalized" $FilePath".Part-"$Counter
		cat $FilePath".NotRegionalized.Regionalized" >> $FilePath".Regionalized"
		rm -f $FilePath".NotRegionalized.Regionalized"
done
SR=$(($FOUND*100/$Total))
#cat $FilePath".Part"* > $FilePath".Regionalized"
#rm $FilePath".Part"*
echo "============================================================================================================="
echo "============================================================================================================="
echo "For file $FileName"
echo "Generally we have $Total Transactions, $FOUND are regionalized and $Unfound are not regionalized, SR=$SR%"
echo "============================================================================================================="
echo "============================================================================================================="
Command="INSERT INTO Regionalizer_Parser(iFileName,SR,Iterator,State,TransID) VALUES('$FilePath','$SR','$Iterator','End','$Rand')"
mysql -uWriter -pWriter -e"$Command" -hCNPVAS01 Loggers &
