
if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ] || [ "$4" = "" ] || [ "$5" = "" ] 
        then
                echo "You must enter"
                echo "1. FileName path"
                echo "2. MSISDN_Field_ID"
                echo "3. FieldSeparator"
                echo "4. DateHour in the format YYYYMMDDHH"
		echo "5. MSISDN format in (1)201XXX (2)01XXX (3)10XXX "
		echo "6. WithIMSICheck (Yes,No), Default is Yes"
                exit
fi
Format=$5
if [ ! "$Format" = "1" ] && [ ! "$Format" = "2" ] && [ ! "$Format" = "3" ]
        then
                echo "5. MSISDN format in (1)201XXX (2)01XXX (3)10XXX "
                exit
fi

FilePath=$1 #Input file must be in the format of 2010XXX,
FileDir=`echo $FilePath | sed 's/\(.*\)\//\1\|/' | sed 's/\|.*//g'`"/"
FileName=`echo $FilePath | sed 's/\(.*\)\//\1|/' | cut -d"|" -f2`
MSISDN_Field_ID=$2
FieldSeparator=$3
MonitoringDateHour=$4 #Format YYYYMMDDHH Date and hour to serach in
ScriptDir=/app_univ/Regional/
ProcessingsDir=$ScriptDir/Processings/
MapDir=/share/Regional/Maps/
WithIMSICheck=$6
if [ "$WithIMSICheck" = "" ]
	then
		WithIMSICheck="Yes"
fi
touch $FilePath".Regionalized"
touch $FilePath".NotRegionalized"
#-------------------------------------------------------------------------------------------------------
Total=`wc -l $FilePath | sed 's/^ *//g;s/ /;/' | cut -d";" -f1`
echo "Input File Record Count:$Total"
if [ "$Total" = "0" ]
	then
		echo "Record count is Zero, no need to proceed"
		
		exit
fi

if [ "$WithIMSICheck" = "Yes" ] || [ "$WithIMSICheck" = "yes" ]
	then

		#--------------------------Formatting File--------------------------------------------
		if [ "$Format" = "1" ] 
			then
				echo `date`" Preparing file"
				if [ -e $ProcessingsDir/$FileName ]
					then
						rm -f $ProcessingsDir/$FileName
				fi
				sed "s/$FieldSeparator/|/g" $FilePath | awk 'BEGIN{FS="|";OFS="|"} {print $MSISDN_Field_ID_,$0}' MSISDN_Field_ID_=$MSISDN_Field_ID | sort -k2 -t"|"  > $ProcessingsDir/$FileName
			else
				if [ "$Format" = "3" ]
					then
						echo `date`" Preparing file and appending 20"
						sed "s/$FieldSeparator/|/g" $FilePath | awk 'BEGIN{FS="|";OFS="|"} {print "20"$MSISDN_Field_ID_,$0}' MSISDN_Field_ID_=$MSISDN_Field_ID | sort -k2 -t"|"  > $ProcessingsDir/$FileName
					else
						echo `date`" Preparing file and appending 2"
						sed "s/$FieldSeparator/|/g" $FilePath | awk 'BEGIN{FS="|";OFS="|"} {print "2"$MSISDN_Field_ID_,$0}' MSISDN_Field_ID_=$MSISDN_Field_ID | sort -k2 -t"|"  > $ProcessingsDir/$FileName
				fi
	fi

	cd $ProcessingsDir

	#-------------------------IMSI Part----------------------------------------------------------

	/usr/xpg4/bin/awk  '{FS="|";OFS="|"}{print > File_Name"_XMSISDN_PartX_"substr($1,11,2)}' File_Name=$FileName $ProcessingsDir/$FileName
	echo `date`" Appending IMSI"
	for Files in `ls $FileName"_XMSISDN_PartX_"*`
		do
			MSISDN_Part=`echo $Files | sed 's/.*_XMSISDN_PartX_//g'`
			NewFileName=`echo $Files | sed 's/XMSISDN_PartX_//g'`
			sort $Files >  $NewFileName
			rm -f $Files
	#		echo `date`" Appending IMSI for file:$Files"
			if [ -e $MapDir/MSISDN_IMSI/MSISDN_End_$MSISDN_Part ] 
				then
					join -j1 1 -j2 3 -a1 -t"|" -o 2.1 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.9 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 $NewFileName  $MapDir/MSISDN_IMSI/MSISDN_End_$MSISDN_Part | sort> $FileName"_Part"$MSISDN_Part
				else
					awk '{FS="|";OFS="|"}{print "XXX",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20}' $NewFileName | sort> $FileName"_Part"$MSISDN_Part
			fi
			rm -f $NewFileName
	done
else
	cd $ProcessingsDir
	echo "Copying file to $ProcessingsDir"
	cp $FilePath $ProcessingsDir"/"$FileName"_Part_XXX"
fi
#---------------------------Cell And Lac Finding----------------------------------------------

echo `date`" Appending Cell And Lac"
echo `date`" Splitting Per IMSI Last"
cat $FileName"_Part"* | /usr/xpg4/bin/awk  '{FS="|";OFS="|"}{print > File_Name"_XIMSIPartX_"substr($1,14,2)}' File_Name=$FileName 
rm -f $FileName"_Part"*
	
Date=`echo $MonitoringDateHour| cut -c1-4`"-"`echo $MonitoringDateHour| cut -c5-6`"-"`echo $MonitoringDateHour| cut -c7-8`
Time=`echo $MonitoringDateHour| cut -c9-10`

echo `date`" Appending Cell And Lac"
for Files in `ls $FileName"_XIMSIPartX_"*`
	do
		IMSI_Part=`echo $Files | sed 's/.*_XIMSIPartX_//g'`
		NewFileName=`echo $Files | sed 's/_XIMSIPartX_//g'`
		sort $Files >  $NewFileName
		rm -f $Files
#		echo `date`" Appending Cell And Lac for file:$Files"
#		ksh $ScriptDir/Uniquer/Uniquer.sh /app_univ/Regional/Regional_Location/$Date/$Time/$IMSI_Part/ "Force"
		join -j1 1 -j2 1  -a1 -t"|" -o 2.2 2.3 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.9 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 $NewFileName /app_univ/Regional/Regional_Location/$Date/$Time/$IMSI_Part/Uniq | sed 's/|/@/' | sort> $FileName"_Part_CellLac_"$IMSI_Part
		rm -f $NewFileName 
	done

#---------------------------Regionalizing based in cell and lac--------------------------------------
grep -h "^@" $FileName"_Part_CellLac_"* | awk '{FS="|";OFS="|"} {print $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20}' > $FileDir/$FileName".NotRegionalized"
echo "Unregionalized file is generated to :"$FileDir/$FileName".NotRegionalized"
grep -vh "^@" $FileName"_Part_CellLac_"* | sort> $FileDir/$FileName".Cell"
rm -f $FileName"_Part_CellLac_"*

join -j1 1 -j2 1 -a1 -t"|" -o 2.1 2.2 2.3 2.4 2.5 2.6 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.16 1.17 1.18 1.9 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 1.33 1.34 1.35 1.36 1.37 1.38 $FileDir/$FileName".Cell" $MapDir/Cell_MAP >  $FileDir/$FileName".Regionalized"
rm $FileDir/$FileName".Cell"
echo "Regionalized file is generated to :"$FileDir/$FileName".Regionalized"

