
cd /share/Regional/Maps
echo "Splitting by 2 digits"
/usr/xpg4/bin/awk  '{FS="^";OFS="|"}{print $1,$2,$3,$4,$5,$6,$7,$8> "MSISDN_End_TempP2_"substr($3,11,2)}' CCSM_ALL_FINAL.LST  
#/usr/xpg4/bin/awk  '{FS="^";OFS="|"}{print $1,$2,$3,$4,$5,$6,$7,$8> "MSISDN_End_TempP1_"substr($3,11,2)}' CCSM_ALL_FINAL.LST  
#echo "Splitting by 3 digits"
#for Files in `ls MSISDN_End_TempP1_*`
#	do
#		/usr/xpg4/bin/awk -F"|" '{print > "MSISDN_End_TempP2_"substr($3,10,3)}' $Files 
#		rm $Files
#done
echo "Sorting Files"
for Files in `ls MSISDN_End_TempP2_*`
	do
		NewFileName=`echo $Files | sed 's/TempP2_//g'`
		sort -t"|" -k3 $Files > ./MSISDN_IMSI/$NewFileName
		rm $Files
done
cd /share/Regional/Maps
sort -t"^" -k3 CCSM_ALL_FINAL.LST| sed 's/\^/|/g' > CCSM_ALL_FINAL.LST_MSISDN_Sorted
