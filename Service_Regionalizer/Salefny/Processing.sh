Date=$1
Last_Hour=$2
Hour_=`echo $Last_Hour | awk '{print substr($0,9,2)}'`
echo $Hour_
Source_Path="/app_univ/StatsCollector"
Regionalizer_Path="/app_univ/Service_Regionalizer/Processings"

#=================================================================================================================================
cd $Source_Path 

Process=`/usr/ucb/ps auxww | grep 'java -jar Stats_Collector.jar' | grep Salefny | grep MSISDN | grep $Date| wc -l | sed 's/^ *//g'`
Process=$((Process+0))

while [ $Process -gt 1 ]
     do
        echo `date`"Found $Process process(es), waiting ..."
        sleep 20
	Process=`/usr/ucb/ps auxww | grep 'java -jar Stats_Collector.jar' | grep Salefny | grep MSISDN | grep $Date| wc -l | sed 's/^ *//g'`
done

echo "Processing Files"
ksh Launcher.sh Salefny_Reg $Date
echo "Waiting to prcesseses to finish"
Process=`/usr/ucb/ps auxww | grep 'java -jar Stats_Collector.jar' | grep Salefny | grep MSISDN| grep $Date | wc -l | sed 's/^ *//g'`
Process=$((Process+0))

while [ $Process -gt 1 ]
     do
        echo `date`"Found $Process process(es), waiting ..."
        sleep 20
	Process=`/usr/ucb/ps auxww | grep 'java -jar Stats_Collector.jar' | grep Salefny |grep $Date| grep MSISDN | wc -l | sed 's/^ *//g'`
done



#--------------------------------------------------------------------------------------------------------------------------

mv $Source_Path/Salefny.1.Sub_Eligability_Process_MSISDN_"$Date" $Regionalizer_Path
mv $Source_Path/Salefny.2.Sub_Eligability_Process_MSISDN_"$Date" $Regionalizer_Path
mv $Source_Path/Salefny.CA5_RX3.Sub_Eligability_Process_MSISDN_"$Date" $Regionalizer_Path
mv $Source_Path/Salefny.CA5_RX4.Sub_Eligability_Process_MSISDN_"$Date" $Regionalizer_Path

cd $Regionalizer_Path

/usr/xpg4/bin/awk '{FS=",";OFS=","}{print > "Salefny_ZHRX1_"Date_"_"$2}' Date_=$Date $Regionalizer_Path/Salefny.1.Sub_Eligability_Process_MSISDN_"$Date"

/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > "Salefny_ZHRX2_"Date_"_"$2}' Date_=$Date $Regionalizer_Path/Salefny.2.Sub_Eligability_Process_MSISDN_"$Date"

/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > "Salefny_CA5_RX3_"Date_"_"$2}' Date_=$Date $Regionalizer_Path/Salefny.CA5_RX3.Sub_Eligability_Process_MSISDN_"$Date" 

/usr/xpg4/bin/awk  '{FS=",";OFS=","}{print > "Salefny_CA5_RX4_"Date_"_"$2}' Date_=$Date $Regionalizer_Path/Salefny.CA5_RX4.Sub_Eligability_Process_MSISDN_"$Date" 
#=================================================================================================================================
echo "Removing Source Files"
rm $Regionalizer_Path/Salefny.1.Sub_Eligability_Process_MSISDN_"$Date"
rm $Regionalizer_Path/Salefny.2.Sub_Eligability_Process_MSISDN_"$Date"
rm $Regionalizer_Path/Salefny.CA5_RX3.Sub_Eligability_Process_MSISDN_"$Date"
rm $Regionalizer_Path/Salefny.CA5_RX4.Sub_Eligability_Process_MSISDN_"$Date"
#================================================================================================================================
echo "Regionalizing Data at Hour "$Hour_

cat $Regionalizer_Path/Salefny_ZHRX1_"$Date"_"$Hour_" > $Regionalizer_Path/Salefny_"$Date"_"$Hour_"
cat $Regionalizer_Path/Salefny_ZHRX2_"$Date"_"$Hour_" >> $Regionalizer_Path/Salefny_"$Date"_"$Hour_"
cat $Regionalizer_Path/Salefny_CA5_RX3_"$Date"_"$Hour_" >> $Regionalizer_Path/Salefny_"$Date"_"$Hour_"
cat $Regionalizer_Path/Salefny_CA5_RX4_"$Date"_"$Hour_" >> $Regionalizer_Path/Salefny_"$Date"_"$Hour_"

ksh /app_univ/Regional/Regionalizer/Regionalizer.sh $Regionalizer_Path/Salefny_"$Date"_"$Hour_" 9 "," $Last_Hour 1 

cat $Regionalizer_Path/Salefny_"$Date"_"$Hour_"".""Regionalized" >> Salefny_"$Date"".""Regionalized"
cat $Regionalizer_Path/Salefny_"$Date"_"$Hour_"".""NotRegionalized" >> Salefny_"$Date"".""NotRegionalized"

ksh /app_univ/Service_Regionalizer/Salefny/Subs.sh $Regionalizer_Path/Salefny_"$Date"".""Regionalized" &

cut -d"|" -f2,3,4,11,10,13,15 $Regionalizer_Path/Salefny_"$Date"_"$Hour_".Regionalized | sort | uniq -c | sed 's/^ *//g;s/ /|/' > $Regionalizer_Path/Regionalized_Borrow_"$Date"_"$Hour_"

mv $Regionalizer_Path/Regionalized_Borrow_"$Date"_"$Hour_" /share/Importers/Salefny_Regionalized
#===============================================================================================================================
echo "Loading Data in DB"

Command="LOAD DATA INFILE '/share/Importers/Salefny_Regionalized/Regionalized_Borrow_"$Date"_"$Hour_"' REPLACE INTO TABLE Salefny.Salefny_Borrow_Regionalized CHARACTER SET UTF8 FIELDS TERMINATED BY '|' (Count,Region,Sub_Region,Governrate,Date,Hour,Request_Status_Reason,Request_Status)"

mysql -uWriter -pWriter -h172.23.201.51 -e"$Command" Salefny
#===============================================================================================================================
echo "Removing the files from the regionalizer path"

rm /share/Importers/Salefny_Regionalized/Regionalized_Borrow_"$Date"_"$Hour_"
rm $Regionalizer_Path/Salefny_ZHRX1_"$Date"_*
rm $Regionalizer_Path/Salefny_ZHRX2_"$Date"_*
rm $Regionalizer_Path/Salefny_CA5_RX3_"$Date"_*
rm $Regionalizer_Path/Salefny_CA5_RX4_"$Date"_*
rm $Regionalizer_Path/Salefny_ZHRX1_"$Date"_
rm $Regionalizer_Path/Salefny_ZHRX2_"$Date"_
rm $Regionalizer_Path/Salefny_CA5_RX3_"$Date"_
rm $Regionalizer_Path/Salefny_CA5_RX4_"$Date"_
rm $Regionalizer_Path/Salefny_"$Date"_"$Hour_"
#rm $Regionalizer_Path/Salefny_"$Date"_"$Hour_"".""Regionalized"
#rm $Regionalizer_Path/Salefny_"$Date"_"$Hour_"".""NotRegionalized"
