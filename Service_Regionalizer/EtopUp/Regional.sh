Date=$1
echo "starting..."
ProcessingDir='/app_univ/Service_Regionalizer/Processings'
cp /app_univ/StatsCollector/app.Collectors.E-TOPUP.Regionalization_$Date $ProcessingDir
echo "copy is Done..."
cd /app_univ/Service_Regionalizer/Processings
#/usr/xpg4/bin/awk -F"," '{print > File_Name"_Hour_"$2}' File_Name=EtopUp_$Date /app_univ/StatsCollector/app.Collectors.E-TOPUP.Regionalization_$Date
/usr/xpg4/bin/awk -F"," '{print > File_Name"_Hour_"$2}' File_Name=EtopUp_$Date $ProcessingDir/app.Collectors.E-TOPUP.Regionalization_$Date
echo "awk command is done ..."

newDate=`echo $Date | sed 's/-//g'` 
echo "$newDate"

for i in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 
	do
#		cp /app_univ/Service_Regionalizer/EtopUp/EtopUp_"$Date"_Hour_"$i" $ProcessingDir
#		rm /app_univ/Service_Regionalizer/EtopUp/EtopUp_"$Date"_Hour_"$i"
#		ksh /app_univ/Regional/Regionalizer/Regionalizer.sh /app_univ/Service_Regionalizer/EtopUp/EtopUp_"$Date"_Hour_"$i" 4 "," $newDate$i 2 3
		ksh /app_univ/Regional/Regionalizer/Regionalizer.sh $ProcessingDir/EtopUp_"$Date"_Hour_"$i" 4 "," $newDate$i 2 3
		rm $ProcessingDir/EtopUp_"$Date"_Hour_"$i"
#		cut -d'|' -f2,3,4,10,11,12,14 $ProcessingDir/EtopUp_"$Date"_Hour_"$i".Regionalized | sort | uniq -c | sed 's/^[  ]*//' | sed 's/ /|/'  > /share/Regionalization/EtopUp/EtopUp_db_$i.txt
		cut -d'|' -f2,3,4,10,11,12,14 $ProcessingDir/EtopUp_"$Date"_Hour_"$i".Regionalized | awk 'BEGIN { FS = "|";OFS="|"} {SumArr[$1"|"$2"|"$3"|"$4"|"$5"|"$6] += $7;CountArr[$1"|"$2"|"$3"|"$4"|"$5"|"$6]+=1} END {for (i in SumArr) { print i"|"SumArr[i]"|"CountArr[i] } }' > /share/Regionalization/EtopUp/EtopUp_db_$i.txt
		Command="LOAD DATA INFILE '/share/Regionalization/EtopUp/EtopUp_db_"$i".txt' REPLACE INTO TABLE Regionalization FIELDS TERMINATED BY '|' (Region,Sub_Region,Governarate,Date,Hour,Status,Amount,Count)"
		mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" ETOPUP
#		rm $ProcessingDir/EtopUp_"$Date"_Hour_"$i".Regionalized
#		rm $ProcessingDir/EtopUp_"$Date"_Hour_"$i".NotRegionalized
	done
echo "Regionalizing Subs"
cat "EtopUp_"$Date"_Hour_"*".Regionalized" | cut -d"|" -f2,3,4,9,10,12,14 | awk 'BEGIN { FS = "|";OFS="|"} {SumArr[$1"|"$2"|"$3"|"$5"|"$6] += $7;CountArr[$1"|"$2"|"$3"|"$5"|"$6]+=1} END {for (i in SumArr) { print i"|"SumArr[i]"|"CountArr[i] }}' > /share/Regionalization/EtopUp/EtopUp_db_$Date.txt
Command="LOAD DATA INFILE '/share/Regionalization/EtopUp/EtopUp_db_$Date.txt' REPLACE INTO TABLE Regionalization_Subs FIELDS TERMINATED BY '|' (Region,Sub_Region,Governarate,Date,Status,Amount,Subs_Count)"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" ETOPUP
