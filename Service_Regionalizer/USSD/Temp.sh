
cd /share/USSD/
Process=`ps -ef | grep USSD_Region.sh | grep -v grep | wc -l | sed 's/^ *//g'`
Process=$(($Process+0))
while [ $Process -gt 3 ]
     do
        echo "Found Another Process,Count=$Process, exit script"
        exit
done

for Files in `ls Response_20140412*.txt`
	do
		if [ -e $Files ]
			then
				Rand=$RANDOM
				mv $Files $Files.Processing
				echo "Aggregating File:$Files"
				cut -d"|" -f2,4,5,6,7,9,10 $Files.Processing  | sort | uniq -c | sed 's/^ *//g;s/ /|/' > $Files.Agg
	
				DateTime=`echo $Files | sed 's/Response_//g;s/.txt.Agg//g'`
				#echo $Files,$DateTime
				ksh /app_univ/Regional/Regionalizer/Regionalizer.sh "/share/USSD/"$Files".Agg" 6 "|" $DateTime 1
				cut -d"|" -f2,3,4,10,11,12,13,14,16 /share/USSD/$Files".Agg.Regionalized" | awk -F"|" '{print $0"|"File}' File=$Files | sort | uniq -c| sed 's/^ *//g' | sed 's/ /|/'> /share/USSD/$Files".imported"
				Command="CREATE TABLE Responses_$Rand (Date varchar(11) NOT NULL,Hour varchar(2) NOT NULL DEFAULT '0',Service varchar(20) NOT NULL,Response varchar(300) CHARACTER SET utf8 NOT NULL DEFAULT '0',Region varchar(11) NOT NULL DEFAULT '0',SubRegion varchar(27) NOT NULL DEFAULT '0',Governorate varchar(27) NOT NULL DEFAULT '0',Count double DEFAULT NULL,Day int(11) DEFAULT NULL,Month int(11) DEFAULT NULL,PRIMARY KEY (Date,Service,Response,Region,SubRegion,Governorate,Hour)) ENGINE=MyISAM DEFAULT CHARSET=latin1"
				mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_Reg

				echo  "Loading Regionalized Data"
				Command="LOAD DATA INFILE '/share/USSD/$Files.imported' REPLACE INTO TABLE Responses_$Rand CHARACTER SET utf8 FIELDS TERMINATED BY '|' (Count,Region,SubRegion,Governorate,Month,Service,Day,Date,Hour,Response)"
				echo $Command
				mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_Reg

				echo  "Loading Unregionalized Data"
				cut -d"|" -f3,4,5,6,7,9 /share/USSD/$Files".Agg.NotRegionalized" | awk -F"|" '{print $0"|"File}' File=$Files | sort | uniq -c| sed 's/^ *//g' | sed 's/ /|/'> /share/USSD/$Files".NotReg.imported"
				Command="LOAD DATA INFILE '/share/USSD/$Files.NotReg.imported' REPLACE INTO TABLE Responses_$Rand CHARACTER SET utf8 FIELDS TERMINATED BY '|' (Count,Month,Service,Day,Date,Hour,Response)"
				echo $Command
				mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_Reg
		
				echo "Updating Responses Table"
				Command="call Regionalizer ('$Files','Responses_$Rand')"
				mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_Reg

				echo "Dropping Responses_$Rand Table"
				Command="DROP TABLE Responses_$Rand"
				mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" USSD_Reg

				mv $Files".Processing" $Files".Parsed"
			else
				echo "Couldn't find file:$Files; Skipping to the next file"
		fi
done
