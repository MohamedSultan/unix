
cd /share/USSD/
for Files in `ls Reg_Response_201*.txt`
	do
		DateTime=`echo $Files | sed 's/Reg_Response_//g;s/.txt//g'`
		mv $Files $Files.Processing
		echo $Files,$DateTime
		ksh /app_univ/Regional/Regionalizer/Regionalizer.sh "/share/USSD/"$Files".Processing" 7 "|" $DateTime 1
		cut -d"|" -f2,3,4,14,15,18 /share/USSD/$Files".Processing.Regionalized" | awk -F"|" '{print $0"|"File}' File=$Files | sort | uniq -c| sed 's/^ *//g' | sed 's/ /|/'> /share/USSD/$Files".imported"
		Command="LOAD DATA INFILE '/share/USSD/$Files.imported' REPLACE INTO TABLE Regionalized_Mega CHARACTER SET utf8 FIELDS TERMINATED BY '|' (Count,Region,SubRegion,Governorate,Date,Hour,Response,FileName)"
		echo $Command
		mysql -uWriter -pWriter -hCNPVAS01 -e"$Command" Mega_Magico
		mv $Files".Processing" $Files".Parsed"
done
