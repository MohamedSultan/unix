Date=$1
Last_Hour=$2

echo "Processing Date Formatting" 
Year=`echo $Last_Hour | awk '{print substr($0,1,4)}'`
Month=`echo $Last_Hour | awk '{print substr($0,5,2)}'`
Day=`echo $Last_Hour | awk '{print substr($0,7,2)}'`

Hour_=`echo $Last_Hour | awk '{print substr($0,9,2)}'`

if [ "$Hour_" = "00" ] ; then
Hour_="0"
fi

if [ "$Hour_" = "01" ] ; then
Hour_="1"
fi

if [ "$Hour_" = "02" ] ; then
Hour_="2"
fi

if [ "$Hour_" = "03" ] ; then
Hour_="3"
fi

if [ "$Hour_" = "04" ] ; then
Hour_="4"
fi

if [ "$Hour_" = "05" ] ; then
Hour_="5"
fi

if [ "$Hour_" = "06" ] ; then
Hour_="6"
fi

if [ "$Hour_" = "07" ] ; then
Hour_="7"
fi

if [ "$Hour_" = "08" ] ; then
Hour_="8"
fi

if [ "$Hour_" = "09" ] ; then
Hour_="9"
fi

if [ "$Day" = "00" ] ; then
Day="0"
fi

if [ "$Day" = "01" ] ; then
Day="1"
fi

if [ "$Day" = "02" ] ; then
Day="2"
fi

if [ "$Day" = "03" ] ; then
Day="3"
fi

if [ "$Day" = "04" ] ; then
Day="4"
fi

if [ "$Day" = "05" ] ; then
Day="5"
fi

if [ "$Day" = "06" ] ; then
Day="6"
fi

if [ "$Day" = "07" ] ; then
Day="7"
fi

if [ "$Day" = "08" ] ; then
Day="8"
fi

if [ "$Day" = "09" ] ; then
Day="9"
fi

if [ "$Month" = "00" ] ; then
Month="0"
fi

if [ "$Month" = "01" ] ; then
Month="1"
fi

if [ "$Month" = "02" ] ; then
Month="2"
fi

if [ "$Month" = "03" ] ; then
Month="3"
fi

if [ "$Month" = "04" ] ; then
Month="4"
fi

if [ "$Month" = "05" ] ; then
Month="5"
fi

if [ "$Month" = "06" ] ; then
Month="6"
fi

if [ "$Month" = "07" ] ; then
Month="7"
fi

if [ "$Month" = "08" ] ; then
Month="8"
fi

if [ "$Month" = "09" ] ; then
Month="9"
fi

Date_Temp=$Day"-"$Month"-"$Year


Regionalizer_Path="/app_univ/Service_Regionalizer/MCK_"

#=================================================================================================================================
echo "Processing Files"

cd $Regionalizer_Path

#if [ Hour_ = "0" ] ; then
/usr/xpg4/bin/awk '{FS="&";OFS="&"}{print > "MCK_"Date_"_"$2}' Date_=$Date $Regionalizer_Path/Switch_Divert_"$Date_Temp".txt
#=================================================================================================================================
echo "Removing Source Files"
#rm $Regionalizer_Path/Switch_Divert_"$Date_Temp".txt
#================================================================================================================================
#fi
echo "Regionalizing Data at Hour "$Hour_

cat $Regionalizer_Path/MCK_"$Date"_"$Hour_" | cut -d"&" -f1,2,3,4,6,10 | sort > $Regionalizer_Path/MCK_"$Date"_"$Hour_"_Processed

ksh /app_univ/Regional/Regionalizer/Regionalizer.sh $Regionalizer_Path/MCK_"$Date"_"$Hour_"_Processed 5 "&" $Last_Hour 1 3 

cut -d"|" -f1,2,3,4,10,11,15 $Regionalizer_Path/MCK_"$Date"_"$Hour_"_Processed.Regionalized | sed 's/@/|/g' | sort | uniq -c | sed 's/^ *//g;s/ /|/' > $Regionalizer_Path/Regionalized_MCK_"$Date"_"$Hour_"_Temp

cat $Regionalizer_Path/Regionalized_MCK_"$Date"_"$Hour_"_Temp | awk -F"|" '{print $1"|"$2"|"$3"|"$4"|"$5"|"$6"|"DateX"|"$8"|"$9}' DateX=$Date > $Regionalizer_Path/Regionalized_MCK_"$Date"_"$Hour_"

mv $Regionalizer_Path/Regionalized_MCK_"$Date"_"$Hour_" /share/Importers/MCK_Regionalized/

#===============================================================================================================================
echo "Loading Data in DB"

Command="LOAD DATA INFILE '/share/Importers/MCK_Regionalized/Regionalized_MCK_"$Date"_"$Hour_"' REPLACE INTO TABLE MCK.Regionalized_MCK_SD CHARACTER SET UTF8 FIELDS TERMINATED BY '|' (Count,Lac,Cell,Region,Sub_Region,Governrate,Date,Hour,MCK_Reason)"

mysql -uWriter -pWriter -h172.23.201.51 -e"$Command" MCK 
#===============================================================================================================================
#echo "Removing the files from the regionalizer path"

#rm $Regionalizer_Path/MCK_"$Date"_*
