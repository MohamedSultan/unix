
FilePath=$1
ProcessingDir=/app_univ/Service_Regionalizer/Processings

FileName=`echo $FilePath | sed 's/.*\///g'`
cd $ProcessingDir

echo "Splitting File into Corresponding date and hour"
#mv -f $FilePath $FilePath.Processing
#cat $FilePath.Processing |sed 's/ /,/g'> $ProcessingDir/$FileName
cat $FilePath |sed 's/ /,/g'> $ProcessingDir/$FileName
echo "Splitting file per hours"
/usr/xpg4/bin/awk -F"," '{print > oFileName"_"$5substr($6,1,2)}' oFileName=$FileName $FileName

for Files in `ls $FileName"_"*`
	do
		SearchDate=`echo $Files | sed 's/.*_//g'`
		echo "Regionalizing file:$Files"
		ksh /app_univ/Regional/Regionalizer/Regionalizer.sh $ProcessingDir/$Files 2 "," $SearchDate 3 3
		echo "Aggregating $Files $Files"
		sed 's/:/|/g' $ProcessingDir/$Files".Regionalized" | cut -d"|" -f2,3,4,14,15,18,19| sort | uniq -c | sed 's/^ *//g;s/ /|/' |awk '{FS="|";OFS="|"}{print substr($5,1,4)"-"substr($5,5,2)"-"substr($5,7,2),$6,$8,$7,$2,$3,$4,$1,File_Name}' File_Name=$Files > /share/Regionalization/PCR/$Files
		echo "Importing /share/Regionalization/PCR/$Files"
		Command="LOAD DATA INFILE '/share/Regionalization/PCR/$Files' REPLACE INTO TABLE PCR_Region FIELDS TERMINATED BY '|' (Date,Hour,Card_Type,Channel,Region,Sub_Region,Governorate,Count,FileName)"
		mysql -uWriter -pWriter -e"$Command" -hCNPVAS03 PCR
		
		rm -f $Files /share/Regionalization/PCR/$Files
		
done	

rm -f $ProcessingDir/$FileName
