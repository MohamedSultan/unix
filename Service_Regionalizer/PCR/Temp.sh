Date=$1
BGW_Files=/share/FTP/Comptel_BGW/Parsed/PCR/$Date


for Unparsed in `ls $BGW_Files/PPAS-PCR_*.TXT.Parsed`
	do
		echo "Regionlizing $Unparsed"
		ksh Regionlizer.sh $Unparsed
		if [ ! -d $BGW_Files/PCR ]
			then
				mkdir $BGW_Files/PCR
		fi
		mv -f $Unparsed.Processing $BGW_Files/PCR/$Unparsed
		echo "File: $Unparsed is Regionlized"
done
