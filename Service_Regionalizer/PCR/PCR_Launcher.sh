

OtherProcess=`ps -ef | grep PCR_Launcher.sh | grep -v grep | wc -l | sed 's/^ *//g'`
if [ $OtherProcess -gt 5 ] 
        then
                echo "Found another Process , exit process ;Other Proocess count:$OtherProcess"
                exit
        else
                echo "Didn't find any other running processes, continuing with the app, Appcount:$OtherProcess"
fi

CurrentDir=`pwd`
BGW_Files=/share/FTP/Comptel_BGW/Parsed/
cd $BGW_Files

for Unparsed in `ls PPAS-PCR_*.TXT.Parsed`
	do
		echo "--------------------------------------------------------------------------------------"
		echo "Regionlizing $Unparsed"
		echo "--------------------------------------------------------------------------------------"
		mv $Unparsed $Unparsed.Processing
		ksh $CurrentDir/Regionlizer.sh $BGW_Files/$Unparsed".Processing"
		if [ ! -d $BGW_Files/PCR ]
			then
				mkdir $BGW_Files/PCR
		fi
		mv -f $BGW_Files/$Unparsed".Processing" $BGW_Files"/PCR/"$Unparsed
		echo "--------------------------------------------------------------------------------------"
		echo "File: $Unparsed is Regionlized"
		echo "--------------------------------------------------------------------------------------"
done
