Importer=/app_univ/StatsCollector/Importer.sh

cd /app_univ/Services/Retail_Stores/Files/

for FileName in `ls *.txt`
do

echo "Processing File "$FileName

Date_Hour=`cat $FileName | sed 's/  */ /g;s/^ //g;s/___________________________________________________________________________/||/g' | grep -v '^$' | grep -v '||' | head -2 | tail -1 | sed 's/ /|/g;s/:/|/g;' | cut -d"|" -f1,2`

Date=`echo $Date_Hour | cut -d"|" -f1 | tr '/' '-' | awk -F"-" '{if (length(substr($0,1,1)) == 1) print substr($0,6,4)"-0"substr($0,1,1)"-"substr($0,3,2)}' | awk -F"-" '{if (length(substr($0,9,2)) == 2) print $0; else print substr($0,1,4)"-"substr($0,6,2)"-0"substr($0,9,1);}'`

Hour=`echo $Date_Hour | cut -d"|" -f2 | tr '/' '-' | awk -F"-" '{if (length(substr($0,1,1)) == 1) print "0"substr($0,1,1)}'`

cat $FileName | sed 's/  */ /g;s/^ //g;s/___________________________________________________________________________/^/g' | grep -v '^$' | sed 's/ //g' | sed 's/(Attached)/_Attached/g' | tr '\n' X | tr '^' '\n' | sed 's/^X//g' | sed 's/X/|/g' | sed 's/,/|/g' | sed 's/)://g' | sed 's/(/|/g' | sed 's/84devicesselected|/X/g' | tr 'X' '\n' | grep -v 'Job' | awk -F"|" '{print Date_"|"Hour_"|"$1"|"$2"|"$3"|"$5"|"$6"|"$7"|"$8"|"}' Date_=$Date Hour_=$Hour | grep -v 'ERROR' | grep -v '^||' | sed 's/dBm//g;s/CurrentRSSI=//g;s/PrimaryScramblingCode=//g;s/CellID=//g;s/PacketService=//g;s/LAC)=//g;' > /share/Importers/Retail_Stores/$FileName


cat $FileName | sed 's/  */ /g;s/^ //g;s/___________________________________________________________________________/^/g' | grep -v '^$' | sed 's/ //g' | sed 's/(Attached)/_Attached/g' | tr '\n' X | tr '^' '\n' | sed 's/^X//g' | sed 's/X/|/g' | sed 's/,/|/g' | sed 's/)://g' | sed 's/(/|/g' | sed 's/84devicesselected|/X/g' | tr 'X' '\n' | grep -v 'Job' | awk -F"|" '{print Date_"|"Hour_"|"$1"|"$2"|"$3"|"$5"|"$6"|"$7"|"$8"|"}' Date_=$Date Hour_=$Hour | grep 'ERROR' | grep -v '^||' | sed 's/dBm//g;s/CurrentRSSI=//g;s/PrimaryScramblingCode=//g;s/CellID=//g;s/PacketService=//g;s/LAC)=//g;' >> /share/Importers/Retail_Stores/$FileName

echo "Importing to DB"
ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/Retail_Stores/$FileName" CNPVAS04 RSSI_KPIs "Date,Hour,Store_Name,Router_IP,Packet_Service,LAC,Cell_ID,Primary_Scrambling_Code,Current_RSSI,Error" "|" Retail_Stores Stores_RSSI

echo "Removing the file"
rm $FileName

done

