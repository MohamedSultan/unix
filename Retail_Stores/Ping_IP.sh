for IP in `cat Router_IP`
do

	/usr/sbin/ping -a $IP >> Ping_IP_Status 


done

cat Ping_IP_Status | grep -v 'no' | sed 's/ //g;s/(/|/g;s/)/|/g' | cut -d"|" -f1,3 > Ping_IP_Status_Final 
cat Ping_IP_Status | grep 'no' | sed 's/ from /|/g;s/(/|/g' | awk -F"|" '{print $2"|"$1}' >> Ping_IP_Status_Final
cat Ping_IP_Status_Final | sort | uniq > Ping_IP_Status_Sorted

rm Ping_IP_Status
rm Ping_IP_Status_Final
