Date=`date '+20%y-%m-%d'`
Hour=`date '+%H'`
Min=`date '+%M'`

ksh Ping_IP.sh


for IP in `cat Router_IP`
do

	/usr/sbin/ping -sa $IP 56 20 >> Ping_IP_Stats_"$Date"_"$Hour"_"$Min" 

done

echo "Exit" >> Ping_IP_Stats_"$Date"_"$Hour"_"$Min"

cat Ping_IP_Stats_"$Date"_"$Hour"_"$Min" | grep -v 'from' | sed 's/ PING Statistics//g' | sed 's/PING/Y/g' | tr '\n' X | tr Y '\n' | tr 'E' '\n' | sed 's/XX/|/g;s/X/|/g;s/,/|/g;s/----//g;s/ //g' | sed 's/=/|/g' | cut -d"|" -f2,3,4,5,7 | grep -v '^$' | tr '/' '|' | sed 's/packetstransmitted//g;s/packetsreceived//g;s/%packetloss//g' | awk -F"|" '{print $1"|"$2"|"$3"|"$4"|"$5"|"$6"|"$7"|"$8}' | sort > Ping_IP_StatsFinal_"$Date"_"$Hour"_"$Min"

paste -d"|" Ping_IP_Status_Sorted Ping_IP_StatsFinal_"$Date"_"$Hour"_"$Min" | awk -F"|" '{print Date_"|"Hour_"|"Min_"|"$1"|"$2"|"$4"|"$5"|"$6"|"$7"|"$8"|"$9"|"$10}' Date_=$Date Hour_=$Hour Min_=$Min > /share/Importers/Retail_Stores/Ping_Stores_"$Date"_"$Hour"_"$Min"

rm Ping_IP_Stats_"$Date"_"$Hour"_"$Min"
rm Ping_IP_StatsFinal_"$Date"_"$Hour"_"$Min"
rm Ping_IP_Status_Sorted

ksh Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh "/share/Importers/Retail_Stores/Ping_Stores_"$Date"_"$Hour"_"$Min"" CNPVAS04 Ping_IP "Date,Hour,Minutes,Router_IP,Ping_Status,Packets_Transmitted,Packets_Received,Packet_Loss,Min_round_trip,Avg_round_trip,Max_round_trip,Stddev_round_trip" "|" Retail_Stores Retail_Stores_Ping 
