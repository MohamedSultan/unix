cd /db/MySQL_Data
Date=`date '+%Y%m%d'`
BaseDir="/db_fc_new/MySQL_Data_Bkp/"
DataBkpPath=$BaseDir"/$Date"
#DataBkpPath="/reg_fc/MySQL_Data_Bkp/$Date"

if [ -d "$DataBkpPath" ]
	then
		echo "Dir Exists, no need to create"
	else
		mkdir $DataBkpPath
fi

find $BaseDir -type d -mtime +7 -exec rm -rf {} \; &

for Dirs in `find . -type d`
	do
		if [ "$Dirs" != "." ]
			then
				echo "backing up $Dirs"
				cp -r $Dirs $DataBkpPath
		fi
done

