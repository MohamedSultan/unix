

Date=$1
DayBefore=`/app_univ/Date_Scripts/X_DaysBefore $Date`
CurrentDir=/app_univ/Services/DataMismatch
Usage=$CurrentDir"/Processings/subscription12_"$Date"_Sorted"
UsageBefore=$CurrentDir"/Processings/subscription12_"$DayBefore"_Sorted"

join -j1 1 -j2 1 -t"," -o 1.1 1.2 1.3 1.4 2.2 $Usage $UsageBefore | awk '{FS=",";OFS=","}{print $1,$2,$3,$4,$5,$2-$5}' > X

echo $Usage" is generated"
