MachineName=$1
MinutesBack=$2

No_Process=`ps -ef | grep FileGetter_FTD.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
No_Process=$(($No_Process+0))

while [ $No_Process -ge 2 ]
do
     echo $No_Process
     echo `date`" Found another running Process, Waiting Process"
     sleep 10
     No_Process=`ps -ef | grep FileGetter_FTD.sh | grep -v grep | grep -v vi | wc -l | sed 's/^ *//g'`
     No_Process=$(($No_Process+0))

done


#if [ "$MinutesBack" = "" ]
#	then
#		MinutesBack=10
#		echo "Minutes Back is not eneterd, 10 minutes is selected"
#fi

echo "Getting FTD Files "$MachineName
#DateTimeFile_Old=`ksh /app_univ/Date_Scripts/LastXMinute.sh $MinutesBack`
#Date_=`echo $DateTimeFile_Old | cut -c1-4``echo $DateTimeFile_Old | cut -c5-6``echo $DateTimeFile_Old | cut -c7-8`
#Time_=`echo $DateTimeFile_Old | cut -c9-11`
#DateTime=`echo $Date_"_"$Time_`

#---------------FTD_SUBMISSION-----------------------------------------------------------
FTD_SUBMISSION="Collected-cdrvfeg_"$MachineName"_"$MinutesBack"*"

if ! [ -d /share/FTP/SMS/FTD/Submissions/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/FTD/Submissions/$MachineName 
fi
ssh CNPVAS02 "scp VASQ@$MachineName:CDRs/$FTD_SUBMISSION /share/FTP/SMS/FTD/Submissions/$MachineName" &
exit
#--------------FTD_Delivered---------------------------------------------------------------

if [ "$MachineName" = "rmd-rtr01" ] || [ "$MachineName" = "rmd-rtr02" ] || [ "$MachineName" = "rmd-rtr03" ] || [ "$MachineName" = "rmd-rtr04" ]
then
        FTD_Delivered="Collected-cdrvfeg_delivered_"$MachineName"_"$DateTime"*"
else
        FTD_Delivered="Collected-cdrvfeg_idelivered_"$MachineName"_"$DateTime"*"
fi

if ! [ -d /share/FTP/SMS/FTD/Delivered/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/FTD/Delivered/$MachineName 
fi

ssh CNPVAS02 "scp VASQ@$MachineName:CDRs/$FTD_Delivered /share/FTP/SMS/FTD/Delivered/$MachineName" &

