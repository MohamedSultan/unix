MachineName=$1
Date=$2

Date_Formatted=`echo $Date | sed 's/-//g'`

DataDir_MO=/app_univ/Services/SMS/Processing/Submissions/$MachineName
Importer="Script_Exporter.sh /app_univ/StatsCollector/ Importer.sh"

ImportingDir_MO=/share/Importers/SMS/Submissions-FTD/$MachineName

if ! [ -d $ImportingDir_MO ]
        then
                mkdir -p $ImportingDir_MO
                chmod 777 $ImportingDir_MO
fi


cd $DataDir_MO

for FileName in `ls *"$Date_Formatted"*.Processed`
do

	echo "Processing file "$FileName	

	Hour=`echo $FileName | cut -d"_" -f4 | awk '{print substr($0,1,2)}'`
	Day=`echo $FileName | cut -d"_" -f3 | awk '{print substr($0,7,2)}'`

	cat $FileName | sed 's/ //g' | sort | uniq -c | sed 's/^ *//g' | sed 's/ /|/g' | awk -F"|" '{print $2"|"$3"|"$4"|"$5"|"$6"|"$7"|"$1"|"Day_}' Day_=$Day > $ImportingDir_MO/$FileName

	echo "Importing Data to DB" 
	ksh $Importer "$ImportingDir_MO/$FileName" CNPVAS03 FTD_MO_CDRs "Date,Hour,Source,Destination,GT,Router,Count,Day" "|" SMS SMS_FTD_MO_CDRs

	rm -f $FileName 
exit
done
