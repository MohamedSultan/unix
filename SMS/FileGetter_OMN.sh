MachineName=$1
DateTime=$2

#---------------AO_SUBMISSION-----------------------------------------------------------
AO_SUBMISSION="AO_SUBMISSION_"$DateTime"*"
if ! [ -d /share/FTP/SMS/OMN/AO_MT/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/OMN/AO_MT/$MachineName 
fi
ssh CNPVAS02 "scp VASQ@$MachineName:CDRS/ao_mt_cdr/ao_mt_bkp/$AO_SUBMISSION /share/FTP/SMS/OMN/AO_MT/$MachineName" 

#--------------AO_Delivered---------------------------------------------------------------
AO_Delivered="AO_DELIVERY_"$DateTime"*"
if ! [ -d /share/FTP/SMS/OMN/AO_Delivered/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/OMN/AO_Delivered/$MachineName
fi
ssh CNPVAS02 "scp VASQ@$MachineName:CDRS/ao_mt_cdr/ao_mt_bkp/$AO_Delivered /share/FTP/SMS/OMN/AO_Delivered/$MachineName" &
#--------------Submittion---------------------------------------------------------------
SUBMISSION="SUBMISSION_"$DateTime"*"
if ! [ -d /share/FTP/SMS/OMN/SUBMISSION/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/OMN/SUBMISSION/$MachineName
fi
ssh CNPVAS02 "scp VASQ@$MachineName:CDRS/bkp/$SUBMISSION /share/FTP/SMS/OMN/SUBMISSION/$MachineName" &
#--------------Delivered---------------------------------------------------------------
DELIVERY="DELIVERY_"$DateTime"*"
if ! [ -d /share/FTP/SMS/OMN/DELIVERY/$MachineName ]
	then
		mkdir -p /share/FTP/SMS/OMN/DELIVERY/$MachineName
fi
ssh CNPVAS02 "scp VASQ@$MachineName:CDRS/bkp/$DELIVERY /share/FTP/SMS/OMN/DELIVERY/$MachineName" &

