MachineName=$1
Date_Temp=$2

DataDir_MO=/share/FTP/SMS/FTD/Submissions/$MachineName

ProcessingDir_MO=/app_univ/Services/SMS/Processing/Submissions/$MachineName

if ! [ -d $ProcessingDir_MO ]
        then
                mkdir -p $ProcessingDir_MO
		chmod 777 $ProcessingDir_MO 
fi  


CDRsDir_MO=/share/FTP/SMS/CDRs/$MachineName

if ! [ -d $CDRsDir_MO ]
        then
                mkdir -p $CDRsDir_MO 
		chmod 777 $CDRsDir_MO 
fi  

cd $DataDir_MO

echo "Unzipping Files"
gunzip *"$Date_Temp"*.gz
rm -f *"$Date_Temp"*.gz 

for FileName in `ls *"$Date_Temp"*.dat`
do
	echo "Processing file "$FileName

  	Machine=`echo $FileName | cut -d"_" -f2`
  	Date=`echo $FileName | cut -d"_" -f3 | awk '{print substr($0,1,4)"-"substr($0,5,2)"-"substr($0,7,2)}'`
  	Hour=`echo $FileName | cut -d"_" -f4 | awk '{print substr($0,1,2)}'`

	tp_fcdr $FileName | /usr/xpg4/bin/grep -e dgtiAddress  -e ogtiAddress -e msisdn -e CDR -e origAddress -e recipAddress | sed '/{/N;s/\n */ /g' | cut -c20-100  | sed 's/CDR:/X/g' | tr -d ' ' | tr '\n' "Y" | tr X '\n' | tr Y "|" | sed 's/dgtiAddress={msisdn=//g'| sed 's/origAddress={msisdn=//g' | sed 's/recipAddress={msisdn=//g' | sed 's/recipAddress={msisdn=//g' | sed 's/recipAddressGSM=//g' | sed 's/origAddressGSM=//g' | sed 's/msisdn=//g' | sed 's/ogtiAddress={//g' | sed 's/ogtiAddressGSM=//g' | tr " " "|" | tr -d \' | awk -F"|" '{if (NF == 9) {print Date_"|"Hour_"|"$2"|"$4"|"$6"|"Machine_;} else if (NF == 7) {print Date_"|"Hour_"|"$2"|"$4"|""NO GT""|"Machine_;}}' Date_=$Date Hour_=$Hour Machine_=$Machine | awk -F"|" '{if (length($3) == 12) {print $1"|"$2"|""Vodafone""|"$4"|"$5"|"$6;} else {print $1"|"$2"|"$3"|"$4"|"$5"|"$6;}}' | awk -F"|" '{if (length($4) == 12) {print $1"|"$2"|"$3"|""Vodafone""|"$5"|"$6;} else {print $1"|"$2"|"$3"|"$4"|"$5"|"$6;}}' > $ProcessingDir_MO/$FileName.Processed 
	
	tp_fcdr $FileName | /usr/xpg4/bin/grep -e dgtiAddress  -e ogtiAddress -e msisdn -e CDR -e origAddress -e recipAddress -e submitTime -e terminTime | sed '/{/N;s/\n */ /g' | cut -c20-100  | sed 's/CDR:/X/g' | tr -d ' ' | tr '\n' "Y" | tr X '\n' | tr Y "|" | sed 's/dgtiAddress={msisdn=//g'| sed 's/origAddress={msisdn=//g' | sed 's/recipAddress={msisdn=//g' | sed 's/recipAddress={msisdn=//g' | sed 's/recipAddressGSM=//g' | sed 's/origAddressGSM=//g' | sed 's/msisdn=//g' | sed 's/ogtiAddress={//g' | sed 's/ogtiAddressGSM=//g' | sed 's/submitTime=//g' | sed 's/terminTime=//g' | tr " " "|" | tr -d \' | cut -d"|" -f1,2,3,4,5,6,7 > $CDRsDir_MO/$FileName.cdr

	rm -f $FileName
done

