FTP_Dir=/share/FTP/Comptel_BGW/
BaseDir=$FTP_Dir/Adj
mv $FTP_Dir/PPAS-ADJ_* $BaseDir

Date=$1
DateBefore=`/app_univ/Date_Scripts/XDayBefore.sh $Date -1 | sed 's/-/_/g'`
DateAfter=`/app_univ/Date_Scripts/XDayBefore.sh $Date 1|sed 's/-/_/g'`
Date_formatted=`echo $Date | sed 's/-/_/g'`
cd $BaseDir

grep -h `echo $Date | sed 's/-//g'` PPAS-ADJ_*$Date_formatted* PPAS-ADJ_*$DateAfter* PPAS-ADJ_*$DateAfter*| awk 'BEGIN {FS = ",";OFS="|"}{ Day=substr($2,7,2);Month=substr($2,5,2);Date=substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2); Time=substr($2,10,2);SumArr[Date"|"Time"|"$3"|"$4"|"$6"|"Day"|"Month] += $5;CounterArr[Date"|"Time"|"$3"|"$4"|"$6"|"Day"|"Month]+=1;} END {for (i in CounterArr) { print i"|"SumArr[i]"|"CounterArr[i]} }' > /share/Importers/BGW/Adj_Agg_$Date

echo "Importing data, File Name: /share/Importers/BGW/Adj_Agg_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/Adj_Agg_$Date" CNPVAS03 Adjustments "Date,Time,Adj_Type,Adj_Code,Channel,Day,Month,Amount,Count" "|" BGW Adjustments

#Command="LOAD DATA INFILE '/share/Importers/BGW/Adj_Agg_$Date' REPLACE INTO TABLE Adjustments FIELDS TERMINATED BY '|' (Date,Time,Adj_Type,Adj_Code,Channel,Day,Month,Amount,Count)"
#mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" BGW  &
