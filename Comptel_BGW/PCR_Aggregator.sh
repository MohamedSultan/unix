FTP_Dir=/share/FTP/Comptel_BGW/
BaseDir=$FTP_Dir/Recharges
mv $FTP_Dir/PPAS-PCR_* $BaseDir

Date=$1
DateBefore=`/app_univ/Date_Scripts/XDayBefore.sh $Date -1 | sed 's/-/_/g'`
DateAfter=`/app_univ/Date_Scripts/XDayBefore.sh $Date 1|sed 's/-/_/g'`
Date_formatted=`echo $Date | sed 's/-/_/g'`
cd $BaseDir

CurrentDir=`pwd`


grep -h `echo $Date | sed 's/-//g'` PPAS-PCR_$Date_formatted* PPAS-PCR_$DateAfter* PPAS-PCR_$DateAfter* | cut -d"," -f4,5,6,7,9,10,15,16,17 | awk 'BEGIN {FS=",";OFS=","}{Amount=$8-$7;Date=substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2);Time=substr($2,10,2);Day=substr($2,7,2);Month=substr($2,5,2); if(substr($3,1,4) == "CCAT") {Channel="CCAT";} else {Channel=$3;};SumArr[Date","Time","Channel","$4","$5","$6","Day","Month","$1","$9] += Amount;CountArr[Date","Time","Channel","$4","$5","$6","Day","Month","$1","$9] +=1;} END {for (i in SumArr) { print i","SumArr[i]","CountArr[i] } }' > /share/Importers/BGW/PCR_$Date


echo "Importing data, File Name:/share/Importers/BGW/PCR_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/PCR_$Date" CNPVAS03 Recharges "Date,time,Channel,Card_Type,Service_Class,DA_ID,Day,Month,Card_Value,TDF,Amount,Count" "," BGW Recharges 

