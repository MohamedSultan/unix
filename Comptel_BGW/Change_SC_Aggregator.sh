
FTP_Dir=/share/FTP/Comptel_BGW/
BaseDir=$FTP_Dir/Change_SC
mv $FTP_Dir/PPAS-DD_CHANGE_SC_* $BaseDir

Date=$1
DateBefore=`/app_univ/Date_Scripts/XDayBefore.sh $Date -1 | sed 's/-/_/g'`
DateAfter=`/app_univ/Date_Scripts/XDayBefore.sh $Date 1|sed 's/-/_/g'`
Date_formatted=`echo $Date | sed 's/-/_/g'`
cd $BaseDir

CurrentDir=`pwd`

grep -h `echo $Date | sed 's/-//g'` PPAS-DD_CHANGE_SC_$Date_formatted* PPAS-DD_CHANGE_SC_$DateAfter* PPAS-DD_CHANGE_SC_$DateAfter* | cut -d"," -f2,3,4,6,7,8 | awk 'BEGIN {FS=",";OFS=","}{Date=substr($3,1,4)"-"substr($3,5,2)"-"substr($3,7,2);Time=substr($3,10,2);CountArr[Date","Time","$1","$2","$4","$5","$6] +=1;} END {for (i in CountArr) { print i","CountArr[i] } }' > /share/Importers/BGW/ChangeSC_$Date

echo "Importing data, File Name:/share/Importers/BGW/ChangeSC_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/ChangeSC_$Date" CNPVAS03 Change_Service_Class "Date,time,Old_service_Class,New_Service_Class,Var1,Var2,Var3,Count" "," BGW Change_Service_Class
#Command="LOAD DATA INFILE '/share/Importers/BGW/ChangeSC_$Date' REPLACE INTO TABLE Change_Service_Class FIELDS TERMINATED BY ',' (Date,time,Old_service_Class,New_Service_Class,Var1,Var2,Var3,Count)"
#mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" BGW  &

