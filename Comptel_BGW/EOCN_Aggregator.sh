
FTP_Dir=/share/FTP/Comptel_BGW/
BaseDir=$FTP_Dir/EOCN
mv $FTP_Dir/PPAS-EON_ADJ_* $BaseDir

Date=$1
DateBefore=`/app_univ/Date_Scripts/XDayBefore.sh $Date -1 | sed 's/-/_/g'`
DateAfter=`/app_univ/Date_Scripts/XDayBefore.sh $Date 1|sed 's/-/_/g'`
Date_formatted=`echo $Date | sed 's/-/_/g'`
cd $BaseDir

CurrentDir=`pwd`

grep -h `echo $Date | sed 's/-//g'` PPAS-EON_ADJ_$Date_formatted* PPAS-EON_ADJ_$DateAfter* PPAS-EON_ADJ_$DateAfter*| sed 's/ /,/g' | cut -d"," -f2,3,4,5,6,8 | awk 'BEGIN { FS = ",";OFS=","}{ Amount=$5;Date=substr($1,1,4)"-"substr($1,5,2)"-"substr($1,7,2);Time=substr($2,1,2);Day=substr($1,7,2);Month=substr($1,5,2); SumArr[Date","Time","$3","$4","$6","Day","Month] += Amount;CountArr[Date","Time","$3","$4","$6","Day","Month]+=1} END {for (i in SumArr) { print i","SumArr[i]","CountArr[i]} }'  > /share/Importers/BGW/EOCN_$Date

echo "Importing data, File Name:/share/Importers/BGW/EOCN_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/EOCN_$Date" CNPVAS03 EOCN_Adj "Date,time,Adj_Type,Adj_Code,Service_Class,Day,Month,Amount,Count" "," BGW EOCN_Adj
#Command="LOAD DATA INFILE '/share/Importers/BGW/EOCN_$Date' REPLACE INTO TABLE EOCN_Adj FIELDS TERMINATED BY ',' (Date,time,Adj_Type,Adj_Code,Service_Class,Day,Month,Amount,Count)"
#mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" BGW  &

#------Distinct Customers--------------------------------------------------------------
exit

echo "Getting Distinct Data"
grep -h `echo $Date | sed 's/-//g'` PPAS-EON_ADJ_$Date_formatted* PPAS-EON_ADJ_$DateAfter* PPAS-EON_ADJ_$DateAfter* | sed 's/ /,/;s/:/,/g'  | cut -d"," -f1,2,3,6,7 | sort | uniq -c | sed 's/^ *//g;s/ /,/' | cut -d "," -f1,3,4,5,6 | sort | uniq -c | sed 's/^ *//g;s/ /,/' | awk 'BEGIN { FS = ",";OFS=","}{ print $1,$2,substr($3,1,4)"-"substr($3,5,2)"-"substr($3,7,2),$4,$5,$6 }' > /share/Importers/BGW/EOCN_Customers_$Date
echo "Importing data, File Name:/share/Importers/BGW/EOCN_Customers_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/EOCN_Customers_$Date" CNPVAS03 EOCN_Adj_Distinct "Count,Repetition_Count,Date,Hour,Adj_Type,Adj_Code" "," BGW EOCN_Adj_Distinct
#Command="LOAD DATA INFILE '/share/Importers/BGW/EOCN_Customers_$Date REPLACE INTO TABLE EOCN_Adj_Distinct FIELDS TERMINATED BY ',' (Count,Repetition_Count,Date,Hour,Adj_Type,Adj_Code)"
#mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" BGW  &

