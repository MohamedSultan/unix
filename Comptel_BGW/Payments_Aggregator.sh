FTP_Dir=/share/FTP/Comptel_BGW/
BaseDir=$FTP_Dir/Payments
mv $FTP_Dir/PPAS-PAYMENT_* $BaseDir

Date=$1
DateBefore=`/app_univ/Date_Scripts/XDayBefore.sh $Date -1 | sed 's/-/_/g'`
DateAfter=`/app_univ/Date_Scripts/XDayBefore.sh $Date 1|sed 's/-/_/g'`
Date_formatted=`echo $Date | sed 's/-/_/g'`
cd $BaseDir

CurrentDir=`pwd`

grep -h `echo $Date | sed 's/-//g'` PPAS-PAYMENT_$Date_formatted* PPAS-PAYMENT_$DateAfter* PPAS-PAYMENT_$DateAfter* | cut -d"," -f5,6,7,10,14,15,16,18,20 | awk 'BEGIN {FS=",";OFS=","}{Date=substr($1,1,4)"-"substr($1,5,2)"-"substr($1,7,2);Time=substr($1,10,2);Day=substr($1,7,2);Month=substr($1,5,2); if(substr($2,1,4) == "CCAT") {Channel="CCAT";} else {Channel=$2;};SumArr[Date","Time","Channel","$3","$4","$5","$6","$7","$9","Day","Month] +=$8;CountArr[Date","Time","Channel","$3","$4","$5","$6","$7","$9","Day","Month] +=1;} END {for (i in SumArr) { print i","SumArr[i]","CountArr[i] } }' > /share/Importers/BGW/Payments_$Date

echo "Importing data, File Name:/share/Importers/BGW/Payments_$Date"
ksh /app_univ/StatsCollector/Importer.sh "/share/Importers/BGW/Payments_$Date" CNPVAS03 Payments "Date,time,Channel,Card_Type,Card_Value,Adj_Type,Adj_Code,DA_ID,TDF,Day,Month,Amount,Count" "," BGW Payments

