Date=$1
CurrentDir=`pwd`

echo $CurrentDir
Yesterday_Date=`ksh /app/XDayBefore.sh $Date -1`
#echo $Yesterday_Date

echo "Getting PCRF Stats for Date $Date"
#---------------------------------------
#grep -i adsl $CurrentDir/Processings/subscription1_$Date.csv |  sed 's/ /\^/g' | sed 's/,/ /g'| sort -k7 | sed 's/ /,/g' | sed 's/\^/ /g' > $CurrentDir/Processings/ADSL_PCRF_Without_Status_$Date

mysql -uWriter -pWriter -hActivedb -e"Select * from PCRF_SUB_STATUS INTO OUTFILE '/log/Shared/Data_Mismatch/PCRF_Sub_Status_$Date' FIELDS TERMINATED BY ','" Data_Mismatch
sort /log/Shared/Data_Mismatch/PCRF_Sub_Status_$Date > $CurrentDir/Processings/PCRF_Sub_Status
join -t"," -j1 1 -j2 8 -a2 -o 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 1.2 $CurrentDir/Processings/PCRF_Sub_Status $CurrentDir/Processings/ADSL_PCRF_Without_Status_$Date | sort > $CurrentDir/Processings/ADSL_PCRF_All_$Date 
grep -v Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_All_$Date > $CurrentDir/Processings/ADSL_PCRF_$Date
grep Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_All_$Date    > $CurrentDir/Processings/ADSL_PCRF_ADSL_Micro_Th_$Date


echo "Geeting ADSL Prepaid Subs"
#--------------------------------
cd $CurrentDir/ADSL/Subs
find . -name "ADSL_LL_*" -mtime +7 -exec rm '{}' \;
echo "Getting ADSL Land Lines from ADSL Machine"
#java -jar Stats_Collector.jar $Date

echo "Getting ADSL Packages"
#--------------------------
cd $CurrentDir/ADSL/ADSL_PACKAGES
echo "Getting ADSL Packages from Billing"
java -jar Stats_Collector.jar $Date

echo "Getting ADSL Packages"
#--------------------------
cd $CurrentDir/ADSL/PERIODS
echo "Getting ADSL PERIODS"
java -jar Stats_Collector.jar $Date

cd ..

sort -t"," -k5 -r ./Subs/ADSL_LL_$Date | /usr/xpg4/bin/awk -F"," '!a[$2]++' | awk  'BEGIN {FS=",";OFS=","} {print $5,$1,$2,$3,$4,$6,$7}' | sed 's/ /^/g' | sort   > $CurrentDir/Processings/ADSL_LL_$Date

Rand=$RANDOM
mysql -uWriter -pWriter -hActivedb -e"Select ADSL_Package, PCRF_Policy, PCRF_Service, PCRF_Main_Quota, \`PCRF_Charge(LE)\`, Type, Speed from ADSL_PACKAGES Where Active=1 INTO OUTFILE '/log/Shared/Data_Mismatch/ADSL_Packages_$Rand' FIELDS TERMINATED BY ','" Data_Mismatch
mv /log/Shared/Data_Mismatch/ADSL_Packages_$Rand  /log/Shared/Data_Mismatch/ADSL_Packages_$Date
sed 's/ /^/g' /log/Shared/Data_Mismatch/ADSL_Packages_$Date | sort > $CurrentDir/Processings/ADSL_Packages

echo "Adding ADSL Package Details to ADSL Land Lines"
#----------------------------------------------------
join -j1 1 -j2 1 -a1 -o 1.2 1.1 1.3 1.4 1.5 1.6 2.3 2.1 2.2 2.4 2.5 2.6 2.7 1.7 -t"," $CurrentDir/Processings/ADSL_LL_$Date $CurrentDir/Processings/ADSL_Packages | sed 's/\^/ /g' |sort > $CurrentDir/Processings/ADSL_LL_Detailed_$Date

echo "Adding PCRF Part"
#---------------------
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 1.14 $CurrentDir/Processings/ADSL_LL_Detailed_$Date $CurrentDir/Processings/ADSL_PCRF_$Date > $CurrentDir/Processings/ADSL_PCRF_Added.txt 

echo "Checking Missing Serv_ADSL_Micro_Th"
#----------------------------------------------------
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 1.14 $CurrentDir/Processings/ADSL_LL_Detailed_$Date $CurrentDir/Processings/ADSL_PCRF_ADSL_Micro_Th_$Date | grep -v DEACTIVATED > $CurrentDir/Processings/ADSL_PCRF_Missing_Micro_Th_$Date

echo "Adding Usage Part"
#----------------------
sed 's/  */ /g' $CurrentDir/Processings/subscription2_$Date.csv | tr " " "," | sort > $CurrentDir/Processings/"subscription2_$Date_Sorted"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 2.2 $CurrentDir/Processings/ADSL_PCRF_Added.txt "$CurrentDir/Processings/subscription2_$Date_Sorted" > $CurrentDir/Processings/ADSL_PCRF_Usage_Added.txt
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 2.2 $CurrentDir/Processings/ADSL_PCRF_Missing_Micro_Th_$Date "$CurrentDir/Processings/subscription2_$Date_Sorted" > $CurrentDir/Processings/ADSL_PCRF_Missing_Micro_Th_Usage_Added_$Date

echo "Joining with Billing"
#-------------------------
Yesterday=`ksh /app/XDayBefore.sh $Date -1`
FormattedDate=`echo $Yesterday| sed 's/\-//g'`

#ODS File Format
#ADSL_NUM ADSL_CONTRACT_ID ACTIVATION_DATE CONTRACT_STATUS BILLCYCLE CUSTOMER_ID ACCOUNT_NUMBER RATEPLAN GSM_NUMBER

#Ehab File Format
#GSM_NUMBER RATEPLAN BILLCYCLE ADSL_NUM CONTRACT_STATUS
#"1000765787","1 Mega Enterprise","B3","015360915","Active"

#cat "/log/FTP/DWH_ODS/ADSL/ADSL_LINE_$FormattedDate.txt" | sed 's/\"//g' | awk -F"," '{print $4","$1","$2","$3","$5}' | sort>  $CurrentDir/Processings/Billing_ADSL_Details_$Date

cat "/log/FTP/DWH_ODS/ODS/Processed/ADSL_$FormattedDate" | sed 's/\"//g' | awk -F"," '{print substr($1,2,length($1))","$9","$8","$5","$4}' | sort>  $CurrentDir/Processings/Billing_ADSL_Details_$Date
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 2.1 2.2 2.3 2.4 2.5 $CurrentDir/Processings/ADSL_PCRF_Usage_Added.txt $CurrentDir/Processings/Billing_ADSL_Details_$Date  |sort> $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt

echo "Getting Missing_Micro_Th"
#--------------------------------------------
grep -v Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_Missing_Micro_Th_Usage_Added_$Date > ADSL_PCRF_Missing_Micro_Th.txt

echo "Uniqing MSISDNs"
#---------------------

mv $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added_Temp.txt
sort -rk5 -t"," $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added_Temp.txt | /usr/xpg4/bin/awk -F"," '!a[$3]++' > $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt

echo "Adding Payments"
#---------------------
#LastMonth=`ksh /app/XDayBefore.sh $Date -30`
#rm $CurrentDir/Processings/ADSL_Payments_$LastMonth
find $CurrentDir/Processings/ -name "ADSL_Payments_*" -mtime +29 -exec mv {$CurrentDir/Processings/2BDeleted} \; 
cat $CurrentDir/Processings/ADSL_Payments_* | sed 's/,/|/' | sort> $CurrentDir/Processings/ADSL_Payments.txt

sed 's/,/|/' $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt | sed 's/,/|/'| sed 's/,/|/'| sed 's/|/,/' | sed 's/|/,/' | sort -k3 -t"," > $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.Temp

join -j1 3 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.26 1.27 1.29 2.2 2.3 $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.Temp $CurrentDir/Processings/ADSL_Payments.txt|sed 's/|/,/g' >  $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Payments_Added.txt

echo "Geeting Speed mismatches and existance"
#--------------------------------------------
cat $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt | awk -F"," '{if ($7!=$16) print $0}' > ADSL_Prepaid_Mismatches.txt

echo "Geeting Staus Mismatches"
#-----------------------------
awk -F"," '{if ($14!="") print $0}' $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt | awk -F"," '{if (($5=="1" && $22!="Active") || ($5!="1" && $22=="Active") )print $0}' > ADSL_Prepaid_Status_Mismatches.txt

echo "Formatting Files"
#----------------------
awk 'BEGIN{FS=",";OFS=","}  {print "Prepaid","Speed",date,$0}' date=$Date ADSL_Prepaid_Mismatches.txt > $CurrentDir/Processings/ADSL_Speed_Mismatches.txt
awk 'BEGIN{FS=",";OFS=","}  {print "Prepaid","Status",date,$0}' date=$Date ADSL_Prepaid_Status_Mismatches.txt > $CurrentDir/Processings/ADSL_Status_Mismatches.txt
awk 'BEGIN{FS=",";OFS=","}  {print "Prepaid","Missing Micro_Th",date,$0}' date=$Date ADSL_PCRF_Missing_Micro_Th.txt > $CurrentDir/Processings/ADSL_Missing_Micro_Th.txt
cat $CurrentDir/Processings/ADSL_Speed_Mismatches.txt $CurrentDir/Processings/ADSL_Status_Mismatches.txt $CurrentDir/Processings/ADSL_Missing_Micro_Th.txt > /log/Shared/Data_Mismatch/ADSL_Mismatches_$Date

echo "Importing Stats"
Command="LOAD DATA INFILE '/log/Shared/Data_Mismatch/ADSL_Mismatches_$Date' REPLACE INTO TABLE \`ADSL Mismatches\` FIELDS TERMINATED BY ',' (User_Type,Mismatch_Type,Insertion_Date, Land_Line, Service, MSISDN, Initial_Charge_Date, Status, Next_Charge_Date, \`Mapped PCRF Service\`, \`Mapped Package\`,\`Mapped PCRF Policy\`, \`Mapped PCRF Main Quota\`, \`Mapped PCRF Charge(L.E)\`, \`Mapped Type\`, \`Mapped Speed\`,\`PCRF Landline\`,\`PCRF BC\`,\`PCRF Package Name\`,\`PCRF Service Name\`,\`PCRF Start Date\`,\`PCRF End Date\`,\`PCRF Throttling Status\`,PCRF_Sub_Status,PCRF_Sub_Status_Desc,\`Prepaid Period\`,\`Usage\`,\`Billing Land Line\`, \`Billing MSISDN\`, \`Billing Speed\`, \`Billing BC\`, \`Billing Status\`)"
mysql -hActivedb -uWriter -pWriter -hActivedb -e"$Command" Data_Mismatch

echo "Preparing Adjustment File"
FormattedDate=`echo $Yesterday| sed 's/\-/_/g'`

grep -h ADSL /log/Comptel_BGW/Parsed/PPAS-ADJ_AIR_$FormattedDate* | sed 's/ /,/g'| awk 'BEGIN {FS=",";OFS=","}{print "20"$1,substr($2,1,4)"-"substr($2,5,2)"-"substr($2,7,2),$3,$4,$5,$6,$7,$8,$9,$10,$11}' > /log/Shared/Data_Mismatch/ADSL_Adj_$Date
Command="LOAD DATA INFILE '/log/Shared/Data_Mismatch/ADSL_Adj_$Date' REPLACE INTO TABLE ADSL_Adjustments FIELDS TERMINATED BY ',' (MSISDN,Date,Time,Adj_Type,Adj_Code,Amount,Channel,Service_Class,ACCOUNT_FLAGS_BEFORE,ACCOUNT_FLAGS_AFTER,ACCOUNT_GROUP_ID,PRODUCT_ID,PACKAGE_ID,ORIGIN_TRANSACTION_ID_FOR_OVI_STORE,ORIGIN_OPERATOR_ID_FOR_OVI_STORE)" 

echo "Importing Adjustments"
Command="LOAD DATA INFILE '/log/Shared/Data_Mismatch/ADSL_Adj_$Date' REPLACE INTO TABLE ADSL_Adjustments FIELDS TERMINATED BY ',' (MSISDN,Date,Time,Adj_Type,Adj_Code,Amount,Channel,Service_Class,ACCOUNT_FLAGS_BEFORE,ACCOUNT_FLAGS_AFTER,ACCOUNT_GROUP_ID,PRODUCT_ID,PACKAGE_ID,ORIGIN_TRANSACTION_ID_FOR_OVI_STORE,ORIGIN_OPERATOR_ID_FOR_OVI_STORE)"
mysql -hActivedb -uWriter -pWriter -hActivedb -e"$Command" Data_Mismatch

ksh $CurrentDir/ADSL/Payments.sh $Yesterday_Date

sed 's/,/|/' $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.txt | sed 's/,/|/'| sed 's/,/|/'| sed 's/|/,/' | sed 's/|/,/' | sort -k3 -t","> $CurrentDir/Processings/ADSL_PCRF_Usage_Billing_Added.Temp


#========================== Post Paid Mismatches ============================================================================
#----------------------------------------------------------------------------------------------------------------------------
echo "+++++++++++++++++++++++++++++++++++++Post Mapid Mismatches++++++++++++++++++++++++++++++++++"

echo "Eliminating Prepaid"
grep -vi "prepaid" $CurrentDir/Processings/Billing_ADSL_Details_$Date | grep -v "Preapaid" | sed 's/ /^/g' | sed 's/,/ /g' | sort -k3 | sed 's/ /,/g' | grep -v "^00" > $CurrentDir/Processings/Billing_$Date

echo "Mapping Package with PCRF"
join -t"," -j1 3 -j2 1 -a1 -o  1.1 1.2 1.3 1.4 1.5 2.1 2.3 2.6 2.7 $CurrentDir/Processings/Billing_$Date $CurrentDir/Processings/ADSL_Packages | sed 's/\^/ /g' | sort> $CurrentDir/Processings/Billing_Detailed_$Date

echo "Adding PCRF Part"
#---------------------
grep -v Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_$Date > $CurrentDir/Processings/ADSL_PCRF_No_Serv_ADSL_Micro_Th_$Yesterday_Date
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 $CurrentDir/Processings/Billing_Detailed_$Date $CurrentDir/Processings/ADSL_PCRF_No_Serv_ADSL_Micro_Th_$Yesterday_Date > $CurrentDir/Processings/Billing_PCRF_Added.txt


echo "Adding Usage Part"
#----------------------
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 2.2 $CurrentDir/Processings/Billing_PCRF_Added.txt $CurrentDir/Processings/"subscription2_$Date_Sorted" > $CurrentDir/Processings/Billing_PCRF_Usage_Added.txt

echo "Gettingsubs who doesnt have Serv_ADSL_Micro_Th"
#---------------------------------------------------
grep "Serv_ADSL_Micro_Th" $CurrentDir/Processings/ADSL_PCRF_$Yesterday_Date > $CurrentDir/Processings/ADSL_PCRF_Micro_Th_only_$Yesterday_Date

#joingin with PCRF Serv_ADSL_Micro_Th
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 $CurrentDir/Processings/Billing_Detailed_$Date $CurrentDir/Processings/ADSL_PCRF_Micro_Th_only_$Yesterday_Date| awk -F"," '{if ($12=="") print $0}'  > $CurrentDir/Processings/Billing_PCRF_Added_missing_Serv_ADSL_Micro_Th_$Yesterday_Date

#adding Usage Part
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 2.2 $CurrentDir/Processings/Billing_PCRF_Added_missing_Serv_ADSL_Micro_Th_$Yesterday_Date $CurrentDir/Processings/"subscription2_$Yesterday_Date_Sorted" > Billing_PCRF_Added_missing_Serv_ADSL_Micro_Th.txt

echo "Geeting Speed mismatches and existance"
#--------------------------------------------
cat $CurrentDir/Processings/Billing_PCRF_Usage_Added.txt | awk -F"," '{if ($7!=$12) print $0}' > ADSL_PostPaid_Mismatches.txt

echo "Geeting Staus Mismatches"
#-----------------------------
cat $CurrentDir/Processings/Billing_PCRF_Usage_Added.txt | awk -F"," '{if ($5!=$18) print $0}' > ADSL_PostPaid_Status_Mismatches.txt

echo "Formatting Files"
#----------------------
awk 'BEGIN{FS=",";OFS=","}  {print "PostPaid","Speed",date,$0}' date=$Date ADSL_PostPaid_Mismatches.txt > /log/Shared/Data_Mismatch/ADSL_PostPaid_Speed_Mismatches.txt
awk 'BEGIN{FS=",";OFS=","}  {print "PostPaid","Status",date,$0}' date=$Date ADSL_PostPaid_Status_Mismatches.txt > /log/Shared/Data_Mismatch/ADSL_PostPaid_Status_Mismatches.txt
awk 'BEGIN{FS=",";OFS=","}  {print "Postpaid","Missing Micro_Th",date,$0}' date=$Date Billing_PCRF_Added_missing_Serv_ADSL_Micro_Th.txt > /log/Shared/Data_Mismatch/ADSL_Postpaid_missing_Serv_ADSL_Micro_Th.txt
#cat /log/Shared/Data_Mismatch/ADSL_PostPaid_Speed_Mismatches.txt /log/Shared/Data_Mismatch/ADSL_PostPaid_Status_Mismatches.txt /log/Shared/Data_Mismatch/ADSL_Postpaid_missing_Serv_ADSL_Micro_Th.txt > /log/Shared/Data_Mismatch/ADSL_Post_Mismatches_$Date
cat /log/Shared/Data_Mismatch/ADSL_PostPaid_Speed_Mismatches.txt /log/Shared/Data_Mismatch/ADSL_PostPaid_Status_Mismatches.txt > /log/Shared/Data_Mismatch/ADSL_Post_Mismatches_$Date

echo "Importing Stats"
Command="LOAD DATA INFILE '/log/Shared/Data_Mismatch/ADSL_Post_Mismatches_$Date' REPLACE INTO TABLE \`ADSL Mismatches\` FIELDS TERMINATED BY ',' (User_Type,Mismatch_Type,Insertion_Date,\`Billing Land Line\`, \`Billing MSISDN\`, \`Billing Speed\`, \`Billing BC\`, \`Billing Status\`,\`Mapped PCRF Service\`, \`Mapped Package\`,\`Mapped Type\`, \`Mapped Speed\`,\`PCRF Landline\`,\`PCRF BC\`,\`PCRF Package Name\`,\`PCRF Service Name\`,\`PCRF Start Date\`,\`PCRF End Date\`,\`PCRF Throttling Status\`,PCRF_Sub_Status,PCRF_Sub_Status_Desc,\`Usage\`)"
mysql -hActivedb -uWriter -pWriter -hActivedb -e"$Command" Data_Mismatch

echo "Analyzing stats"
Command="Call DataAnlyzer('$Date')"
mysql -hActivedb -uWriter -pWriter -hActivedb -e"delete from \`ADSL Mismatches\` where Mismatch_Type='Missing Micro_Th' and User_Type='Postpaid'" Data_Mismatch
mysql -hActivedb -uWriter -pWriter -hActivedb -e"$Command" Data_Mismatch
