Date=$1
CurrentDir=`pwd`

echo $CurrentDir
Yesterday_Date=`ksh /app/XDayBefore.sh $Date -1`
#echo $Yesterday_Date

Rand=$RANDOM

PCRF=$CurrentDir/Processings/ADSL_PCRF1_$Date
#PCRF_Th=$CurrentDir/Processings/ADSL1_PCRF_ADSL_Micro_Th_$Date
CI=$CurrentDir/Processings/ADSL1_LL_Detailed_$Date
Usage=$CurrentDir"/Processings/subscription12_"$Date"_Sorted"
Billing=$CurrentDir/Processings/Billing1_ADSL_Details_$Date
Payments=$CurrentDir/Processings/ADSL1_Payments.txt
Radius=$CurrentDir/Processings/Radius1_All_Sorted_$Date
Bras=$CurrentDir/Processings/Bras_$Date
Promos=$CurrentDir/Processings/Promotion_Subs_$Date
MSISDN_Type=/share/Regional/Maps/CCSM_ALL_FINAL.LST_MSISDN_Sorted
All_ADSLs=$CurrentDir"/Processings/All_MSISDNs_"$Date

DateTime=`echo $Date|sed 's/-//g'`

echo "Preparing Promo Part"
#--------------------------
awk '{FS=",";OFS=","}{if($5>Date_ && $6==1){print $0}}' Date_=$Date /app_univ/StatsCollector/app.Services.Data_Mismatch.ADSL.Promotion_Subs_$Date| sort > $Promos

echo "Preparing PCRF Stats for Date $Date"
#---------------------------------------

grep -i adsl $CurrentDir/Processings/subscription1_$Date.csv | grep -v Promo| awk '{FS=",";OFS=","}{if (substr($6,1,8)>DateTime_ || $6==""){print $0}}' DateTime_=$DateTime | sort> $PCRF 
#grep -i adsl $CurrentDir/Processings/subscription1_$Date.csv | grep -v Serv_ADSL_Micro_Th | grep -v Promo| awk '{FS=",";OFS=","}{if (substr($6,1,8)>DateTime_ || $6==""){print $0}}' DateTime_=$DateTime | sort> $PCRF 

#grep -v Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_Without_Status_$Date | grep -v Promo| awk '{FS=",";OFS=","}{if (substr($6,1,8)>DateTime_ || $6==""){print $0}}' DateTime_=$DateTime | sort> $PCRF
#grep Serv_ADSL_Micro_Th $CurrentDir/Processings/ADSL_PCRF_Without_Status_$Date > $PCRF_Th

echo $PCRF" is generated"

#=================================================================================================================

echo "Preparing ADSL Prepaid Subs"
#-------------------------------
cd $CurrentDir/ADSL/Subs
find . -name "ADSL_LL_*" -mtime +7 -exec rm '{}' \;
#echo "Getting ADSL Land Lines from ADSL Machine"
#java -jar Stats_Collector.jar $Date

echo "Getting ADSL Packages"
cd $CurrentDir/ADSL/ADSL_PACKAGES
echo "Getting ADSL Packages from Billing"
java -jar Stats_Collector.jar $Date

echo "Getting ADSL Period"
cd $CurrentDir/ADSL/PERIODS
echo "Getting ADSL PERIODS"
java -jar Stats_Collector.jar $Date

cd ..

#echo "Processing ADSL CI Data"
#-----------------------------
find /app_univ/StatsCollector/ -name "app.Services.Data_Mismatch.ADSL.Subs_*" -mtime +10 -exec rm {} \;
cp /app_univ/StatsCollector/app.Services.Data_Mismatch.ADSL.Subs_$Date $CurrentDir/Processings/ADSL_LL_All_$Date

awk '{FS=",";OFS=","}{print $0,$8"-"$1}' $CurrentDir/Processings/ADSL_LL_All_$Date | sort -t"," -n -k9 -r |/usr/xpg4/bin/awk -F"," '!a[$1]++' | awk  'BEGIN {FS=",";OFS=","} {print $5,$1,$2,$3,$4,$6,$7}' | sed 's/ /^/g' | sort   > $CurrentDir/Processings/ADSL_LL_$Date

#sort -t"," -k8 -r $CurrentDir/Processings/ADSL_LL_All_$Date| /usr/xpg4/bin/awk -F"," '!a[$8]++' | awk  'BEGIN {FS=",";OFS=","} {print $5,$1,$2,$3,$4,$6,$7}' | sed 's/ /^/g' | sort   > $CurrentDir/Processings/ADSL_LL_$Date

mysql -uWriter -pWriter -hActivedb -e"Select ADSL_Package, PCRF_Policy, PCRF_Service, PCRF_Main_Quota, \`PCRF_Charge(LE)\`, Type, Speed from ADSL_PACKAGES Where Active=1 INTO OUTFILE '/share/Data_Mismatch/ADSL_Packages_$Rand' FIELDS TERMINATED BY ','" Data_Mismatch
mv /share/Data_Mismatch/ADSL_Packages_$Rand  /share/Data_Mismatch/ADSL_Packages_$Date
sed 's/ /^/g' /share/Data_Mismatch/ADSL_Packages_$Date | sort > $CurrentDir/Processings/ADSL_Packages

echo "Adding ADSL Package Details to ADSL Land Lines"
#----------------------------------------------------
join -j1 1 -j2 1 -a1 -o 1.2 1.1 1.3 1.4 1.5 1.6 1.7 2.3 2.1 2.4 2.5 2.6 2.7 -t"," $CurrentDir/Processings/ADSL_LL_$Date $CurrentDir/Processings/ADSL_Packages | sed 's/\^/ /g' |sort > $CI

echo $CI" is generated"

#=================================================================================================================


echo "Generating Billing"
#-------------------------
Yesterday=`ksh /app/XDayBefore.sh $Date -1`
FormattedDate=`echo $Yesterday| sed 's/\-//g'`

#ODS File Format
#ADSL_NUM ADSL_CONTRACT_ID ACTIVATION_DATE CONTRACT_STATUS BILLCYCLE CUSTOMER_ID ACCOUNT_NUMBER,RATEPLAN GSM_NUMBER,Linne activation date, line deactivation date

#Ehab File Format
#GSM_NUMBER RATEPLAN BILLCYCLE ADSL_NUM CONTRACT_STATUS
#"1000765787","1 Mega Enterprise","B3","015360915","Active"

#cat "/log/FTP/DWH_ODS/ADSL/ADSL_LINE_$FormattedDate.txt" | sed 's/\"//g' | awk -F"," '{print $4","$1","$2","$3","$5}' | sort>  $CurrentDir/Processings/Billing_ADSL_Details_$Date

#cat "/log/FTP/DWH_ODS/ODS/Processed/ADSL_"$FormattedDate | sed 's/\"//g' | awk -F"," '{print substr($1,2,length($1))","$9","$8","$5","$4","$3}' | sed 's/ /^/g' | sed 's/,/ /g' | sed 's/ /,/g' |  awk '{FS=",";OFS=","}{if ($5=="Active") {print $0,100} else {print $0,1}}' | sort -t"," -k6,7 -r | /usr/xpg4/bin/awk -F"," '!a[$1]++' |sort -t"," -k1>  $CurrentDir/Processings/Billing__$Date

cat "/log/FTP/DWH_ODS/ODS/Processed/ADSL_"$FormattedDate "/log/FTP/DWH_ODS/ODS/ADSL_"$FormattedDate| sed 's/\"//g' | awk  '{FS=",";OFS=","}{if ($11=="") {print substr($1,2,length($1)),$9,$8,$5,$4,$3,$10,"XXX",$2,$7,$6} else {print substr($1,2,length($1)),$9,$8,$5,$4,$3,$10,$11,$2,$7,$6}}' | sed 's/ /^/g' |  sort -t"," -k8 -r |/usr/xpg4/bin/awk -F"," '!a[$9]++'|  awk '{FS=",";OFS=","}{if ($5=="Active") {print $0,100} else {print $0,1}}' | sort -t"," -k6,7 -r | /usr/xpg4/bin/awk -F"," '!a[$1]++' |sort -t"," -k3>  $CurrentDir/Processings/Billing__$Date

#cp $CurrentDir/Processings/Billing__$Date $Billing

join -t"," -j1 3 -j2 1 -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 2.1 2.3 2.6 2.7 $CurrentDir/Processings/Billing__$Date $CurrentDir/Processings/ADSL_Packages | sed 's/\^/ /g' | sort > $Billing

echo $Billing" is generated"

#====================================================================================================================

echo "Genrating Payments"
#---------------------
#LastMonth=`ksh /app/XDayBefore.sh $Date -30`
#rm $CurrentDir/Processings/ADSL_Payments_$LastMonth
find $CurrentDir/Processings/ -name "ADSL_Payments_*" -mtime +29 -exec mv {} $CurrentDir/ADSL/2BDeleted \; 
#cat $CurrentDir/Processings/ADSL_Payments_* | awk '{FS=",";OFS=","}{print substr($1,3,1000),$2,$3,$4}' | sort> $Payments
cat $CurrentDir/Processings/ADSL_Payments_* | sort> $Payments

echo "$Payments Is generated"

#====================================================================================================================

echo "Genrating Raduis"
#----------------------
find /app_univ/StatsCollector/ -name "app.Services.Data_Mismatch.ADSL.Radius_*" -mtime +10 -exec rm {} \;
sort /app_univ/StatsCollector/app.Services.Data_Mismatch.ADSL.Radius_$Date | sort > $Radius

#sort $CurrentDir/ADSL/Radius_All.csv > $Radius

echo "$Radius is generated"

#=================================================================================================================

echo "Generating Usage Part"
#--------------------------
sed 's/  */ /g' $CurrentDir/Processings/subscription2_$Date.csv | tr " " "," | sort > $Usage"_Temp"
cut -d"," -f1 $PCRF | sort | uniq | sort > $CurrentDir/Processings/PCRF_MSISDNs
join -j1 1 -j2 1 -t"," -o 2.1 2.2 2.3 2.4 $CurrentDir/Processings/PCRF_MSISDNs $Usage"_Temp" |  awk '{FS=",";OFS=","}{UsageSum[$1] += $2 ; InitialSum[$1] += $3 ; BalanceSum[$1] += $4} END {for (i in UsageSum) { print i","UsageSum[i]","InitialSum[i]","BalanceSum[i]}}' | sort> $Usage

echo $Usage" is generated"


#=================================================================================================================

echo "Generating Bras Part"
#--------------------------

Year=`date +%y`

#cat BRAS2 | sed "s/[ ]*ppp[ ]*/|ppp|/g; s/[ ]*default[ ]*/|default|/g; s/[ ]*$Year\/[ ]*/|$Year\//g; s/[ ]*$//g" > $Bras


#======================================================================================================================
#======================================================================================================================

cat $PCRF $CI $Billing $Radius | cut -d"," -f1 | sed 's/ *//g' | grep -vi "slot" | sort | uniq |sort > $All_ADSLs


echo "Getting PCRF Part"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 $All_ADSLs $PCRF > $CurrentDir/Processings/ADSL_PCRF.txt

echo "Getting CI Part"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 2.10 2.11 2.12 2.13 $CurrentDir/Processings/ADSL_PCRF.txt $CI > $CurrentDir/Processings/ADSL_PCRF_CI.txt

echo "Getting Usage Part"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 2.1 2.2 2.3 2.4 $CurrentDir/Processings/ADSL_PCRF_CI.txt $Usage > $CurrentDir/Processings/ADSL_PCRF_CI_Usage.txt

echo "Getting Billing Part"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 2.10 2.11 2.12 2.13 2.14 2.15 $CurrentDir/Processings/ADSL_PCRF_CI_Usage.txt $Billing > $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing.txt

echo "Getting Radius Part"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 1.33 1.34 1.35 1.36 1.37 1.38 1.39 1.40 1.41 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 2.10 2.11 2.12 $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing.txt $Radius > $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius.txt

echo "Adding Payments"
sort -t"," -k12 $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius.txt > $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_MSISDN_Sorted
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 1.33 1.34 1.35 1.36 1.37 1.38 1.39 1.40 1.41 1.42 1.43 1.44 1.45 1.46 1.47 1.48 1.49 1.50 1.51 1.52 1.53 2.2 2.3 2.4 $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_MSISDN_Sorted $Payments | sort> $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments.txt

echo "Adding Promos"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 1.33 1.34 1.35 1.36 1.37 1.38 1.39 1.40 1.41 1.42 1.43 1.44 1.45 1.46 1.47 1.48 1.49 1.50 1.51 1.52 1.53 1.54 1.55 1.56 2.3 $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments.txt $Promos > $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments_Promos.txt

echo "Adding Subs Type"
join -j1 1 -j2 1 -t"," -a1 -o 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15 1.16 1.17 1.18 1.19 1.20 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30 1.31 1.32 1.33 1.34 1.35 1.36 1.37 1.38 1.39 1.40 1.41 1.42 1.43 1.44 1.45 1.46 1.47 1.48 1.49 1.50 1.51 1.52 1.53 1.54 1.55 1.56 1.57 2.4 2.9 $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments_Promos.txt $MSISDN_Type > $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments_Promos_MSISDNType.txt



echo "Importing Base Data"
#-------------------------
echo "Formnatting file:$CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments_Promos_MSISDNType.txt to /share/Data_Mismatch/ADSL_Subs_Detailed_"$Date"_"$Rand
awk 'BEGIN{FS=",";OFS=","}  {print substr(date,9,2),date,$0}' date=$Date $CurrentDir/Processings/ADSL_PCRF_CI_Usage_Billing_Radius_Payments_Promos_MSISDNType.txt |sort > "/share/Data_Mismatch/ADSL_Subs_Detailed_"$Date"_"$Rand

echo "Loading File"
Day=`echo $Date | cut -d"-" -f3`
Command="ALTER TABLE \`ADSL Subs Detailed\` TRUNCATE PARTITION Day"$Day
#mysql -uWriter -pWriter -hActivedb -e"$Command" Data_Mismatch &
mysql -uWriter -pWriter -hCNPVAS03 -e"$Command" Data_Mismatch &

sleep 10

Command="LOAD DATA INFILE '/share/Data_Mismatch/ADSL_Subs_Detailed_"$Date"_"$Rand"' REPLACE INTO TABLE \`ADSL Subs Detailed\` FIELDS TERMINATED BY ',' (day,Insertion_Date, Land_Line, \`PCRF Landline\`,\`PCRF BC\`,\`PCRF Package Name\`,\`PCRF Service Name\`,\`PCRF Start Date\`,\`PCRF End Date\`,\`PCRF Throttling Status\`,PCRF_Sub_Status,\`CI Land Line\`,\`CI Service\`,\`CI MSISDN\`, \`CI Initial_Charge_Date\`,\`CI Status\`,\`CI Next_Charge_Date\`,\`CI Prepaid Period\`, \`Mapped PCRF Service\`, \`Mapped Package\`, \`Mapped PCRF Main Quota\`, \`Mapped PCRF Charge(L.E)\`, \`Mapped Type\`, \`Mapped Speed\`,\`Usage Land Line\`,\`Usage\`,\`Usage Initial VALUE\`,\`Usage BALANCE\`,\`Billing Land Line\`, \`Billing MSISDN\`, \`Billing Speed\`, \`Billing BC\`, \`Billing Status\`,\`Billing Activation Date\`,\`Billing Landline Activation DateTime\`,\`Billing Landline Deactivation DateTime\`,\`Billing Contact ID\`,\`Billing Account Number\`,\`Billing Customer ID\`, \`Mapped Billing ADSL_Package\`,\`Mapped Billing PCRF_Service\`,\`Mapped Billing Type\`,\`Mapped Billing Speed\`,Radius_ADSL_USER_NAME,Radius_ADSL_STATUS,Radius_CREATED_BY,Radius_CREATION_DATE,Radius_DEACTIVATED_BY,Radius_TRANS_TIMESTAMP,Radius_DEACTIVATION_DATE,Radius_SR_NUMBER,Radius_AGENT_COMMENT,Radius_DSL_IP_ADDRESS,Radius_DSL_SLOT,Radius_DSL_PORT,\`Last Payment\`,\`Payment Value\`,\`Payment Count\`,\`CI Promo Service\`,\`Assigned MSISDN Type\`,\`Assigned MSISDN Rate Plan\`)"
#mysql -hActivedb -uWriter -pWriter -e"$Command" Data_Mismatch &
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" Data_Mismatch 

Command="call UsageExceedCalculator('$Date')"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" Data_Mismatch 

Command="call \`ADSL Subs Detailed Analyzer\`('$Date')"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" Data_Mismatch  &

Command="call \`ADSL Subs Detailed Analyzer Checker\`('$Date')"
mysql -hCNPVAS03 -uWriter -pWriter -e"$Command" Data_Mismatch 
