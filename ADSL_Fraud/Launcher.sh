Date=$1
Date_Formatted=`echo $Date | sed 's/-//g'`
Importer=/app_univ/StatsCollector/Importer.sh

cd /share/FTP/DWH_ODS/ODS/Processed

echo "Processing Data"
cat ADSL_$Date_Formatted | grep ,Active, | awk -F"," '{print $9","$1","$8","$4}' | sort > ADSL_Data_1_$Date_Formatted

cat /share/Regional/Maps/CCSM_ALL_FINAL.LST_MSISDN_Sorted | cut -d, -f3,4,9 | sed 's/ //g'| sort > ADSL_Data_2_$Date_Formatted

join -t"," -j1 1 -j2 1 -o 1.1 1.2 1.3 2.1 2.2 2.3 ADSL_Data_2_$Date_Formatted ADSL_Data_1_$Date_Formatted | sed 's/ /,/g' | grep -v ,Enterprise | grep -v ,Light, | awk -F"," '{if ($2 != $8) print Date_","$1","$2","$3","$4","$5","$6","$7","$8}' Date_=$Date > /share/Importers/ADSL_Fraud/ADSL_Fraud_$Date_Formatted

echo "Importing Data"
ksh $Importer "/share/Importers/ADSL_Fraud/ADSL_Fraud_$Date_Formatted" CNPVAS03 ADSL_Fraud "Date,MSISDN1,Type,RatePlan,MSISDN2,Landline,Package1,Package2,Package3" "," Data_Mismatch ADSL_Fraud 


