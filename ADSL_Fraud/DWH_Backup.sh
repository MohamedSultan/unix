cd /share/FTP/DWH_ODS/ODS/Processed
for Files in `ls ADSL_2*`
	do
		if [ -e /app_univ/ADSL_ODS/$Files ]
			then
				echo "File $Files Exists, nothing to do"
			else
				echo "Copying $Files to /app_univ/ADSL_ODS"
				cp $Files /app_univ/ADSL_ODS
		fi
done
