ps -ef | grep DualSIM | sed 's/DualSIM//;s/^ *//g' | cut -d" " -f1 > DualSIM_Processes.txt
for Process in `cat DualSIM_Processes.txt`
	do
		echo "Killing Process: $Process"
		kill -9 $Process
done
rm DualSIM_Processes.txt
